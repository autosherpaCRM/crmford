/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import java.util.List;

import javax.inject.Named;

import models.Location;
import models.Role;
import models.Workshop;
import models.WyzUser;

/**
 *
 * @author W-1004
 */

@Named
public interface SuperAdminRepository {


	public void addLocationdataBySuperAdmin(Location location, Workshop workshop, String userLoginName);

	public void addUsersBySuperAdmin(WyzUser wyzUser, Location location, Workshop workshop, Role role,String userLoginName,String userLogindealerCode);

	public List<WyzUser> getExistingWyzUsers(String uname);

	//public List<SMSTemplate> getAllSMSTemplate(SMSTemplate smstemplate, String userLoginName, String userLogindealerCode);

}