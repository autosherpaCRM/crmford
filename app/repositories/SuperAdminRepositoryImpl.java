/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import configs.JinqSource;
import models.Driver;
import models.Location;
import models.Role;
import models.ServiceAdvisor;
import models.Tenant;
import models.Workshop;
import models.WyzUser;
import play.Logger.ALogger;

/**
 *
 * @author W-1004
 */
@Repository("SuperAdminRepository")
@Transactional
public class SuperAdminRepositoryImpl implements SuperAdminRepository {

	ALogger logger = play.Logger.of("application");

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private JinqSource source;
	
	

	@Override
	public List<WyzUser> getExistingWyzUsers(String uname) {
		logger.info("user name is :"+uname);
		return source.wyzUsers(em).where(u -> u.getUserName().equals(uname)).toList();
	}

	@Override
	public void addLocationdataBySuperAdmin(Location location, Workshop workshop,
			String userLoginName) {
		
		String locName  	= location.getName();
		String locCode 		= location.getLocCode();
		String pinCode 		= location.getPinCode();
		String stateName 	= location.getState();
		String workshopName = workshop.getWorkshopName();
		
		location.setName(locName);
		location.setLocCode(locCode);
		location.setPinCode(pinCode);
		location.setState(stateName);
		
		em.persist(location);
		
		workshop.setWorkshopName(workshopName);
		workshop.setLocation(location);
		
		em.persist(workshop);
	}





	@Override
	public void addUsersBySuperAdmin(WyzUser wyzUser, Location location, Workshop workshop, Role role ,String userLoginName ,String userLogindealerCode) {
		// TODO Auto-generated method stub

		String locName = location.getName();
		String firstName = wyzUser.getFirstName();
		String lastName  = wyzUser.getLastName();
		String userName = wyzUser.getUserName();
		long workshopId = workshop.getId();
		String pincode = workshop.getPincode();
		String phonenum = wyzUser.getPhoneNumber();
		String password = wyzUser.getPassword();
		String roleName = wyzUser.getRole();
		
		logger.info("role name:"+roleName);
		logger.info("location name:"+locName);
		logger.info("workshop name:"+workshopId);
		logger.info("dealer code is:"+userLogindealerCode);
		
		
		WyzUser newWyzUser = new WyzUser();
		long loccountId = source.locations(em).where(u -> u.getName().equals(locName)).count();

		if(loccountId > 0){			
			Location locdata = source.locations(em).where(u -> u.getName().equals(locName)).getOnlyValue();
			long workshopcount = source.workshop(em).where(u -> u.getId() == workshopId).count();
			Workshop workshopdata = source.workshop(em).where(u -> u.getId() == workshopId).getOnlyValue();
			Tenant tenant=source.tenant(em).where(u ->u.getTenantCode().equals(userLogindealerCode)).getOnlyValue();
			
			logger.info("workshopcount : "+workshopcount +"locdata : "+locdata.getName());

			
			newWyzUser.setFirstName(firstName);
			newWyzUser.setLastName(lastName);
			newWyzUser.setPhoneNumber(phonenum);
			newWyzUser.setUserName(userName);
			newWyzUser.setDealerCode(userLogindealerCode);
			newWyzUser.setDealerId(userLogindealerCode);
			newWyzUser.setDealerName(userLogindealerCode);						
			newWyzUser.setPassword(password);
			newWyzUser.setLocation(locdata);
			newWyzUser.setDealer(null);
			newWyzUser.setTenant(tenant);
			
			if(workshopcount > 0 ){
			
				newWyzUser.setWorkshop(workshopdata);
				em.persist(newWyzUser);
		
			}else{
				
				em.persist(newWyzUser);
				
			}
			if(roleName.equals("SA")){
				ServiceAdvisor newServiceAdvisor = new ServiceAdvisor();
				newServiceAdvisor.setAdvisorName(firstName);
				newServiceAdvisor.setWorkshop(workshopdata);
				newServiceAdvisor.setWyzUser(newWyzUser);	
				em.persist(newServiceAdvisor);
			}
			
			if(roleName.equals("Driver")){
				Driver newDriver = new Driver();
				logger.info("it is Driver");
				newDriver.setDriverName(firstName);
				newDriver.setWyzUser(newWyzUser);
				em.persist(newDriver);

			}
			
	

		}else{
			
			Tenant tenant=source.tenant(em).where(u ->u.getTenantCode().equals(userLogindealerCode)).getOnlyValue();			
			newWyzUser.setFirstName(firstName);
			newWyzUser.setLastName(lastName);
			newWyzUser.setUserName(userName);
			newWyzUser.setPhoneNumber(phonenum);
			newWyzUser.setPassword(password);
			newWyzUser.setDealerCode(userLogindealerCode);
			newWyzUser.setDealerId(userLogindealerCode);
			newWyzUser.setDealerName(userLogindealerCode);
			newWyzUser.setDealer(null);
			newWyzUser.setTenant(tenant);
			em.persist(newWyzUser);
			
			

			if(roleName.equals("SA")){
				ServiceAdvisor newServiceAdvisors = new ServiceAdvisor();
				newServiceAdvisors.setAdvisorName(firstName);
				newServiceAdvisors.setWyzUser(newWyzUser);	
				em.persist(newServiceAdvisors);

			}
			
			if(roleName.equals("Driver")){
				Driver newDrivers = new Driver();
				logger.info("it is Driver");
				newDrivers.setDriverName(firstName);
				newDrivers.setWyzUser(newWyzUser);
				em.persist(newDrivers);

			}
			
			
			
			
		}
		Config configuration = ConfigFactory.load();
		String defaultDataBaseName = configuration.getString("app.defaultdatabase");
		logger.info("defaultDataBaseName : "+defaultDataBaseName);

 		 String query="USE "+defaultDataBaseName+";";
		 Session hibernateSession = em.unwrap(Session.class);
		 hibernateSession.doWork(new org.hibernate.jdbc.Work() {
		
		 @Override
		 public void execute(Connection connection) throws SQLException {
		 Statement smt=connection.createStatement();
		 Boolean resultOfExecution=smt.execute(query);
		 
		 Tenant tenant=source.tenant(em).where(u ->u.getTenantCode().equals(userLogindealerCode)).getOnlyValue();
			WyzUser newWyzUserInmain = new WyzUser();
			newWyzUserInmain.setFirstName(firstName);
			newWyzUserInmain.setLastName(lastName);
			newWyzUserInmain.setUserName(userName);
			newWyzUserInmain.setPhoneNumber(phonenum);
			newWyzUserInmain.setPassword(password);
			newWyzUserInmain.setDealerCode(userLogindealerCode);
			newWyzUserInmain.setDealerId(userLogindealerCode);
			newWyzUserInmain.setDealerName(userLogindealerCode);		 
			newWyzUserInmain.setLocation(null);
			newWyzUserInmain.setWorkshop(null);
			newWyzUserInmain.setTenant(tenant);
			newWyzUserInmain.setDealer(null);
			em.persist(newWyzUserInmain);
		 
		 }});

	
	}

}