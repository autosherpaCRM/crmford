/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import static play.libs.Json.toJson;

import java.util.List;

import javax.inject.Inject;

import org.pac4j.core.profile.CommonProfile;
import org.pac4j.core.profile.ProfileManager;
import org.pac4j.play.PlayWebContext;
import org.pac4j.play.java.Secure;
import org.pac4j.play.store.PlaySessionStore;
import org.springframework.stereotype.Controller;

import models.Location;
import models.Role;
import models.Workshop;
import models.WyzUser;
import play.Logger.ALogger;
import play.data.Form;
import play.mvc.Result;
import repositories.CallInfoRepository;
import repositories.CallInteractionsRepository;
import repositories.SMSTriggerRespository;
import repositories.SuperAdminRepository;
import repositories.WyzUserRepository;
import views.html.addMasterDataSuperAdmin;
import views.html.addMasterDataUsersBySuperAdmin;

@Controller
public class SuperAdminController extends play.mvc.Controller {

	ALogger logger = play.Logger.of("application");
	private final CallInfoRepository repo;
	private final CallInteractionsRepository call_int_repo;
	private final WyzUserRepository wyzRepo;
	private final SMSTriggerRespository sms_repo;
	private final SuperAdminRepository superAdminRepo;
	private PlaySessionStore playSessionStore;

	@Inject
	public SuperAdminController(CallInfoRepository repository, org.pac4j.core.config.Config config,
			CallInteractionsRepository interRepo, WyzUserRepository wyzRepository, SMSTriggerRespository smsRepository, SuperAdminRepository AdminRepo,
			PlaySessionStore plstore) {
		repo 			 = repository;
		call_int_repo 	 = interRepo;
		wyzRepo 		 = wyzRepository;
		sms_repo 		 = smsRepository;
		superAdminRepo 	 = AdminRepo;
		config 		     = (org.pac4j.core.config.Config) config;
		playSessionStore = plstore;

	}

	private CommonProfile getUserProfile() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.debug("..............Obtained profiles........." + profiles);
		return profiles.get(0);
	}

	private String getUserLogindealerCode() {
		String dealer_Code = (String) getUserProfile().getAttribute("DEALER_CODE");
		return dealer_Code;

	}

	private String getUserLoginName() {
		String userloginname = getUserProfile().getId();
		return userloginname;

	}

	private String getDealerName() {
		String dealerName = (String) getUserProfile().getAttribute("DEALER_NAME");
		return dealerName;

	}

	private String getOEMOfDealer() {

		String oemIS = (String) getUserProfile().getAttribute("OEM");

		return oemIS;

	}
	
	
	
	
	// Add location by Super admin method
	/**
	 *
	 * @return
	 */
	@Secure(clients = "FormClient")
	public Result LocationBySuperAdmin() {
		List<Location> locationList = call_int_repo.getLocationList();
		List<Workshop> workshopList = call_int_repo.getWorkshopList();
		return ok(addMasterDataSuperAdmin.render(getOEMOfDealer(), getUserLogindealerCode(), getDealerName(), getUserLoginName(), locationList , workshopList));
	}
	
	
	@Secure(clients = "FormClient")
	public Result postLocationBySuperAdmin(){
		Form<Location> Location_data    = Form.form(Location.class).bindFromRequest();
		Location Location			    = Location_data.get();
		
		
	
		Form<Workshop> Workshop_data 	= Form.form(Workshop.class).bindFromRequest();
		Workshop Workshop 				= Workshop_data.get();
		
		
		superAdminRepo.addLocationdataBySuperAdmin(Location, Workshop, getUserLoginName());
		return redirect(controllers.routes.SuperAdminController.LocationBySuperAdmin());
	}
	
	@Secure(clients = "FormClient")
	public Result UsersBySuperAdmin(){
		List<Location> locationList = call_int_repo.getLocationList();
		List<Workshop> workshopList = call_int_repo.getWorkshopList();
		List<Role> roleList 		= call_int_repo.getRoles();
		return ok(addMasterDataUsersBySuperAdmin.render(getOEMOfDealer(), getUserLogindealerCode(), getDealerName(), getUserLoginName(),locationList,workshopList,roleList));
	}
	
	@Secure(clients = "FormClient")
	public Result postUserBySuperAdmin(){
		Form<WyzUser> Wyzuser_data 		= Form.form(WyzUser.class).bindFromRequest();
		WyzUser WyzUser 				= Wyzuser_data.get();
		
		Form<Location> Location_data 	= Form.form(Location.class).bindFromRequest();
		Location Location 				= Location_data.get();
		
		Form<Workshop> workshop_data 	= Form.form(Workshop.class).bindFromRequest();
		Workshop Workshop 				= workshop_data.get();
		
		Form<Role> roleData 			= Form.form(Role.class).bindFromRequest();
		Role Role 						= roleData.get();
		
		//logger.info("inside controller:"+ Location.getName());
		superAdminRepo.addUsersBySuperAdmin(WyzUser , Location, Workshop ,Role,getUserLoginName(),getUserLogindealerCode());
		
		return redirect(controllers.routes.SuperAdminController.UsersBySuperAdmin());
	}
	
	
	@Secure(clients = "FormClient")
	public Result checkIfUserExists(String uname){
		List<WyzUser> UsersList = superAdminRepo.getExistingWyzUsers(uname);
		return ok(toJson(UsersList));
	}
	

}