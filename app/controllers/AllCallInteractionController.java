package controllers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.sql.DataSource;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.play.java.Secure;
import org.pac4j.play.PlayWebContext;
import org.pac4j.play.store.PlaySessionStore;
import org.pac4j.core.profile.ProfileManager;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import controllers.webmodels.CallInteractionHistory;
import controllers.webmodels.CallInteractionHistory_dwnld;
import controllers.webmodels.CallLogAjaxLoadInsurance;
import controllers.webmodels.CallLogDispositionLoadMR;
import controllers.webmodels.ServiceBookedResultList;
import controllers.webmodels.InsuranceHistoryReport;
import models.CallInteraction;
import models.ListingForm;
import models.WyzUser;
import models.CallDispositionData;
import models.Campaign;
import play.Logger.ALogger;
import play.data.Form;
import play.db.DB;
import play.libs.Json;
import play.mvc.Result;
import repositories.AllCallInteractionRepository;
import repositories.CallInfoRepository;
import repositories.CallInteractionsRepository;
import repositories.InboundCallInteractionRepository;
import repositories.InsuranceRepository;
import repositories.SearchRepository;
import repositories.ServiceBookRepository;
import repositories.WyzUserRepository;
import views.html.callInteractionsTable;
import views.html.smsInteractionReports;
import views.html.*;


public class AllCallInteractionController extends play.mvc.Controller {

	ALogger logger = play.Logger.of("application");

	private final AllCallInteractionRepository all_call_int_repo;
	private final InsuranceRepository insurRepo;
	private final CallInteractionsRepository call_int_repo;
	private final CallInfoRepository repo;
	private final WyzUserRepository wyzRepo;
	private final InboundCallInteractionRepository inbound_repo;
	private final ServiceBookRepository serviceBookRepo;
	public final SearchRepository searchRepo;
	private PlaySessionStore playSessionStore;

	private org.pac4j.core.config.Config config;

	@Inject
	public AllCallInteractionController(InsuranceRepository insurRepo, CallInteractionsRepository call_int_repo,
			CallInfoRepository repo, WyzUserRepository wyzRepo, InboundCallInteractionRepository inbound_repo,
			SearchRepository search_Repo,org.pac4j.core.config.Config config,PlaySessionStore playSession,ServiceBookRepository serviceRepo, AllCallInteractionRepository all_call_int_repo) {

		this.all_call_int_repo=all_call_int_repo;
		this.insurRepo = insurRepo;
		this.call_int_repo = call_int_repo;
		this.repo = repo;
		this.wyzRepo = wyzRepo;
		this.inbound_repo = inbound_repo;
		searchRepo = search_Repo;
		config = (org.pac4j.core.config.Config) config;
		this.playSessionStore=playSession;
		this.serviceBookRepo=serviceRepo;
	}

	
	private CommonProfile getProfiles() {
		final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
		final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
		logger.debug("............About to obtain the profiles..........");
		List<CommonProfile> profiles = profileManager.getAll(true);
		logger.debug("..............Obtained profiles........." + profiles);
		return profiles.get(0);
	}
	
	private String getUserLoginName() {
		String userloginname = getProfiles().getId();
		
		logger.info(" userloginname : "+userloginname);

		return userloginname;

	}

	private String getDealerName() {
		String dealerName = (String) getProfiles().getAttribute("DEALER_NAME");

		return dealerName;

	}
	private static CellProcessor[] getProcessors() {

		final CellProcessor[] processors = new CellProcessor[] { new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(), new Optional(),
				new Optional(), new Optional()};

		return processors;
	}
	
	/*@Secure(clients = "FormClient")
	public Result getAllCallInteractions() throws IOException, ParseException {

		Map<String, String[]> paramMap = request().queryString();
		String fromdate = (paramMap.get("singleData")[0]);
		String todate = (paramMap.get("tranferData")[0]);
		Form<ListingForm> data_range = Form.form(ListingForm.class).bindFromRequest();
        ListingForm range = data_range.get();
		List<String> creName_list = range.getCrename();
		
		for(String sa:creName_list){
			
			logger.info("selected user : "+sa);
			
		}
		List<String> dispositions_list = range.getAll_dispositions();
		List<String> location_list = range.getLocations();
		for(String li:location_list){
			
			logger.info("location li si: "+li);
		}
		
		String creName;
		StringBuffer br = new StringBuffer();
		if(creName_list != null)
		for(String s : creName_list){
			br.append(s + ",");
		}
		creName = br.toString();
		br = new StringBuffer();
		String dispositions;
		if(dispositions_list != null)
		for(String s :dispositions_list){
			br.append(s + ",");
		}
		dispositions = br.toString();
		br = new StringBuffer();
		String locations;
		if(location_list != null)
		for(String s :location_list){
			br.append(s + ",");
		}
		locations = br.toString();
		
		logger.info("got all the parameters required download"+" "+fromdate+" "+todate+" "+creName+" "+dispositions+" "+locations);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date fd = null;
		Date td = null;
		if (!fromdate.equals("") && !todate.equals("")) {
			fd = format.parse(fromdate);
			td = format.parse(todate);
			Calendar c = Calendar.getInstance();
			c.setTime(td);
			c.add(Calendar.DATE, 1);
			td = c.getTime();
			c.setTime(fd);
			c.add(Calendar.DATE, -1);
			fd = c.getTime();
		} else if (fromdate.equals("") && !todate.equals("")) {
			td = format.parse(todate);
			Calendar c = Calendar.getInstance();
			c.setTime(td);
			c.add(Calendar.DATE, 1);
			td = c.getTime();
		} else if (!fromdate.equals("") && todate.equals("")) {
			fd = format.parse(fromdate);
			Calendar c = Calendar.getInstance();
			c.setTime(fd);
			c.add(Calendar.DATE, -1);
			fd = c.getTime();
		}

		String userName = getProfiles().getId();
		List<CallInteractionHistory> allCallInteractions = all_call_int_repo.getAllCallInteractionDetails(userName, fd,
				td, creName, dispositions,locations);
		logger.info("got all the required call interaction history for download"+allCallInteractions.size());
		//logger.info("got required parameters for download"+" fromdate:-"+fd+",todate:-"+td+",creName:-"+creName+",disposition:"+dispositions+",locations:"+locations);
		Config configuration = ConfigFactory.load();
		String pathOfTemp = configuration.getString("app.configuration");

		File fis = new File(pathOfTemp + "CallInteractions.csv");

		ICsvBeanWriter csvwriter = new CsvBeanWriter(new FileWriter(fis, false), CsvPreference.STANDARD_PREFERENCE);
		final String[] headers = new String[] {"Location","Cre_Name", "callTime", "callDate", "preffered_Contact_number",
				"callType", "Customer_name", "DOb", "office_address", "residential_address", "permenant_address",
				"Email_id", "customer_category", "customer_city", "callDuration", "Veh_Reg_no",
				"chassisNo", "Model", "fueltype", "Variant", "last_serviceDate", "lastServiceType",
				"nextServicedate", "nextServicetype","forecastLogic","previous_disposition","primary_disposition","secondary_dispostion","Tertiary_disposition","serviceType","psf_status"};

		try {

			final CellProcessor[] processors = getProcessors();

			csvwriter.writeHeader(headers);
			

			for (CallInteractionHistory aci : allCallInteractions) {
				CallInteractionHistory_dwnld cihd = new CallInteractionHistory_dwnld();
				cihd.setCallDate(aci.getCallDate());
				cihd.setCallDuration(aci.getCallDuration());
				cihd.setCallTime(aci.getCallTime());
				cihd.setCallType(aci.getCallType());
				cihd.setChassisNo(aci.getChassisNo());
				cihd.setCre_Name(aci.getCre_Name());
				cihd.setCustomer_category(aci.getCustomer_category());
				cihd.setCustomer_city(aci.getCustomer_city());
				cihd.setCustomer_name(aci.getCustomer_name());
				cihd.setDOb(aci.getDOb());
				cihd.setEmail_id(aci.getEmail_id());
				cihd.setForecastLogic(aci.getForecastLogic());
				cihd.setFueltype(aci.getFueltype());
				cihd.setLast_serviceDate(aci.getLast_serviceDate());
				cihd.setLastServiceType(aci.getLastServiceType());
				cihd.setLocation(aci.getLocation());
				cihd.setModel(aci.getModel());
				cihd.setNextServicedate(aci.getNextServicedate());
				cihd.setNextServicetype(aci.getNextServicetype());
				cihd.setOffice_address(aci.getOffice_address());
				cihd.setPermenant_address(aci.getPermenant_address());
				cihd.setPreffered_Contact_number(aci.getPreffered_Contact_number());
				cihd.setPrevious_disposition(aci.getPrevious_disposition());
				cihd.setPrimary_disposition(aci.getPrimary_disposition());
				cihd.setPsf_status(aci.getPsf_status());
				cihd.setResidential_address(aci.getResidential_address());
				cihd.setSecondary_dispostion(aci.getSecondary_dispostion());
				cihd.setServiceType(aci.getServiceType());
				cihd.setTertiary_disposition(aci.getTertiary_disposition());
				cihd.setVariant(aci.getVariant());
				csvwriter.write(cihd, headers, processors);

			}
			logger.info("csv created anfd ready to download");
		} catch (IOException ex) {
			logger.info("Error writing the CSV file: " + ex);
		} finally {
			try {
				logger.info("bean writer is not null ");
				csvwriter.close();
			} catch (IOException ex) {
				logger.info("Error closing the writer: " + ex);
			}
		}
		response().setContentType("application/x-download");
		response().setHeader("Content-disposition", "attachment; filename=CallInteractions.csv");
		return ok(fis, "callInteractions.csv");
	}*/


	
	private String getStringDate(Date call_date) {
	// TODO Auto-generated method stub
	if (call_date != null) {

		String pattern = "dd/MM/yyyy";
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		String stringDate= formatter.format(call_date);
		return stringDate;
		
	}else{
		
	}
	return "";
}
	
	@Secure(clients ="FormClient")
	public Result getAllCallInteractions(){

		logger.info("inside controller of call reports");
		
		Map<String, String[]> parambody = request().body().asFormUrlEncoded();

	
		ObjectNode result = Json.newObject();


		String fromDateNew ="";
		String toDateNew="";
		String location="";
		String workshop="";
		String disposition="";
		String creidsare="";

		
		 fromDateNew = (parambody.get("fromDate")[0]);
		 toDateNew = (parambody.get("toDate")[0]);
		 location = (parambody.get("locations")[0]);
		 workshop = (parambody.get("workshop")[0]);
		 creidsare= (parambody.get("UserIds")[0]);
		 disposition=(parambody.get("dispositions")[0]);
		 
		 logger.info("from date new :"+fromDateNew);
		 logger.info("to date new :"+toDateNew);
		 logger.info("location call reports :"+location);
		 logger.info("workshop call reports:"+workshop);
		 logger.info("disposition reports:"+disposition);
		 logger.info("creidsare date new :"+creidsare);

		String searchPattern = "";
		boolean allflag = false;
		boolean globalSearch = false;

		if (parambody.get("search[value]") != null) {
			searchPattern = parambody.get("search[value]")[0];
		}

		if (searchPattern.length() == 0 && fromDateNew.length() == 0
				&& toDateNew.length() == 0 && location.length() == 0 && workshop.length() == 0 &&  disposition.length() == 0) {
			allflag = true;
			logger.info("search length is 0 ");
		}
		if (searchPattern.length() > 0) {
			globalSearch = true;
			logger.debug("Global Search is true");
		}

		long fromIndex = Long.valueOf(parambody.get("start")[0]);
		long toIndex = Long.valueOf(parambody.get("length")[0]);
		
		logger.info("fromIndex :"+fromIndex);
		logger.info("toIndex :"+toIndex);
		long totalSize = 0;
		
		totalSize = searchRepo.getAllCallhistoryCount(getUserLoginName(),"","","","","","");
		logger.info("totalSize of reports call history: " + totalSize);
		if (toIndex < 0) {
			toIndex = 10;
		}
		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;
		List<CallInteractionHistory> callhistoryList = searchRepo.getAllCallHistoryData(getUserLoginName(),fromDateNew, toDateNew,location,workshop,creidsare,disposition, fromIndex,
				toIndex);
		if (!allflag) {
			if (globalSearch)
				//patternCount = totalSize;
				patternCount = searchRepo.getAllCallhistoryCount(getUserLoginName(),fromDateNew,toDateNew,location,workshop,disposition,creidsare);
			else
				patternCount = searchRepo.getAllCallhistoryCount(getUserLoginName(),fromDateNew,toDateNew,location,workshop,disposition,creidsare);
		}
		result.put("draw", Integer.valueOf(parambody.get("draw")[0]));
		result.put("recordsTotal", totalSize);
		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		ArrayNode an = result.putArray("data");
		for (CallInteractionHistory c : callhistoryList) {
		
			ObjectNode row = Json.newObject();
			row.put("0", c.getLocation());
			row.put("1", c.getCre_Name());
			row.put("2", c.getCallTime());
			row.put("3", c.getCallDate());
			row.put("4", c.getPreffered_Contact_number());
			row.put("5", c.getCallType());
			row.put("6", c.getCustomer_name());
			row.put("7", c.getDOb());
			row.put("8", c.getOffice_address());
			row.put("9", c.getResidential_address());
			row.put("10", c.getPermenant_address());
			row.put("11", c.getEmail_id());
			row.put("12", c.getCustomer_category());
			row.put("13", c.getCustomer_city());
			row.put("14", c.getCallDuration());
			row.put("15", c.getCustomer_remarks());
			row.put("16", c.getVeh_Reg_no());
			row.put("17", c.getChassisNo());
			row.put("18", c.getModel());
			row.put("19", c.getFueltype());
			row.put("20", c.getVariant());
			row.put("21", c.getLast_serviceDate());
			row.put("22", c.getLastServiceType());
			row.put("23", c.getNextServicedate());
			row.put("24", c.getNextServicetype());
			row.put("25", c.getForecastLogic());	
			row.put("26", c.getPrevious_disposition());
			row.put("27", c.getPrimary_disposition());
			row.put("28", c.getSecondary_dispostion());
			row.put("29", c.getTertiary_disposition());
			row.put("30", c.getServiceType());
			row.put("31", c.getPsf_status());
			row.put("32", c.getCalling_data_type());
			row.put("33", c.getTypeofpickup());
			row.put("34", c.getFromtimeofpick());
			row.put("35", c.getUpto());
			row.put("36", c.getDriver());
			row.put("37", c.getService_advisor());
			row.put("38", c.getUpselltype());
			row.put("39", String.valueOf(c.getAssigned_date()));
			row.put("40", c.getIsCallinitaited());
			an.add(row);
		}
		return ok(result);
	}
	@Secure(clients = "FormClient")
	public Result getAllCallInteractions_paging() throws ParseException {
		Map<String, String[]> paramMap = request().queryString();
		Map<String,String []> parambody = request().body().asFormUrlEncoded();
		boolean allflag = false;
		String searchPattern = "";
		if (paramMap.get("search[value]") != null) {
			searchPattern = paramMap.get("search[value]")[0];
		}
		if (searchPattern.length() == 0) {
			allflag = true;
		}
		long fromIndex = Long.valueOf(parambody.get("start")[0]);
		long toIndex = Long.valueOf(parambody.get("length")[0]);
		String fromdate = (parambody.get("fromdate")[0]);
		String todate = (parambody.get("todate")[0]);
		String creName = (parambody.get("crename")[0]);
		String dispositions = (parambody.get("dispositions")[0]);
		String locations = (parambody.get("locations")[0]);
		//logger.info("got required parameters to view"+" fromdate:-"+fromdate+",todate:-"+todate+",creName:-"+creName+",disposition:"+dispositions+",locations:"+locations);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date fd = null;
		Date td = null;
		if(fromdate.equals("")||todate.equals("")||creName.equals("")||dispositions.equals("")||locations.equals("")){
			ObjectNode result = Json.newObject();	
			return ok();
		}else{
			if (!fromdate.equals("") && !todate.equals("")) {
				fd = format.parse(fromdate);
				td = format.parse(todate);
				Calendar c = Calendar.getInstance();
				c.setTime(td);
				c.add(Calendar.DATE, 1);
				td = c.getTime();
				c.setTime(fd);
				c.add(Calendar.DATE, -1);
				fd=c.getTime();

			} else if (fromdate.equals("") && !todate.equals("")) {
				td = format.parse(todate);
				fd = null;
				Calendar c = Calendar.getInstance();
				c.setTime(td);
				c.add(Calendar.DATE, 1);
				td = c.getTime();

			} else if (!fromdate.equals("") && todate.equals("")) {
				fd = format.parse(fromdate);
				td = null;
				Calendar c = Calendar.getInstance();
				c.setTime(fd);
				c.add(Calendar.DATE,-1);
				fd = c.getTime();

			} else if (fromdate.equals("") && todate.equals("")) {
				fd = null;
				td = null;
			}
		
			List<CallInteractionHistory> allCallInteractions = all_call_int_repo
					.getAllCallInteractionDetails(getProfiles().getId(), fd, td, creName, dispositions,locations);
			logger.info("got all the required call interaction history view"+allCallInteractions.size());
			long totalsize = allCallInteractions.size();
			if (toIndex < 0) {
				toIndex = 10;

			} else {
				toIndex = fromIndex + toIndex;
			}

			if (toIndex > totalsize) {

				toIndex = totalsize;
			}

			ObjectNode result = Json.newObject();
			long patternCount = 0;

			List<CallInteractionHistory> req_allCallInteractions = new ArrayList<CallInteractionHistory>();
			
			for (int i = (int) fromIndex; i < toIndex; i++) {
				CallInteractionHistory aci = allCallInteractions.get((int) i);
				if (searchPattern.length() == 0) {
					req_allCallInteractions.add(aci);
				} else if (searchPattern.length() != 0) {
					if ((aci.getCallDate()!= null &&aci.getCallDate().equals(searchPattern)) || (aci.getCallDuration() != null && aci.getCallDuration().equals(searchPattern))
							|| (aci.getCallTime() != null && aci.getCallTime().equals(searchPattern)) || (aci.getCallType() != null && aci.getCallType().equals(searchPattern))
							|| (aci.getLocation() != null && aci.getLocation().equals(searchPattern)) || (aci.getLocation() != null && aci.getLocation().equals(searchPattern))
							|| (aci.getServiceType()!= null && aci.getServiceType().equals(searchPattern)) || (aci.getPreffered_Contact_number()!= null && aci.getPreffered_Contact_number().equals(searchPattern))
							|| (aci.getCre_Name() != null && aci.getCre_Name().equals(searchPattern)) || (aci.getCustomer_category()!= null && aci.getCustomer_category().equals(searchPattern))
							|| (aci.getCustomer_name() != null && aci.getCustomer_name().equals(searchPattern)) || (aci.getForecastLogic() != null && aci.getForecastLogic().equals(searchPattern))
							|| (aci.getFueltype() != null && aci.getFueltype().equals(searchPattern)) || (aci.getLast_serviceDate()!=null && aci.getLast_serviceDate().equals(searchPattern))
							|| (aci.getLastServiceType() != null && aci.getLastServiceType().equals(searchPattern)) || (aci.getModel() != null && aci.getModel().equals(searchPattern))
							|| (aci.getPrevious_disposition() != null && aci.getPrevious_disposition().equals(searchPattern))
							|| (aci.getPrimary_disposition() != null && aci.getPrimary_disposition().equals(searchPattern))
							|| (aci.getPsf_status() != null &&aci.getPsf_status().equals(searchPattern)) || (aci.getVeh_Reg_no()!= null && aci.getVeh_Reg_no().equals(searchPattern))
							|| (aci.getSecondary_dispostion()!=null && aci.getSecondary_dispostion().equals(searchPattern))
							|| (aci.getNextServicedate()!=null && aci.getNextServicedate().equals(searchPattern)) || (aci.getNextServicetype()!= null && aci.getNextServicetype().equals(searchPattern))
							|| (aci.getTertiary_disposition() != null && aci.getTertiary_disposition().equals(searchPattern))
							|| (aci.getVariant()!= null && aci.getVariant().equals(searchPattern))) {
						req_allCallInteractions.add(aci);
					}
				}

			}

			if (!allflag) {
				patternCount = req_allCallInteractions.size();
				logger.info("patternCount of assignedList : " + patternCount);

			}
			
			result.put("recordsTotal", totalsize);
			if (allflag) {
				result.put("recordsFiltered", totalsize);
			} else {
				result.put("recordsFiltered", patternCount);
			}
			logger.info("got callinteraction history for callhistory table");
			ArrayNode an = result.putArray("data");
			for (CallInteractionHistory aci : req_allCallInteractions) {
				
				String s = "<a href=/CREManager/downloadMediaFile?calldate="+aci.getCallDate()+"&calltime="+aci.getCallTime()+"><i class='fa fa-play fa-2x' data-toggle='tooltip' data-placement='bottom' title='audio'></i></a>";
				
				ObjectNode row = Json.newObject();
				row.put("0", aci.getLocation());
				row.put("1", aci.getCre_Name());
				row.put("2", aci.getCallTime());
				row.put("3", aci.getCallDate());
				row.put("4", aci.getPreffered_Contact_number());
				row.put("5", aci.getCallType());
				row.put("6", aci.getCustomer_name());
				row.put("7", aci.getDOb());
				row.put("8", aci.getOffice_address());
				row.put("9", aci.getResidential_address());
				row.put("10", aci.getPermenant_address());
				row.put("11", aci.getEmail_id());
				row.put("12", aci.getCustomer_category());
				row.put("13", aci.getCustomer_city());
				row.put("14", aci.getCallDuration());
				row.put("15", aci.getVeh_Reg_no());
				row.put("16", aci.getChassisNo());
				row.put("17", aci.getModel());
				row.put("18", aci.getFueltype());
				row.put("19", aci.getVariant());
				row.put("20", aci.getLast_serviceDate());
				row.put("21", aci.getLastServiceType());
				row.put("22", aci.getNextServicedate());
				row.put("23", aci.getNextServicetype());
				row.put("24", aci.getForecastLogic());
				row.put("25", aci.getPrevious_disposition());
				row.put("26", aci.getPrimary_disposition());
				row.put("27", aci.getSecondary_dispostion());
				row.put("28", aci.getTertiary_disposition());
				row.put("29", aci.getServiceType());
				row.put("30", aci.getPsf_status());
				
				/*if(aci.getMedia_avl().equals("available")){
					row.put("31",s);
					}else{
						row.put("31", "");
					}*/
												
				an.add(row);
			}

			return ok(result);
		}
		
	}

	@Secure(clients = "FormClient")
	public Result viewAllCallIntearctions() {
		List<String> allCRE = all_call_int_repo.getAllCreNames(getProfiles().getId());
		List<CallDispositionData> allDispositions = all_call_int_repo.getAllDispositions();
		List<String> locations = all_call_int_repo.getAllLocations();
		return ok(callInteractionsTable.render("", "", getUserLoginName(), getDealerName(), allCRE, allDispositions,locations));
	}
	@Secure(clients = "FormClient")
	public Result getRecording()throws IOException{
		Map<String, String[]> paramMap = request().queryString();
		String calldate = (paramMap.get("calldate")[0]);
		String calltime = (paramMap.get("calltime")[0]);
		byte[] bytes = all_call_int_repo.getMediaFilefromDB(calldate, calltime);
		
		List<String> car_phone=all_call_int_repo.getCRENAmeAndNumber(calldate, calltime);
		
		int size;
		if(bytes!=null){
		
		String nameMedia=car_phone.get(0)+"_"+calldate+"_"+calltime+"_"+car_phone.get(1);
			size = bytes.length;
		response().setContentType("audio/mp3");
		response().setHeader("Content-Disposition",
				"attachment;filename="+nameMedia+".mp3");
		response().setHeader("Cache-Control", "no-cache");
		return ok(bytes);
		}
		
		return ok("Empty");
	}
	
	// download call history reports
	@Secure(clients = "FormClient")
	public Result downloadCallHistoryReport() throws IOException, IllegalArgumentException, IllegalAccessException {
		
		logger.info("call history reports:");
		WyzUser userdata = wyzRepo.getUserbyUserName(getUserLoginName());
		
		String fromDateNew ="";
		String toDateNew="";
		String location="";
		String workshop="";
		String disposition="";
		String creidsare="";
		
		Map<String, String[]> parambody = request().body().asFormUrlEncoded();

		if ((parambody.get("frombilldaterange")) != null) {
			fromDateNew = (parambody.get("frombilldaterange")[0]);
		}
		if ((parambody.get("tobilldaterange")) != null) {
			toDateNew = (parambody.get("tobilldaterange")[0]);
		}
		if ((parambody.get("locations")) != null) {

			if ((parambody.get("locations")[0]).equals("0")) {
				location = "";
			} else {
				String[] locationArray= (parambody.get("locations"));
				int j = 0;
			    for (int i = 0; i < locationArray.length; i++) {
			        
			            if (j == 0)
			            	location = locationArray[i];
			            else
			            	location = location + "," + locationArray[i];
			            
			            j++;
			        
			    }
			}
		}
		if ((parambody.get("workshopname")) != null) {
			if ((parambody.get("workshopname")[0]).equals("select")) {
				workshop = "";
			} else {
				String[] workshopArray= (parambody.get("workshopname"));
				int j = 0;
			    for (int i = 0; i < workshopArray.length; i++) {
			        
			            if (j == 0)
			            	workshop = workshopArray[i];
			            else
			            	workshop = workshop + "," + workshopArray[i];
			            
			            j++;
			        
			    }
			}
		}

				
		if ((parambody.get("crename")) != null) {
			if ((parambody.get("crename")[0]).equals("0")) {
				creidsare = "";
			} else {
				String[] creidsareArray= (parambody.get("crename"));
				int j = 0;
			    for (int i = 0; i < creidsareArray.length; i++) {
			        
			            if (j == 0)
			            	creidsare = creidsareArray[i];
			            else
			            	creidsare = creidsare + "," + creidsareArray[i];
			            
			            j++;
			        
			    }
			}
		}
		
		
		if ((parambody.get("all_dispositions")) != null) {
			if ((parambody.get("all_dispositions")[0]).equals("0")) {
				disposition = "";
			} else if ((parambody.get("all_dispositions")[0]).equals("All")) {
				(parambody.get("all_dispositions")[0]) = "";
			} else {
				String[] dispositionArray= (parambody.get("all_dispositions"));
				int j = 0;
			    for (int i = 0; i < dispositionArray.length; i++) {
			        
			            if (j == 0)
			            	disposition = dispositionArray[i];
			            else
			            	disposition = disposition + "," + dispositionArray[i];
			            
			            j++;
			        
			    }
			}
		}
		
		
		logger.info("D from date new :"+fromDateNew);
		 logger.info("D to date new :"+toDateNew);
		 logger.info("D location call reports :"+location);
		 logger.info("D workshop call reports:"+workshop);
		 logger.info("D disposition reports:"+disposition);
		 logger.info("D creidsare date new :"+creidsare);
	
		long totalSize = 0;
		
		totalSize = searchRepo.getAllCallhistoryCount(getUserLoginName(),fromDateNew,toDateNew,location,workshop,disposition,creidsare);
		logger.info("totalSize of download in reports call history: " + totalSize);

		/*if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {

			toIndex = totalSize;
		}*/
		long patternCount = 0;

		List<CallInteractionHistory> callhistoryList = searchRepo.getAllCallHistoryData(getUserLoginName(),fromDateNew, toDateNew,location,workshop,creidsare,disposition, 
				0,
				totalSize);		
		
		Config configuration = ConfigFactory.load();
		String pathOfTemp = configuration.getString("app.configuration");

		String fileName = currentDate() + "_CallHistory_Report.xlsx";

		File fis = new File(pathOfTemp + fileName);

		FileOutputStream fileOut = new FileOutputStream(fis);
		XSSFWorkbook wb = new XSSFWorkbook();
		Sheet sheet = wb.createSheet("Sheet1");

		String[] headers = new String[] { 
				"Location", 
				"wyzUser_id", 
				"Cre Name",
				"Call Made Date and Time",
				"Call Time",
				"Call Date",
				"Preferred Contact Number",
				"Call Type",
				"Customer name",
				"Dob",
				"Office Address",
				"Residential Address",
				"Permanent Address",
				"Email id",
				"Customer Category",
				"Call Duration",
				"Customer Remarks",
				"Vehicle Reg No",
				"Chassis No",
				"Model",
				"Fuel Type",
				"Variant",
				"Last Service Date",
				"Last Service Type",
				"Next Service Type",
				"Forecast Logic",
				"Previous Disposition",
				"Primary Disposition",
				"Secondary Disposition",
				"Tertiary Disposition",
				"Service Type",
				"PSF Status",
				"CallInteraction_id",
				"sr_disposition_id",
				"Calling Data type",
				"Type Of Pickup",
				"From time of pickup",
				"Upto",
				"Driver",
				"Service Advisor",
				"Up Sell Type",
				"Assigned Date",
				"Is CallInitiated"};

		int rowCount = 1;
		Row r = sheet.createRow(0);
		for (int rn = 0; rn < headers.length; rn++) {
			r.createCell(rn).setCellValue(headers[rn]);
		}
		for (CallInteractionHistory aBook : callhistoryList) {
			Row row = sheet.createRow(rowCount++);
			int columnCount = 0;

			for (Field field : aBook.getClass().getDeclaredFields()) {
				if (columnCount < headers.length) {
					Cell cell = row.createCell(columnCount++);
					cell.setCellValue(String.valueOf(field.get(aBook)));
					// logger.info("cell value : " + field.get(aBook));

				}
			}

		}
		wb.write(fileOut);
		fileOut.close();
		return ok(fis, fileName);

	}

	
	@Secure(clients = "FormClient")
		public Result viewAllSMSIntearctions() {
			List<String> allUsers = all_call_int_repo.getAllUsers();
			List<String> locations = all_call_int_repo.getAllLocations();
			return ok(smsInteractionReports.render("", "", getUserLoginName(), getDealerName(), allUsers,locations));
		}
	
	@Secure(clients ="FormClient")
	public Result getAllSMSHistoryData(){

		logger.info("inside controller of call reports");
		
		Map<String, String[]> parambody = request().body().asFormUrlEncoded();

	
		ObjectNode result = Json.newObject();


		String fromDateNew ="";
		String toDateNew="";
		String location="";
		String workshop="";
		String sentStatuses="";
		String creidsare="";
		String convertedFromDate="";
		String convertedToDate="";
			
		
		// location = (parambody.get("locations")[0]);
		// workshop = (parambody.get("workshop")[0]);
		 creidsare= (parambody.get("UserIds")[0]);
		 sentStatuses=(parambody.get("sentStatuses")[0]);
		 
		 SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
		 Date fromDateOld=new Date();
		 Date toDateOld=new Date();
		 String pattern = "dd/MM/yyyy";
		 SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		 
		 if(parambody.get("fromDate")[0].length()!=0){
			 
			 fromDateNew = (parambody.get("fromDate")[0]);
			 try {
				 fromDateOld=dmyFormat.parse(fromDateNew);
				 
				 } catch (Exception e) {
					 
				 }
			 convertedFromDate=formatter.format(fromDateOld);
		 }
		 if(parambody.get("toDate")[0].length()!=0){
			 toDateNew = (parambody.get("toDate")[0]);
			 try {
				  toDateOld=dmyFormat.parse(toDateNew);
				 } catch (Exception e) {
					 
				 }
			 convertedToDate=formatter.format(toDateOld);
		 }
			 logger.info("convertedFromDate:"+convertedFromDate);
			 logger.info("convertedToDate:"+convertedToDate);			 
				
		String searchPattern = "";
		boolean allflag = false;
		boolean globalSearch = false;

		if (parambody.get("search[value]") != null) {
			searchPattern = parambody.get("search[value]")[0];
		}

		if (searchPattern.length() == 0 && fromDateNew.length() == 0
				&& toDateNew.length() == 0 &&  sentStatuses.length() == 0) {
			allflag = true;
			logger.info("search length is 0 ");
		}
		if (searchPattern.length() > 0) {
			globalSearch = true;
			logger.debug("Global Search is true");
		}

		long fromIndex = Long.valueOf(parambody.get("start")[0]);
		long toIndex = Long.valueOf(parambody.get("length")[0]);
		
		logger.info("fromIndex :"+fromIndex);
		logger.info("toIndex :"+toIndex);
		long totalSize = 0;
		
		totalSize = searchRepo.getAllSMShistoryCount("","","","");
		logger.info("totalSize of reports sms history: " + totalSize);
		if (toIndex < 0) {
			toIndex = 10;
		}
		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;
		List<CallInteractionHistory> smshistoryList = searchRepo.
				getAllSMSHistoryData(convertedFromDate, convertedToDate,creidsare,sentStatuses,fromIndex,toIndex);
		logger.info("smshistorylist:"+smshistoryList.size());
		if (!allflag) {
			if (globalSearch)
				//patternCount = totalSize;
				patternCount = searchRepo.getAllSMShistoryCount(convertedFromDate,convertedToDate,sentStatuses,creidsare);
			else
				patternCount = searchRepo.getAllSMShistoryCount(convertedFromDate,convertedToDate,sentStatuses,creidsare);
		}
		result.put("draw", Integer.valueOf(parambody.get("draw")[0]));
		result.put("recordsTotal", totalSize);
		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		
		ArrayNode an = result.putArray("data");
		
		for (CallInteractionHistory c : smshistoryList) {
			logger.info("c.getInteractionDate():"+c.getInteractionDate());
			
			ObjectNode row = Json.newObject();
			row.put("0", c.getInteractionDate());
			row.put("1", c.getInteractionTime());
			row.put("2", c.getSmsMessage());
			row.put("3", c.smsStatus);
			if(c.smsStatus){
				row.put("3", "SUCCESS");
			}else{
				row.put("3", "FAILURE");
			}
			row.put("4", c.getCustomerName());
			row.put("5", c.getVehicleRegNo());
			row.put("6", c.getWyzuserName());
			row.put("7", c.getMobileNumber());

			an.add(row);
		}
		return ok(result);
	}
	@Secure(clients = "FormClient")
	public Result downloadSMSHistoryReport() throws IOException, IllegalArgumentException, IllegalAccessException {
			
			logger.info("call history reports:");
			WyzUser userdata = wyzRepo.getUserbyUserName(getUserLoginName());
			
			String fromDateNew ="";
			String toDateNew="";
			String location="";
			String workshop="";
			String sentStatuses="";
			String creidsare="";
			String convertedFromDate="";
			String convertedToDate="";
			
			Map<String, String[]> parambody = request().body().asFormUrlEncoded();
	
			if ((parambody.get("frombilldaterange")) != null) {
				fromDateNew = (parambody.get("frombilldaterange")[0]);
	}
			if ((parambody.get("tobilldaterange")) != null) {
				toDateNew = (parambody.get("tobilldaterange")[0]);
			}
			if ((parambody.get("locations")) != null) {
	
				if ((parambody.get("locations")[0]).equals("0")) {
					location = "";
				} else {
					String[] locationArray= (parambody.get("locations"));
					int j = 0;
				    for (int i = 0; i < locationArray.length; i++) {
				        
				            if (j == 0)
				            	location = locationArray[i];
				            else
				            	location = location + "," + locationArray[i];
				            
				            j++;
				        
				    }
				}
			}
		if ((parambody.get("workshopname")) != null) {
					if ((parambody.get("workshopname")[0]).equals("select")) {
					workshop = "";
				} else {
						String[] workshopArray= (parambody.get("workshopname"));
				int j = 0;
			    for (int i = 0; i < workshopArray.length; i++) {
				        
				            if (j == 0)
				            	workshop = workshopArray[i];
				            else
				            	workshop = workshop + "," + workshopArray[i];
				            
				            j++;
				        
				    }
				}
			}
	
					
			if ((parambody.get("crename")) != null) {
				if ((parambody.get("crename")[0]).equals("0")) {
					creidsare = "";
				} else {
					String[] creidsareArray= (parambody.get("crename"));
					int j = 0;
				    for (int i = 0; i < creidsareArray.length; i++) {
				        
				            if (j == 0)
				            	creidsare = creidsareArray[i];
				            else
				            	creidsare = creidsare + "," + creidsareArray[i];
				            
				            j++;
				        
				    }
				}
			}
			
			
			if ((parambody.get("all_dispositions")) != null) {
				if ((parambody.get("all_dispositions")[0]).equals("0")) {
					sentStatuses = "";
				} else if ((parambody.get("all_dispositions")[0]).equals("All")) {
					(parambody.get("all_dispositions")[0]) = "";
				} else {
				String[] dispositionArray= (parambody.get("all_dispositions"));
					int j = 0;
				    for (int i = 0; i < dispositionArray.length; i++) {
				        
				            if (j == 0)
				            	sentStatuses = dispositionArray[i];
				            else
				            	sentStatuses = sentStatuses + "," + dispositionArray[i];
				            
				            j++;
				        
				    }
				}
			}
			
	
		
			long totalSize = 0;
			
			totalSize =searchRepo.getAllSMShistoryCount(convertedFromDate,convertedToDate,sentStatuses,creidsare);
	
			logger.info("totalSize of download in reports call history: " + totalSize);
	
			/*if (toIndex < 0) {
	+			toIndex = 10;
	+
	+		}
	+
	+		if (toIndex > totalSize) {
	+
	+			toIndex = totalSize;
			}*/
			long patternCount = 0;
	
			List<CallInteractionHistory> smshistoryList = searchRepo.getAllSMSHistoryData(convertedFromDate, convertedToDate,creidsare,sentStatuses,0,
					totalSize);
			logger.info("smshistoryList:"+smshistoryList.size());
			Config configuration = ConfigFactory.load();
			String pathOfTemp = configuration.getString("app.configuration");
	
			String fileName = currentDate() + "_SmsHistory_Report.xlsx";
	
			File fis = new File(pathOfTemp + fileName);
	
			FileOutputStream fileOut = new FileOutputStream(fis);
			XSSFWorkbook wb = new XSSFWorkbook();
			Sheet sheet = wb.createSheet("Sheet1");
	
			String[] headers = new String[] { 
					"Interaction Date", 
					"Interaction Time", 
					"Message Content",
					"SMS Status",
					"Customer Name",
					"Vehicle Reg no",
					"WyzUsername",
					"Mobile Number"};
	
			int rowCount = 1;
			Row r = sheet.createRow(0);
			for (int rn = 0; rn < headers.length; rn++) {
				r.createCell(rn).setCellValue(headers[rn]);
			}
			for (CallInteractionHistory aBook : smshistoryList) {
				Row row = sheet.createRow(rowCount++);
				int columnCount = 0;
				int j=0;
	
				for (Field field : aBook.getClass().getDeclaredFields()) {
					if (columnCount < headers.length) {
						if(field.getName().equals("interactionDate")){
							++j;
						Cell cell = row.createCell(columnCount++);
						cell.setCellValue(String.valueOf(field.get(aBook)));
						logger.info("cell value : " + field.get(aBook));
						logger.info("filed name : " + field.getName());
						logger.info("columnCount value : " + columnCount);
						}
						logger.info("j value : " + j);
						if(j>0){
							Cell cell = row.createCell(columnCount++);
							cell.setCellValue(String.valueOf(field.get(aBook)));
							logger.info("cell value : " + field.get(aBook));
							logger.info("filed name : " + field.getName());
							logger.info("columnCount value : " + columnCount);
						}
					}
				}
			}
			wb.write(fileOut);
			fileOut.close();
			return ok(fis, fileName);
	
	
		}
	
	
	
	@Secure(clients = "FormClient")
	public Result viewAllInsuranceIntearctions() {
		List<String> allUsers = all_call_int_repo.getAllUsers();
		List<String> locations = all_call_int_repo.getAllLocations();
		List<Campaign> campaignListInsurance = call_int_repo.getAllCampaignListInsurance();
		return ok(insuranceReports.render("", "", getUserLoginName(), getDealerName(), allUsers,locations,campaignListInsurance));
	}
	
	
	
	@Secure(clients ="FormClient")
	public Result getAllInsuranceHistoryData(){

		logger.info("inside controller of insurance reports");
		
		Map<String, String[]> parambody = request().body().asFormUrlEncoded();

	
		ObjectNode result = Json.newObject();


		String fromDateNew ="";
		String toDateNew="";
		String location="";
		String workshop="";
		String campaignName="";
		String creidsare="";
		String convertedFromDate="";
		String convertedToDate="";
			
		
		// location = (parambody.get("locations")[0]);
		// workshop = (parambody.get("workshop")[0]);
		 creidsare= (parambody.get("UserIds")[0]);
		 campaignName=(parambody.get("campaignName")[0]);
		 
if(parambody.get("fromDate")[0].length()!=0){
			 
			
			 convertedFromDate=(parambody.get("fromDate")[0]);
		 }
		 if(parambody.get("toDate")[0].length()!=0){
			
			 convertedToDate=(parambody.get("toDate")[0]);
		 }
		 
		 
		/* SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
		 Date fromDateOld=new Date();
		 Date toDateOld=new Date();
		 String pattern = "dd/MM/yyyy";
		 SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		 
		 if(parambody.get("fromDate")[0].length()!=0){
			 
			 fromDateNew = (parambody.get("fromDate")[0]);
			 try {
				 fromDateOld=dmyFormat.parse(fromDateNew);
				 
				 } catch (Exception e) {
					 
				 }
			 convertedFromDate=formatter.format(fromDateOld);
		 }
		 if(parambody.get("toDate")[0].length()!=0){
			 toDateNew = (parambody.get("toDate")[0]);
			 try {
				  toDateOld=dmyFormat.parse(toDateNew);
				 } catch (Exception e) {
					 
				 }
			 convertedToDate=formatter.format(toDateOld);
		 }*/
			 logger.info("convertedFromDate:"+convertedFromDate);
			 logger.info("convertedToDate:"+convertedToDate);		
			 logger.info("campaignName:"+campaignName);
				
		String searchPattern = "";
		boolean allflag = false;
		boolean globalSearch = false;

		if (parambody.get("search[value]") != null) {
			searchPattern = parambody.get("search[value]")[0];
		}

		if (searchPattern.length() == 0 && fromDateNew.length() == 0
				&& toDateNew.length() == 0 &&  campaignName.length() == 0) {
			allflag = true;
			logger.info("search length is 0 ");
		}
		if (searchPattern.length() > 0) {
			globalSearch = true;
			logger.debug("Global Search is true");
		}

		long fromIndex = Long.valueOf(parambody.get("start")[0]);
		long toIndex = Long.valueOf(parambody.get("length")[0]);
		
		logger.info("fromIndex :"+fromIndex);
		logger.info("toIndex :"+toIndex);
		long totalSize = 0;
		
		totalSize = searchRepo.getAllInsurancehistoryCount("","","","");
		logger.info("totalSize of reports insurance history: " + totalSize);
		if (toIndex < 0) {
			toIndex = 10;
		}
		if (toIndex > totalSize) {

			toIndex = totalSize;
		}
		long patternCount = 0;
		List<InsuranceHistoryReport> InsurancehistoryList = searchRepo.getAllInsuranceHistoryData(convertedFromDate, convertedToDate,
				creidsare,campaignName,fromIndex,toIndex);
		logger.info("InsurancehistoryList:"+InsurancehistoryList.size());
		if (!allflag) {
			if (globalSearch)
				//patternCount = totalSize;
				patternCount = searchRepo.getAllInsurancehistoryCount(convertedFromDate,convertedToDate,creidsare,campaignName);
			else
				patternCount = searchRepo.getAllInsurancehistoryCount(convertedFromDate,convertedToDate,creidsare,campaignName);
		}
		
		logger.info("pattern count:"+patternCount);
		
		result.put("draw", Integer.valueOf(parambody.get("draw")[0]));
		result.put("recordsTotal", totalSize);
		if (allflag) {
			result.put("recordsFiltered", totalSize);
		} else {
			result.put("recordsFiltered", patternCount);
		}
		
		ArrayNode an = result.putArray("data");
		
		for (InsuranceHistoryReport c : InsurancehistoryList) {
			ObjectNode row = Json.newObject();
			row.put("0", c.getCreName());
			row.put("1", String.valueOf(c.getCallDate()));
			row.put("2", c.getCallTime());
			row.put("3", c.getIsCallinitaited());
			row.put("4", c.getCallType());
			row.put("5", c.getCallDuration());
			row.put("6", c.getRingTime());
			row.put("7", c.getCampaignTYpe());
			row.put("8", c.getChassisNo());
			row.put("9", c.getModel());
			row.put("10", c.getVariant());
			row.put("11", c.getSaleDate());
			row.put("12", c.getVehicleRegno());
			row.put("13", c.getCustomerName());
			row.put("14", c.getPreffered_address());
			row.put("15", c.getPrefferedPhoneNumber());
			row.put("16", c.getPrimaryDisposition());
			row.put("17", c.getSecondaryDisposition());
			row.put("18", c.getCoverNoteNo());
			row.put("19", c.getLastRenewedLocation());
			row.put("20", c.getLastRenewalDate());
			row.put("21", c.getPremimum());
			row.put("22", c.getRenewalDoneBy());
			row.put("23", c.getTertiary_disposition());
			row.put("24", String.valueOf(c.getFollowUpDate()));
			row.put("25", c.getFollowUpTime());
			row.put("26", c.getAddOnsPrefered_OtherOptionsData());
			row.put("27", c.getAddOnsPrefered_PopularOptionsData());
			row.put("28", c.getRemarksOfFb());
			row.put("29", c.getDepartmentForFB());
			row.put("30", c.getAddressOfVisit());
			row.put("31", c.getAppointeeName());
			row.put("32", String.valueOf(c.getAppointmentDate()));
			row.put("33", c.getAppointmentFromTime());
			row.put("34", c.getDsa());
			row.put("35", c.getInsuranceAgentData());
			row.put("36", c.getInsuranceCompany());
			row.put("37", c.getNomineeName());
			row.put("38", c.getNomineeRelationWithOwner());
			row.put("39", c.getPremiumwithTax());
			row.put("40", c.getRenewalMode());
			row.put("41", c.getRenewalType());
			row.put("42", c.getTypeOfPickup());
			row.put("43", c.getPremiumYes());
			row.put("44", c.getComments());
			row.put("45", c.getReason());
			row.put("46", c.getCremanager());
			row.put("47", String.valueOf(c.getUpdatedDate()));
			row.put("48", c.getReasonForSNR());
			an.add(row);
		}
		return ok(result);
		}

	@Secure(clients = "FormClient")
	public Result downloadInsuranceHistoryReport() throws IOException, IllegalArgumentException, IllegalAccessException {
		
		logger.info("call history reports:");
		WyzUser userdata = wyzRepo.getUserbyUserName(getUserLoginName());
		
		String fromDateNew ="";
		String toDateNew="";
		String location="";
		String workshop="";
		String campaignName="";
		String creidsare="";
		
		Map<String, String[]> parambody = request().body().asFormUrlEncoded();

		if ((parambody.get("frombilldaterange")) != null) {
			fromDateNew = (parambody.get("frombilldaterange")[0]);
		}
		if ((parambody.get("tobilldaterange")) != null) {
			toDateNew = (parambody.get("tobilldaterange")[0]);
		}
		if ((parambody.get("locations")) != null) {

			if ((parambody.get("locations")[0]).equals("0")) {
				location = "";
			} else {
				String[] locationArray= (parambody.get("locations"));
				int j = 0;
			    for (int i = 0; i < locationArray.length; i++) {
			        
			            if (j == 0)
			            	location = locationArray[i];
			            else
			            	location = location + "," + locationArray[i];
			            
			            j++;
			        
			    }
			}
		}
		if ((parambody.get("workshopname")) != null) {
			if ((parambody.get("workshopname")[0]).equals("select")) {
				workshop = "";
			} else {
				String[] workshopArray= (parambody.get("workshopname"));
				int j = 0;
			    for (int i = 0; i < workshopArray.length; i++) {
			        
			            if (j == 0)
			            	workshop = workshopArray[i];
			            else
			            	workshop = workshop + "," + workshopArray[i];
			            
			            j++;
			        
			    }
			}
		}

				
		if ((parambody.get("crename")) != null) {
			if ((parambody.get("crename")[0]).equals("0")) {
				creidsare = "";
			} else {
				String[] creidsareArray= (parambody.get("crename"));
				int j = 0;
			    for (int i = 0; i < creidsareArray.length; i++) {
			        
			            if (j == 0)
			            	creidsare = creidsareArray[i];
			            else
			            	creidsare = creidsare + "," + creidsareArray[i];
			            
			            j++;
			        
			    }
			}
		}
		
		
		if ((parambody.get("campaignName")) != null) {
			if ((parambody.get("campaignName")[0]).equals("0")) {
				campaignName = "";
			} else if ((parambody.get("campaignName")[0]).equals("All")) {
				(parambody.get("campaignName")[0]) = "";
			} else {
				String[] dispositionArray= (parambody.get("campaignName"));
				int j = 0;
			    for (int i = 0; i < dispositionArray.length; i++) {
			        
			            if (j == 0)
			            	campaignName = dispositionArray[i];
			            else
			            	campaignName = campaignName + "," + dispositionArray[i];
			            
			            j++;
			        
			    }
			}
		}
		
		
		 logger.info("D from date new :"+fromDateNew);
		 logger.info("D to date new :"+toDateNew);
		 logger.info("D location call reports :"+location);
		 logger.info("D workshop call reports:"+workshop);
		 logger.info("D disposition reports:"+campaignName);
		 logger.info("D creidsare date new :"+creidsare);
	
		long totalSize = 0;
		
		totalSize =searchRepo.getAllInsurancehistoryCount(fromDateNew,toDateNew,campaignName,creidsare);

		logger.info("totalSize of download in reports insurance history: " + totalSize);

		/*if (toIndex < 0) {
			toIndex = 10;

		}

		if (toIndex > totalSize) {

			toIndex = totalSize;
		}*/
		long patternCount = 0;

		List<InsuranceHistoryReport> InsurancehistoryList = searchRepo.getAllInsuranceHistoryData(fromDateNew, toDateNew,creidsare,campaignName,0,
				totalSize);
		logger.info("InsuarancehistoryList:"+InsurancehistoryList.size());
		Config configuration = ConfigFactory.load();
		String pathOfTemp = configuration.getString("app.configuration");

		String fileName = currentDate() + "_Insurance_History_Report.xlsx";

		File fis = new File(pathOfTemp + fileName);

		FileOutputStream fileOut = new FileOutputStream(fis);
		XSSFWorkbook wb = new XSSFWorkbook();
		Sheet sheet = wb.createSheet("Sheet1");

		String[] headers = new String[] { 
				"CRE Name", 
				"Call Date", 
				"Call Time",
				"Is CallInitiated",
				"Call Type",
				"Call Duration",
				"Ring Time",
				"Campaign Type",
				"Chassis No",
				"Model",
				"Variant",
				"SaleDate",
				"Vehicle Reg No",
				"Customer Name",
				"Preffered Address",
				"Preffered Phone Number",
				"Primary Disposition",
				"Secondary Disposition",
				"Cover Note No",
				"Last Renewed Location",
				"Last Renewal Date",
				"Premium",
				"Renewal Done By",
				"Tertiary Disposition",
				"Follow Up Date",
				"Follow Up Time",
				"Add Ons Preffered Other options",
				"Add Ons Preffered Popular options",
				"Remarks Of FB",
				"Department For FB",
				"Address Of Visit",
				"Appointee Name",
				"Appointment Date",
				"Appointment From Time",
				"DSA",
				"Insurance Agent Data",
				"Insurance Company",
				"Nominee Name",
				"Nominee Relation With Owner",
				"Premium With Tax",
				"Renewal Mode",
				"Renewal Type",
				"Type of Pickup",
				"Premium Yes",
				"Comments",
				"Reason",
				"CRE Manager",
				"Updated Date",
				"Reason For SNR"
		};
		

		int rowCount = 1;
		Row r = sheet.createRow(0);
		for (int rn = 0; rn < headers.length; rn++) {
			r.createCell(rn).setCellValue(headers[rn]);
		}
		for (InsuranceHistoryReport aBook : InsurancehistoryList) {
			Row row = sheet.createRow(rowCount++);
			int columnCount = 0;

			for (Field field : aBook.getClass().getDeclaredFields()) {
				if (columnCount < headers.length) {
					Cell cell = row.createCell(columnCount++);
					cell.setCellValue(String.valueOf(field.get(aBook)));
					// logger.info("cell value : " + field.get(aBook));

				}
			}

		}
		wb.write(fileOut);
		fileOut.close();
		return ok(fis, fileName);


	}
	
	private java.sql.Date currentDate() {

		return new java.sql.Date(System.currentTimeMillis());
	}
	
}
