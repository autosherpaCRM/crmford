
// @GENERATOR:play-routes-compiler
// @SOURCE:D:/CRMFORDAUTOSHERPA/crmford/conf/routes
// @DATE:Fri Mar 16 17:47:48 IST 2018

package org.pac4j.play;

import router.RoutesPrefix;

public class routes {
  
  public static final org.pac4j.play.ReverseCallbackController CallbackController = new org.pac4j.play.ReverseCallbackController(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final org.pac4j.play.javascript.ReverseCallbackController CallbackController = new org.pac4j.play.javascript.ReverseCallbackController(RoutesPrefix.byNamePrefix());
  }

}
