
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object uploadSummaryReportPage_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class uploadSummaryReportPage extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[TreeMap[String, String],String,String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(alltypeOfReport:TreeMap[String,String],dealercode:String,dealerName:String,userName:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.94*/("""
"""),_display_(/*2.2*/mainPageCREManger("AutoSherpaCRM",userName,dealerName)/*2.56*/ {_display_(Seq[Any](format.raw/*2.58*/("""
	
	"""),format.raw/*4.2*/("""<div class="panel panel-info">
   
    <div class="panel-body"> 
	     """),_display_(/*7.8*/helper/*7.14*/.form(action = routes.ReportsController.downloadUploadSummaryReport)/*7.82*/ {_display_(Seq[Any](format.raw/*7.84*/("""
	
	 """),format.raw/*9.3*/("""<div class="row">
	<div class="col-md-12" id="" style="">   
							<div class="col-lg-2">
							<label>Select File Type :</label>
							<div class="form-group">
                                
                                <select id="dataFileType" class="filter form-control selectpicker" data-column-index="0">                                                                                                        
                                   """),_display_(/*16.37*/for(key <- alltypeOfReport.keySet()) yield /*16.73*/{_display_(Seq[Any](format.raw/*16.74*/("""                       
                                    
							
										"""),format.raw/*19.11*/("""<option value="""),_display_(/*19.26*/key),format.raw/*19.29*/(""" """),format.raw/*19.30*/(""">"""),_display_(/*19.32*/alltypeOfReport/*19.47*/.get(key)),format.raw/*19.56*/("""</option>							
					  
                    
                    """)))}),format.raw/*22.22*/("""

                                """),format.raw/*24.33*/("""</select> 
                            </div>
                            </div>
                            
                            <div class="col-lg-2">
                            <div class="form-group">
                                <label>From Date :</label>
                                <input type='text' id="fromDate" placeholder="Enter From Date" data-column-index="1" class="filter form-control datepickerFilterDefault" required readonly/>                               
                            </div>
                            </div>
                            
                             <div class="col-lg-2">
                             <div class="form-group">
                                <label>To Date :</label>
                                <input type='text' id="toDate" placeholder="Enter To Date" data-column-index="2" class="filter form-control datepickerFilterDefault" required readonly/>                               
                            </div>
                            </div>
                             
				            <div class="col-lg-4">
				            <div class="form-group">
				            <lable>&nbsp;</lable><br/>
				              <button type='button' class="btn btn-primary" id="searchCust" ><i class="fa fa-list" aria-hidden="true"></i> View</button>
				               <button type="submit" id="searchCust" name="downloadFile" class="btn btn-warning"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;Download</button>
				              </div>
				            </div>
				         
            
                            
                            
                            </div>
                            </div>
                            </div>
                            """)))}),format.raw/*56.30*/("""
                            """),format.raw/*57.29*/("""</div>
	
	

<div class="panel panel-info">
    <div class="panel-heading"><strong>Upload Summary Day wise Table</strong></div>
    <div class="panel-body"> 
	  <div class="dataTable_wrapper">
              
                  <table class="table table-striped table-bordered table-hover" id="uploadSummaryReport">
        <thead>
            <tr>
                <th>DATE</th>
                <th>DAY</th>
                <th>RECORDED DATE</th>
                <th>COUNT</th>
                <th>LOCATION</th>				
            </tr>
        </thead>
 
        <tbody>                   
                  </tbody>
                </table>
             
              <!-- /.table-responsive -->
            </div>
    </div>
 </div>


    
	
    """)))}),format.raw/*89.6*/("""
    """),format.raw/*90.5*/("""<script type="text/javascript">

 $(document).ready(function() """),format.raw/*92.31*/("""{"""),format.raw/*92.32*/("""
    """),format.raw/*93.5*/("""var table = $('#uploadSummaryReport').dataTable( """),format.raw/*93.54*/("""{"""),format.raw/*93.55*/("""
        """),format.raw/*94.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
        "paging": true,
        "searching":true,
        "ordering":false,
        "ajax": "/CREManager/uploadSummaryDataTable"
        
    """),format.raw/*103.5*/("""}"""),format.raw/*103.6*/(""" """),format.raw/*103.7*/(""");

    var values=[];

  
	
   $('.filter').on('change', function() """),format.raw/*109.41*/("""{"""),format.raw/*109.42*/("""
	   
	   """),format.raw/*111.5*/("""var i= $(this).data('columnIndex');
	   var v = $(this).val();
	   if(v.length>0)"""),format.raw/*113.19*/("""{"""),format.raw/*113.20*/("""

			    """),format.raw/*115.8*/("""values[i] = v;
		  		   
	   """),format.raw/*117.5*/("""}"""),format.raw/*117.6*/("""
	   
	   	"""),format.raw/*119.6*/("""console.log(JSON.stringify(values));
	   	
     	  //clear global search values
    	  //table.api().search('');
    	  $(this).data('columnIndex');
    	  
    	  
   """),format.raw/*126.4*/("""}"""),format.raw/*126.5*/(""");


   $('#searchCust').on('click',function()"""),format.raw/*129.42*/("""{"""),format.raw/*129.43*/("""

	  """),format.raw/*131.4*/("""// alert(values.length);
	   if(values.length>0) """),format.raw/*132.25*/("""{"""),format.raw/*132.26*/("""
			"""),format.raw/*133.4*/("""for(var i=0;i<values.length;i++)"""),format.raw/*133.36*/("""{"""),format.raw/*133.37*/("""
				
			   """),format.raw/*135.7*/("""table.api().columns(i).search(values[i]);
			"""),format.raw/*136.4*/("""}"""),format.raw/*136.5*/("""
			"""),format.raw/*137.4*/("""table.api().draw();
	   """),format.raw/*138.5*/("""}"""),format.raw/*138.6*/("""
	"""),format.raw/*139.2*/("""}"""),format.raw/*139.3*/("""); 

  
 """),format.raw/*142.2*/("""}"""),format.raw/*142.3*/(""");
 </script>
      
 
	"""))
      }
    }
  }

  def render(alltypeOfReport:TreeMap[String, String],dealercode:String,dealerName:String,userName:String): play.twirl.api.HtmlFormat.Appendable = apply(alltypeOfReport,dealercode,dealerName,userName)

  def f:((TreeMap[String, String],String,String,String) => play.twirl.api.HtmlFormat.Appendable) = (alltypeOfReport,dealercode,dealerName,userName) => apply(alltypeOfReport,dealercode,dealerName,userName)

  def ref: this.type = this

}


}

/**/
object uploadSummaryReportPage extends uploadSummaryReportPage_Scope0.uploadSummaryReportPage
              /*
                  -- GENERATED --
                  DATE: Fri Mar 16 17:48:07 IST 2018
                  SOURCE: D:/CRMFORDAUTOSHERPA/crmford/app/views/uploadSummaryReportPage.scala.html
                  HASH: a7993133eb2393b58f656405bce55724e13a0b54
                  MATRIX: 819->1|1006->93|1033->95|1095->149|1134->151|1164->155|1261->227|1275->233|1351->301|1390->303|1421->308|1907->767|1959->803|1998->804|2105->883|2147->898|2171->901|2200->902|2229->904|2253->919|2283->928|2381->995|2443->1029|4250->2805|4307->2834|5082->3579|5114->3584|5205->3647|5234->3648|5266->3653|5343->3702|5372->3703|5408->3712|5676->3952|5705->3953|5734->3954|5832->4023|5862->4024|5900->4034|6010->4115|6040->4116|6077->4125|6134->4154|6163->4155|6202->4166|6398->4334|6427->4335|6502->4381|6532->4382|6565->4387|6643->4436|6673->4437|6705->4441|6766->4473|6796->4474|6836->4486|6909->4531|6938->4532|6970->4536|7022->4560|7051->4561|7081->4563|7110->4564|7147->4573|7176->4574
                  LINES: 27->1|32->1|33->2|33->2|33->2|35->4|38->7|38->7|38->7|38->7|40->9|47->16|47->16|47->16|50->19|50->19|50->19|50->19|50->19|50->19|50->19|53->22|55->24|87->56|88->57|120->89|121->90|123->92|123->92|124->93|124->93|124->93|125->94|134->103|134->103|134->103|140->109|140->109|142->111|144->113|144->113|146->115|148->117|148->117|150->119|157->126|157->126|160->129|160->129|162->131|163->132|163->132|164->133|164->133|164->133|166->135|167->136|167->136|168->137|169->138|169->138|170->139|170->139|173->142|173->142
                  -- GENERATED --
              */
          