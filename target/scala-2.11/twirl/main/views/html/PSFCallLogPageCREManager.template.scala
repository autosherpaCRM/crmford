
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object PSFCallLogPageCREManager_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class PSFCallLogPageCREManager extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template7[List[Location],String,String,Map[Long, String],List[Campaign],List[ServiceTypes],List[CallDispositionData],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(listCity:List[Location],dealerName:String,user:String,allCREHash:Map[Long,String],campaignListPSF :List[Campaign],serviceTypeList :List[ServiceTypes],dispoList:List[CallDispositionData]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.189*/("""
"""),_display_(/*2.2*/mainPageCREManger("AutoSherpaCRM",user,dealerName)/*2.52*/ {_display_(Seq[Any](format.raw/*2.54*/("""	



"""),format.raw/*6.1*/("""<style>
    td, th """),format.raw/*7.12*/("""{"""),format.raw/*7.13*/("""
        """),format.raw/*8.9*/("""border: 1px solid black;
        overflow: hidden; 
        text-align:center!important;
    """),format.raw/*11.5*/("""}"""),format.raw/*11.6*/("""
"""),format.raw/*12.1*/("""</style>
 
  <div>

        <div class="panel panel-primary">
            <div class="panel-heading">   
                PSF Call Log Information </div>
            <div class="panel-body">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#home" data-toggle="tab" id="Tab1" onclick="PSFassignedInteractionDataMR();">Scheduled</a> </li>
                    <li><a href="#profile" data-toggle="tab" id="Tab2" onclick="ajaxPSFCallForFollowUpRequiredServerMR(4);">Follow Up Required</a> </li>
                    <li><a href="#messages" data-toggle="tab" id="Tab3" onclick="ajaxPSFCallForFollowUpRequiredServerMR(22);">Completed Survey</a> </li>
                    <li><a href="#settings" data-toggle="tab" id="Tab4" onclick="ajaxPSFCallForFollowUpRequiredServerMR(25);" >Appointments</a> </li>
                    <li><a href="#nonContacts" data-toggle="tab" id="Tab5" onclick="ajaxPSFCallForNonContactsServerMR(1);">Non Contacts</a> </li>
                    <li><a href="#droppedBuc" data-toggle="tab" id="Tab6" onclick="ajaxPSFCallForNonContactsServerMR(2);">Dropped</a> </li>
                </ul>
                <div class="tab-content">
                
                <div class="row">
		<div class="col-md-2" id="cityDiv">
		<div class="form-group">
                <label>City </label>
               
               <select class="form-control" id="city" name="cityName" onchange="ajaxCallToLoadWorkShopByCity();"> 
                <option value='--Select--'>--Select--</option>
                
				"""),_display_(/*38.6*/for(city <- listCity) yield /*38.27*/{_display_(Seq[Any](format.raw/*38.28*/("""                  	                         
                    """),format.raw/*39.21*/("""<option value=""""),_display_(/*39.37*/city/*39.41*/.getName()),format.raw/*39.51*/("""">"""),_display_(/*39.54*/city/*39.58*/.getName()),format.raw/*39.68*/("""</option>
                    """)))}),format.raw/*40.22*/("""
                """),format.raw/*41.17*/("""</select>
</div>
            </div> 
			<div class="col-md-2" id="workshopDiv">
			<div class="form-group">
                <label>WorkShop Location </label>               
                <select class="form-control" id="workshop" name="workshopId" onchange="ajaxCallToLoadCRESByWorkshop();">                 			    
                

                </select>
			</div>
            </div> 
		
            <div class="col-md-2" id="cresDiv">
			 <label>Select CRE's </label>
			<div class="form-group">
              <select class="filter form-control" data-column-index="7" id="ddlCreIds" name="ddlCreIds" multiple>
                
                    
                </select> 
</div>


            </div>
            
                <div class="col-md-2 tab-pane" id="campaignDiv" >
                                
                                <label>Select Campaign</label>
						<select class="filter form-control" id="campaignName" data-column-index="0" name="campaignName">
							<option value="0" >--Select--</option>
							"""),_display_(/*71.9*/for(campaign_List<-campaignListPSF) yield /*71.44*/{_display_(Seq[Any](format.raw/*71.45*/("""
		                                """),format.raw/*72.35*/("""<option value=""""),_display_(/*72.51*/campaign_List/*72.64*/.getId()),format.raw/*72.72*/("""">"""),_display_(/*72.75*/campaign_List/*72.88*/.getCampaignName()),format.raw/*72.106*/("""</option>
		                             """)))}),format.raw/*73.33*/("""
		                                
						"""),format.raw/*75.7*/("""</select>
					</div>
					
					<div class="col-md-2" id="fromDateDiv">
					<label>From Assign Date : </label>
					<input type="text" class="filter form-control datepickerFilter" data-column-index="1" id="fromduedaterange" name="fromduedaterange" readonly>
				  </div>
				  <div class="col-md-2" id="toDateDiv">
					<label>To Assign Date : </label>
					<input type="text" class="filter form-control datepickerFilter" data-column-index="2" id="toduedaterange" name="toduedaterange" readonly>
				  </div>
				  </div>
				  <div class="row">
				   
						<div class="col-md-2" id="serviceTypeDiv" style="display:none;">
					
			
						<label>Select PSF Day</label>
						<select class="filter form-control" id="ServiceTypeName" data-column-index="3" name="ServiceTypeName">
							<option value="0" >--Select--</option>
							<option value="psf1stday">PSF 1st Day</option>
							<option value="psf3rdday">PSF 3rd Day</option>
							<option value="psf4thday">PSF 4th Day</option>
							<option value="psf6thday">PSF 6th Day</option>
							<option value="psf15thday">PSF 15th Day</option>
							<option value="psf30thday">PSF 30th Day</option>
							
			                                
						</select>
					</div>
					 
					<div class="col-md-3" id="serviceBookTypeDiv" style="display:none">						
					<label>Select Booked service type</label>
						<select class="filter form-control" id="serviceBookedType" data-column-index="4" name="serviceBookedType">
							<option value="0" >--Select--</option>
	
					                                
						</select>
					</div>
					<div class="col-md-3" id="lastDispoTypeDiv" style="display:none">						
					<label>Last Disposition</label>
						<select class="filter form-control" id="lastDispo" data-column-index="5" >
							<option value="0" >--Select--</option>
								"""),_display_(/*118.10*/for(dispo <- dispoList) yield /*118.33*/{_display_(Seq[Any](format.raw/*118.34*/("""
			"""),format.raw/*119.4*/("""<option value=""""),_display_(/*119.20*/dispo/*119.25*/.getDisposition()),format.raw/*119.42*/("""">"""),_display_(/*119.45*/dispo/*119.50*/.getDisposition()),format.raw/*119.67*/("""</option>
			
		""")))}),format.raw/*121.4*/("""
							
					                                
						"""),format.raw/*124.7*/("""</select>
					</div>
					<div class="col-md-3" id="droppedcountDiv" style="display:none">						
					<label>Dropped Count</label>
						<select class="filter form-control" id="droppedCount" data-column-index="6">
							<option value="0" >--Select--</option>
							<option value="1" >1</option>
							<option value="2" >2</option>
							<option value="3" >3</option>
							<option value="4" >4</option>
							
					                                
						</select>
					</div>
				</div>
                
                    <div class="tab-pane fade in active" id="home">
                        <div class="panel-body inf-content">              

                            <div class="dataTable_wrapper">                            
                                <div >
                                    <table class="table table-striped table-bordered table-hover" id="PSFassignedInteractionTableMR">
                                        <thead>
                                            <tr> 

                                            
				    		<th>Campaign Name</th>
				    		<th>CRE Name</th>
				    		<th>Customer Name</th>
				    		<th>Mobile Number</th>
				    		<th>Vehicle Reg No</th>
				    		<th>Model</th>
				    		<th>RONumber</th>
				    		<th>RODate</th>
				    		<th>Bill Date</th>
				    		<th>Category</th>
                                            </tr>
                                        </thead>
                                        <tbody>								
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile">
                        <div class="panel-body inf-content">
                            <div class="dataTable_wrapper">                            
                                <div >
                                    <table class="table table-striped table-bordered table-hover" id="PSFfollowUpRequiredServerDataMR4" >
                                        <thead>
                                         <tr> 
                                          		<th>Campaign Name</th>
                                          		<th>CRE Name</th>
                                                <th>Customer Name</th>
                                                <th>Mobile Number</th>                                                                   
                                                <th>Vehicle RegNo.</th>
                                                <th>FollowUp Date</th>                                    
                                                <th>FollowUp Time</th>
                                                <th>Call date</th>
                                                <th>RO Number</th>
                                                <th>RO Date</th>
                                                <th>Bill Date</th>
                                                <th>Survey Date</th>
                                                <th>Model</th>                                                                                               
                                                
                                            </tr>   
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="messages">
                        <div class="panel-body inf-content">
                            <div class="dataTable_wrapper">                            
                                <div >
                                    <table class="table table-striped table-bordered table-hover" id="PSFfollowUpRequiredServerDataMR22">
                                        <thead>
                                            <tr> 
                                            	<th>Campaign Name</th>
                                          		<th>CRE Name</th>
                                                <th>Customer Name</th>
                                                <th>Mobile Number</th>                                                                   
                                                <th>Vehicle RegNo.</th>
                                                <th>Call date</th>
                                                <th>RO Number</th>
                                                <th>RO Date</th>
                                                <th>Bill Date</th>
                                                <th>Survey Date</th>
                                                <th>Model</th> 
                                                                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="settings">
                         <div class="panel-body inf-content">
                        <div class="dataTable_wrapper">                            
                            <div >
                                <table class="table table-striped table-bordered table-hover" id="PSFfollowUpRequiredServerDataMR25">
                                    <thead>
                                        <tr> 
                                        	<th>Campaign Name</th>
                                          		<th>CRE Name</th>
                                                <th>Customer Name</th>
                                                <th>Mobile Number</th>                                                                   
                                                <th>Vehicle RegNo.</th>
                                                <th>Call date</th>
                                                <th>RO Number</th>
                                                <th>RO Date</th>
                                                <th>Bill Date</th>
                                                <th>Survey Date</th>
                                                <th>Model</th>
                                                <th>PSF Appointment Date</th>
                                                <th>PSF Appointment Time</th>
                                                  
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    </div><!--End tab panel-->
                    <div class="tab-pane fade " id="nonContacts">
                        <div class="panel-body inf-content"> 
                            <div >
                                <table class="table table-striped table-bordered table-hover" id="PSFnonContactsServerDataMR1">
                                    <thead>
                                        <tr> 
                                        	<th>Campaign Name</th>
                                       	 	<th>CRE Name</th>                         
                                            <th>Customer Name</th>
                                            <th>Mobile Number</th>         
                                            <th>Vehicle RegNo.</th>
                                            <th>Call date</th>
                                                <th>RO Number</th>
                                                <th>Bill Date</th>
                                                <th>Survey Date</th>
                                                <th>Model</th>
                                                <th>Last Disposition</th>
                                                 											
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="droppedBuc">
                        <div class="panel-body inf-content">
                            <div class="dataTable_wrapper">                            
                                <div style="overflow-x: auto">
                                    <table class="table table-striped table-bordered table-hover" id="PSFnonContactsServerDataMR2">
                                        <thead>
                                                 <tr> 
                                        	<th>Campaign Name</th>
                                       	 	<th>CRE Name</th>                         
                                            <th>Customer Name</th>
                                            <th>Mobile Number</th>         
                                            <th>Vehicle RegNo.</th>                                      
                                                <th>Call date</th>
                                                <th>RO Number</th>
                                                <th>Bill Date</th>
                                                <th>Survey Date</th>
                                                <th>Model</th>
                                                <th>Last Disposition</th>
                                                 											
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
				</div><!--End tab content-->
            </div><!--End panel body-->
        </div>
   
</div>
""")))}),format.raw/*316.2*/(""" 

"""),format.raw/*318.1*/("""<script type="text/javascript"> 

function PSFassignedInteractionDataMR()"""),format.raw/*320.40*/("""{"""),format.raw/*320.41*/("""
    """),format.raw/*321.5*/("""// alert("server side data load");
    document.getElementById("cresDiv").style.display = "block";
	//document.getElementById("workshopDiv").style.display = "block";
	//document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("fromDateDiv").style.display = "block";
	document.getElementById("toDateDiv").style.display = "block";
	document.getElementById("campaignDiv").style.display = "block";
    //document.getElementById("serviceBookTypeDiv").style.display = "none";
    //document.getElementById("serviceTypeDiv").style.display = "block";

	//document.getElementById("lastDispoTypeDiv").style.display = "none";
	//document.getElementById("droppedcountDiv").style.display = "none";

    var UserIds="",i;
    myOption = document.getElementById('ddlCreIds');
    
for (i=0;i<myOption.options.length;i++)"""),format.raw/*337.40*/("""{"""),format.raw/*337.41*/("""
    """),format.raw/*338.5*/("""if(myOption.options[i].selected)"""),format.raw/*338.37*/("""{"""),format.raw/*338.38*/("""
        """),format.raw/*339.9*/("""if(myOption.options[i].value != "--Select--")"""),format.raw/*339.54*/("""{"""),format.raw/*339.55*/("""
    	 """),format.raw/*340.7*/("""UserIds = UserIds + myOption.options[i].value + ",";
    	 console.log("Users id"+UserIds);
        """),format.raw/*342.9*/("""}"""),format.raw/*342.10*/("""
    """),format.raw/*343.5*/("""}"""),format.raw/*343.6*/("""
"""),format.raw/*344.1*/("""}"""),format.raw/*344.2*/("""
"""),format.raw/*345.1*/("""if(UserIds.length > 0)"""),format.raw/*345.23*/("""{"""),format.raw/*345.24*/("""
    	"""),format.raw/*346.6*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
"""),format.raw/*347.1*/("""}"""),format.raw/*347.2*/("""
"""),format.raw/*348.1*/("""else"""),format.raw/*348.5*/("""{"""),format.raw/*348.6*/("""
"""),format.raw/*349.1*/("""UserIds="Select";
"""),format.raw/*350.1*/("""}"""),format.raw/*350.2*/("""
    """),format.raw/*351.5*/("""var ajaxUrl = "/CREManager/PSFassignedInteractionTableDataMR/"+UserIds+"";
    
    
    var table= $('#PSFassignedInteractionTableMR').dataTable( """),format.raw/*354.63*/("""{"""),format.raw/*354.64*/("""
        """),format.raw/*355.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
		 "scrollX": true,
        "paging": true,
        "searching":true,
        "ordering":false,	
        "ajax": ajaxUrl,
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*364.59*/("""{"""),format.raw/*364.60*/("""
              """),format.raw/*365.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*367.15*/("""}"""),format.raw/*367.16*/("""
    """),format.raw/*368.5*/("""}"""),format.raw/*368.6*/(""" """),format.raw/*368.7*/(""");

    FilterOptionDataMR(table);
    
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e)"""),format.raw/*372.61*/("""{"""),format.raw/*372.62*/("""
      """),format.raw/*373.7*/("""$($.fn.dataTable.tables(true)).DataTable()
         .columns.adjust();
   """),format.raw/*375.4*/("""}"""),format.raw/*375.5*/(""");
    
    """),format.raw/*377.5*/("""}"""),format.raw/*377.6*/("""




"""),format.raw/*382.1*/("""function ajaxPSFCallForFollowUpRequiredServerMR(buckettype)"""),format.raw/*382.60*/("""{"""),format.raw/*382.61*/("""
	"""),format.raw/*383.2*/("""document.getElementById("cresDiv").style.display = "block";
	//document.getElementById("workshopDiv").style.display = "block";
	//document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("campaignDiv").style.display = "block";
	//document.getElementById("serviceBookTypeDiv").style.display = "none";
	//document.getElementById("serviceTypeDiv").style.display = "block";
	document.getElementById("fromDateDiv").style.display = "block";
	document.getElementById("toDateDiv").style.display = "block";
	document.getElementById("lastDispoTypeDiv").style.display = "block";
	//document.getElementById("droppedcountDiv").style.display = "none";
	          

	    var UserIds="",i;
	    myOption = document.getElementById('ddlCreIds');
	    
	for (i=0;i<myOption.options.length;i++)"""),format.raw/*398.41*/("""{"""),format.raw/*398.42*/("""
	    """),format.raw/*399.6*/("""if(myOption.options[i].selected)"""),format.raw/*399.38*/("""{"""),format.raw/*399.39*/("""
	        """),format.raw/*400.10*/("""if(myOption.options[i].value != "--Select--")"""),format.raw/*400.55*/("""{"""),format.raw/*400.56*/("""
	    	 """),format.raw/*401.8*/("""UserIds = UserIds + myOption.options[i].value + ",";
	        """),format.raw/*402.10*/("""}"""),format.raw/*402.11*/("""
	    """),format.raw/*403.6*/("""}"""),format.raw/*403.7*/("""
	"""),format.raw/*404.2*/("""}"""),format.raw/*404.3*/("""
	"""),format.raw/*405.2*/("""if(UserIds.length > 0)"""),format.raw/*405.24*/("""{"""),format.raw/*405.25*/("""
	    	"""),format.raw/*406.7*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*407.2*/("""}"""),format.raw/*407.3*/("""
	"""),format.raw/*408.2*/("""else"""),format.raw/*408.6*/("""{"""),format.raw/*408.7*/("""
	"""),format.raw/*409.2*/("""UserIds="Select";
	"""),format.raw/*410.2*/("""}"""),format.raw/*410.3*/("""
	    """),format.raw/*411.6*/("""var ajaxUrl = "/CREManager/PSFfollowUpCallLogTableDataMR/"+UserIds+"/"+buckettype+"";
	    
	    
	    var table= $('#PSFfollowUpRequiredServerDataMR'+buckettype).dataTable( """),format.raw/*414.77*/("""{"""),format.raw/*414.78*/("""
	        """),format.raw/*415.10*/(""""bDestroy": true,
	        "processing": true,
	        "serverSide": true,
	        "scrollY": 300,
			 "scrollX": true,
	        "paging": true,
	        "searching":true,
	        "ordering":false,	
	        "ajax": ajaxUrl,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*424.60*/("""{"""),format.raw/*424.61*/("""
              """),format.raw/*425.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*427.15*/("""}"""),format.raw/*427.16*/("""
	    """),format.raw/*428.6*/("""}"""),format.raw/*428.7*/(""" """),format.raw/*428.8*/(""");

	    FilterOptionDataMR(table);
	    
	"""),format.raw/*432.2*/("""}"""),format.raw/*432.3*/("""



"""),format.raw/*436.1*/("""function ajaxPSFCallForNonContactsServerMR(buckettype)"""),format.raw/*436.55*/("""{"""),format.raw/*436.56*/("""
	"""),format.raw/*437.2*/("""//alert("noncontact");
	
		document.getElementById("cresDiv").style.display = "block";
		//document.getElementById("workshopDiv").style.display = "block";
		//document.getElementById("cityDiv").style.display = "block";	
		document.getElementById("campaignDiv").style.display = "block";
		//document.getElementById("serviceBookTypeDiv").style.display = "none";
		//document.getElementById("serviceTypeDiv").style.display = "none";
		document.getElementById("fromDateDiv").style.display = "block";
		document.getElementById("toDateDiv").style.display = "block";
		document.getElementById("lastDispoTypeDiv").style.display = "block";
		//document.getElementById("droppedcountDiv").style.display = "block";

	var UserIds="",i;
    myOption = document.getElementById('ddlCreIds');
    
for (i=0;i<myOption.options.length;i++)"""),format.raw/*453.40*/("""{"""),format.raw/*453.41*/("""
    """),format.raw/*454.5*/("""if(myOption.options[i].selected)"""),format.raw/*454.37*/("""{"""),format.raw/*454.38*/("""
        """),format.raw/*455.9*/("""if(myOption.options[i].value != "Select")"""),format.raw/*455.50*/("""{"""),format.raw/*455.51*/("""
    		"""),format.raw/*456.7*/("""UserIds = UserIds + myOption.options[i].value + ",";
        """),format.raw/*457.9*/("""}"""),format.raw/*457.10*/("""
    """),format.raw/*458.5*/("""}"""),format.raw/*458.6*/("""
"""),format.raw/*459.1*/("""}"""),format.raw/*459.2*/("""
	"""),format.raw/*460.2*/("""if(UserIds.length > 0)"""),format.raw/*460.24*/("""{"""),format.raw/*460.25*/("""
    	"""),format.raw/*461.6*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*462.2*/("""}"""),format.raw/*462.3*/("""
	"""),format.raw/*463.2*/("""else"""),format.raw/*463.6*/("""{"""),format.raw/*463.7*/("""
		"""),format.raw/*464.3*/("""UserIds="Select";
	"""),format.raw/*465.2*/("""}"""),format.raw/*465.3*/("""
    """),format.raw/*466.5*/("""var ajaxUrl = "/CREManager/PSFnonContactsServerDataTableMR/"+UserIds+"/"+buckettype+"";
     
    var table = $('#PSFnonContactsServerDataMR'+buckettype).dataTable( """),format.raw/*468.72*/("""{"""),format.raw/*468.73*/("""
        """),format.raw/*469.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
		 "scrollX": true,
        "paging": true,
        "searching":true,
        "ordering":false,			
        "ajax": ajaxUrl,
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*478.59*/("""{"""),format.raw/*478.60*/("""
              """),format.raw/*479.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*481.15*/("""}"""),format.raw/*481.16*/("""
    """),format.raw/*482.5*/("""}"""),format.raw/*482.6*/(""" """),format.raw/*482.7*/(""");

    FilterOptionDataMR(table);
    
    """),format.raw/*486.5*/("""}"""),format.raw/*486.6*/("""






"""),format.raw/*493.1*/("""</script>
<script type="text/javascript">  
function FilterOptionDataMR(table)"""),format.raw/*495.35*/("""{"""),format.raw/*495.36*/("""
	"""),format.raw/*496.2*/("""var values = [];
	$('.filter').on('change', function() """),format.raw/*497.39*/("""{"""),format.raw/*497.40*/("""
    	"""),format.raw/*498.6*/("""console.log("inside filteroptionsdata data");
	    var i= $(this).data('columnIndex');
	    var v = $(this).val();
	    console.log("i : "+i);
		console.log("v length "+v.length);
	    
	    if(v.length>0)"""),format.raw/*504.20*/("""{"""),format.raw/*504.21*/("""
		    
		    """),format.raw/*506.7*/("""if(i == 7)"""),format.raw/*506.17*/("""{"""),format.raw/*506.18*/("""	

		    	"""),format.raw/*508.8*/("""var UserIds="",j;
		        myOption = document.getElementById('ddlCreIds');
		        console.log("here"+myOption);		        
		    for (j=0;j<myOption.options.length;j++)"""),format.raw/*511.46*/("""{"""),format.raw/*511.47*/("""
		        """),format.raw/*512.11*/("""if(myOption.options[j].selected)"""),format.raw/*512.43*/("""{"""),format.raw/*512.44*/("""
		            """),format.raw/*513.15*/("""if(myOption.options[j].value != "Select")"""),format.raw/*513.56*/("""{"""),format.raw/*513.57*/("""
		        		"""),format.raw/*514.13*/("""UserIds = UserIds + myOption.options[j].value + ",";
		            """),format.raw/*515.15*/("""}"""),format.raw/*515.16*/("""
		        """),format.raw/*516.11*/("""}"""),format.raw/*516.12*/("""
		    """),format.raw/*517.7*/("""}"""),format.raw/*517.8*/("""
		    	"""),format.raw/*518.8*/("""if(UserIds.length > 0)"""),format.raw/*518.30*/("""{"""),format.raw/*518.31*/("""
		        	"""),format.raw/*519.12*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
		        	values[7]=UserIds;
		    	"""),format.raw/*521.8*/("""}"""),format.raw/*521.9*/("""else"""),format.raw/*521.13*/("""{"""),format.raw/*521.14*/("""
		    		"""),format.raw/*522.9*/("""UserIds="Select";
		    		values[7]=UserIds;
		    	"""),format.raw/*524.8*/("""}"""),format.raw/*524.9*/("""
		    
		    """),format.raw/*526.7*/("""}"""),format.raw/*526.8*/("""else"""),format.raw/*526.12*/("""{"""),format.raw/*526.13*/("""
		    	"""),format.raw/*527.8*/("""values[i] = v;
			    """),format.raw/*528.8*/("""}"""),format.raw/*528.9*/("""
	
			       
		  
		    """),format.raw/*532.7*/("""}"""),format.raw/*532.8*/("""		    
	    
	    	"""),format.raw/*534.7*/("""console.log(JSON.stringify(values));	   	
	  	  //clear global search values
	 	  //table.api().search('');
	 	  $(this).data('columnIndex');
	 	 //var table = $('#nonContactsServerDataMR').dataTable();
	 	 if(values.length>0) """),format.raw/*539.25*/("""{"""),format.raw/*539.26*/("""
			 """),format.raw/*540.5*/("""for(var i=0;i<values.length;i++)"""),format.raw/*540.37*/("""{"""),format.raw/*540.38*/("""
				 """),format.raw/*541.6*/("""console.log("values[i] : "+values[i]);	
			    table.api().columns(i).search(values[i]);
			 """),format.raw/*543.5*/("""}"""),format.raw/*543.6*/("""
			 """),format.raw/*544.5*/("""table.api().draw(); 
		"""),format.raw/*545.3*/("""}"""),format.raw/*545.4*/("""
	 	
		 	  
	"""),format.raw/*548.2*/("""}"""),format.raw/*548.3*/(""");
	
	
"""),format.raw/*551.1*/("""}"""),format.raw/*551.2*/("""
"""),format.raw/*552.1*/("""</script>

 <script type="text/javascript">
	window.onload = PSFassignedInteractionDataMR();
</script>
    """))
      }
    }
  }

  def render(listCity:List[Location],dealerName:String,user:String,allCREHash:Map[Long, String],campaignListPSF:List[Campaign],serviceTypeList:List[ServiceTypes],dispoList:List[CallDispositionData]): play.twirl.api.HtmlFormat.Appendable = apply(listCity,dealerName,user,allCREHash,campaignListPSF,serviceTypeList,dispoList)

  def f:((List[Location],String,String,Map[Long, String],List[Campaign],List[ServiceTypes],List[CallDispositionData]) => play.twirl.api.HtmlFormat.Appendable) = (listCity,dealerName,user,allCREHash,campaignListPSF,serviceTypeList,dispoList) => apply(listCity,dealerName,user,allCREHash,campaignListPSF,serviceTypeList,dispoList)

  def ref: this.type = this

}


}

/**/
object PSFCallLogPageCREManager extends PSFCallLogPageCREManager_Scope0.PSFCallLogPageCREManager
              /*
                  -- GENERATED --
                  DATE: Fri Mar 16 17:48:05 IST 2018
                  SOURCE: D:/CRMFORDAUTOSHERPA/crmford/app/views/PSFCallLogPageCREManager.scala.html
                  HASH: 01f7e46343aa6a518e369bc80c7357f7df5846ae
                  MATRIX: 883->1|1166->188|1193->190|1251->240|1290->242|1321->247|1367->266|1395->267|1430->276|1550->369|1578->370|1606->371|3184->1923|3221->1944|3260->1945|3353->2010|3396->2026|3409->2030|3440->2040|3470->2043|3483->2047|3514->2057|3576->2088|3621->2105|4690->3148|4741->3183|4780->3184|4843->3219|4886->3235|4908->3248|4937->3256|4967->3259|4989->3272|5029->3290|5102->3332|5171->3374|7037->5212|7077->5235|7117->5236|7149->5240|7193->5256|7208->5261|7247->5278|7278->5281|7293->5286|7332->5303|7380->5320|7461->5373|18188->16069|18219->16072|18321->16145|18351->16146|18384->16151|19247->16985|19277->16986|19310->16991|19371->17023|19401->17024|19438->17033|19512->17078|19542->17079|19577->17086|19705->17186|19735->17187|19768->17192|19797->17193|19826->17194|19855->17195|19884->17196|19935->17218|19965->17219|19999->17225|20079->17277|20108->17278|20137->17279|20169->17283|20198->17284|20227->17285|20273->17303|20302->17304|20335->17309|20511->17456|20541->17457|20578->17466|20885->17744|20915->17745|20959->17760|21064->17836|21094->17837|21127->17842|21156->17843|21185->17844|21314->17944|21344->17945|21379->17952|21481->18026|21510->18027|21550->18039|21579->18040|21612->18045|21700->18104|21730->18105|21760->18107|22592->18910|22622->18911|22656->18917|22717->18949|22747->18950|22786->18960|22860->19005|22890->19006|22926->19014|23017->19076|23047->19077|23081->19083|23110->19084|23140->19086|23169->19087|23199->19089|23250->19111|23280->19112|23315->19119|23396->19172|23425->19173|23455->19175|23487->19179|23516->19180|23546->19182|23593->19201|23622->19202|23656->19208|23859->19382|23889->19383|23928->19393|24244->19680|24274->19681|24318->19696|24423->19772|24453->19773|24487->19779|24516->19780|24545->19781|24616->19824|24645->19825|24677->19829|24760->19883|24790->19884|24820->19886|25669->20706|25699->20707|25732->20712|25793->20744|25823->20745|25860->20754|25930->20795|25960->20796|25995->20803|26084->20864|26114->20865|26147->20870|26176->20871|26205->20872|26234->20873|26264->20875|26315->20897|26345->20898|26379->20904|26460->20957|26489->20958|26519->20960|26551->20964|26580->20965|26611->20968|26658->20987|26687->20988|26720->20993|26914->21158|26944->21159|26981->21168|27290->21448|27320->21449|27364->21464|27469->21540|27499->21541|27532->21546|27561->21547|27590->21548|27662->21592|27691->21593|27726->21600|27833->21678|27863->21679|27893->21681|27977->21736|28007->21737|28041->21743|28275->21948|28305->21949|28347->21963|28386->21973|28416->21974|28454->21984|28655->22156|28685->22157|28725->22168|28786->22200|28816->22201|28860->22216|28930->22257|28960->22258|29002->22271|29098->22338|29128->22339|29168->22350|29198->22351|29233->22358|29262->22359|29298->22367|29349->22389|29379->22390|29420->22402|29537->22491|29566->22492|29599->22496|29629->22497|29666->22506|29746->22558|29775->22559|29817->22573|29846->22574|29879->22578|29909->22579|29945->22587|29995->22609|30024->22610|30077->22635|30106->22636|30153->22655|30409->22882|30439->22883|30472->22888|30533->22920|30563->22921|30597->22927|30718->23020|30747->23021|30780->23026|30831->23049|30860->23050|30901->23063|30930->23064|30965->23071|30994->23072|31023->23073
                  LINES: 27->1|32->1|33->2|33->2|33->2|37->6|38->7|38->7|39->8|42->11|42->11|43->12|69->38|69->38|69->38|70->39|70->39|70->39|70->39|70->39|70->39|70->39|71->40|72->41|102->71|102->71|102->71|103->72|103->72|103->72|103->72|103->72|103->72|103->72|104->73|106->75|149->118|149->118|149->118|150->119|150->119|150->119|150->119|150->119|150->119|150->119|152->121|155->124|347->316|349->318|351->320|351->320|352->321|368->337|368->337|369->338|369->338|369->338|370->339|370->339|370->339|371->340|373->342|373->342|374->343|374->343|375->344|375->344|376->345|376->345|376->345|377->346|378->347|378->347|379->348|379->348|379->348|380->349|381->350|381->350|382->351|385->354|385->354|386->355|395->364|395->364|396->365|398->367|398->367|399->368|399->368|399->368|403->372|403->372|404->373|406->375|406->375|408->377|408->377|413->382|413->382|413->382|414->383|429->398|429->398|430->399|430->399|430->399|431->400|431->400|431->400|432->401|433->402|433->402|434->403|434->403|435->404|435->404|436->405|436->405|436->405|437->406|438->407|438->407|439->408|439->408|439->408|440->409|441->410|441->410|442->411|445->414|445->414|446->415|455->424|455->424|456->425|458->427|458->427|459->428|459->428|459->428|463->432|463->432|467->436|467->436|467->436|468->437|484->453|484->453|485->454|485->454|485->454|486->455|486->455|486->455|487->456|488->457|488->457|489->458|489->458|490->459|490->459|491->460|491->460|491->460|492->461|493->462|493->462|494->463|494->463|494->463|495->464|496->465|496->465|497->466|499->468|499->468|500->469|509->478|509->478|510->479|512->481|512->481|513->482|513->482|513->482|517->486|517->486|524->493|526->495|526->495|527->496|528->497|528->497|529->498|535->504|535->504|537->506|537->506|537->506|539->508|542->511|542->511|543->512|543->512|543->512|544->513|544->513|544->513|545->514|546->515|546->515|547->516|547->516|548->517|548->517|549->518|549->518|549->518|550->519|552->521|552->521|552->521|552->521|553->522|555->524|555->524|557->526|557->526|557->526|557->526|558->527|559->528|559->528|563->532|563->532|565->534|570->539|570->539|571->540|571->540|571->540|572->541|574->543|574->543|575->544|576->545|576->545|579->548|579->548|582->551|582->551|583->552
                  -- GENERATED --
              */
          