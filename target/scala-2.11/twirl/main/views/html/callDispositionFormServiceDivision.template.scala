
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object callDispositionFormServiceDivision_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class callDispositionFormServiceDivision extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template13[String,String,String,String,String,String,String,String,String,List[Campaign],List[ServiceTypes],List[String],List[CallDispositionData],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(toActivateTab1:String,toActivateTab2:String,toActivateTab3:String,toActivateTab4:String,toActivateTab5:String,dealercode:String,dealerName:String,userName:String,oem:String,campaignList :List[Campaign],serviceTypeList :List[ServiceTypes],bookedServiceTypeList:List[String],dispoList:List[CallDispositionData]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.312*/("""
"""),_display_(/*2.2*/mainPageCRE("AutoSherpaCRM",userName,dealerName,dealercode,oem)/*2.65*/ {_display_(Seq[Any](format.raw/*2.67*/("""

"""),format.raw/*4.1*/("""<input type="hidden" id="user" value=""""),_display_(/*4.40*/userName),format.raw/*4.48*/("""">

<input type="hidden" id="toActivateTab1" value=""""),_display_(/*6.50*/toActivateTab1),format.raw/*6.64*/("""">
<input type="hidden" id="toActivateTab2" value=""""),_display_(/*7.50*/toActivateTab2),format.raw/*7.64*/("""">
<input type="hidden" id="toActivateTab3" value=""""),_display_(/*8.50*/toActivateTab3),format.raw/*8.64*/("""">
<input type="hidden" id="toActivateTab4" value=""""),_display_(/*9.50*/toActivateTab4),format.raw/*9.64*/("""">
<input type="hidden" id="toActivateTab5" value=""""),_display_(/*10.50*/toActivateTab5),format.raw/*10.64*/("""">

<style>
    td, th """),format.raw/*13.12*/("""{"""),format.raw/*13.13*/("""
                
        """),format.raw/*15.9*/("""border: 1px solid black;
        overflow: hidden; 
        text-align:center!important;
    """),format.raw/*18.5*/("""}"""),format.raw/*18.6*/("""
"""),format.raw/*19.1*/("""</style>

<div class="row" >

        <div class="col-sm-2"> 
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><div id="FreshCallsCountCRE"><i class="fa fa-spinner fa-pulse fa-fw"></i></div></h3>
              <p>Fresh Calls</p>
            </div>
            <div class="icon"> <i class="ion-android-call"></i> </div>
             
            <!--<span class="pull-left" data-toggle="modal" data-target=".myModal1">View Details</span>--> 
          </div>
        </div>      <!-- ./col -->
        <div class="col-sm-2 col-half-offset"> 
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><div id="followupcountCRE"><i class="fa fa-spinner fa-pulse fa-fw"></i></div></h3>
              <p>Pending Follow Ups</p>
            </div>
            <div class="icon"> <i class="ion-android-checkbox-outline"></i> </div>
            </div>
        </div>        <!-- ./col -->
        <div class="col-sm-2 col-half-offset"> 
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><div id="nonContactsCRE"><i class="fa fa-spinner fa-pulse fa-fw"></i></div></h3>
              <p>Non Contacts</p>
            </div>
            <div class="icon"> <i class="ion-person-add"></i> </div>	
             </div>
        </div>        <!-- ./col -->
        <div class="col-sm-2 col-half-offset"> 
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><div id="overDueCountCRE"><i class="fa fa-spinner fa-pulse fa-fw"></i></div></h3>
              <p>Over Due Bookings</p>
            </div>
            <div class="icon"> <i class="ion-android-download"></i> </div>
             </div>
        </div>
        <div class="col-sm-2 col-half-offset"> 
          <!-- small box -->
          <div class="small-box bg-yellow" style="height: 102px;">
             <div class="row"  style="margin-left:4px">
   <div class=" col-sm-12 col-xs-12">
                 
                 <h4><span id="creTotalBookingsperDay" style="font-size: 22px; font-weight: bold;"></span>&nbsp;&nbsp;&nbsp;&nbsp;Booking</h4>
  </div>
  </div>
  <p style="border-bottom:4px solid white;width:55px; margin-left:4px"></p>
 <div class="row" style="position:absolute;margin-top:-12px; margin-left:4px">
   <div class="col-sm-12 col-xs-12">
                 
                  <h4><span id="creTotalCallsperDay" style="font-size: 22px; font-weight: bold;"></span> &nbsp;&nbsp;&nbsp;&nbsp;Total Calls</h4>
</div></div>
 <div class="pull-left" style="margin-left: 100px; position: absolute; margin-top: 20px;" id="creCurrentdate">...</div>
 
           
          </div>
        </div>  
		     <!-- ./col --> 
    
</div>
<div class="row">



    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading"> <b>Call Log Information</b> </div>
            <div class="panel-body">
			<div class="panel panel-default">
                <ul class="nav nav-tabs">
                    <li><a href="#home" data-toggle="tab" id="Tab1" onclick="assignedInteractionData();">Scheduled</a> </li>
                    <li><a href="#profile" data-toggle="tab" id="Tab2" onclick="ajaxCallForFollowUpRequiredServer();">Follow Up Required</a> </li>
                    <li><a href="#messages" data-toggle="tab" id="Tab3" onclick="ajaxCallForServiceBookedServer();">Service Booked</a> </li>
                    <li><a href="#settings" data-toggle="tab" id="Tab4" onclick="ajaxCallForServiceNotRequiredServer();" >Service Not Required</a> </li>
                    <li><a href="#nonContacts" data-toggle="tab" id="Tab5" onclick="ajaxCallForNonContactsServer();">Non Contacts</a> </li>
                    <li><a href="#droppedBuc" data-toggle="tab" id="Tab6" onclick="ajaxCallForDroppedCallsServer();">Dropped</a> </li>
                </ul>
                <div class="tab-content">
				<div class="panel panel-default" style="border-top: #fff;">
				<div class="panel-body">
                 <div class="row">
                <div class="col-md-3 tab-pane" id="campaignDiv">
                                
                                <label>Select Campaign</label>
						<select class="filter form-control" id="campaignName" data-column-index="0" name="campaignName">
							<option value="0" >--Select--</option>
							"""),_display_(/*114.9*/for(campaign_List<-campaignList) yield /*114.41*/{_display_(Seq[Any](format.raw/*114.42*/("""
		                                """),format.raw/*115.35*/("""<option value=""""),_display_(/*115.51*/campaign_List/*115.64*/.getCampaignName()),format.raw/*115.82*/("""">"""),_display_(/*115.85*/campaign_List/*115.98*/.getCampaignName()),format.raw/*115.116*/("""</option>
		                             """)))}),format.raw/*116.33*/("""
		                                
						"""),format.raw/*118.7*/("""</select>
					</div>
					
					<div class="col-md-3" id="fromDateDiv">
					<label>From Date : </label>
					<input type="text" class="filter form-control datepickerFilter" data-column-index="1" placeholder="Enter From Date" id="fromduedaterange" name="fromduedaterange" readonly>
				  </div>
				   <div class="col-md-3" id="toDateDiv">
					<label>To Date : </label>
					<input type="text" class="filter form-control datepickerFilter" data-column-index="2" placeholder="Enter To Date" id="toduedaterange" name="toduedaterange" readonly>
				  </div>
						<div class="col-md-3" id="serviceTypeDiv">
					
			
						<label>Select service type</label>
						<select class="filter form-control" id="ServiceTypeName" data-column-index="3" name="ServiceTypeName">
							<option value="0" >--Select--</option>
							"""),_display_(/*135.9*/for(serviceType_List<-serviceTypeList) yield /*135.47*/{_display_(Seq[Any](format.raw/*135.48*/("""
			                                """),format.raw/*136.36*/("""<option value=""""),_display_(/*136.52*/serviceType_List/*136.68*/.getServiceTypeName()),format.raw/*136.89*/("""">"""),_display_(/*136.92*/serviceType_List/*136.108*/.getServiceTypeName()),format.raw/*136.129*/("""</option>
			                             """)))}),format.raw/*137.34*/("""
			                                
						"""),format.raw/*139.7*/("""</select>
					</div>
					<div class="col-md-3" id="serviceBookTypeDiv" style="display:none">						
					<label>Select Booked service type</label>
						<select class="filter form-control" id="serviceBookedType" data-column-index="4" name="serviceBookedType">
							<option value="0" >--Select--</option>
							"""),_display_(/*145.9*/for(bookedServiceType_List<-bookedServiceTypeList) yield /*145.59*/{_display_(Seq[Any](format.raw/*145.60*/("""
 """),format.raw/*146.2*/("""<option value=""""),_display_(/*146.18*/bookedServiceType_List),format.raw/*146.40*/("""">"""),_display_(/*146.43*/bookedServiceType_List),format.raw/*146.65*/("""</option>					                   
           """)))}),format.raw/*147.13*/("""
					                                
						"""),format.raw/*149.7*/("""</select>
					</div>
					<div class="col-md-3" id="lastDispoTypeDiv" style="display:none">						
					<label>Last Disposition</label>
						<select class="filter form-control" id="lastDispo" data-column-index="5" >
							<option value="0" >--Select--</option>
								"""),_display_(/*155.10*/for(dispo <- dispoList) yield /*155.33*/{_display_(Seq[Any](format.raw/*155.34*/("""
			"""),format.raw/*156.4*/("""<option value=""""),_display_(/*156.20*/dispo/*156.25*/.getDisposition()),format.raw/*156.42*/("""">"""),_display_(/*156.45*/dispo/*156.50*/.getDisposition()),format.raw/*156.67*/("""</option>
			
		""")))}),format.raw/*158.4*/("""
							
					                                
						"""),format.raw/*161.7*/("""</select>
					</div>
					<div class="col-md-3" id="droppedcountDiv" style="display:none">						
					<label>Dropped Count</label>
						<select class="filter form-control" id="droppedCount" data-column-index="6">
							<option value="0" >--Select--</option>
							<option value="1" >1</option>
							<option value="2" >2</option>
							<option value="3" >3</option>
							<option value="4" >4</option>
							
					                                
						</select>
					</div>
				</div>
				</div>
				</div>
                                
                            
                    <div class="panel panel-default tab-pane fade """),_display_(/*180.68*/toActivateTab1),format.raw/*180.82*/("""" id="home">
                        <div class="panel-body inf-content">              

                            <div class="dataTable_wrapper">                            
                                <div style="overflow-x: auto">
                                
                                        <table class="table table-striped table-bordered table-hover" id="assignedInteractionTable" width="100%">
                                        <thead>
                                            <tr> 
											<th>Campaign</th>	
                                             <th>Customer&nbsp;Name</th>
                                                <th>RegNo.</th>
                                                <th>Model</th> 
                                                <th>Category</th>                                                
                                                <th>DueDate</th>
                                                <th>Type</th>                                                
                                                <th>PSFStatus</th>
                                                <th>DND</th>
                                                <th>Complaint</th>
						<th class="no-sort"></th>





                                            </tr>
                                        </thead>
                                        <tbody>								




                                        </tbody>

                                    </table>




                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="panel panel-default tab-pane fade """),_display_(/*225.68*/toActivateTab2),format.raw/*225.82*/("""" id="profile">
                        <div class="panel-body inf-content">

                            <div class="dataTable_wrapper">                            
                                
                                    <table class="table table-striped table-bordered table-hover" id="followUpRequiredServerData">
                                        <thead>
                                         <tr> 
                                                <th>Campaign</th> 
                                                <th>Call Date</th>
                                                <th>Customer Name</th>
                                                <th>Mobile Number</th>                                                                   
                                                <th>Vehicle RegNo.</th>
                                                <th>Service Due</th>
                                                <th>FollowUp Date</th>                                    
                                                <th>FollowUp Time</th>
                                                <th>Last Disposition</th>
                                                <th class="no-sort"></th>



                                            </tr>   
                                        </thead>
                                        <tbody>

                                        </tbody>

                                    </table>

                            </div>

                        </div>
                    </div>
                    <div class="panel panel-default tab-pane fade """),_display_(/*258.68*/toActivateTab3),format.raw/*258.82*/("""" id="messages">
                        <div class="panel-body inf-content">


                            <div class="dataTable_wrapper">                            
                                
                                    <table class="table table-striped table-bordered table-hover" id="serviceBookedServerData">
                                        <thead>
                                            <tr> 
												<th>Booking ID</th>												
                                                <th>Campaign</th> 
                                                <th>Call Date</th>                           	
                                                <th>Customer Name</th>
                                                <th>Mobile Number</th>         
                                                <th>Vehicle RegNo.</th>
                                                <th>Service Due</th>
                                                <th>Service ScheduledDate</th>  
                                                <th>Service ScheduledTime</th> 
                                                <th>Service Advisor</th>  
                                                <th>Booking Status</th>
                                                <th>Last Disposition</th>
                                                <th class="no-sort"></th>



                                            </tr>
                                        </thead>
                                        <tbody>




                                        </tbody>

                                    </table>


                            </div>

                        </div>
                    </div>
                    <div class="panel panel-default tab-pane fade """),_display_(/*299.68*/toActivateTab4),format.raw/*299.82*/("""" id="settings">
                         <div class="panel-body inf-content">

                        <div class="dataTable_wrapper">                            
                            <div style="overflow-x: auto">
                                <table class="table table-striped table-bordered table-hover" id="serviceNotRequiredServerData">
                                    <thead>
                                        <tr> 
                                            <th>Campaign</th>
                                            <th>Call Date</th>                           	
                                            <th>Customer Name</th>
                                            <th>Mobile Number</th>         
                                            <th>Vehicle RegNo.</th>
                                            <th>Service Due</th>
                                            <th>Last Disposition</th>
                                            <th>Reason</th>
                                            <th class="no-sort"></th>



                                        </tr>
                                    </thead>
                                    <tbody>




                                    </tbody>

                                </table>




                            </div>

                        </div>
                    </div>

                    </div><!--End tab panel-->
                    <div class="panel panel-default tab-pane fade """),_display_(/*339.68*/toActivateTab5),format.raw/*339.82*/("""" id="nonContacts">
                        
                        <div class="panel-body inf-content"> 
                           
                            <div style="overflow-x: auto">
                                <table class="table table-striped table-bordered table-hover" id="nonContactsServerData" width="100%">
                                    <thead>
                                        <tr> 
                                           
                                           <th>Campaign</th> 
                                           <th>Call Date</th>
                                            <th>Customer Name</th>
                                            <th>Mobile Number</th>         
                                            <th>Vehicle RegNo.</th>
                                            <th>Service Due</th>
                                            <th>Last Disposition</th>
                                            <th class="no-sort"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>


                    <div class="panel panel-default tab-pane fade" id="droppedBuc">
                        <div class="panel-body inf-content">


                            <div class="dataTable_wrapper">                            
                                <div style="overflow-x: auto">
                                    <table class="table table-striped table-bordered table-hover" id="droppedCallsServerData" width="100%">
                                        <thead>
                                            <tr> 
												   <th>Campaign</th>
												   <th>Call Date</th>
                                                <th>Customer Name</th>
                                                <th>Mobile Number</th>         
                                                <th>Vehicle RegNo.</th>
                                                <th>Service Due</th>
                                                <th>Last Disposition</th>





                                            </tr>
                                        </thead>
                                        <tbody>



                                        </tbody>

                                    </table>




                                </div>

                            </div>

                        </div>
                    </div>
                    </div>




                </div><!--End tab content-->
            </div><!--End panel body-->
        </div>
    </div>
</div>

""")))}),format.raw/*418.2*/("""
"""),format.raw/*419.1*/("""<script type="text/javascript">
    window.onload = ajaxRequestDataOFCREIndexPageWithTab;
</script>
<script>

    function getvalMain(sel) """),format.raw/*424.30*/("""{"""),format.raw/*424.31*/("""
        """),format.raw/*425.9*/("""var selVal = sel.value;
        var selValuess = 'Already Serviced';

        if (selVal == selValuess) """),format.raw/*428.35*/("""{"""),format.raw/*428.36*/("""
            """),format.raw/*429.13*/("""$('.show1').css('display', 'block');
        """),format.raw/*430.9*/("""}"""),format.raw/*430.10*/(""" """),format.raw/*430.11*/("""else """),format.raw/*430.16*/("""{"""),format.raw/*430.17*/("""
            """),format.raw/*431.13*/("""$('.show1').css('display', 'none');

        """),format.raw/*433.9*/("""}"""),format.raw/*433.10*/("""
    """),format.raw/*434.5*/("""}"""),format.raw/*434.6*/("""

    """),format.raw/*436.5*/("""function getval(sel) """),format.raw/*436.26*/("""{"""),format.raw/*436.27*/("""
        """),format.raw/*437.9*/("""var onSelVal = sel.value;
        var selValuesTab2 = 'Serviced At Other Dealer';

        if (onSelVal == selValuesTab2) """),format.raw/*440.40*/("""{"""),format.raw/*440.41*/("""
            """),format.raw/*441.13*/("""$('.show2').css('display', 'block');
        """),format.raw/*442.9*/("""}"""),format.raw/*442.10*/(""" """),format.raw/*442.11*/("""else """),format.raw/*442.16*/("""{"""),format.raw/*442.17*/("""
            """),format.raw/*443.13*/("""$('.show2').css('display', 'none');

        """),format.raw/*445.9*/("""}"""),format.raw/*445.10*/("""
    """),format.raw/*446.5*/("""}"""),format.raw/*446.6*/("""

"""),format.raw/*448.1*/("""</script>

<script type="text/javascript">
      
 function assignedInteractionData()"""),format.raw/*452.36*/("""{"""),format.raw/*452.37*/("""
    """),format.raw/*453.5*/("""// alert("server side data load");
    document.getElementById("campaignDiv").style.display = "block";
    document.getElementById("serviceBookTypeDiv").style.display = "none";
    document.getElementById("serviceTypeDiv").style.display = "block";
    document.getElementById("fromDateDiv").style.display = "block";
	document.getElementById("toDateDiv").style.display = "block";
	document.getElementById("lastDispoTypeDiv").style.display = "none";
	document.getElementById("droppedcountDiv").style.display = "none";
     
    var table= $('#assignedInteractionTable').dataTable( """),format.raw/*462.58*/("""{"""),format.raw/*462.59*/("""
        """),format.raw/*463.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
		"scrollX": true,
        "paging": true,
        "searching":true,
        "ordering":false,			
        "ajax": "/assignedInteractionTableData",
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*472.59*/("""{"""),format.raw/*472.60*/("""
              """),format.raw/*473.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*475.15*/("""}"""),format.raw/*475.16*/("""
    """),format.raw/*476.5*/("""}"""),format.raw/*476.6*/(""" """),format.raw/*476.7*/(""");

    FilterOptionData(table);
    
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e)"""),format.raw/*480.61*/("""{"""),format.raw/*480.62*/("""
      """),format.raw/*481.7*/("""$($.fn.dataTable.tables(true)).DataTable()
         .columns.adjust();
   """),format.raw/*483.4*/("""}"""),format.raw/*483.5*/(""");
    
    """),format.raw/*485.5*/("""}"""),format.raw/*485.6*/("""
    
    
    """),format.raw/*488.5*/("""function ajaxCallForFollowUpRequiredServer()"""),format.raw/*488.49*/("""{"""),format.raw/*488.50*/("""
    	"""),format.raw/*489.6*/("""document.getElementById("campaignDiv").style.display = "block";
    	document.getElementById("serviceBookTypeDiv").style.display = "none";
    	document.getElementById("serviceTypeDiv").style.display = "block";
    	document.getElementById("fromDateDiv").style.display = "block";
    	document.getElementById("toDateDiv").style.display = "block";
    	document.getElementById("lastDispoTypeDiv").style.display = "none";
    	document.getElementById("droppedcountDiv").style.display = "none";

    	
              
        //alert("follow up server side data");
        var table= $('#followUpRequiredServerData').dataTable( """),format.raw/*500.64*/("""{"""),format.raw/*500.65*/("""
        """),format.raw/*501.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
		"scrollX": true,
        "paging": true,
        "searching": true,
        "ordering":false,	
        "ajax": "/followUpCallLogTableData",
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*510.59*/("""{"""),format.raw/*510.60*/("""
              """),format.raw/*511.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*513.15*/("""}"""),format.raw/*513.16*/("""
    """),format.raw/*514.5*/("""}"""),format.raw/*514.6*/(""" """),format.raw/*514.7*/(""");

        FilterOptionData(table);
        
    """),format.raw/*518.5*/("""}"""),format.raw/*518.6*/("""
    
    
    
    """),format.raw/*522.5*/("""function ajaxCallForServiceBookedServer()"""),format.raw/*522.46*/("""{"""),format.raw/*522.47*/("""

    	"""),format.raw/*524.6*/("""document.getElementById("campaignDiv").style.display = "block";
    	document.getElementById("serviceBookTypeDiv").style.display = "block";
    	document.getElementById("serviceTypeDiv").style.display = "none";
    	document.getElementById("fromDateDiv").style.display = "block";
    	document.getElementById("toDateDiv").style.display = "block";
    	document.getElementById("lastDispoTypeDiv").style.display = "none";
    	document.getElementById("droppedcountDiv").style.display = "none";
        
    	 var table= $('#serviceBookedServerData').dataTable( """),format.raw/*532.59*/("""{"""),format.raw/*532.60*/("""
        """),format.raw/*533.9*/(""""bDestroy": true,  
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
		"scrollX": true,
        "paging": true,
        "searching": true,
        "ordering":false,		
        "ajax": "/serviceBookedServerDataTable",
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*542.59*/("""{"""),format.raw/*542.60*/("""
              """),format.raw/*543.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*545.15*/("""}"""),format.raw/*545.16*/("""
    """),format.raw/*546.5*/("""}"""),format.raw/*546.6*/(""" """),format.raw/*546.7*/(""");  
    	 FilterOptionData(table);
        
    """),format.raw/*549.5*/("""}"""),format.raw/*549.6*/("""
    
    """),format.raw/*551.5*/("""function ajaxCallForServiceNotRequiredServer()"""),format.raw/*551.51*/("""{"""),format.raw/*551.52*/("""
    	"""),format.raw/*552.6*/("""document.getElementById("campaignDiv").style.display = "block";
    	document.getElementById("serviceBookTypeDiv").style.display = "none";
    	document.getElementById("serviceTypeDiv").style.display = "none";
    	document.getElementById("fromDateDiv").style.display = "none";
    	document.getElementById("toDateDiv").style.display = "none";
    	document.getElementById("lastDispoTypeDiv").style.display = "none";
    	document.getElementById("droppedcountDiv").style.display = "none";
        
    	 var table= $('#serviceNotRequiredServerData').dataTable( """),format.raw/*560.64*/("""{"""),format.raw/*560.65*/("""
        
        """),format.raw/*562.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
		"scrollX": true,
        "paging": true,
        "searching": true,
        "ordering":false,		
        "ajax": "/serviceNotRequiredServerDataTable",
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*571.59*/("""{"""),format.raw/*571.60*/("""
              """),format.raw/*572.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*574.15*/("""}"""),format.raw/*574.16*/("""
    """),format.raw/*575.5*/("""}"""),format.raw/*575.6*/(""" """),format.raw/*575.7*/(""");    

    	 FilterOptionData(table);  
        
    """),format.raw/*579.5*/("""}"""),format.raw/*579.6*/("""
    
    
    """),format.raw/*582.5*/("""function ajaxCallForNonContactsServer()"""),format.raw/*582.44*/("""{"""),format.raw/*582.45*/("""

    	"""),format.raw/*584.6*/("""document.getElementById("campaignDiv").style.display = "block";
    	document.getElementById("serviceBookTypeDiv").style.display = "none";
    	document.getElementById("serviceTypeDiv").style.display = "none";
    	document.getElementById("fromDateDiv").style.display = "block";
    	document.getElementById("toDateDiv").style.display = "block";
    	document.getElementById("lastDispoTypeDiv").style.display = "block";
    	document.getElementById("droppedcountDiv").style.display = "block";
      
    	var table= $('#nonContactsServerData').dataTable( """),format.raw/*592.56*/("""{"""),format.raw/*592.57*/("""
        """),format.raw/*593.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
		"scrollX": true,
        "paging": true,
        "searching": true,
        "ordering":false,				
        "ajax": "/nonContactsServerDataTable",
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*602.59*/("""{"""),format.raw/*602.60*/("""
              """),format.raw/*603.15*/("""$('td', nRow).attr('nowrap','nowrap');
              return nRow;
              """),format.raw/*605.15*/("""}"""),format.raw/*605.16*/("""
    """),format.raw/*606.5*/("""}"""),format.raw/*606.6*/(""" """),format.raw/*606.7*/(""");
        
         FilterOptionData(table);  
        
    """),format.raw/*610.5*/("""}"""),format.raw/*610.6*/("""
    
    
    """),format.raw/*613.5*/("""function ajaxCallForDroppedCallsServer()"""),format.raw/*613.45*/("""{"""),format.raw/*613.46*/("""
        
    	"""),format.raw/*615.6*/("""document.getElementById("campaignDiv").style.display = "none";
    	document.getElementById("serviceBookTypeDiv").style.display = "none";
    	document.getElementById("serviceTypeDiv").style.display = "none";
    	document.getElementById("fromDateDiv").style.display = "none";
    	document.getElementById("toDateDiv").style.display = "none";
    	document.getElementById("lastDispoTypeDiv").style.display = "none";
    	document.getElementById("droppedcountDiv").style.display = "none";
        
       $('#droppedCallsServerData').dataTable( """),format.raw/*623.48*/("""{"""),format.raw/*623.49*/("""
        """),format.raw/*624.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
		"scrollX": true,
        "paging": true,
        "searching": true,
        "ordering":false,        
        "ajax": "/droppedCallsServerDataTable",
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*633.59*/("""{"""),format.raw/*633.60*/("""
              """),format.raw/*634.15*/("""$('td', nRow).attr('nowrap','nowrap');
              return nRow;
              """),format.raw/*636.15*/("""}"""),format.raw/*636.16*/("""
    """),format.raw/*637.5*/("""}"""),format.raw/*637.6*/("""); 
       FilterOptionData(table);    
    """),format.raw/*639.5*/("""}"""),format.raw/*639.6*/("""
    
"""),format.raw/*641.1*/("""</script>
<script>
	 
	var urlDisposition = "/CRE/dashboardCounts";
    $.ajax("""),format.raw/*645.12*/("""{"""),format.raw/*645.13*/("""
        """),format.raw/*646.9*/("""url: urlDisposition

    """),format.raw/*648.5*/("""}"""),format.raw/*648.6*/(""").done(function (dashboardCount) """),format.raw/*648.39*/("""{"""),format.raw/*648.40*/("""

        """),format.raw/*650.9*/("""//console.log(dashboardCount);
        var dateis = GetFormattedDate();
	
	 console.log("dateis : "+dateis);
	 	
       $("#creCurrentdate").html(dateis);

  	  var dashCount = dashboardCount;
  		 $("#FreshCallsCountCRE").html(dashCount[0]);
  		 $("#followupcountCRE").html(dashCount[1]);
  	     $("#nonContactsCRE").html(dashCount[2]);
  	     $("#overDueCountCRE").html(dashCount[3]);
  	     $("#creTotalBookingsperDay").html(dashCount[4]);
  	   	 $("#creTotalCallsperDay").html(dashCount[5]);
        
    """),format.raw/*665.5*/("""}"""),format.raw/*665.6*/(""");



</script>
<script>

function FilterOptionData(table)"""),format.raw/*672.33*/("""{"""),format.raw/*672.34*/("""
	"""),format.raw/*673.2*/("""var values = [];
	$('.filter').on('change', function() """),format.raw/*674.39*/("""{"""),format.raw/*674.40*/("""
    	"""),format.raw/*675.6*/("""console.log("inside filteroptionsdata data");
	    var i= $(this).data('columnIndex');
	    var v = $(this).val();
		console.log(v);
		console.log(i);
	    
	    if(v.length>0)"""),format.raw/*681.20*/("""{"""),format.raw/*681.21*/("""
		    	   
		   """),format.raw/*683.6*/("""values[i] = v;
	    
	    """),format.raw/*685.6*/("""}"""),format.raw/*685.7*/("""
	    
	    	"""),format.raw/*687.7*/("""console.log(JSON.stringify(values));	   	
	  	  //clear global search values
	 	  //table.api().search('');
	 	  $(this).data('columnIndex');

	 	 if(values.length>0) """),format.raw/*692.25*/("""{"""),format.raw/*692.26*/("""
			 """),format.raw/*693.5*/("""for(var i=0;i<values.length;i++)"""),format.raw/*693.37*/("""{"""),format.raw/*693.38*/("""	
			    """),format.raw/*694.8*/("""table.api().columns(i).search(values[i]);
			 """),format.raw/*695.5*/("""}"""),format.raw/*695.6*/("""
			 """),format.raw/*696.5*/("""table.api().draw();
			 """),format.raw/*697.5*/("""}"""),format.raw/*697.6*/(""" 
		 	  
	"""),format.raw/*699.2*/("""}"""),format.raw/*699.3*/(""");
	
	
"""),format.raw/*702.1*/("""}"""),format.raw/*702.2*/("""
"""),format.raw/*703.1*/("""</script>"""))
      }
    }
  }

  def render(toActivateTab1:String,toActivateTab2:String,toActivateTab3:String,toActivateTab4:String,toActivateTab5:String,dealercode:String,dealerName:String,userName:String,oem:String,campaignList:List[Campaign],serviceTypeList:List[ServiceTypes],bookedServiceTypeList:List[String],dispoList:List[CallDispositionData]): play.twirl.api.HtmlFormat.Appendable = apply(toActivateTab1,toActivateTab2,toActivateTab3,toActivateTab4,toActivateTab5,dealercode,dealerName,userName,oem,campaignList,serviceTypeList,bookedServiceTypeList,dispoList)

  def f:((String,String,String,String,String,String,String,String,String,List[Campaign],List[ServiceTypes],List[String],List[CallDispositionData]) => play.twirl.api.HtmlFormat.Appendable) = (toActivateTab1,toActivateTab2,toActivateTab3,toActivateTab4,toActivateTab5,dealercode,dealerName,userName,oem,campaignList,serviceTypeList,bookedServiceTypeList,dispoList) => apply(toActivateTab1,toActivateTab2,toActivateTab3,toActivateTab4,toActivateTab5,dealercode,dealerName,userName,oem,campaignList,serviceTypeList,bookedServiceTypeList,dispoList)

  def ref: this.type = this

}


}

/**/
object callDispositionFormServiceDivision extends callDispositionFormServiceDivision_Scope0.callDispositionFormServiceDivision
              /*
                  -- GENERATED --
                  DATE: Fri Mar 16 17:47:54 IST 2018
                  SOURCE: D:/CRMFORDAUTOSHERPA/crmford/app/views/callDispositionFormServiceDivision.scala.html
                  HASH: 164be3fd623a2eeb60c340ed228223f967b9e760
                  MATRIX: 933->1|1339->311|1367->314|1438->377|1477->379|1507->383|1572->422|1600->430|1681->485|1715->499|1794->552|1828->566|1907->619|1941->633|2020->686|2054->700|2134->753|2169->767|2223->793|2252->794|2307->822|2430->918|2458->919|2487->921|7097->5504|7146->5536|7186->5537|7251->5573|7295->5589|7318->5602|7358->5620|7389->5623|7412->5636|7453->5654|7528->5697|7600->5741|8463->6577|8518->6615|8558->6616|8624->6653|8668->6669|8694->6685|8737->6706|8768->6709|8795->6725|8839->6746|8915->6790|8988->6835|9336->7156|9403->7206|9443->7207|9474->7210|9518->7226|9562->7248|9593->7251|9637->7273|9716->7320|9791->7367|10097->7645|10137->7668|10177->7669|10210->7674|10254->7690|10269->7695|10308->7712|10339->7715|10354->7720|10393->7737|10443->7756|10527->7812|11219->8476|11255->8490|13056->10263|13092->10277|14799->11956|14835->11970|16697->13804|16733->13818|18314->15371|18350->15385|21288->18292|21318->18294|21491->18438|21521->18439|21559->18449|21695->18556|21725->18557|21768->18571|21842->18617|21872->18618|21902->18619|21936->18624|21966->18625|22009->18639|22084->18686|22114->18687|22148->18693|22177->18694|22213->18702|22263->18723|22293->18724|22331->18734|22485->18859|22515->18860|22558->18874|22632->18920|22662->18921|22692->18922|22726->18927|22756->18928|22799->18942|22874->18989|22904->18990|22938->18996|22967->18997|22999->19001|23117->19090|23147->19091|23181->19097|23798->19685|23828->19686|23866->19696|24207->20008|24237->20009|24282->20025|24389->20103|24419->20104|24453->20110|24482->20111|24511->20112|24642->20214|24672->20215|24708->20223|24812->20299|24841->20300|24883->20314|24912->20315|24958->20333|25031->20377|25061->20378|25096->20385|25760->21020|25790->21021|25828->21031|26164->21338|26194->21339|26239->21355|26346->21433|26376->21434|26410->21440|26439->21441|26468->21442|26550->21496|26579->21497|26631->21521|26701->21562|26731->21563|26768->21572|27364->22139|27394->22140|27432->22150|27775->22464|27805->22465|27850->22481|27957->22559|27987->22560|28021->22566|28050->22567|28079->22568|28159->22620|28188->22621|28228->22633|28303->22679|28333->22680|28368->22687|28966->23256|28996->23257|29044->23277|29390->23594|29420->23595|29465->23611|29572->23689|29602->23690|29636->23696|29665->23697|29694->23698|29780->23756|29809->23757|29855->23775|29923->23814|29953->23815|29990->23824|30582->24387|30612->24388|30650->24398|30991->24710|31021->24711|31066->24727|31177->24809|31207->24810|31241->24816|31270->24817|31299->24818|31392->24883|31421->24884|31467->24902|31536->24942|31566->24943|31611->24960|32192->25512|32222->25513|32260->25523|32606->25840|32636->25841|32681->25857|32792->25939|32822->25940|32856->25946|32885->25947|32959->25993|32988->25994|33024->26002|33136->26085|33166->26086|33204->26096|33259->26123|33288->26124|33350->26157|33380->26158|33420->26170|33977->26699|34006->26700|34100->26765|34130->26766|34161->26769|34246->26825|34276->26826|34311->26833|34522->27015|34552->27016|34599->27035|34655->27063|34684->27064|34727->27079|34928->27251|34958->27252|34992->27258|35053->27290|35083->27291|35121->27301|35196->27348|35225->27349|35259->27355|35312->27380|35341->27381|35381->27393|35410->27394|35448->27404|35477->27405|35507->27407
                  LINES: 27->1|32->1|33->2|33->2|33->2|35->4|35->4|35->4|37->6|37->6|38->7|38->7|39->8|39->8|40->9|40->9|41->10|41->10|44->13|44->13|46->15|49->18|49->18|50->19|145->114|145->114|145->114|146->115|146->115|146->115|146->115|146->115|146->115|146->115|147->116|149->118|166->135|166->135|166->135|167->136|167->136|167->136|167->136|167->136|167->136|167->136|168->137|170->139|176->145|176->145|176->145|177->146|177->146|177->146|177->146|177->146|178->147|180->149|186->155|186->155|186->155|187->156|187->156|187->156|187->156|187->156|187->156|187->156|189->158|192->161|211->180|211->180|256->225|256->225|289->258|289->258|330->299|330->299|370->339|370->339|449->418|450->419|455->424|455->424|456->425|459->428|459->428|460->429|461->430|461->430|461->430|461->430|461->430|462->431|464->433|464->433|465->434|465->434|467->436|467->436|467->436|468->437|471->440|471->440|472->441|473->442|473->442|473->442|473->442|473->442|474->443|476->445|476->445|477->446|477->446|479->448|483->452|483->452|484->453|493->462|493->462|494->463|503->472|503->472|504->473|506->475|506->475|507->476|507->476|507->476|511->480|511->480|512->481|514->483|514->483|516->485|516->485|519->488|519->488|519->488|520->489|531->500|531->500|532->501|541->510|541->510|542->511|544->513|544->513|545->514|545->514|545->514|549->518|549->518|553->522|553->522|553->522|555->524|563->532|563->532|564->533|573->542|573->542|574->543|576->545|576->545|577->546|577->546|577->546|580->549|580->549|582->551|582->551|582->551|583->552|591->560|591->560|593->562|602->571|602->571|603->572|605->574|605->574|606->575|606->575|606->575|610->579|610->579|613->582|613->582|613->582|615->584|623->592|623->592|624->593|633->602|633->602|634->603|636->605|636->605|637->606|637->606|637->606|641->610|641->610|644->613|644->613|644->613|646->615|654->623|654->623|655->624|664->633|664->633|665->634|667->636|667->636|668->637|668->637|670->639|670->639|672->641|676->645|676->645|677->646|679->648|679->648|679->648|679->648|681->650|696->665|696->665|703->672|703->672|704->673|705->674|705->674|706->675|712->681|712->681|714->683|716->685|716->685|718->687|723->692|723->692|724->693|724->693|724->693|725->694|726->695|726->695|727->696|728->697|728->697|730->699|730->699|733->702|733->702|734->703
                  -- GENERATED --
              */
          