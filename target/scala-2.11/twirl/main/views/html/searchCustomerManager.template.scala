
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object searchCustomerManager_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class searchCustomerManager extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[String,String,String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(dealercode:String,dealerName:String,userName:String,oem:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.66*/("""
"""),_display_(/*3.2*/mainPageCREManger("AutoSherpaCRM",userName,dealerName)/*3.56*/ {_display_(Seq[Any](format.raw/*3.58*/("""

"""),format.raw/*5.1*/("""<script type="text/javascript">

 $(document).ready(function() """),format.raw/*7.31*/("""{"""),format.raw/*7.32*/("""
    """),format.raw/*8.5*/("""var table = $('#customerSearchTable').dataTable( """),format.raw/*8.54*/("""{"""),format.raw/*8.55*/("""
        """),format.raw/*9.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
        "paging": true,
        "searching":true,
        "ordering":false,
        "ajax": "/searchByCustomerv2"
        
    """),format.raw/*18.5*/("""}"""),format.raw/*18.6*/(""" """),format.raw/*18.7*/(""");

    var values=[];

  
	
   $('.filter').on('change', function() """),format.raw/*24.41*/("""{"""),format.raw/*24.42*/("""
	   
	   """),format.raw/*26.5*/("""var i= $(this).data('columnIndex');
	   var v = $(this).val();
	   if(v.length>0)"""),format.raw/*28.19*/("""{"""),format.raw/*28.20*/("""
		  """),format.raw/*29.5*/("""if(i < 9)
		   values[i] = v;
		  else if(i == 9)"""),format.raw/*31.20*/("""{"""),format.raw/*31.21*/("""			  
			  """),format.raw/*32.6*/("""values[9] = v;
			  values[4] = values[4]+","+values[9];"""),format.raw/*33.42*/("""}"""),format.raw/*33.43*/("""
		   
	   """),format.raw/*35.5*/("""}"""),format.raw/*35.6*/("""
	   
	   	"""),format.raw/*37.6*/("""console.log(JSON.stringify(values));
	   	
     	  //clear global search values
    	  //table.api().search('');
    	  $(this).data('columnIndex');
    	  
    	  
   """),format.raw/*44.4*/("""}"""),format.raw/*44.5*/(""");


   $('#searchCust').on('click',function()"""),format.raw/*47.42*/("""{"""),format.raw/*47.43*/("""

	  """),format.raw/*49.4*/("""// alert(values.length);
	   if(values.length>0) """),format.raw/*50.25*/("""{"""),format.raw/*50.26*/("""
			"""),format.raw/*51.4*/("""for(var i=0;i<values.length;i++)"""),format.raw/*51.36*/("""{"""),format.raw/*51.37*/("""
				
			   """),format.raw/*53.7*/("""table.api().columns(i).search(values[i]);
			"""),format.raw/*54.4*/("""}"""),format.raw/*54.5*/("""
			"""),format.raw/*55.4*/("""table.api().draw();
	   """),format.raw/*56.5*/("""}"""),format.raw/*56.6*/("""
	"""),format.raw/*57.2*/("""}"""),format.raw/*57.3*/("""); 

   $('#saveSearchbtn').on('click',function()"""),format.raw/*59.45*/("""{"""),format.raw/*59.46*/("""	  
	  """),format.raw/*60.4*/("""var searchnamesave = document.getElementById('searchnamesave').value;
      var urlDisposition = "/checkIfSearchNameExists/" + searchnamesave + "";
          $.ajax("""),format.raw/*62.18*/("""{"""),format.raw/*62.19*/("""
        """),format.raw/*63.9*/("""url: urlDisposition

    """),format.raw/*65.5*/("""}"""),format.raw/*65.6*/(""").done(function (data) """),format.raw/*65.29*/("""{"""),format.raw/*65.30*/("""
	"""),format.raw/*66.2*/("""console.log("data:"+data);
	
        if(data == 0)"""),format.raw/*68.22*/("""{"""),format.raw/*68.23*/("""
        	"""),format.raw/*69.10*/("""if(values.length>0) """),format.raw/*69.30*/("""{"""),format.raw/*69.31*/("""

                
    			"""),format.raw/*72.8*/("""for(var i=0;i<values.length;i++)"""),format.raw/*72.40*/("""{"""),format.raw/*72.41*/("""
    				
    			   """),format.raw/*74.11*/("""table.api().columns(i).search(values[i]);
    			"""),format.raw/*75.8*/("""}"""),format.raw/*75.9*/("""
    			"""),format.raw/*76.8*/("""table.api().draw();
    			Lobibox.notify('success', """),format.raw/*77.34*/("""{"""),format.raw/*77.35*/("""
    	            """),format.raw/*78.18*/("""msg: 'Sucessfully saved....'
    	        """),format.raw/*79.14*/("""}"""),format.raw/*79.15*/(""");
    			
    	   """),format.raw/*81.9*/("""}"""),format.raw/*81.10*/("""
        """),format.raw/*82.9*/("""}"""),format.raw/*82.10*/("""else"""),format.raw/*82.14*/("""{"""),format.raw/*82.15*/("""
        	"""),format.raw/*83.10*/("""Lobibox.notify('warning', """),format.raw/*83.36*/("""{"""),format.raw/*83.37*/("""
	            """),format.raw/*84.14*/("""msg: 'Saved Search name already exists....'
	        """),format.raw/*85.10*/("""}"""),format.raw/*85.11*/(""");
        """),format.raw/*86.9*/("""}"""),format.raw/*86.10*/("""
        
    """),format.raw/*88.5*/("""}"""),format.raw/*88.6*/(""");
   """),format.raw/*89.4*/("""}"""),format.raw/*89.5*/(""");
     
   
   
   $('#clearFilters').on('click',function()"""),format.raw/*93.44*/("""{"""),format.raw/*93.45*/("""

	 
	
		"""),format.raw/*97.3*/("""$('.filter').each(function()"""),format.raw/*97.31*/("""{"""),format.raw/*97.32*/("""
				"""),format.raw/*98.5*/("""$(this).val('');
			"""),format.raw/*99.4*/("""}"""),format.raw/*99.5*/(""");
		
		table = $('#customerSearchTable').dataTable( """),format.raw/*101.48*/("""{"""),format.raw/*101.49*/("""
	        """),format.raw/*102.10*/(""""bDestroy": true,
	        "processing": true,
	        "serverSide": true,
	        "scrollY": 300,
	        "paging": true,
	        "searching":true,
	        "ordering":false,
	        "ajax": "/searchByCustomerv2"
	        
	    """),format.raw/*111.6*/("""}"""),format.raw/*111.7*/(""" """),format.raw/*111.8*/(""");
	   
	"""),format.raw/*113.2*/("""}"""),format.raw/*113.3*/(""");
  
    
 
"""),format.raw/*117.1*/("""}"""),format.raw/*117.2*/(""");
</script>	
	
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-primary">
      <div class="panel-heading"><b>Customer Search</b></div>
      <div class="panel-body">
	  <div class="panel panel-default">
	  <div class="panel-body">
      <div class="row">
      	<div class="col-md-12">
      		
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>Search Name</label>
      						<input type="text" class="filter form-control" data-column-index="0" placeholder="Search Name" />
      				</div>
      			</div>
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>Search Vehicle</label>
      						<input type="text" class="filter form-control" data-column-index="1" placeholder="Search Vehicle" />
      				</div>
      			</div>
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>Search Category</label>
      						<input type="text" class="filter form-control" data-column-index="2" placeholder="Search Category" />
      				</div>
      			</div>
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>Search Phone</label>
      						<input type="text" class="filter form-control" data-column-index="3" placeholder="Search Phone" />
      				</div>
      			</div>
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>Search Service Date From</label>
      					<input type="text" class="form-control" data-column-index="4" placeholder="Search Sercie Date" />	
      				</div>
      			</div>
      		 <div class="col-md-2">
      				<div class="form-group">
      				<label>Search Service Date To</label>
      					<input type="text" class="form-control" data-column-index="9" placeholder="Search Sercie Date" />	
      				</div>
      			</div>
      			
      			
      			
      	</div>
      </div>
  
      <div class="row">
      	<div class="col-md-12">
      		<div class="col-md-2">
      				<div class="form-group">
      				<label>Search Model</label>
      					<input type="text" class="filter form-control" data-column-index="5" placeholder="Search Model" />	
      				</div>
      			</div>
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>Search Variant</label>
      					<input type="text" class="filter form-control" data-column-index="6" placeholder="Search Variant" />
      				</div>
      			</div>
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>Search disposition</label>
      					<input type="text" class="filter form-control" data-column-index="7" placeholder="Search disposition" />	
      				</div>
      			</div>
      			<div class="col-md-2">
      				<div class="form-group">
      				<label>&nbsp;</label>
      					 <button type="submit" id="searchCust" class="btn btn-primary start btn-block">
                    			<i class="fa fa-search"></i>
                    			<span>Search</span>
        				</button>
						</div>
						</div>
						<div class="col-md-2">
      				<div class="form-group">
      				<label>&nbsp;</label>
        				 <button type="submit" id="clearFilters" class="btn btn-primary start btn-block">
                    			<i class="fa fa-eraser"></i>
                    			<span>Clear</span>
        				</button>
 	
      				</div>
      			</div>
      			 
      	</div>
      </div>
      </div>
      </div>
      
       
		  <div class="panel panel-default">
		  <div class="panel-body">
      <div class="dataTable_wrapper">
      <table class="table table-striped table-bordered table-hover" id="customerSearchTable">
      
       
                 <thead>
                                 <tr>
                                    <th>Customer Name</th>
                                    <th>Vehicle RegNo.</th>
                                    <th>Category</th>                                   
                                    <th>Phone Number</th>                                    
                                    <th>Next Service Date</th>
                                    <th>Model</th>
                                    <th>Variant</th>
                                    <th>Last Disposition</th>
                                    
                                 </tr>
                              </thead>

                                <tbody>
                                   
                   				</tbody>
           </table>
        </div>
        </div>
        </div>
        <div class="row">
        	<div class="col-md-12">
        		<div class="col-md-2">
        			<div class="form-group">
        			<label>Saved Search</label>
        			<input type="text"  id ="searchnamesave" class="filter form-control" data-column-index="8" placeholder="Search Name">
        			</div>
        		</div>
        		<div class="col-md-2">
        			<div class="form-group">
        			<label>&nbsp;</label> <br />
        			  <button type="submit" id="saveSearchbtn" class="btn btn-primary start">
                    <i class="fa floppy-o"></i>
                    <span>Save</span>
        </button>
        			</div>
        		</div>
        	</div>
        </div>
     
    
       </div>
    </div>
  </div>
</div>

""")))}))
      }
    }
  }

  def render(dealercode:String,dealerName:String,userName:String,oem:String): play.twirl.api.HtmlFormat.Appendable = apply(dealercode,dealerName,userName,oem)

  def f:((String,String,String,String) => play.twirl.api.HtmlFormat.Appendable) = (dealercode,dealerName,userName,oem) => apply(dealercode,dealerName,userName,oem)

  def ref: this.type = this

}


}

/**/
object searchCustomerManager extends searchCustomerManager_Scope0.searchCustomerManager
              /*
                  -- GENERATED --
                  DATE: Fri Mar 16 17:48:06 IST 2018
                  SOURCE: D:/CRMFORDAUTOSHERPA/crmford/app/views/searchCustomerManager.scala.html
                  HASH: fa27ebe060d782611433608e680b86a418f0d6b9
                  MATRIX: 798->3|957->67|985->70|1047->124|1086->126|1116->130|1208->195|1236->196|1268->202|1344->251|1372->252|1408->262|1669->496|1697->497|1725->498|1828->573|1857->574|1896->586|2007->669|2036->670|2069->676|2148->727|2177->728|2216->740|2301->797|2330->798|2370->811|2398->812|2438->825|2640->1000|2668->1001|2745->1050|2774->1051|2808->1058|2886->1108|2915->1109|2947->1114|3007->1146|3036->1147|3077->1161|3150->1207|3178->1208|3210->1213|3262->1238|3290->1239|3320->1242|3348->1243|3427->1294|3456->1295|3491->1303|3686->1470|3715->1471|3752->1481|3806->1508|3834->1509|3885->1532|3914->1533|3944->1536|4024->1588|4053->1589|4092->1600|4140->1620|4169->1621|4225->1650|4285->1682|4314->1683|4364->1705|4441->1755|4469->1756|4505->1765|4587->1819|4616->1820|4663->1839|4734->1882|4763->1883|4811->1904|4840->1905|4877->1915|4906->1916|4938->1920|4967->1921|5006->1932|5060->1958|5089->1959|5132->1974|5214->2028|5243->2029|5282->2041|5311->2042|5354->2058|5382->2059|5416->2066|5444->2067|5536->2131|5565->2132|5605->2145|5661->2173|5690->2174|5723->2180|5771->2201|5799->2202|5883->2257|5913->2258|5953->2269|6224->2512|6253->2513|6282->2514|6321->2525|6350->2526|6395->2543|6424->2544
                  LINES: 27->2|32->2|33->3|33->3|33->3|35->5|37->7|37->7|38->8|38->8|38->8|39->9|48->18|48->18|48->18|54->24|54->24|56->26|58->28|58->28|59->29|61->31|61->31|62->32|63->33|63->33|65->35|65->35|67->37|74->44|74->44|77->47|77->47|79->49|80->50|80->50|81->51|81->51|81->51|83->53|84->54|84->54|85->55|86->56|86->56|87->57|87->57|89->59|89->59|90->60|92->62|92->62|93->63|95->65|95->65|95->65|95->65|96->66|98->68|98->68|99->69|99->69|99->69|102->72|102->72|102->72|104->74|105->75|105->75|106->76|107->77|107->77|108->78|109->79|109->79|111->81|111->81|112->82|112->82|112->82|112->82|113->83|113->83|113->83|114->84|115->85|115->85|116->86|116->86|118->88|118->88|119->89|119->89|123->93|123->93|127->97|127->97|127->97|128->98|129->99|129->99|131->101|131->101|132->102|141->111|141->111|141->111|143->113|143->113|147->117|147->117
                  -- GENERATED --
              */
          