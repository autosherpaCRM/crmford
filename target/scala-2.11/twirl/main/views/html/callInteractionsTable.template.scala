
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object callInteractionsTable_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class callInteractionsTable extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template7[String,String,String,String,List[String],List[CallDispositionData],List[String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(fromDateValue:String,toDateValue:String,user:String,dealerName:String,allCRE:List[String],allDisposition:List[CallDispositionData],locations:List[String]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.157*/("""

"""),_display_(/*4.2*/mainPageCREManger("AutoSherpaCRM",user,dealerName)/*4.52*/ {_display_(Seq[Any](format.raw/*4.54*/("""

"""),format.raw/*6.1*/("""<input type="hidden" name="fromDateToAss" value=""""),_display_(/*6.51*/fromDateValue),format.raw/*6.64*/("""">
<input type="hidden" name="toDateToAss" value=""""),_display_(/*7.49*/toDateValue),format.raw/*7.60*/("""">
<div class="panel panel-primary">
        <div class="panel-heading"><strong>Select Date Range To view Call Interactions</strong></div>
        <div class="panel-body"> 
             """),_display_(/*11.15*/helper/*11.21*/.form(action = routes.AllCallInteractionController.downloadCallHistoryReport)/*11.98*/ {_display_(Seq[Any](format.raw/*11.100*/("""
  """),format.raw/*12.3*/("""<div class="row">
  <div class="col-md-2">   
      <div class="form-group">
        
<label>Call Date From :<span style="color:red">*</span></label>    
	   
		<input type="text" class="form-control datepickerFilter" placeholder="Enter Call Date From" id="frombilldaterange" name="frombilldaterange" readonly>
      </div>
      </div>
  
   <div class="col-md-2">   
      <div class="form-group">   
       <label>Call Date To :<span style="color:red">*</span></label>    
					<input type="text" class="form-control datepickerFilter" placeholder="Enter Call Date To"  id="tobilldaterange" name="tobilldaterange" readonly>
     	</div>
      </div>

   
        <div class="col-md-2">
   
                    <label>Select Location :</label>
                     <div class="form-group">
    	  	<select  multiple="multiple"  id="location"  name="locations[]" class="selectpicker  form-control" onchange="workshopsBasedOnLocations(this);" required> 
    	                       	     
                     	"""),_display_(/*36.24*/for(data3 <- locations) yield /*36.47*/{_display_(Seq[Any](format.raw/*36.48*/("""                       	                         
                    
                        """),format.raw/*38.25*/("""<option value=""""),_display_(/*38.41*/data3),format.raw/*38.46*/("""">"""),_display_(/*38.49*/data3),format.raw/*38.54*/("""</option>
                      	
                      	""")))}),format.raw/*40.25*/("""	  	
                    	"""),format.raw/*41.22*/("""</select>	
                    	<span type="label" id="locationERR" style="color:red"></span>	
           
      </div>
       </div>
       
       <div class="col-md-2">
   
                    <label>Select Workshops :</label>
                     <div class="form-group">
    	  	<select  multiple="multiple"  id="workshopname"  name="workshopname[]" class="form-control" onchange="cresBasedOnLocations(this);"  required> 
    	                       	     
                     	  	
                    	</select>
                    	
           
      </div>
       </div>
       
        <div class="col-md-2">
   
                    <label>Select CRE's :</label>
                     <div class="form-group">
    	  	<select  multiple="multiple"  id="crename"  name="crename[]" class="form-control"  required> 
    	                       	     
                     	  	
                    	</select>
                    	<span type="label" id="crenameERR" style="color:red"></span>	
           
      </div>
       </div>
       <div class="col-md-2">
                    <label>Select Dispositions :</label>
                     <div class="form-group">
    			<select  multiple="multiple" id="all_dispositions" name="all_dispositions" class="selectpicker form-control" >
    					                       			    
                     	"""),_display_(/*77.24*/for(data2 <- allDisposition) yield /*77.52*/{_display_(Seq[Any](format.raw/*77.53*/("""                      	                         
                    
                        """),format.raw/*79.25*/("""<option value=""""),_display_(/*79.41*/data2/*79.46*/.getId()),format.raw/*79.54*/("""">"""),_display_(/*79.57*/data2/*79.62*/.getDisposition()),format.raw/*79.79*/("""</option>
                      				
                      	""")))}),format.raw/*81.25*/("""			 	
                      					
                	"""),format.raw/*83.18*/("""</select>
                				
            </div>
             </div>
        
</div>
<div class="col-md-12">
  <div class="form-group pull-right">
  		<button type="button" name="selectedFile" class="btn btn-sm btn-primary viewCallsforReports" value="viewCalls"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;View Calls</button>		     	
  		 <button type="submit" name="downloadFile" class="btn btn-sm btn-warning"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;Download</button>
		</div>
</div> 
  </div>
 
  """)))}),format.raw/*97.4*/("""
  """),format.raw/*98.3*/("""</div>      
<span type="label" id="viewCallsERR" style="color:red"></span>
<div class="row">
      <div class="col-lg-12">
        <div class="panel panel-primary">
          <div class="panel-heading"><b> List Of  Call Interactions</b>
          </div>
          <div class="panel-body" style="overflow:auto">
          <table class="table table-striped table-bordered table-hover"
						id="datatableCallIntraction">
						<thead>
							<tr>

								<th>Location</th> 
                                    <th>CRE Name</th> 
                                    <th>Call Time</th>                                   
                                    <th>Call Date</th>
                                    <th>Preferred ContactNo</th>
                                    <th>Call Type</th>
                                    <th>Customer Name</th>
                                    <th>DOB</th>
                                    <th>Office Address</th>
                                    <th>Residential Address</th>
                                    <th>Permanent Address</th>
                                    <th>Email</th>
                                    <th>Customer Category</th>
                                    <th>Customer City</th>
                                    <th>Call Duration</th>
                                    <th>Customer Remarks</th>
                                    <th>Vehicle RegNo</th>
                                    <th>Chassis No</th>
                                    <th>Model</th>
                                    <th>Fuel Type</th>
                                    <th>Variant</th>
                                    <th>Last Service Date</th>
                                    <th>Last Service Type</th>
                                    <th>Next Service Date</th>
                                    <th>Next Service Type</th>
                                    <th>Forecast Logic</th>
                                    <th>Previous Disposition</th>
                                    <th>Primary Disposition</th>
                                    <th>Secondary Disposition</th>
                                    <th>Tertiary Disposition</th>
                                    <th>Service Type</th>
                                    <th>PSF Status</th>
                                    <th>Calling data type</th>
                                    <th>Type of Pickup</th>
                                    <th>Time of PickUp</th>
                                    <th>Up to</th>
                                    <th>Driver</th>
                                    <th>Service Advisor</th>
                                    <th>Upsell Type</th>
                                    <th>Assigned Date</th>
                                    <th>Is Call initiated</th>


							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
                     
          </div>
          <!-- /.panel-body --> 
        </div>
        <!-- /.panel --> 
      </div>
      <!-- /.col-lg-12 --> 
    </div>
""")))}),format.raw/*168.2*/("""
	
"""),format.raw/*170.1*/("""<script>
$(document).ready(function () """),format.raw/*171.31*/("""{"""),format.raw/*171.32*/("""
	"""),format.raw/*172.2*/("""$('.viewCallsforReports').click(function()"""),format.raw/*172.44*/("""{"""),format.raw/*172.45*/("""
    	
        """),format.raw/*174.9*/("""var fromDate     = $("#frombilldaterange").val();
        var toDate       = $("#tobilldaterange").val();
       
              
        var UserIds="",j,k;
        var dispositions="";
        var location ="";
        var workshop = "";
        
        myOption = document.getElementById('crename');
        console.log("here"+myOption);		        
    for (j=0;j<myOption.options.length;j++)"""),format.raw/*185.44*/("""{"""),format.raw/*185.45*/("""
        """),format.raw/*186.9*/("""if(myOption.options[j].selected)"""),format.raw/*186.41*/("""{"""),format.raw/*186.42*/("""
            """),format.raw/*187.13*/("""if(myOption.options[j].value != "Select")"""),format.raw/*187.54*/("""{"""),format.raw/*187.55*/("""
        		"""),format.raw/*188.11*/("""UserIds = UserIds + myOption.options[j].value + ",";
            """),format.raw/*189.13*/("""}"""),format.raw/*189.14*/("""else"""),format.raw/*189.18*/("""{"""),format.raw/*189.19*/("""
				"""),format.raw/*190.5*/("""UserIds ="";
            """),format.raw/*191.13*/("""}"""),format.raw/*191.14*/("""
        """),format.raw/*192.9*/("""}"""),format.raw/*192.10*/("""
    """),format.raw/*193.5*/("""}"""),format.raw/*193.6*/("""
    	"""),format.raw/*194.6*/("""if(UserIds.length > 0)"""),format.raw/*194.28*/("""{"""),format.raw/*194.29*/("""
        	"""),format.raw/*195.10*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
        	
    	"""),format.raw/*197.6*/("""}"""),format.raw/*197.7*/("""else"""),format.raw/*197.11*/("""{"""),format.raw/*197.12*/("""
    		"""),format.raw/*198.7*/("""UserIds="";
    	"""),format.raw/*199.6*/("""}"""),format.raw/*199.7*/("""

    	"""),format.raw/*201.6*/("""mydispo = document.getElementById('all_dispositions');
        console.log("here"+mydispo);
    	 for (j=0;j<mydispo.options.length;j++)"""),format.raw/*203.45*/("""{"""),format.raw/*203.46*/("""
    	        """),format.raw/*204.14*/("""if(mydispo.options[j].selected)"""),format.raw/*204.45*/("""{"""),format.raw/*204.46*/("""
    	            """),format.raw/*205.18*/("""if(mydispo.options[j].value != "Select")"""),format.raw/*205.58*/("""{"""),format.raw/*205.59*/("""
    	        		"""),format.raw/*206.16*/("""dispositions = dispositions + mydispo.options[j].value + ",";
    	            """),format.raw/*207.18*/("""}"""),format.raw/*207.19*/("""else"""),format.raw/*207.23*/("""{"""),format.raw/*207.24*/("""
						"""),format.raw/*208.7*/("""dispositions ="";

    	            """),format.raw/*210.18*/("""}"""),format.raw/*210.19*/("""
        	            
    	        """),format.raw/*212.14*/("""}"""),format.raw/*212.15*/("""
    	    """),format.raw/*213.10*/("""}"""),format.raw/*213.11*/("""	   	
    	 """),format.raw/*214.7*/("""if(dispositions.length > 0)"""),format.raw/*214.34*/("""{"""),format.raw/*214.35*/("""
    		 """),format.raw/*215.8*/("""dispositions = dispositions.substring(0, dispositions.length - 1);
         	
     	"""),format.raw/*217.7*/("""}"""),format.raw/*217.8*/("""else"""),format.raw/*217.12*/("""{"""),format.raw/*217.13*/("""
     		"""),format.raw/*218.8*/("""dispositions="";
     	"""),format.raw/*219.7*/("""}"""),format.raw/*219.8*/("""


    	 """),format.raw/*222.7*/("""myloc = document.getElementById('all_dispositions');
         console.log("here"+myloc);
     	 for (j=0;j<myloc.options.length;j++)"""),format.raw/*224.44*/("""{"""),format.raw/*224.45*/("""
     	        """),format.raw/*225.15*/("""if(myloc.options[j].selected)"""),format.raw/*225.44*/("""{"""),format.raw/*225.45*/("""
     	            """),format.raw/*226.19*/("""if(myloc.options[j].value != "Select")"""),format.raw/*226.57*/("""{"""),format.raw/*226.58*/("""
     	        		"""),format.raw/*227.17*/("""locations = location + myloc.options[j].value + ",";
     	            """),format.raw/*228.19*/("""}"""),format.raw/*228.20*/("""else"""),format.raw/*228.24*/("""{"""),format.raw/*228.25*/("""
						"""),format.raw/*229.7*/("""locations ="";
     	            """),format.raw/*230.19*/("""}"""),format.raw/*230.20*/("""
     	        """),format.raw/*231.15*/("""}"""),format.raw/*231.16*/("""
     	    """),format.raw/*232.11*/("""}"""),format.raw/*232.12*/("""	   	
     	 """),format.raw/*233.8*/("""if(location.length > 0)"""),format.raw/*233.31*/("""{"""),format.raw/*233.32*/("""
     		 """),format.raw/*234.9*/("""locations = location.substring(0, myloc.length - 1);
          	
      	"""),format.raw/*236.8*/("""}"""),format.raw/*236.9*/("""else"""),format.raw/*236.13*/("""{"""),format.raw/*236.14*/("""
      		"""),format.raw/*237.9*/("""locations="";
      	"""),format.raw/*238.8*/("""}"""),format.raw/*238.9*/("""

     	 """),format.raw/*240.8*/("""myworkshop = document.getElementById('workshopname');
         console.log("here"+myworkshop);
     	 for (j=0;j<myworkshop.options.length;j++)"""),format.raw/*242.49*/("""{"""),format.raw/*242.50*/("""
     	        """),format.raw/*243.15*/("""if(myworkshop.options[j].selected)"""),format.raw/*243.49*/("""{"""),format.raw/*243.50*/("""
     	            """),format.raw/*244.19*/("""if(myworkshop.options[j].value != "Select")"""),format.raw/*244.62*/("""{"""),format.raw/*244.63*/("""
     	        		"""),format.raw/*245.17*/("""workshop = workshop + myworkshop.options[j].value + ",";
     	        		
     	            """),format.raw/*247.19*/("""}"""),format.raw/*247.20*/("""else"""),format.raw/*247.24*/("""{"""),format.raw/*247.25*/("""
     	        		"""),format.raw/*248.17*/("""workshop = "";
         	            
     	            """),format.raw/*250.19*/("""}"""),format.raw/*250.20*/("""
     	        """),format.raw/*251.15*/("""}"""),format.raw/*251.16*/("""
     	    """),format.raw/*252.11*/("""}"""),format.raw/*252.12*/("""	   	
     	 """),format.raw/*253.8*/("""if(workshop.length > 0)"""),format.raw/*253.31*/("""{"""),format.raw/*253.32*/("""
     		 """),format.raw/*254.9*/("""workshop = workshop.substring(0, myworkshop.length - 1);
          	
      	"""),format.raw/*256.8*/("""}"""),format.raw/*256.9*/("""else"""),format.raw/*256.13*/("""{"""),format.raw/*256.14*/("""
      		"""),format.raw/*257.9*/("""workshop="";
      	"""),format.raw/*258.8*/("""}"""),format.raw/*258.9*/("""
     	"""),format.raw/*259.7*/("""var urlLink = "/CREManager/getAllCallHistoryData"
    	    
    	    $('#datatableCallIntraction').dataTable( """),format.raw/*261.51*/("""{"""),format.raw/*261.52*/("""
    	     """),format.raw/*262.11*/(""""bDestroy": true,
    	     "processing": true,
    	     "serverSide": true,
    	      "scrollY": 400,
 	        "scrollX": true,
    	     "paging": true,
    	     "searching": false,
    	     "ordering":false,
    	     "ajax": """),format.raw/*270.19*/("""{"""),format.raw/*270.20*/("""
    		        """),format.raw/*271.15*/("""'type': 'POST',
    		        'url': urlLink,
    		        'data': """),format.raw/*273.23*/("""{"""),format.raw/*273.24*/("""    		        	
    		        	"""),format.raw/*274.16*/("""fromDate:''+fromDate,
    		        	toDate:''+toDate ,
    		        	UserIds:''+UserIds,
    		        	dispositions:''+dispositions,
    		        	locations:''+locations,
    		        	workshop:''+workshop,	        	
    		        	
    		        	
    		        """),format.raw/*282.15*/("""}"""),format.raw/*282.16*/("""
        	        
    	     """),format.raw/*284.11*/("""}"""),format.raw/*284.12*/(""",
    	     "fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*285.67*/("""{"""),format.raw/*285.68*/("""
                 """),format.raw/*286.18*/("""$('td', nRow).attr('nowrap','nowrap');
                 return nRow;
                 """),format.raw/*288.18*/("""}"""),format.raw/*288.19*/("""
    	 """),format.raw/*289.7*/("""}"""),format.raw/*289.8*/(""" """),format.raw/*289.9*/(""");


    """),format.raw/*292.5*/("""}"""),format.raw/*292.6*/(""");
"""),format.raw/*293.1*/("""}"""),format.raw/*293.2*/(""");



</script>
"""))
      }
    }
  }

  def render(fromDateValue:String,toDateValue:String,user:String,dealerName:String,allCRE:List[String],allDisposition:List[CallDispositionData],locations:List[String]): play.twirl.api.HtmlFormat.Appendable = apply(fromDateValue,toDateValue,user,dealerName,allCRE,allDisposition,locations)

  def f:((String,String,String,String,List[String],List[CallDispositionData],List[String]) => play.twirl.api.HtmlFormat.Appendable) = (fromDateValue,toDateValue,user,dealerName,allCRE,allDisposition,locations) => apply(fromDateValue,toDateValue,user,dealerName,allCRE,allDisposition,locations)

  def ref: this.type = this

}


}

/**/
object callInteractionsTable extends callInteractionsTable_Scope0.callInteractionsTable
              /*
                  -- GENERATED --
                  DATE: Tue Mar 20 14:39:16 IST 2018
                  SOURCE: D:/CRMFORDAUTOSHERPA/crmford/app/views/callInteractionsTable.scala.html
                  HASH: c0081757804e47b5df119d5c1dc68a61513ba3d8
                  MATRIX: 850->3|1101->158|1131->163|1189->213|1228->215|1258->219|1334->269|1367->282|1445->334|1476->345|1694->536|1709->542|1795->619|1836->621|1867->625|2929->1660|2968->1683|3007->1684|3132->1781|3175->1797|3201->1802|3231->1805|3257->1810|3348->1870|3403->1897|4815->3282|4859->3310|4898->3311|5022->3407|5065->3423|5079->3428|5108->3436|5138->3439|5152->3444|5190->3461|5284->3524|5365->3577|5933->4115|5964->4119|9179->7303|9212->7308|9281->7348|9311->7349|9342->7352|9413->7394|9443->7395|9488->7412|9922->7817|9952->7818|9990->7828|10051->7860|10081->7861|10124->7875|10194->7916|10224->7917|10265->7929|10360->7995|10390->7996|10423->8000|10453->8001|10487->8007|10542->8033|10572->8034|10610->8044|10640->8045|10674->8051|10703->8052|10738->8059|10789->8081|10819->8082|10859->8093|10956->8162|10985->8163|11018->8167|11048->8168|11084->8176|11130->8194|11159->8195|11196->8204|11363->8342|11393->8343|11437->8358|11497->8389|11527->8390|11575->8409|11644->8449|11674->8450|11720->8467|11829->8547|11859->8548|11892->8552|11922->8553|11958->8561|12025->8599|12055->8600|12122->8638|12152->8639|12192->8650|12222->8651|12263->8664|12319->8691|12349->8692|12386->8701|12500->8787|12529->8788|12562->8792|12592->8793|12629->8802|12681->8826|12710->8827|12750->8839|12913->8973|12943->8974|12988->8990|13046->9019|13076->9020|13125->9040|13192->9078|13222->9079|13269->9097|13370->9169|13400->9170|13433->9174|13463->9175|13499->9183|13562->9217|13592->9218|13637->9234|13667->9235|13708->9247|13738->9248|13780->9262|13832->9285|13862->9286|13900->9296|14002->9370|14031->9371|14064->9375|14094->9376|14132->9386|14182->9408|14211->9409|14250->9420|14424->9565|14454->9566|14499->9582|14562->9616|14592->9617|14641->9637|14713->9680|14743->9681|14790->9699|14913->9793|14943->9794|14976->9798|15006->9799|15053->9817|15140->9875|15170->9876|15215->9892|15245->9893|15286->9905|15316->9906|15358->9920|15410->9943|15440->9944|15478->9954|15584->10032|15613->10033|15646->10037|15676->10038|15714->10048|15763->10069|15792->10070|15828->10078|15969->10190|15999->10191|16040->10203|16311->10445|16341->10446|16386->10462|16485->10532|16515->10533|16576->10565|16881->10841|16911->10842|16971->10873|17001->10874|17099->10943|17129->10944|17177->10963|17294->11051|17324->11052|17360->11060|17389->11061|17418->11062|17458->11074|17487->11075|17519->11079|17548->11080
                  LINES: 27->2|32->2|34->4|34->4|34->4|36->6|36->6|36->6|37->7|37->7|41->11|41->11|41->11|41->11|42->12|66->36|66->36|66->36|68->38|68->38|68->38|68->38|68->38|70->40|71->41|107->77|107->77|107->77|109->79|109->79|109->79|109->79|109->79|109->79|109->79|111->81|113->83|127->97|128->98|198->168|200->170|201->171|201->171|202->172|202->172|202->172|204->174|215->185|215->185|216->186|216->186|216->186|217->187|217->187|217->187|218->188|219->189|219->189|219->189|219->189|220->190|221->191|221->191|222->192|222->192|223->193|223->193|224->194|224->194|224->194|225->195|227->197|227->197|227->197|227->197|228->198|229->199|229->199|231->201|233->203|233->203|234->204|234->204|234->204|235->205|235->205|235->205|236->206|237->207|237->207|237->207|237->207|238->208|240->210|240->210|242->212|242->212|243->213|243->213|244->214|244->214|244->214|245->215|247->217|247->217|247->217|247->217|248->218|249->219|249->219|252->222|254->224|254->224|255->225|255->225|255->225|256->226|256->226|256->226|257->227|258->228|258->228|258->228|258->228|259->229|260->230|260->230|261->231|261->231|262->232|262->232|263->233|263->233|263->233|264->234|266->236|266->236|266->236|266->236|267->237|268->238|268->238|270->240|272->242|272->242|273->243|273->243|273->243|274->244|274->244|274->244|275->245|277->247|277->247|277->247|277->247|278->248|280->250|280->250|281->251|281->251|282->252|282->252|283->253|283->253|283->253|284->254|286->256|286->256|286->256|286->256|287->257|288->258|288->258|289->259|291->261|291->261|292->262|300->270|300->270|301->271|303->273|303->273|304->274|312->282|312->282|314->284|314->284|315->285|315->285|316->286|318->288|318->288|319->289|319->289|319->289|322->292|322->292|323->293|323->293
                  -- GENERATED --
              */
          