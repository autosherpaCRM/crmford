
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object commonPSFDispositionPage_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class commonPSFDispositionPage extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template17[String,Long,Long,Long,Long,String,String,String,Customer,Vehicle,WyzUser,Service,List[SMSTemplate],List[String],List[String],PSFAssignedInteraction,Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(oem:String,typeOfPSF:Long,dispositionHistory:Long,interactionid:Long,uniqueid:Long,dealercode:String,dealerName:String,userName:String,customerData:Customer,vehicleData:Vehicle,userData:WyzUser,latestService:Service,smsTemplates:List[SMSTemplate],complaintOFCust:List[String],statesList:List[String],lastPSFAssignStatus:PSFAssignedInteraction,psfDayPage:Html):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.362*/("""

"""),_display_(/*3.2*/mainPageCRE("AutoSherpaCRM",userName,dealerName,dealercode,oem)/*3.65*/ {_display_(Seq[Any](format.raw/*3.67*/("""
	
	"""),format.raw/*5.2*/("""<style>
	.ColorGreen"""),format.raw/*6.13*/("""{"""),format.raw/*6.14*/("""
		"""),format.raw/*7.3*/("""background-color:green;
	"""),format.raw/*8.2*/("""}"""),format.raw/*8.3*/("""
	
	"""),format.raw/*10.2*/(""".ColorRed"""),format.raw/*10.11*/("""{"""),format.raw/*10.12*/("""
		"""),format.raw/*11.3*/("""background-color:red;
	"""),format.raw/*12.2*/("""}"""),format.raw/*12.3*/("""
"""),format.raw/*13.1*/("""</style>

"""),_display_(/*15.2*/helper/*15.8*/.form(action=routes.PSFController.postCommonPSFDispositionPage())/*15.73*/{_display_(Seq[Any](format.raw/*15.74*/("""
"""),format.raw/*16.1*/("""<link rel="stylesheet" href=""""),_display_(/*16.31*/routes/*16.37*/.Assets.at("css/cre.css")),format.raw/*16.62*/("""">
<input type="hidden" id="pFlag" name="pFlag" value=""""),_display_(/*17.54*/{if(customerData.getPreferredAdress()!=null){customerData.getPreferredAdress().getAddressType()}}),format.raw/*17.151*/("""">
<input type="hidden" name="customer_Id" id="customer_Id" value=""""),_display_(/*18.66*/{customerData.getId()}),format.raw/*18.88*/("""">
<input type="hidden" id="wyzUser_Id" name="wyzUser_Id" value=""""),_display_(/*19.64*/{userData.getId()}),format.raw/*19.82*/("""">
<input type="hidden" id="vehical_Id" name="vehicalId" value=""""),_display_(/*20.63*/{vehicleData.getVehicle_id()}),format.raw/*20.92*/("""">
<input type="hidden" name="uniqueidForCallSync" value=""""),_display_(/*21.57*/uniqueid),format.raw/*21.65*/("""">
<input type="hidden" id="workshopIdis" value=""""),_display_(/*22.48*/{if(latestService.getWorkshop()!=null){latestService.getWorkshop().getId()}}),format.raw/*22.124*/("""">
<input type="hidden" id="location_Id" value=""""),_display_(/*23.47*/{userData.getLocation().getCityId()}),format.raw/*23.83*/("""">
<input type="hidden" name="interactionid" value='"""),_display_(/*24.51*/interactionid),format.raw/*24.64*/("""'>
<input type="hidden" name="dispositionHistory" value='"""),_display_(/*25.56*/dispositionHistory),format.raw/*25.74*/("""'>
<input type="hidden" name="typeOfPSF" value='"""),_display_(/*26.47*/typeOfPSF),format.raw/*26.56*/("""'>
<input type="hidden" name="oem" value='"""),_display_(/*27.41*/oem),format.raw/*27.44*/("""'>



<div class="row" style="text-transform:uppercase;">
<!-- 4User div Starts -->
	<div class="col-md-4">
			  <div id="panelDemo10" class="panel panel-info custinfoData">
      <div class="panel-heading" style="text-align:center; padding: 5px;"> <b><i class="fa fa-user"></i>&nbsp;
        
        """),_display_(/*37.10*/{if(customerData.getCustomerName()!=null){
        
        customerData.getCustomerName();
        
        }else{
        
        customerData.getConcatinatedName();							
        
        }}),format.raw/*45.11*/(""" """),format.raw/*45.12*/("""</b><br />
        <b><i class="fa fa-user-circle">
		<input type="text" id="driverIdUpdate" style="border:none;background-color:#1797be;" value=""""),_display_(/*47.96*/{if(customerData.getUserDriver()!=null){
        
        customerData.getUserDriver();
        
        }}),format.raw/*51.11*/("""" readonly></i>
         </b></div>
      <div class="panel-body">
        <div class="row">
          <div class="col-sm-6 phoneselect">
            <div class="col-xs-3">
             <!--  <input type="button"  class="btn btn-primary" value=""""),_display_(/*57.74*/{customerData.getPreferredPhone().getPhoneNumber()}),format.raw/*57.125*/("""" id="ddl_phone_no" name="ddl_phone_no" style="cursor:default ;width: 150px;margin-left: -137px;font-size: medium;"> -->
			 <select class="btn btn-primary" id="ddl_phone_no" name="ddl_phone_no" style="cursor:default ;width: 150px;margin-left: -137px;font-size: medium;">
									"""),_display_(/*59.11*/for( phoneList <- customerData.getPhones()) yield /*59.54*/{_display_(Seq[Any](format.raw/*59.55*/("""
											
									"""),_display_(/*61.11*/{if(phoneList.isIsPreferredPhone ==true){
									
									if(phoneList.getPhoneNumber()!=null){
									<option value={phoneList.getPhoneNumber()} selected="selected">{phoneList.getPhoneNumber()}</option>
									}}else{ 
									
										if(phoneList.getPhoneNumber()!=null){
										<option value={phoneList.getPhoneNumber()} >{phoneList.getPhoneNumber()}</option>
										}}}),format.raw/*69.14*/("""
									""")))}),format.raw/*70.11*/("""						
								 """),format.raw/*71.10*/("""</select>
            </div>
            <button type="button" class="btn btn-success phoneinfobtn"  data-target="#addBtn" data-toggle="modal" data-backdrop="static" data-keyboard="false"><i class="fa fa-pencil-square-o fa-lg"></i></button>
            
<button type="button" style="Display:none" id="DNDbtn" class="btn btn-danger">DND</button>			
            <!--<button type="button" class="btn btn-success test" id="btnDnd" data-target="#DNDConfirm" data-toggle="modal" data-backdrop="static" data-keyboard="false">DND</button>--> 
            
          </div>
          <div class="col-sm-12" style="margin-top: 10px;">
            <div class="col-xs-4">
              <input type="hidden" id="isCallinitaited" name="isCallinitaited" value="NotDailed">
              <input type="hidden" id="pref_number_callini" value=""""),_display_(/*82.69*/{customerData.getPreferredPhone().getPhone_Id()}),format.raw/*82.117*/("""">
              <button type="button" id="callIdBtn" class="btn btn-primary btn-lg" onclick ="callfunctionFromPage('"""),_display_(/*83.116*/{customerData.getPreferredPhone().getPhone_Id()}),format.raw/*83.164*/("""','"""),_display_(/*83.168*/uniqueid),format.raw/*83.176*/("""','"""),_display_(/*83.180*/customerData/*83.192*/.getId()),format.raw/*83.200*/("""');"  style="border-radius: 29px;" ><i class="fa fa-phone-square fa-2x" aria-hidden="true" title="Make Call" ></i></button>
            </div>
            <div class="col-xs-4">
              <button type="button" class="btn btn-primary btn-lg " style="border-radius: 29px;"><i class="fa fa-envelope-square fa-2x" aria-hidden="true"></i></button>
            </div>
            <div class="col-xs-4">
              <button type="button" data-toggle="modal" data-target="#smsPopuId" data-backdrop="static" data-keyboard="false" class="btn btn-primary  btn-lg" style="border-radius: 29px;"><i class="fa fa-comment fa-2x" title="SMS" ></i></button>
            </div>
          </div>
          <div class="col-sm-12" style="margin-top: 6px;">
            <div class="row">
              <div class="col-xs-12" > <span><b><u>Email:</u>&nbsp; </b></span> <span style="font-size:15px" id="ddl_email"> """),_display_(/*94.127*/{if(customerData.getPreferredEmail()!=null){
                customerData.getPreferredEmail().getEmailAddress()
                }}),format.raw/*96.19*/(""" """),format.raw/*96.20*/("""</span> </div>
            </div>
            <div class="row">
              <div class="col-xs-6"> <span><b><u>DOA:</u>&nbsp; </b></span> <span>"""),_display_(/*99.84*/{customerData.getAnniversaryDateStr()}),format.raw/*99.122*/("""</span> </div>
              <div class="col-xs-6" > <span><b><u>DOB:</u>&nbsp; </b></span> <span>"""),_display_(/*100.85*/{customerData.getDobStr()}),format.raw/*100.111*/("""</span> </div>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="row">
              <div class="col-xs-12"> <span><b><u>Address:</u>&nbsp; </b></span> <span class="addressBlog" id="prefAdressUpdate"> """),_display_(/*105.132*/{if(customerData.getPreferredAdress()!=null){
                if(customerData.getPreferredAdress().getConcatenatedAdress()!=null){
                customerData.getPreferredAdress().getConcatenatedAdress();
                }else{
                if(customerData.getConcatinatedPrefAddree()!=null){
                customerData.getConcatinatedPrefAddree();
                }    
                }
				}}),format.raw/*113.7*/(""" """),format.raw/*113.8*/("""</span> </div>
            </div>
          </div>
          <div class="col-xs-12">
            <div class="row">
              <div class="col-xs-6"> <b><u>Pref Time:</u>&nbsp;</b><span>"""),_display_(/*118.75*/{customerData.getPreferred_time_start()}),format.raw/*118.115*/(""" """),format.raw/*118.116*/("""To """),_display_(/*118.120*/{customerData.getPreferred_time_end()}),format.raw/*118.158*/("""</span> </div>
              <div class="col-xs-6"> <b><u>Pref Contact:</u>&nbsp;</b> <span>"""),_display_(/*119.79*/{customerData.getMode_of_contact()}),format.raw/*119.114*/("""</span> </div>
            </div>
          </div>
		  <div class="col-xs-12">
            <div class="row">
              <div class="col-xs-12"> <b><u>Pref Day:</u>&nbsp;</b> <span>"""),_display_(/*124.76*/{customerData.getPreferred_day()}),format.raw/*124.109*/("""</span> </div>
              
            </div>
          </div>
          <!--button-->
          <div class="col-sm-12">
            <div class="row">
              <div class="col-sm-6"> """),_display_(/*131.39*/if(customerData.getSegment()!=null)/*131.74*/{_display_(Seq[Any](format.raw/*131.75*/("""
                
                """),_display_(/*133.18*/for(segmentIterate <- customerData.getSegment()) yield /*133.66*/{_display_(Seq[Any](format.raw/*133.67*/("""
                """),format.raw/*134.17*/("""<button type="button" class="btn btn-info btn-flat btn-block loya" style="cursor:default;"><b style="font-size:15px"> """),_display_(/*134.136*/if(segmentIterate.getName()!="" && segmentIterate.getName()!=null)/*134.202*/{_display_(Seq[Any](format.raw/*134.203*/("""
                """),_display_(/*135.18*/segmentIterate/*135.32*/.getName()),format.raw/*135.42*/("""
                """)))}/*136.18*/else/*136.22*/{_display_(Seq[Any](format.raw/*136.23*/("""
                """),format.raw/*137.17*/("""NA
                """)))}),format.raw/*138.18*/(""" """),format.raw/*138.19*/("""</b></button>
                """)))}),format.raw/*139.18*/("""
                """)))}),format.raw/*140.18*/("""
				"""),format.raw/*141.5*/("""</div>
            </div>
          </div>
        </div>
      </div>
    </div>
			   </div>
		  <div class="col-md-8" style="padding: 0px;">
							<div class="col-sm-4" style="padding: 0px;">
			<!-- START panel-->
					<div id="panelDemo10" class="panel panel-info">
					<div class="panel-heading"><b>VEHICLE&nbsp;INFO</b><img src=""""),_display_(/*152.68*/routes/*152.74*/.Assets.at("images/car.png")),format.raw/*152.102*/("""" class="pull-right" style="width:28px;"></img></div>
						<div class="panel-body" style="height: 110px;overflow: auto;padding: 5px;">
						"""),_display_(/*154.8*/if(vehicleData!=null)/*154.29*/{_display_(Seq[Any](format.raw/*154.30*/("""
		
						
						
						"""),format.raw/*158.7*/("""<span><b>"""),_display_(/*158.17*/{vehicleData.getVehicleRegNo()}),format.raw/*158.48*/("""</b></span>&nbsp;|&nbsp;<span><b>"""),_display_(/*158.82*/{vehicleData.getModel()}),format.raw/*158.106*/("""</b></span></br>
						<span><b>"""),_display_(/*159.17*/{vehicleData.getVariant()}),format.raw/*159.43*/("""</b>&nbsp;|&nbsp;<b>"""),_display_(/*159.64*/{vehicleData.getColor()}),format.raw/*159.88*/("""</b></span></br>
						<span>Mileage(Kms):&nbsp;<b>"""),_display_(/*160.36*/{latestService.getServiceOdometerReading()}),format.raw/*160.79*/("""</b></span></br>
						<span>Sale&nbsp;Date:&nbsp;<b class="servi" style="background-color: #f05050; color: #fff;border-radius: 3px;   font-size: 13px;border-color: transparent;">"""),_display_(/*161.164*/{vehicleData.getSaleDateStr()}),format.raw/*161.194*/("""</b></b></span>
										
						
				""")))}),format.raw/*164.6*/("""
						"""),format.raw/*165.7*/("""</div>
									</div>
									<!-- END panel-->
							</div>
<div class="col-sm-4" style="padding: 0px;">
<!-- START panel-->
<div id="panelDemo10" class="panel panel-info">
<div class="panel-heading specialoffer"><b>NEXT&nbsp;SERVICE&nbsp;DUE</b><img src=""""),_display_(/*172.85*/routes/*172.91*/.Assets.at("images/services1.png")),format.raw/*172.125*/("""" class="pull-right" style="width:28px;"></img></div>
<div class="panel-body" style="height: 110px;overflow-x:hidden;padding: 5px;">
<span><b class="servi" style="background-color: #f05050; color: #fff;border-radius: 3px;font-size: 13px;border-color: transparent;"></b>&nbsp;|&nbsp;<b></b></span><br>
<span>FORECAST&nbsp;LOGIC: &nbsp;<b>"""),_display_(/*175.38*/{vehicleData.getForecastLogic()}),format.raw/*175.70*/("""</b></span><br>
<span>AVG&nbsp;RUNNING/MONTH(Kms):&nbsp;<b>"""),_display_(/*176.45*/{vehicleData.getAverageRunning()}),format.raw/*176.78*/("""</b> </span></br>
<span>NO&nbsp;SHOW(Months):&nbsp;<b>"""),_display_(/*177.38*/{vehicleData.getRunningBetweenvisits()}),format.raw/*177.77*/("""</b> </span>

</div>
</div>
</div>
							
<div class="col-sm-4" style="padding: 0px;">
<!-- START panel-->
<div id="panelDemo10" class="panel panel-info">
<div class="panel-heading"><b>LAST&nbsp;SERVICE&nbsp;STATUS</b><img src=""""),_display_(/*186.75*/routes/*186.81*/.Assets.at("images/car-insurance.png")),format.raw/*186.119*/("""" class="pull-right" style="width:28px;"></img></div>
<div class="panel-body" style="height: 110px;overflow: auto;padding: 5px;">
"""),_display_(/*188.2*/if(vehicleData!=null)/*188.23*/{_display_(Seq[Any](format.raw/*188.24*/("""
"""),format.raw/*189.1*/("""<span>Last Service : <b style="background-color: #f05050; color: #fff;border-radius: 3px;   font-size: 13px;border-color: transparent;padding:2px">"""),_display_(/*189.149*/latestService/*189.162*/.getLastServiceDateStr()),format.raw/*189.186*/("""</b> &nbsp;|&nbsp;<b>"""),_display_(/*189.208*/latestService/*189.221*/.getJobCardNumber()),format.raw/*189.240*/("""</b>&nbsp;|&nbsp;<b>"""),_display_(/*189.261*/latestService/*189.274*/.getLastServiceType()),format.raw/*189.295*/("""</b></span><br>
<span>&nbsp;<b>"""),_display_(/*190.17*/latestService/*190.30*/.getSaName()),format.raw/*190.42*/("""</b>&nbsp;|&nbsp;<b>
"""),_display_(/*191.2*/{if(latestService.getWorkshop()!=null){
latestService.getWorkshop().getWorkshopName()
}
}),format.raw/*194.2*/("""
""")))}),format.raw/*195.2*/("""</b></span><br>
<span>Last PSF:&nbsp;<b>
"""),_display_(/*197.2*/{if(lastPSFAssignStatus.getCampaign()!=null && lastPSFAssignStatus.getService()!=null){
	if(lastPSFAssignStatus.getCampaign().getId()==5){
lastPSFAssignStatus.getService().getPsf2Str()
}
}else if(lastPSFAssignStatus.getCampaign()!=null && lastPSFAssignStatus.getService()!=null){
if(lastPSFAssignStatus.getCampaign().getId()==4){
lastPSFAssignStatus.getService().getPsf1Str()
}
}else if(lastPSFAssignStatus.getCampaign()!=null && lastPSFAssignStatus.getService()!=null){
if(lastPSFAssignStatus.getCampaign().getId()==6){
lastPSFAssignStatus.getService().getPsf3Str()
}
}else if(lastPSFAssignStatus.getCampaign()!=null && lastPSFAssignStatus.getService()!=null){
if(lastPSFAssignStatus.getCampaign().getId()==7){
lastPSFAssignStatus.getService().getPsf4Str()
}
}
}),format.raw/*214.2*/("""
"""),format.raw/*215.1*/("""</b>&nbsp;|&nbsp;<b>"""),_display_(/*215.22*/{if(lastPSFAssignStatus.getService()!=null){lastPSFAssignStatus.getService().getJobCardNumber()}}),format.raw/*215.119*/("""</b>&nbsp;|&nbsp;<b>"""),_display_(/*215.140*/{if(lastPSFAssignStatus.getService()!=null){lastPSFAssignStatus.getService().getLastServiceType()}}),format.raw/*215.239*/("""</b></span><br>
<span>&nbsp;<b>"""),_display_(/*216.17*/{if(lastPSFAssignStatus.getService()!=null){lastPSFAssignStatus.getService().getSaName()}}),format.raw/*216.107*/("""</b>&nbsp;|&nbsp;<b>

"""),_display_(/*218.2*/{if(lastPSFAssignStatus.getFinalDisposition()!=null){
	lastPSFAssignStatus.getFinalDisposition().getDisposition()

}}),format.raw/*221.3*/("""
"""),format.raw/*222.1*/("""</b></span><br>
<br>
</div>

</div>
<!-- END panel-->
</div>

<div class="col-sm-4" style="padding: 0px;">
<!-- START panel-->
<div id="panelDemo10" class="panel panel-info">
<div class="panel-heading"><b>INSURANCE</b><img src=""""),_display_(/*233.55*/routes/*233.61*/.Assets.at("images/truck.png")),format.raw/*233.91*/("""" class="pull-right" style="width:28px;"></img></div>
<div class="panel-body" style="height: 110px;overflow: auto;padding: 5px;">

		

<span>Expires&nbsp;On:&nbsp;<b class="servi" style="background-color: #f05050;color: #fff;border-radius: 3px;   font-size: 13px;border-color: transparent;"></b></b></span><br>
<span style="font-size:13px">New&nbsp;India&nbsp;Assurance</span><br>
<span style="font-size:13px">Policy&nbsp;No:&nbsp;<b></b></span><br>
<span style="font-size:13px">Premium(Rs.):&nbsp;<b></b></span>


</div>

</div>
<!-- END panel-->
</div>
<div class="col-sm-4" style="padding: 0px;">
<!-- START panel-->
<div id="panelDemo10" class="panel panel-info">
<div class="panel-heading"><b>OTHER&nbsp;UPSELL</b><img src=""""),_display_(/*252.63*/routes/*252.69*/.Assets.at("images/warranty-certificate.png")),format.raw/*252.114*/("""" class="pull-right" style="width:28px;"></img></div>
<div class="panel-body" style="height: 110px;overflow: auto;padding: 5px;">
<span>MAXICARE:&nbsp;<b class="servi" style="background-color: #f05050;color: #fff;border-radius: 3px; font-size: 13px;
border-color: transparent;"></b></span><br>
<span>RSA Expiry Date:&nbsp;<b class="servi" style="font-size: 13px;">"""),_display_(/*256.72*/latestService/*256.85*/.getRsaExpiryDate()),format.raw/*256.104*/("""</b></span><br>
<span style="font-size: 13px;">SHIELD:&nbsp;<b>"""),_display_(/*257.49*/latestService/*257.62*/.getShieldExpiryDate()),format.raw/*257.84*/("""</b></span></br>
<span>&nbsp;</span>
</div>
</div>
<!-- END panel-->
</div>
<div class="col-sm-4" style="padding: 0px;">
<!-- START panel-->
<div id="panelDemo10" class="panel panel-info">
<div class="panel-heading"><b>COMPLAINTS</b><img src=""""),_display_(/*266.56*/routes/*266.62*/.Assets.at("images/businessman.png")),format.raw/*266.98*/("""" class="pull-right" style="width:28px;"></img></div>
<div class="panel-body" style="height: 110px;overflow: auto;padding: 5px;">
<span>OPEN:<b class="servi" style="background-color: #f05050; margin-left: 15px; color: #fff;border-radius: 3px; font-size: 17px;    border-color: transparent;">"""),_display_(/*268.163*/complaintOFCust/*268.178*/.get(0)),format.raw/*268.185*/("""</b></span><br>
<span> CLOSED:<b class="servi" style="background-color: #27c24c; color: #fff;border-radius: 3px;     font-size: 13px;
border-color: transparent;">"""),_display_(/*270.30*/complaintOFCust/*270.45*/.get(1)),format.raw/*270.52*/("""</b></span><br>
<span style="font-size: 14PX;">Last&nbsp;Raised&nbsp;On:&nbsp;<b>"""),_display_(/*271.67*/complaintOFCust/*271.82*/.get(2)),format.raw/*271.89*/("""</b></span><br>
<span style="font-size: 13px;">RESOLVED&nbsp;BY:&nbsp;<b>"""),_display_(/*272.59*/complaintOFCust/*272.74*/.get(3)),format.raw/*272.81*/("""</b>&nbsp;</span>

</div>

</div>
<!-- END panel-->
</div>
			</div>
  <div class="modal fade" id="addBtn" role="dialog">
								<div class="modal-dialog modal-lg">

									<!-- Modal content-->
									<div class="modal-content">
										<div class="modal-header" style="padding:1px;text-align:center;color:red;">
											<button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*286.103*/routes/*286.109*/.Assets.at("images/close1.png")),format.raw/*286.140*/(""""></button>
											<h4 class="modal-title"><b>UPDATE CUSTOMER INFORMATION</b></h4>
										</div>
										
								 <div class="modal-body">
					 <div class="row">
						  <div class="col-sm-12" style="margin-top: 5px;">
								<div class="col-sm-3">
								  <lable><b>Owner:</b></lable>
								  <input type="text" class="form-control" id="customerNameEdit" value=""""),_display_(/*295.81*/customerData/*295.93*/.getCustomerName()),format.raw/*295.111*/("""" name="" readonly>
								  <br/>
								</div>
								
								<div class="col-sm-3">
								  <lable><b>User/Driver:</b></lable>
								  <input type="text" class="form-control textOnlyAccepted" id="driverNameEdit" value=""""),_display_(/*301.96*/customerData/*301.108*/.getUserDriver()),format.raw/*301.124*/("""" name="">
								  <br/>
								</div>
								
								
							  </div>
							  </br>
							  <div class="col-sm-12">
								<div class="col-sm-3">
								  <lable><b>Permanent Address:</b></lable>
								  <textarea class="form-control permanentAddress" rows="2" id="permanentAddress" name="permanentAddress" readonly >"""),_display_(/*311.125*/{if(customerData.getPermanentAddressData()!=null){
	  
	  customerData.getPermanentAddressData().getAddressLine1();
	  
	  }}),format.raw/*315.6*/("""_
	  """),_display_(/*316.5*/{if(customerData.getPermanentAddressData()!=null){
	  
	  customerData.getPermanentAddressData().getAddressLine2();
	  
	  }}),format.raw/*320.6*/("""_
	  """),_display_(/*321.5*/{if(customerData.getPermanentAddressData()!=null){
	  
	  customerData.getPermanentAddressData().getAddressLine3();
	  
	  }}),format.raw/*325.6*/("""</textarea>
								  <br/>
								</div>
								<div class="col-sm-1">
								  <button type="button" class="btn btn-success peditAddressmodal" data-toggle="modal" data-target="#driver_modal1" style="margin-top: 25px;">Add</button>
								</div>
								<div class="col-md-3">
								  <lable><b>Residence Address:</b></lable>
								  <textarea class="form-control" rows="2" id="residenceAddress" name="residenceAddress" readonly>"""),_display_(/*333.107*/{if(customerData.getResidenceAddressData()!=null){
	  
	  customerData.getResidenceAddressData().getAddressLine1();
	  
	  }}),format.raw/*337.6*/("""_
	  """),_display_(/*338.5*/{if(customerData.getResidenceAddressData()!=null){
	  
	  customerData.getResidenceAddressData().getAddressLine2();
	  
	  }}),format.raw/*342.6*/("""_
	  """),_display_(/*343.5*/{if(customerData.getResidenceAddressData()!=null){
	  
	  customerData.getResidenceAddressData().getAddressLine3();
	  
	  }}),format.raw/*347.6*/("""</textarea>
								</div>
								<div class="col-md-1">
								  <button type="button" class="btn btn-success reditAddressmodal"  data-toggle="modal" data-target="#driver_modal2" style="margin-top: 25px;">Add</button>
								</div>
								<div class="col-md-3">
								  <lable><b>Office Address:</b></lable>
								  <textarea class="form-control" rows="2" id="officeAddress" name="officeAddress" readonly >"""),_display_(/*354.102*/{if(customerData.getOfficeAddressData()!=null){
	  
	  customerData.getOfficeAddressData().getAddressLine1();
	  
	  }}),format.raw/*358.6*/("""_
	  """),_display_(/*359.5*/{if(customerData.getOfficeAddressData()!=null){
	  
	  customerData.getOfficeAddressData().getAddressLine2();
	  
	  }}),format.raw/*363.6*/("""_
	  """),_display_(/*364.5*/{if(customerData.getOfficeAddressData()!=null){
	  
	  customerData.getOfficeAddressData().getAddressLine3();
	  
	  }}),format.raw/*368.6*/("""</textarea>
								</div>
								<div class="col-md-1">
								  <button type="button" class="btn btn-success oeditAddressmodal" data-toggle="modal" data-target="#driver_modal3" style="margin-top: 25px;">Add</button>
								</div>
							  </div>
							  <hr>
							  <div class="col-sm-12">
								<p style="color:Red;"><b>PREFERRED COMMUNICATION ADDRESS</b></p>
								<div class="col-md-4">
								  <input type="radio"  id="checkbox1" name="ADDRESS" value="0">
								  Permanent Address</div>
								<div class="col-md-4">
								  <input type="radio"  id="checkbox2" name="ADDRESS" value="2" >
								  Residence Address</div>
								<div class="col-md-4">
								  <input type="radio"  id="checkbox3" name="ADDRESS" value="1" >
								  Office Address</div>
							  </div>
							   <hr>
							  <div class="col-sm-12" style="margin-top: 5px;">
								<div class="col-sm-3">
								  <lable><b>Preferred Email:</b></lable>
								  <select class="form-control" id="email" name="email">
									"""),_display_(/*392.11*/for( emailList <- customerData.getEmails()) yield /*392.54*/{_display_(Seq[Any](format.raw/*392.55*/("""
											
									"""),_display_(/*394.11*/{if(emailList.isIsPreferredEmail() ==true)
									
									<option value={emailList.getEmailAddress()} selected="selected">{emailList.getEmailAddress()}</option>
									else 
										<option value={emailList.getEmailAddress()} >{emailList.getEmailAddress()}</option>
										}),format.raw/*399.12*/("""
									""")))}),format.raw/*400.11*/("""						
								 """),format.raw/*401.10*/("""</select>
								</div>
								<div class="col-xs-1"><br />  
								  <button type="button" class="btn btn-success oeditEmailmodal" data-toggle="modal" data-target="#driver_modal4">Add</button>
								</div>
								<div class="col-sm-3">
								  <lable><b>Date OF Birth:</b></lable>
								  <input type="text" class="datepicMYDropDown form-control" id="dob" value=""""),_display_(/*408.86*/{customerData.getDob()}),format.raw/*408.109*/("""" name="dob" readonly>
								</div>
								<div class="col-md-3">
								  <lable><b>Date of Anniversary</b></lable>
								  <input type="text" class="datepicMYDropDown form-control" id="anniversary_date" value=""""),_display_(/*412.99*/{customerData.getAnniversary_date()}),format.raw/*412.135*/("""" name="anniversary_date" readonly>
								</div>
							  </div>
							   <hr>
							  <div class="col-sm-12" style="margin-top: 5px;">
								<h5 style="color:Red;"><b>MANAGE PREFERENCES</b></h5>
								<div class="col-sm-3">
								  <lable><b>Preferred Contact #:</b></lable>
								   <select class="form-control" id="preffered_contact_num" name="preffered_contact_num">
									"""),_display_(/*421.11*/for( phoneList <- customerData.getPhones()) yield /*421.54*/{_display_(Seq[Any](format.raw/*421.55*/("""
											
									"""),_display_(/*423.11*/{if(phoneList.isIsPreferredPhone ==true)
									
									<option value={phoneList.phone_Id.toString()} selected="selected">{phoneList.getPhoneNumber()}</option>
									else 
										<option value={phoneList.phone_Id.toString()} >{phoneList.getPhoneNumber()}</option>
										}),format.raw/*428.12*/("""
									""")))}),format.raw/*429.11*/("""						
								 """),format.raw/*430.10*/("""</select>
								  
								</div>
								<div class="col-sm-1">
								 <input type="button"  class="btn btn-success"  id="tanniversary_edit" data-toggle="modal" data-target="#myModalAddPhone"   style="margin-top: 20px;"  value="Add">
										   </div>
								<div class="col-md-3">
								  <lable><b>Preferred Mode of Contact:</b></lable>
								  <div class="col-sm-12" id="modeOfCon"  style="display:none">
									<select class="form-control" id="preffered_mode_contact" name="preffered_mode_contact" multiple="multiple" >
									  <option value="Email">Email</option>
									  <option value="Phone">Phone</option>
									  <option value="SMS">SMS</option>
									</select>
								  </div>
								  <input type="text" class="form-control" id="tpreffered_mode_contact"  value=""""),_display_(/*445.89*/{customerData.getMode_of_contact()}),format.raw/*445.124*/("""" readonly>
								</div>
								<div class="col-md-1">
								  <input type="button"  class="btn btn-success"  id="tpreffered_mode_contact_edit" style="margin-top: 20px;" value="Add">
								  
								</div>
								<div class="col-md-3">
								  <lable><b>Preferred Day to Contact:</b></lable>
								  <div class="col-sm-12" id="daysWeek" style="display:none">
									<select class="form-control" id="preffered_day_contact" name="preffered_day_contact" multiple="multiple">
									  <option value="Sunday">Sunday</option>
									  <option value="Monday">Monday</option>
									  <option value="Tuesday">Tuesday</option>
									  <option value="Wednesday">Wednesday</option>
									  <option value="Thursday">Thursday</option>
									  <option value="Friday">Friday</option>
									  <option value="Saturday">Saturday</option>
									</select>
								  </div>
								  <input type="text" class="form-control" id="tpreffered_day_contact"  value=""""),_display_(/*464.88*/{customerData.getPreferred_day()}),format.raw/*464.121*/("""" readonly >
								</div>
								<div class="col-md-1">
								  <input type="button"  class="btn btn-success"  id="tpreffered_day_contact_edit" style="margin-top: 20px;" value="Add">
								</div>
							  </div>
							  <br>
							   <hr>
							  <div class="col-sm-12" style="margin-top: 5px;">
								<div class="col-sm-3">
								  <lable><b>Preferred Time Of Contact(Start Time):</b></lable>
								  <input type="text" class="form-control single-input" id="dateStarttime" name="dateStarttime" value=""""),_display_(/*475.112*/{customerData.getPreferred_time_start()}),format.raw/*475.152*/("""" readonly>
								</div>
								<div class="col-md-3">
								  <lable><b>Preferred Time Of Contact(End Time):</b></lable>
								  <input type="text" class="form-control single-input" id="dateEndtime" name="dateEndtime" value=""""),_display_(/*479.108*/{customerData.getPreferred_time_end()}),format.raw/*479.146*/("""" readonly>
								</div>
								<div class="col-sm-6">
								  <lable><b>Comments:(250 characters only)</b></lable>
								  <textarea type="text" class="form-control" id="custEditComment" maxlength="250" rows="2" name="custEditComment" value=""""),_display_(/*483.131*/{customerData.getComments()}),format.raw/*483.159*/("""">"""),_display_(/*483.162*/{customerData.getComments()}),format.raw/*483.190*/("""</textarea>
								</div>
							  </div>
							 
							</div>
				</div>

										<div class="modal-footer">
											<button type="button" id="pn_save" class="btn btn-success" onClick="ajaxCallForAddcustomerinfo();" data-dismiss="modal">Save</button>

										</div>
									</div>

								</div>
							</div>
		  
			  
		  <!-- 4User Div End -->
   


</div>



<!--sashi model-->
<div class="modal fade" id="driver_modal1" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*513.94*/routes/*513.100*/.Assets.at("images/close1.png")),format.raw/*513.131*/(""""></button>
		
		<p class="modal-title" style="tex-align:center"><b>Permanent Address</b></p>
	  </div>
	  <div class="modal-body">
		<table>
		  <tr>
			<td>Address Line1:</td>
			<td><textarea class="form-control paddr_line1" rows="1"   placeholder="Please enter Company Name/Flat No & Building name." ></textarea>
			  <br/></td>
		 
			<td>Address Line2:</td>
			<td><textarea class="form-control paddr_line2" rows="1"   placeholder="Please enter Road number/ Road name."></textarea>
			  <br/></td>
			  <td>Address Line3:</td>
			<td><textarea class="form-control paddr_line3" rows="1"  placeholder="Please enter Road number/ Road name."></textarea>
			  <br/></td>
		  </tr>
		  <tr>
			<td>City</td>
			<td><input type="text" class="form-control paddr_line4 textOnlyAccepted" rows="1" value=""""),_display_(/*533.94*/{if( customerData.getPermanentAddressData()!=null){ customerData.getPermanentAddressData().getCity()} }),format.raw/*533.197*/(""""  placeholder="Please enter City" /><br/></td>
		  
			<td>State</td>
			<td><input type="text"  class="form-control paddr_line5 textOnlyAccepted" rows="1"  value=""""),_display_(/*536.96*/{if( customerData.getPermanentAddressData()!=null){ customerData.getPermanentAddressData().getState()} }),format.raw/*536.200*/("""" placeholder="Please enter State"/><br/></td>
		  </tr>

		  <tr>
			<td>PinCode</td>			
			<td><input type="text"   maxlength="6" class="form-control paddr_line6 numberOnly" rows="1" value=""""),_display_(/*541.104*/{if( customerData.getResidenceAddressData()!=null){ customerData.getResidenceAddressData().getPincode()}else{0} }),format.raw/*541.217*/(""""  placeholder="Please enter PinCode"/><br/></td>
		  
			<td>Country</td>
			
			<td><input type="text"  class="form-control paddr_line7" rows="1" value="India"  readonly><br/></td>
		  </tr>
		</table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary paddr_submit" data-dismiss="modal">Submit</button>
	  </div>
	</div>
  </div>
</div>
<div class="modal fade" id="driver_modal2" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*559.94*/routes/*559.100*/.Assets.at("images/close1.png")),format.raw/*559.131*/(""""></button>
		
		<p class="modal-title" style="tex-align:center"><b>Residence Address</b></p>
	  </div>
	  <div class="modal-body">
		<table>
		  <tr>
			<td>Address Line1:</td>
			<td><textarea class="form-control raddr_line1" rows="1"  name="raddressline1" placeholder="Please enter Company Name/Flat No & Building name." ></textarea>
			  <br/></td>
		
			<td>Address Line2:</td>
			<td><textarea class="form-control raddr_line2" rows="1"  name="raddressline2" placeholder="Please enter Road number/ Road name."></textarea>
			  <br/></td>
			  
			 <td>Address Line3:</td>
			<td><textarea class="form-control raddr_line3" rows="1"  name="raddressline3" placeholder="Please enter Road number/ Road name."></textarea>
			  <br/></td> 
		  </tr>
		   <tr>
			<td>City</td>
			<td><input type="text" class="form-control raddr_line4 textOnlyAccepted" rows="1"   value=""""),_display_(/*580.96*/{if( customerData.getResidenceAddressData()!=null){ customerData.getResidenceAddressData().getCity()} }),format.raw/*580.199*/("""" placeholder="Please enter City" /><br/></td>
		  
			<td>State</td>
			<td><input type="text"  class="form-control raddr_line5 textOnlyAccepted" rows="1"  value=""""),_display_(/*583.96*/{if( customerData.getResidenceAddressData()!=null){ customerData.getResidenceAddressData().getState()} }),format.raw/*583.200*/("""" placeholder="Please enter State"/><br/></td>
		  </tr>

		  <tr>
			<td>PinCode</td>
			<td><input type="text" maxlength="6" class="form-control raddr_line6 numberOnly" rows="1"  value=""""),_display_(/*588.103*/{if( customerData.getResidenceAddressData()!=null){ customerData.getResidenceAddressData().getPincode()}else{0} }),format.raw/*588.216*/("""" placeholder="Please enter PinCode"/><br/></td>
		  
			<td>Country</td>
			<td><input type="text"  class="form-control raddr_line7" rows="1" value="India" readonly><br/></td>
		  </tr>
		
		</table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary raddr_submit" data-dismiss="modal">Submit</button>
	  </div>
	</div>
  </div>
</div>
<div class="modal fade" id="driver_modal3" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*606.94*/routes/*606.100*/.Assets.at("images/close1.png")),format.raw/*606.131*/(""""></button>
		<p class="modal-title" style="tex-align:center"><b>Office Address</b></p>
	  </div>
	  <div class="modal-body">
		<table>
		  <tr>
			<td>Address Line1:</td>
			<td><textarea class="form-control oaddr_line1" rows="2"  placeholder="Please enter Company Name/Flat No & Building name." name="oaddressline1">"""),_display_(/*613.148*/{if( customerData.getOfficeAddressData()!=null){ customerData.getOfficeAddressData().getAddressLine1()} }),format.raw/*613.253*/("""</textarea>
			  <br/></td>
		  
			<td>Address Line2:</td>
			<td><textarea class="form-control oaddr_line2" rows="2" placeholder="Please enter Road number/ Road name." name="oaddressline2">"""),_display_(/*617.133*/{if( customerData.getOfficeAddressData()!=null){ customerData.getOfficeAddressData().getAddressLine2()} }),format.raw/*617.238*/("""</textarea>
			  <br/></td>
			<td>Address Line3:</td>
			<td><textarea class="form-control oaddr_line3" rows="2" placeholder="Please enter Road number/ Road name." name="oaddressline3">"""),_display_(/*620.133*/{if( customerData.getOfficeAddressData()!=null){ customerData.getOfficeAddressData().getAddressLine3()} }),format.raw/*620.238*/("""</textarea>
			  <br/></td>
		  </tr>
		  <tr>
			<td>City</td>
			<td><input type="text" class="form-control oaddr_line7 textOnlyAccepted" id="paddr_line7" rows="1"  name="paddressline3" value=""""),_display_(/*625.133*/{if( customerData.getOfficeAddressData()!=null){ customerData.getOfficeAddressData().getCity()} }),format.raw/*625.230*/(""""placeholder="Please enter City" /><br/></td>
		  
			<td>State</td>
			<td><input type="text"  class="form-control oaddr_line4 textOnlyAccepted" id="paddr_line4" rows="1"  name="paddressline4" value=""""),_display_(/*628.134*/{if( customerData.getOfficeAddressData()!=null){ customerData.getOfficeAddressData().getState()} }),format.raw/*628.232*/("""" placeholder="Please enter State" /><br/></td>
		  </tr>

		  <tr>
			<td>PinCode</td>
			<td><input type="text" maxlength="6"  class="form-control oaddr_line5 numberOnly" id="paddr_line5" rows="1" value=""""),_display_(/*633.120*/{if( customerData.getResidenceAddressData()!=null){ customerData.getResidenceAddressData().getPincode()}else{0} }),format.raw/*633.233*/("""" name="paddressline5" placeholder="Please enter PinCode"/><br/></td>
		  
			<td>Country</td>
			<td><input type="text"  class="form-control oaddr_line6" rows="1" value="India" readonly><br/></td>
			
		  </tr>
		</table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary oaddr_submit" data-dismiss="modal">Submit</button>
	  </div>
	</div>
  </div>
</div>

<div class="modal fade" id="driver_modal4" role="dialog">
    <div class="modal-dialog modal-xs">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*654.102*/routes/*654.108*/.Assets.at("images/close1.png")),format.raw/*654.139*/(""""></button>
          <h4 class="modal-title">ADD EMAIL</h4>
        </div>
		
		
        <div class="modal-body">
		<div class="row">
        <div class="col-xs-12">
        <div class="col-xs-4">
        <input type="text" name="myEmailNum" class="form-control email"  maxlength="30" min="5" max="30" id="myEmailNum" >
      </div>
        </div>
        </div>
		</div>
        <div class="modal-footer">
   <button type="button" class="btn btn-default" data-dismiss="modal" id="saveEmailCust">SAVE</button>
        </div>
      </div>
      
    </div>
  </div>
<div id="smallModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
		<h4 class="modal-title">Message</h4>
	  </div>
	  <div class="modal-body">
		<p>This feature is not enabled for """),_display_(/*683.39*/userName),format.raw/*683.47*/(""".<br>
		  Contact Administrator.</p>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
	  </div>
	</div>
  </div>
</div>
<div id="smallModal1" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
		<h4 class="modal-title">E-Mail</h4>
	  </div>
	  <div class="modal-body">
		<p>This feature is not enabled for """),_display_(/*700.39*/userName),format.raw/*700.47*/(""" """),format.raw/*700.48*/(""".<br>
		  Contact Administrator.</p>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
	  </div>
	</div>
  </div>
</div>


<!---bottom fixed popup----->
<div class="modal fade" id="bottonFixedId" role="dialog">
<div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
	<div class="modal-header">
	  <button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*718.95*/routes/*718.101*/.Assets.at("images/close1.png")),format.raw/*718.132*/(""""></button>
	  <h4 class="modal-title">Call Script</h4>
	</div>
	<div class="modal-body">
	  <p>Hi Good Morning!. </p>
	</div>
	<div class="modal-footer">
	  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
  </div>
  
</div>
</div>

  
  
  <div class="modal fade" id="DNDConfirm" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*740.102*/routes/*740.108*/.Assets.at("images/close1.png")),format.raw/*740.139*/(""""></button>
          <h4 class="modal-title" style="text-align:center;">Do Not Disturb(DND) Tagging</h4>
        </div>
        <div class="modal-body">
          <b>Do you want to tag the customer """),_display_(/*744.47*/customerData/*744.59*/.getCustomerName()),format.raw/*744.77*/("""  """),format.raw/*744.79*/("""as �Do Not Disturb /Call� ?<br> Click �YES� to confirm</b><br><br>
		  
  <div class="radio-inline">
      <label><input type="radio" name="RadioDND" id="RadioDNDYes" value="makeDND">YES</label>
    </div>
    <div class="radio-inline">
      <label><input type="radio" name="RadioDND" id="RadioDNDNo" value="removeDND">NO</label>
    </div>
        </div>
        <div class="modal-footer" style="text-align:center;" >
         
<button type="button" class="btn btn-success" id="submitDndBtn" data-dismiss="modal">submit</button>		  
        </div>
      </div>
      
    </div>
  </div>
  
  
   <div class="modal fade" id="smsPopuId" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="text-align:center;background-color: #106A86;color: #fff;">
          <button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*769.102*/routes/*769.108*/.Assets.at("images/close1.png")),format.raw/*769.139*/(""""></button>
          <h4 class="modal-title" ><b>SMS OPTIONS<b></h4>
        </div>
        <div class="">
   <ul class="nav nav-pills nav-stacked col-md-3">
   """),_display_(/*774.5*/for(smsTemplate<-smsTemplates) yield /*774.35*/{_display_(Seq[Any](format.raw/*774.36*/("""
   		"""),format.raw/*775.6*/("""<li><a class="clssTemplType" id="typAnch_"""),_display_(/*775.48*/{smsTemplate.getSmsId()}),format.raw/*775.72*/("""" href="#tab_"""),_display_(/*775.86*/{smsTemplate.getSmsId()}),format.raw/*775.110*/("""" data-toggle="pill">"""),_display_(/*775.132*/{smsTemplate.getSmsType()}),format.raw/*775.158*/("""</a></li>
   	""")))}),format.raw/*776.6*/("""
 
"""),format.raw/*778.1*/("""</ul>
<div class="tab-content col-md-9 smstabclass">
<input type="hidden" id="smsrequestReference" value=""""),_display_(/*780.55*/{vehicleData.getVehicle_id()}),format.raw/*780.84*/("""" />
  """),_display_(/*781.4*/for(smsTemplate<-smsTemplates) yield /*781.34*/{_display_(Seq[Any](format.raw/*781.35*/("""
  	

        """),format.raw/*784.9*/("""<div class="tab-pane """),_display_(/*784.31*/if(smsTemplate.getSmsId()==1)/*784.60*/{_display_(Seq[Any](format.raw/*784.61*/("""active""")))}),format.raw/*784.68*/("""" id="tab_"""),_display_(/*784.79*/{smsTemplate.getSmsId()}),format.raw/*784.103*/("""">
        <input type="hidden" id="typ_"""),_display_(/*785.39*/{smsTemplate.getSmsId()}),format.raw/*785.63*/(""""  />
             <h4><u>"""),_display_(/*786.22*/{smsTemplate.getSmsType()}),format.raw/*786.48*/("""</u></h4>
			 <textarea name="text" rows="5" cols="45" id="txt_"""),_display_(/*787.55*/{smsTemplate.getSmsId()}),format.raw/*787.79*/("""" class="cl_"""),_display_(/*787.92*/{smsTemplate.getSmsId()}),format.raw/*787.116*/("""" disabled="disabled" style="border:none; background-color:#fff;">"""),_display_(/*787.183*/{smsTemplate.getSmsTemplate()}),format.raw/*787.213*/("""</textarea>
<br/>
		<input type="button" name="set_Value" id="ed_"""),_display_(/*789.49*/{smsTemplate.getSmsId()}),format.raw/*789.73*/("""" class="btn btn-info cmmnbtnclass"  value="Edit"  >
     

        </div>
 
 """)))}),format.raw/*794.3*/("""
 """),format.raw/*795.2*/("""</div>	
<!-- tab content -->
        </div>
        <div class="modal-footer">
          <button type="button" id="smsSendbtn" class="btn btn-success" data-dismiss="modal">SEND SMS</button>
        </div>
      </div>
    </div>
  </div>
 
  <!-- Trigger the modal with a button -->

  <!-- Modal -->
  <div class="modal fade" id="myModalForInfo" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="background-color:#1797BE;text-align:center;">
          <button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*814.102*/routes/*814.108*/.Assets.at("images/close1.png")),format.raw/*814.139*/(""""></button>
          <h4 class="modal-title heading" style="color:#fff"><b>SERVICE HISTORY -<<VEH No.>> - <<SERVICE DATE>><b> </h4>
        </div>
        <div class="modal-body">
         

   <div class="row" style="border:2px solid #1797BE;margin-right: -8px;margin-left: -6px;">
                <div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                    <tr>
                      <td><strong>CUSTOMER ID</strong></td>
                      <td class="text-primary"> 1311935638 </td>
                    </tr>
					
					<tr>
                      <td><strong>S/ADVISOR</strong></td>
                      <td class="text-primary"> </td>
                    </tr>
					<tr>
                      <td><strong>MILEAGE (Kms)</strong></td>
                      <td class="text-primary">34233</td>
                    </tr>
					  
					 <tr>
                      <td><strong>PART DISC</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
                   <tr>
                      <td><strong>PARTS BILL</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
                    </tbody>
                    </table>
                </div>
                <div class="col-md-4">
                 <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                 
					<tr>
                      <td><strong> JOBCARD No.</strong></td>
                      <td class="text-primary">JC13010045</td>
                    </tr>
				
					<tr>
                      <td><strong>TECHNICIAN</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					<tr>
                      <td><strong>Bill No.</strong></td>
                      <td class="text-primary">BR13010474</td>
                    </tr>
					 
					
					<tr>
                      <td><strong>LAB DISC</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					 <tr>
                      <td><strong>LAB BILL</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
                   
                  </tbody>
                </table>
                </div>
                 <div class="col-md-4">
                 <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                
					<tr>
                      <td><strong>W/SHOP</strong></td>
                      <td class="text-primary">UNIV RD</td>
                    </tr>
					
					
					<tr>
                      <td><strong>S/TYPE</strong></td>
                      <td class="text-primary">FR3</td>
                    </tr>
					<tr>
                      <td><strong>Bill Date</strong></td>
                      <td class="text-primary">10/27/2015</td>
                    </tr>
					
					
					 <tr>
                      <td><strong>DISCOUNT</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
                  
					 <tr>
                      <td><strong>BILL AMOUNT</strong></td>
                      <td class="text-primary">656577</td>
                    </tr>
                  </tbody>
                </table>
                </div>
	</div>
			<div class="row" style="border:2px solid #1797BE;margin-top: 5px;margin-right: -8px;margin-left: -6px;">
	   
		
			<h4 style="text-align:center;"><b>REMARKS<b></h4>
    			<div class="col-lg-4" style="text-align: center;">
  			       <div class="form-group">
     			    <label for="">DEFECT DETAILS</label>
     			     <textarea type="" class="form-control" rows="3" id=""  readonly></textarea>
 			     </div>
		 </div> 
  		    <div class="col-lg-4" style="    text-align: center;">
  			<div class="form-group">
     			 <label for="">LABOUR DETAILS</label>
     			 <textarea type="" class="form-control" id=""  rows="3" readonly></textarea>
 			 </div>
		 </div>
       
  		<div class="col-lg-4" style="    text-align: center;">
  			<div class="form-group">
     			 <label for="">JC REMARKS</label>
     			  <textarea type="" class="form-control" id=""  rows="3" readonly></textarea>
 			 </div>
		 </div>
		 
 

</div>

<div class="row" style="border:2px solid #1797BE;    margin-right: -8px;
    margin-left: -6px;    margin-top: 5px;">
<div class="row">
                <div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                    <tr>
                      <td><strong>3rd DAY PSF</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4" >
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                  
					<tr>
                      <td><strong>DATE</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                   
					
					<tr>
                      <td><strong>DONE BY</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
                    </tbody>
                    </table>
                </div>
				</div>
				
				
<div class="row">
                <div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                    <tr>
                      <td><strong>6th DAY PSF</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                  
					<tr>
                      <td><strong>DATE</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                   
					
					<tr>
                      <td><strong>DONE BY</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
                    </tbody>
                    </table>
                </div>
				</div>
				
<div class="row">
                <div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                    <tr>
                      <td><strong>30th DAY PSF</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                  
					<tr>
                      <td><strong>DATE</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                   
					
					<tr>
                      <td><strong>DONE BY</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
                    </tbody>
                    </table>
                </div>
				</div>
</div>
        </div>
      
      </div>
      
    </div>
  </div>
  
  <!-- PSF Disposition Form --> 
  <div class="row">
  <div class="col-lg-12"> 
		
					
					"""),_display_(/*1083.7*/psfDayPage),format.raw/*1083.17*/("""


				
	"""),format.raw/*1087.2*/("""</div>
	</div>
	
<!-----------INTERACTIONH HISTORY--------->
<div class="row">
<div class="col-lg-12">
	<div class="panel panel-primary">
  <div class="panel-heading" align="center"; style="background-color:#1797BE;" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne3">
		<h4 class="panel-title accordion-toggle"></h4>
		<img src=""""),_display_(/*1096.14*/routes/*1096.20*/.Assets.at("images/INTERACTION HISTORY.png")),format.raw/*1096.64*/("""" style="width:17px;"/>&nbsp;<b>INTERACTION HISTORY</b> </div>
  <div id="collapseOne3" class="panel-collapse collapse in">
		<div class="panel-body">
	  <div class="panel with-nav-tabs">
			<div class="panel-heading clearfix">
		  <div>
				<ul class="nav nav-tabs">
			  <!--  <li class="expand"><a href="#tab1primary" data-toggle="tab" onclick="callHistoryByVehicle();">PREVIOUS COMMENTS</a></li> -->
			  <li class="expand"><a href="#tab2primary" data-toggle="tab" onclick="callHistoryByVehicle();">DISPOSITIONS</a></li>
			  <li class="expand"><a href="#tab3primary" data-toggle="tab" onclick="smsHistoryOfUser();">SMS</a></li>
			  <li class="expand"><a href="#tab4primary" data-toggle="tab">E-Mail</a></li>
			  <li class="expand"><a href="#tab5primary" data-toggle="tab" onclick="complaintsOFVehicle();">COMPLAINTS</a></li>
			</ul>
			  </div>
		</div>
			<div class="panel-body">
		  <div class="tab-content">
				<!-- <div class="tab-pane fade in active" id="tab1primary">
			  <div class="row">
					<div class="col-sm-12">
				  <div class="form-group">
						<table class="table table-bordered table-responsive table-user-information" id="previousCommentTable">
					  <thead>
							<tr>
						  <th></th>
						  <th></th>						  
						 
						</tr>
						  </thead>
					  <tbody>
						  
					 
						</tbody>
					  
					  
					</table>
					  </div>
				</div>
				  </div>
			</div> -->
				<div class="tab-pane fade" id="tab2primary">
			  <div class="row">
					<div class="col-sm-12">
				  <div class="form-group">
						<table class="table table-bordered table-responsive" id="dispositionHistory">
					  <thead>
							<tr>
						  <th style="text-align:center">Module</th>
						  <th style="text-align:center">Call Date</th>
						  <th style="text-align:center">Time</th>
						  <th style="text-align:center">CRE ID</th>
						  <th style="text-align:center">Call Type</th>
						  <th style="text-align:center">Primary Disposition</th>
						  <th style="text-align:center">Secondary Disposition</th>
						  <th style="text-align:center">Remarks</th>						 
						  <th style="text-align:center">Recording</th>						 
						</tr>
						  </thead>
					  <tbody>
						  
					 
						</tbody>
					  
					</table>
					  </div>
				</div>
				  </div>
			</div>
			<div class="tab-pane fade" id="tab3primary">
			  <div class="row">
				<div class="col-sm-6">
				
						<table class="table table-condensed table-responsive" id="sms_history">
						 <thead>
							<tr>
						  <th>DATE</th>
						  <th>TIME</th>
						  <th>SOURCE AUTO/CRE ID</th>
						  <th>SCENARIO</th>
						  <th>MESSAGE</th>
						  <th>STATUS</th>
						 
						</tr>
						  </thead>
					  <tbody>
							
						</tbody>
					  
					</table>
					
				</div>
				
				</div>
			</div>
			<div class="tab-pane fade" id="tab4primary">
			  <div class="row">
					<div class="col-sm-6">
				  <div class="form-group">
						<table class="table table-condensed table-responsive table-user-information">
					  <tbody>
							
						</tbody>
					  
					</table>
					  </div>
				</div>
				  </div>
			</div>
			<div class="tab-pane fade" id="tab5primary">
			  <div class="row">
					<div class="col-sm-6">
				  <div class="form-group">
						<table class="table table-condensed table-responsive table-user-information" id="complaint_history">
					  <thead>
							<tr>
						  <th>COMPLAINT No.</th>
						  <th>ISSUE DATE</th>
						  <th>SOURCE</th>
						  <th>FUNCTION</th>
						  <th>CATEGORY</th>
						  <th>STATUS</th>
						 
						</tr>
						  </thead>
					  <tbody>
							
						</tbody>
					  
					</table>
					  </div>
				</div>
				  </div>
			</div>
			  </div>
		</div>
		  </div>
	</div>
	  </div>
</div>
  </div>
</div>

    <div class="modal fade" id="myModalAddPhone" role="dialog">
    <div class="modal-dialog modal-xs">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*1244.102*/routes/*1244.108*/.Assets.at("images/close1.png")),format.raw/*1244.139*/(""""></button>
          <h4 class="modal-title">ADD PHONE NUMBER</h4>
        </div>
		
		
        <div class="modal-body">
		<div class="row">
        <div class="col-xs-12">
        <div class="col-xs-4">
        <input type="text" name="myPhoneNum" class="form-control numberOnly" maxlength="10" min="10" max="10" id="myPhoneNum" >
      </div>
        </div>
        </div>
		</div>
        <div class="modal-footer">
   <button type="button" class="btn btn-default" data-dismiss="modal" id="savePhoneCust">SAVE</button>
        </div>
      </div>
      
    </div>
  </div>
   <!--------------------------------Side bar popup start------------------------------------------>
  <div id="fixedsocial">
    <div class="facebookflat" data-toggle="modal" data-target="#myModal1" >
	<!-- <img src='"""),_display_(/*1268.18*/routes/*1268.24*/.Assets.at("images/car.png")),format.raw/*1268.52*/("""' style="width: 50px;" /> -->
	<img src='"""),_display_(/*1269.13*/routes/*1269.19*/.Assets.at("images/sports-car.jpg")),format.raw/*1269.54*/("""' style="width: 50px; border-radius: 5px;" data-toggle="tooltip" data-placement="left" title="Vehicle" />
	</div>
    <div class="twitterflat" data-toggle="modal" data-target="#myModal2" >
	<img src='"""),_display_(/*1272.13*/routes/*1272.19*/.Assets.at("images/service.jpg")),format.raw/*1272.51*/("""' style="width: 50px;border-radius: 5px;" data-toggle="tooltip" data-placement="left" title="Service" onclick="ajaxCallServiceLoadOfCustomer();"/>
	<!-- <img src='"""),_display_(/*1273.18*/routes/*1273.24*/.Assets.at("images/car-insurance.png")),format.raw/*1273.62*/("""' style="width: 50px;" /> -->
	</div> 
     <div class="twitterflat1" data-toggle="modal" data-target="#myModal3" >
	 <img src='"""),_display_(/*1276.14*/routes/*1276.20*/.Assets.at("images/car-insu.jpg")),format.raw/*1276.53*/("""' style="width: 50px; border-radius: 5px;" data-toggle="tooltip" data-placement="left" title="Insurance"/> 
	 <!-- <img src='"""),_display_(/*1277.19*/routes/*1277.25*/.Assets.at("images/INSURANCE1.png")),format.raw/*1277.60*/("""' style="width: 50px;" /> --> 
	 </div> 
      
</div>
<div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding: 5px; text-align: center;">
          <button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*1287.102*/routes/*1287.108*/.Assets.at("images/close1.png")),format.raw/*1287.139*/(""""></button>
          <h4 class="modal-title" style="text-align: center;"><b>VEHICLE INFORMATION<b /></h4>
        </div>
        <div class="modal-body">
		<div class="table table-responsive">
          <table id="example700" class = "table table-bordered">
 
  <thead class="makefixed">
				   <tr>
				  <th style="width: 11%;">Model</th>
			   <th style="width: 11%;">Variant</th>
				<th style="width: 11%;">Color</th>
			   <th style="width: 12%;">Registration&nbsp;No</th>
				<th style="width: 11%;">Engine&nbsp;No</th>
				<th style="width: 11%;">Chassis&nbsp;No</th>
				  <th style="width: 11%;">Sale&nbsp;Date</th>
				<th style="width: 11%;">Fuel&nbsp;Type</th>
				</tr>
			  </thead>
			  <tbody>
			   
			"""),_display_(/*1308.5*/for(post <- customerData.getVehicles()) yield /*1308.44*/ {_display_(Seq[Any](format.raw/*1308.46*/("""
			"""),format.raw/*1309.4*/("""<tr>
			  <td style="text-align:center"><input type="text-primary" id="model" value=""""),_display_(/*1310.82*/post/*1310.86*/.getModel()),format.raw/*1310.97*/(""""  style="border:none;text-align:center" readonly></td>
			  <td style="text-align:center"><input type="text-primary" id="variant" value=""""),_display_(/*1311.84*/post/*1311.88*/.getVariant()),format.raw/*1311.101*/("""" style="border:none;text-align:center" readonly></td>
			  <td style="text-align:center"><input type="text-primary" id="color" value=""""),_display_(/*1312.82*/post/*1312.86*/.getColor()),format.raw/*1312.97*/("""" style="border:none;text-align:center" readonly></td>
			  <td style="text-align:center">
			  <div class="col-sm-12">
			  <div class="col-sm-8">
			  <input type="text-primary" class="vehicalRegNo" id="vehicalRegNo" value=""""),_display_(/*1316.80*/post/*1316.84*/.getVehicleRegNo()),format.raw/*1316.102*/("""" style="border:none;margin-right: 15px;" readonly>
				</p>
				</div>
				<div class="col-sm-2">
				  <input type="button" value="+"  id="editRegistrationno" style="color: rgb(255, 255, 255); background-color: rgba(0, 166, 90, 0.85098);">
				 </div>
				 <div class="col-sm-2">
				  <input type="button" class="btn btn-success btn-xs" value="Add" id="updateRegistrationno" onmouseenter="ajaxAddRegistrationno();" style="display:none;">
				</div>
			  </div></td>
			  <td style="text-align:center"> 
			  <div class="col-sm-12">
			   <div class="col-sm-8">
			  <input type="text-primary" id="engineNo" value=""""),_display_(/*1329.55*/post/*1329.59*/.getEngineNo()),format.raw/*1329.73*/("""" style="border:none;margin-right: 15px;" readonly>
				</p>
				  </div>
			<!-- <div class="col-sm-2">
				  <input type="button" value="+" id="editEngineno" style="color: rgb(255, 255, 255); background-color: rgba(0, 166, 90, 0.85098);">
				</div>  <div class="col-sm-2">
				  <input type="button" class="btn btn-success btn-xs" value="Add" id="updateEngineno" onmouseenter="ajaxAddEngineno();" style="display:none;">
				</div>-->
				</div></td>
			  <td style="text-align:center"><div class="col-sm-12 ">
			   <div class="col-sm-8">
			  <input type="text-primary" id="chassisNo" value=""""),_display_(/*1340.56*/post/*1340.60*/.getChassisNo()),format.raw/*1340.75*/("""" style="border:none;margin-right: 15px;" readonly>
				 </div>
				<!--<div class="col-sm-2">
				  <input type="button" value="+" id="editChassisno" style="color: rgb(255, 255, 255); background-color: rgba(0, 166, 90, 0.85098);">
				  </div>  
				  <div class="col-sm-2">  
				  <input type="button" class="btn btn-success btn-xs" value="Add" id="updateChassisno" onmouseenter="ajaxAddChassisno();" style="display:none;">
				</div>-->
				</div></td>
			
			  <td style="text-align:center"><input type="text-primary" id="saleDate" value=""""),_display_(/*1350.85*/post/*1350.89*/.getSaleDate()),format.raw/*1350.103*/("""" style="border:none;text-align:center" readonly></td>
			  <td style="text-align:center"><input type="text-primary" id="fuelType" value=""""),_display_(/*1351.85*/post/*1351.89*/.getFuelType()),format.raw/*1351.103*/("""" style="border:none;text-align:center" readonly></td>
			</tr>			""")))}),format.raw/*1352.13*/("""
				"""),format.raw/*1353.5*/("""</tbody>
</table>
</div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
  <!-- Modal -->
  <div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header" style="padding: 5px; text-align: center;">
          <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1372.80*/routes/*1372.86*/.Assets.at("images/close1.png")),format.raw/*1372.117*/(""""></button>
          <h4 class="modal-title">SERVICE INFORMATION</h4>
        </div>
        <div class="modal-body"  style="padding: 5px;">
		<div class="table table-responsive">
          <table id="example800" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
 
   <thead class="makefixed">
                <tr>
						<th>Repair Order&nbsp;Date</th>
						<th>Repair Order&nbsp;No.</th>
						<th>Bill&nbsp;Date</th>
						<th>Kilometer</th>
						<th>Model</th>
						<th>Registration No.</th>
						<th>Service Type</th>
						<th>Menu Code Description</th>
						<th>Service Advisor Name</th>
						<th>Customer Name</th>
						<th>Group (Category)</th>
						<th>Part Amount</th>
						<th>Labour Amount</th>
						<th>Total Amount</th> 
					</tr>
                  </thead>
				   <tbody>
                                   
                    </tbody>
			  
</table>

</div>
        </div>
     
      </div>
      
    </div>
  </div>
  
  <!-- Modal -->
  <div class="modal fade" id="myModal3" role="dialog">
    <div class="modal-dialog modal-lg" >
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" class="crossBtnCorner"><img src=""""),_display_(/*1418.102*/routes/*1418.108*/.Assets.at("images/close1.png")),format.raw/*1418.139*/(""""></button>
          <h4 class="modal-title" style="tex-align:center;"><b>INSURANCE INFORMATION</b></h4>
        </div>
        <div class="modal-body">
		<div class="table table-responsive">
          <table id="" class = "table table-bordered">
	 
	   <thead class="makefixed">
					  <tr>
					<th>Policy Issue Date</th>
					<th>Policy Number</th>
					<th>Policy type</th>
					<th>Class</th>
					<th>Add-On</th>
					<th>Due Date</th>
					<th>Gross Premium</th>
					<th>IDV</th>
					<th>OD(%)</th>
					<th>Basic OD</th>
					<th>NCB(%)</th>
					<th>NCB Value</th>
					<th>Discount</th>
					<th>OD Premium</th>
					<th>TP Premium</th>
					<th>PA Premium</th>
					<th>Legal Liability</th>
					<th>Net Liability</th>
					<th>Add-On Premium</th>
					<th>Other Premium</th>
					<th>Net Premium</th>
					<th>Tax</th>
					<!--<th>Gross Premium</th>
					<th>Coverage&nbsp;Period</th>
					<th>Policy&nbsp;Due&nbsp;Date</th>
					<th>Insurance&nbsp;Company</th>
					<th>NCB(%age)</th>
					<th>NCB&nbsp;Amount(Rs.)</th>
					<th>PREMIUM</th>
					<th>Service&nbsp;Tax</th>
					<th>Total&nbsp;Premium</th>
					<th>Last&nbsp;Renewal&nbsp;By</th>-->
					
					</tr>
				  </thead>
				<tbody>
				"""),_display_(/*1463.6*/for(insuranceData <-customerData.getInsurances()) yield /*1463.55*/{_display_(Seq[Any](format.raw/*1463.56*/("""
				 
				  """),format.raw/*1465.7*/("""<tr>
				  <td><a data-toggle="modal" data-target="#otherdetails" ><i class="fa fa-info-circle" data-toggel="tooltip" title="Other Details" style="font-size:30px;color:red;"></i></a></td>
					<td>"""),_display_(/*1467.11*/insuranceData/*1467.24*/.getPolicyDueDateStr()),format.raw/*1467.46*/("""</td>
					<td>"""),_display_(/*1468.11*/insuranceData/*1468.24*/.getPolicyNo()),format.raw/*1468.38*/("""</td>
					<td>"""),_display_(/*1469.11*/insuranceData/*1469.24*/.getInsuranceCompanyName()),format.raw/*1469.50*/("""</td>
					<td>"""),_display_(/*1470.11*/insuranceData/*1470.24*/.getPolicyType()),format.raw/*1470.40*/("""</td>
					<td>"""),_display_(/*1471.11*/insuranceData/*1471.24*/.getClassType()),format.raw/*1471.39*/("""</td>
					<td>"""),_display_(/*1472.11*/insuranceData/*1472.24*/.getAddOn()),format.raw/*1472.35*/("""</td>
					<td>"""),_display_(/*1473.11*/insuranceData/*1473.24*/.getPolicyDueDate()),format.raw/*1473.43*/("""</td>
					<td>"""),_display_(/*1474.11*/insuranceData/*1474.24*/.getGrossPremium()),format.raw/*1474.42*/("""</td>
					<td>"""),_display_(/*1475.11*/insuranceData/*1475.24*/.getIdv()),format.raw/*1475.33*/("""</td>
					<td>"""),_display_(/*1476.11*/insuranceData/*1476.24*/.getOdPercentage()),format.raw/*1476.42*/("""</td>
					 <td>"""),_display_(/*1477.12*/insuranceData/*1477.25*/.getNcBPercentage()),format.raw/*1477.44*/("""</td>
					<td>"""),_display_(/*1478.11*/insuranceData/*1478.24*/.getNcBAmountStr()),format.raw/*1478.42*/("""</td>
					<td>"""),_display_(/*1479.11*/insuranceData/*1479.24*/.getDiscountPercentage()),format.raw/*1479.48*/("""</td>
					<td>"""),_display_(/*1480.11*/insuranceData/*1480.24*/.getODpremium()),format.raw/*1480.39*/("""</td>
					<td>"""),_display_(/*1481.11*/insuranceData/*1481.24*/.getaPPremium()),format.raw/*1481.39*/("""</td>
					<td>"""),_display_(/*1482.11*/insuranceData/*1482.24*/.getpAPremium()),format.raw/*1482.39*/("""</td>
					<td>"""),_display_(/*1483.11*/insuranceData/*1483.24*/.getLegalLiability()),format.raw/*1483.44*/("""</td>
					<td>"""),_display_(/*1484.11*/insuranceData/*1484.24*/.getNetLiability()),format.raw/*1484.42*/("""</td>
					<td>"""),_display_(/*1485.11*/insuranceData/*1485.24*/.getAdd_ON_Premium()),format.raw/*1485.44*/("""</td>
					<td>"""),_display_(/*1486.11*/insuranceData/*1486.24*/.getOtherPremium()),format.raw/*1486.42*/("""</td>
					<td>"""),_display_(/*1487.11*/insuranceData/*1487.24*/.getPremiumAmountStr()),format.raw/*1487.46*/("""</td>
					<td>"""),_display_(/*1488.11*/insuranceData/*1488.24*/.getServiceTax()),format.raw/*1488.40*/("""</td>
				  </tr>
				  """)))}),format.raw/*1490.8*/("""
					"""),format.raw/*1491.6*/("""</tbody> 
	</table>
        </div>
		</div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  <!--------------------------------Side bar popup end------------------------------------------>
  
  <!--Popup For Driver Allocation-->
<div class="modal fade" id="DriverAllcationPOPUp" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
	   <div class="modal-header" style="text-align:center">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Driver Allocation</h4>																													
        </div>
             <div class="modal-body" style="padding:4px;">
  <div class="panel panel-primary" >
	<div class="panel-heading" style="padding:0px;"> </div>
	<div class="panel-body">
			<div class="col-md-12">
			
			<div class="form-inline">
  <div class="form-group">
<label for="comments"><b> BOOKING DATE :<b></label>
<input type="text" class="form-control datepicMYDropDown" id="changeserviceBookingDate" name="pickupDate" onchange="AssignBtnBkreviewScript();" readonly>
  </div>
  </div>
<br />
</div>

                <div class="col-md-12">
                <table class="table table-responsive">
                  <tbody>
                    <tr>
                      <td><strong> New Booking Date: </strong></td>
                      <td class="text-primary" id="newbookingdate"></td>
                    </tr>
                     <tr>
                      <td><strong> New Driver Name:</strong></td>
                      <td class="text-primary" id="newDriver"></td>
                    </tr>
					<tr>
                      <td><strong>New Time Slot:</strong></td>
                      <td class="text-primary" id="newTimeSlot"></td>
                    </tr>
                  </tbody>
                </table>
                </div>
              
	 <div>
        <input type="hidden" id="tempValue" value="0">
        <input type="hidden" id="tempIncreValue" value="0">
        <input type="hidden" name="time_From" id="startValue" value="0">
        <input type="hidden" name="time_To" id="endValue" value="0">
        <input type="hidden" name="driverId" id="driverValue" value="0">
      </div>
  <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" id="tableID"  style="cursor: pointer;">
    <thead>  
        
    </thead>
    <tbody> 
           
    </tbody>
  </table>
  <button type="button" id="allcateSubmit" class="btn btn-primary pull-right" data-dismiss="modal">Submit</button>
</div>
</div>
 </div>
       </div>
      
    </div>
  </div>
  
  <!-- Adress pop up is -->
  <div class="modal fade" id="AddNewAddressPopup" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1579.78*/routes/*1579.84*/.Assets.at("images/close1.png")),format.raw/*1579.115*/(""""></button>
        <h4 class="modal-title"><b style="text-align:center; color:Red;">Add New Address</b></h4>
      </div>
      <div class="modal-body">
	 
		  <div class="row" id="AddAddressDiv">
		   <div class="col-md-12">
			<div class="col-md-6">
			  <div class="form-group">
				<label for="comments"><b>Add Address1<b></label>
				<textarea class="form-control" rows="1" id="AddAddress1MMS" name="address1New" ></textarea>
			  </div>
			</div>
			<div class="col-md-6">
			  <div class="form-group">
				<label for="comments"><b>Add Address2<b></label>
				<textarea class="form-control" rows="1" id="AddAddress2MMS" name="address2New" ></textarea>
			  </div>
			</div>
			
			<div class="col-md-6">
			  <div class="form-group">
				<label for="comments"><b>State<b></label>
				<select class="form-control" name="stateNew" id="AddAddrMMSState" onchange="getCityByStateSelection('AddAddrMMSState','cityInputPopup3');">
				  <option value="0">--SELECT--</option>
		"""),_display_(/*1604.4*/for(states <- statesList) yield /*1604.29*/{_display_(Seq[Any](format.raw/*1604.30*/("""
           
          """),format.raw/*1606.11*/("""<option value=""""),_display_(/*1606.27*/states),format.raw/*1606.33*/("""">"""),_display_(/*1606.36*/states),format.raw/*1606.42*/("""</option>
            """)))}),format.raw/*1607.14*/("""
		
				"""),format.raw/*1609.5*/("""</select>
			  </div>
			</div>
			
			<div class="col-md-6">
			  <div class="form-group">
				<label for="comments"><b>City<b></label>
				<select class="form-control" name="cityNew" placeholder="City" id="cityInputPopup3">	
                                
                              
        
                                    </select>
			  </div>
			</div>
			<div class="col-md-6">
			  <div class="form-group">
				<label for="comments"><b>PinCode<b></label>
				<input type="text" class="form-control numberOnly" name="pincodeNew" value="0" id="PinCode1MMS" maxlength="6" />
			  </div>
			</div>
			
			</div>
		   
		  </div>
  

<div class="modal-footer">
  <button type="button" class="btn btn-success" id="AddAddrMMSSave" data-dismiss="modal">Save</button>
</div>
</div>
    </div>
  </div>
</div>


<!-- Incomplete Survey -->

<div class="modal fade" id="IncompleteSurvey" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Incomplete Survey</h4>
        </div>
        <div class="modal-body">
         <div class="row">
				  <div class="col-md-3">
					<div class="form-group">
					  <label for="followUpDate">Follow Up Date</label>
					  <input type="text" name="psfAppointmentDate" class="showOnlyFutureDate form-control" id="incompleteDate" readonly>
					</div>
				  </div>
				  <div class="col-md-3">
					<div class="form-group">
					  <label for="followUpTime">Follow Up Time</label>
					  <input type="text" name="psfAppointmentTime" class="single-input form-control" id="" readonly>
					</div>
				  </div>
				   <div class="col-md-6">
					<div class="form-group">
					  <label for="followUpTime">Remarks</label>
					  <textarea type="text" name="remarksList[0]" class="form-control" id=""></textarea>
					</div>
				  </div>
				
        </div>
		
        </div>
<div class="modal-footer">
          <button type="submit" class="btn btn-success" name="" data-dismiss="modal">Submit</button>
        </div>      
      </div>
      
    </div>
  </div>
  
  
""")))}),format.raw/*1688.2*/("""


"""),format.raw/*1691.1*/("""<script src=""""),_display_(/*1691.15*/routes/*1691.21*/.Assets.at("javascripts/disposition.js")),format.raw/*1691.61*/("""" type="text/javascript"></script>


""")))}),format.raw/*1694.2*/("""
"""))
      }
    }
  }

  def render(oem:String,typeOfPSF:Long,dispositionHistory:Long,interactionid:Long,uniqueid:Long,dealercode:String,dealerName:String,userName:String,customerData:Customer,vehicleData:Vehicle,userData:WyzUser,latestService:Service,smsTemplates:List[SMSTemplate],complaintOFCust:List[String],statesList:List[String],lastPSFAssignStatus:PSFAssignedInteraction,psfDayPage:Html): play.twirl.api.HtmlFormat.Appendable = apply(oem,typeOfPSF,dispositionHistory,interactionid,uniqueid,dealercode,dealerName,userName,customerData,vehicleData,userData,latestService,smsTemplates,complaintOFCust,statesList,lastPSFAssignStatus,psfDayPage)

  def f:((String,Long,Long,Long,Long,String,String,String,Customer,Vehicle,WyzUser,Service,List[SMSTemplate],List[String],List[String],PSFAssignedInteraction,Html) => play.twirl.api.HtmlFormat.Appendable) = (oem,typeOfPSF,dispositionHistory,interactionid,uniqueid,dealercode,dealerName,userName,customerData,vehicleData,userData,latestService,smsTemplates,complaintOFCust,statesList,lastPSFAssignStatus,psfDayPage) => apply(oem,typeOfPSF,dispositionHistory,interactionid,uniqueid,dealercode,dealerName,userName,customerData,vehicleData,userData,latestService,smsTemplates,complaintOFCust,statesList,lastPSFAssignStatus,psfDayPage)

  def ref: this.type = this

}


}

/**/
object commonPSFDispositionPage extends commonPSFDispositionPage_Scope0.commonPSFDispositionPage
              /*
                  -- GENERATED --
                  DATE: Tue Mar 20 09:34:20 IST 2018
                  SOURCE: D:/CRMFORDAUTOSHERPA/crmford/app/views/commonPSFDispositionPage.scala.html
                  HASH: 13ea016ce77e0f6ec1b180442153692fe38a2298
                  MATRIX: 930->1|1386->361|1414->364|1485->427|1524->429|1554->433|1601->453|1629->454|1658->457|1709->482|1736->483|1767->487|1804->496|1833->497|1863->500|1913->523|1941->524|1969->525|2006->536|2020->542|2094->607|2133->608|2161->609|2218->639|2233->645|2279->670|2362->726|2481->823|2576->891|2619->913|2712->979|2751->997|2843->1062|2893->1091|2979->1150|3008->1158|3085->1208|3183->1284|3259->1333|3316->1369|3396->1422|3430->1435|3515->1493|3554->1511|3630->1560|3660->1569|3730->1612|3754->1615|4084->1918|4300->2113|4329->2114|4503->2261|4631->2368|4904->2614|4977->2665|5286->2947|5345->2990|5384->2991|5434->3014|5845->3404|5887->3415|5931->3431|6784->4257|6854->4305|7000->4423|7070->4471|7102->4475|7132->4483|7164->4487|7186->4499|7216->4507|8141->5404|8292->5534|8321->5535|8495->5682|8555->5720|8682->5819|8731->5845|9006->6091|9428->6492|9457->6493|9674->6682|9737->6722|9768->6723|9801->6727|9862->6765|9983->6858|10041->6893|10253->7077|10309->7110|10529->7302|10574->7337|10614->7338|10677->7373|10742->7421|10782->7422|10828->7439|10976->7558|11053->7624|11094->7625|11140->7643|11164->7657|11196->7667|11234->7685|11248->7689|11288->7690|11334->7707|11386->7727|11416->7728|11479->7759|11529->7777|11562->7782|11929->8121|11945->8127|11996->8155|12166->8298|12197->8319|12237->8320|12289->8344|12327->8354|12380->8385|12442->8419|12489->8443|12550->8476|12598->8502|12647->8523|12693->8547|12773->8599|12838->8642|13047->8822|13100->8852|13170->8891|13205->8898|13494->9159|13510->9165|13567->9199|13933->9537|13987->9569|14075->9629|14130->9662|14213->9717|14274->9756|14532->9986|14548->9992|14609->10030|14767->10161|14798->10182|14838->10183|14867->10184|15044->10332|15068->10345|15115->10369|15166->10391|15190->10404|15232->10423|15282->10444|15306->10457|15350->10478|15410->10510|15433->10523|15467->10535|15516->10557|15626->10646|15659->10648|15728->10690|16512->11453|16541->11454|16590->11475|16710->11572|16760->11593|16882->11692|16942->11724|17055->11814|17105->11837|17243->11954|17272->11955|17529->12184|17545->12190|17597->12220|18355->12950|18371->12956|18439->13001|18832->13366|18855->13379|18897->13398|18989->13462|19012->13475|19056->13497|19328->13741|19344->13747|19402->13783|19723->14075|19749->14090|19779->14097|19970->14260|19995->14275|20024->14282|20134->14364|20159->14379|20188->14386|20290->14460|20315->14475|20344->14482|20796->14905|20813->14911|20867->14942|21275->15322|21297->15334|21338->15352|21597->15583|21620->15595|21659->15611|22018->15941|22164->16066|22197->16072|22343->16197|22376->16203|22522->16328|22990->16767|23136->16892|23169->16898|23315->17023|23348->17029|23494->17154|23939->17570|24079->17689|24112->17695|24252->17814|24285->17820|24425->17939|25477->18963|25537->19006|25577->19007|25628->19030|25935->19315|25978->19326|26023->19342|26427->19718|26473->19741|26720->19960|26779->19996|27200->20389|27260->20432|27300->20433|27351->20456|27658->20741|27701->20752|27746->20768|28576->21570|28634->21605|29638->22581|29694->22614|30242->23133|30305->23173|30568->23407|30629->23445|30909->23696|30960->23724|30992->23727|31043->23755|31716->24400|31733->24406|31787->24437|32616->25238|32742->25341|32936->25507|33063->25611|33285->25804|33421->25917|34073->26541|34090->26547|34144->26578|35042->27448|35168->27551|35361->27716|35488->27820|35706->28009|35842->28122|36491->28743|36508->28749|36562->28780|36910->29099|37038->29204|37259->29396|37387->29501|37603->29688|37731->29793|37956->29989|38076->30086|38307->30288|38428->30386|38664->30593|38800->30706|39526->31403|39543->31409|39597->31440|40561->32376|40591->32384|41181->32946|41211->32954|41241->32955|41755->33441|41772->33447|41826->33478|42427->34050|42444->34056|42498->34087|42726->34287|42748->34299|42788->34317|42819->34319|43800->35271|43817->35277|43871->35308|44061->35471|44108->35501|44148->35502|44182->35508|44252->35550|44298->35574|44340->35588|44387->35612|44438->35634|44487->35660|44533->35675|44564->35678|44699->35785|44750->35814|44785->35822|44832->35852|44872->35853|44914->35867|44964->35889|45003->35918|45043->35919|45082->35926|45121->35937|45168->35961|45237->36002|45283->36026|45338->36053|45386->36079|45478->36143|45524->36167|45565->36180|45612->36204|45708->36271|45761->36301|45855->36367|45901->36391|46011->36470|46041->36472|46727->37129|46744->37135|46798->37166|55330->45670|55363->45680|55401->45689|55786->46045|55803->46051|55870->46095|59974->50169|59992->50175|60047->50206|60873->51003|60890->51009|60941->51037|61012->51079|61029->51085|61087->51120|61317->51321|61334->51327|61389->51359|61582->51523|61599->51529|61660->51567|61818->51696|61835->51702|61891->51735|62046->51861|62063->51867|62121->51902|62544->52295|62562->52301|62617->52332|63368->53055|63425->53094|63467->53096|63500->53100|63615->53186|63630->53190|63664->53201|63832->53340|63847->53344|63884->53357|64049->53493|64064->53497|64098->53508|64354->53735|64369->53739|64411->53757|65058->54375|65073->54379|65110->54393|65736->54990|65751->54994|65789->55009|66363->55554|66378->55558|66416->55572|66584->55711|66599->55715|66637->55729|66737->55796|66771->55801|67358->56359|67375->56365|67430->56396|68771->57707|68789->57713|68844->57744|70087->58959|70154->59008|70195->59009|70237->59022|70464->59220|70488->59233|70533->59255|70578->59271|70602->59284|70639->59298|70684->59314|70708->59327|70757->59353|70802->59369|70826->59382|70865->59398|70910->59414|70934->59427|70972->59442|71017->59458|71041->59471|71075->59482|71120->59498|71144->59511|71186->59530|71231->59546|71255->59559|71296->59577|71341->59593|71365->59606|71397->59615|71442->59631|71466->59644|71507->59662|71553->59679|71577->59692|71619->59711|71664->59727|71688->59740|71729->59758|71774->59774|71798->59787|71845->59811|71890->59827|71914->59840|71952->59855|71997->59871|72021->59884|72059->59899|72104->59915|72128->59928|72166->59943|72211->59959|72235->59972|72278->59992|72323->60008|72347->60021|72388->60039|72433->60055|72457->60068|72500->60088|72545->60104|72569->60117|72610->60135|72655->60151|72679->60164|72724->60186|72769->60202|72793->60215|72832->60231|72889->60256|72924->60262|76049->63358|76066->63364|76121->63395|77127->64373|77170->64398|77211->64399|77264->64422|77309->64438|77338->64444|77370->64447|77399->64453|77455->64476|77492->64484|79743->66703|79775->66706|79818->66720|79835->66726|79898->66766|79968->66804
                  LINES: 27->1|32->1|34->3|34->3|34->3|36->5|37->6|37->6|38->7|39->8|39->8|41->10|41->10|41->10|42->11|43->12|43->12|44->13|46->15|46->15|46->15|46->15|47->16|47->16|47->16|47->16|48->17|48->17|49->18|49->18|50->19|50->19|51->20|51->20|52->21|52->21|53->22|53->22|54->23|54->23|55->24|55->24|56->25|56->25|57->26|57->26|58->27|58->27|68->37|76->45|76->45|78->47|82->51|88->57|88->57|90->59|90->59|90->59|92->61|100->69|101->70|102->71|113->82|113->82|114->83|114->83|114->83|114->83|114->83|114->83|114->83|125->94|127->96|127->96|130->99|130->99|131->100|131->100|136->105|144->113|144->113|149->118|149->118|149->118|149->118|149->118|150->119|150->119|155->124|155->124|162->131|162->131|162->131|164->133|164->133|164->133|165->134|165->134|165->134|165->134|166->135|166->135|166->135|167->136|167->136|167->136|168->137|169->138|169->138|170->139|171->140|172->141|183->152|183->152|183->152|185->154|185->154|185->154|189->158|189->158|189->158|189->158|189->158|190->159|190->159|190->159|190->159|191->160|191->160|192->161|192->161|195->164|196->165|203->172|203->172|203->172|206->175|206->175|207->176|207->176|208->177|208->177|217->186|217->186|217->186|219->188|219->188|219->188|220->189|220->189|220->189|220->189|220->189|220->189|220->189|220->189|220->189|220->189|221->190|221->190|221->190|222->191|225->194|226->195|228->197|245->214|246->215|246->215|246->215|246->215|246->215|247->216|247->216|249->218|252->221|253->222|264->233|264->233|264->233|283->252|283->252|283->252|287->256|287->256|287->256|288->257|288->257|288->257|297->266|297->266|297->266|299->268|299->268|299->268|301->270|301->270|301->270|302->271|302->271|302->271|303->272|303->272|303->272|317->286|317->286|317->286|326->295|326->295|326->295|332->301|332->301|332->301|342->311|346->315|347->316|351->320|352->321|356->325|364->333|368->337|369->338|373->342|374->343|378->347|385->354|389->358|390->359|394->363|395->364|399->368|423->392|423->392|423->392|425->394|430->399|431->400|432->401|439->408|439->408|443->412|443->412|452->421|452->421|452->421|454->423|459->428|460->429|461->430|476->445|476->445|495->464|495->464|506->475|506->475|510->479|510->479|514->483|514->483|514->483|514->483|544->513|544->513|544->513|564->533|564->533|567->536|567->536|572->541|572->541|590->559|590->559|590->559|611->580|611->580|614->583|614->583|619->588|619->588|637->606|637->606|637->606|644->613|644->613|648->617|648->617|651->620|651->620|656->625|656->625|659->628|659->628|664->633|664->633|685->654|685->654|685->654|714->683|714->683|731->700|731->700|731->700|749->718|749->718|749->718|771->740|771->740|771->740|775->744|775->744|775->744|775->744|800->769|800->769|800->769|805->774|805->774|805->774|806->775|806->775|806->775|806->775|806->775|806->775|806->775|807->776|809->778|811->780|811->780|812->781|812->781|812->781|815->784|815->784|815->784|815->784|815->784|815->784|815->784|816->785|816->785|817->786|817->786|818->787|818->787|818->787|818->787|818->787|818->787|820->789|820->789|825->794|826->795|845->814|845->814|845->814|1114->1083|1114->1083|1118->1087|1127->1096|1127->1096|1127->1096|1275->1244|1275->1244|1275->1244|1299->1268|1299->1268|1299->1268|1300->1269|1300->1269|1300->1269|1303->1272|1303->1272|1303->1272|1304->1273|1304->1273|1304->1273|1307->1276|1307->1276|1307->1276|1308->1277|1308->1277|1308->1277|1318->1287|1318->1287|1318->1287|1339->1308|1339->1308|1339->1308|1340->1309|1341->1310|1341->1310|1341->1310|1342->1311|1342->1311|1342->1311|1343->1312|1343->1312|1343->1312|1347->1316|1347->1316|1347->1316|1360->1329|1360->1329|1360->1329|1371->1340|1371->1340|1371->1340|1381->1350|1381->1350|1381->1350|1382->1351|1382->1351|1382->1351|1383->1352|1384->1353|1403->1372|1403->1372|1403->1372|1449->1418|1449->1418|1449->1418|1494->1463|1494->1463|1494->1463|1496->1465|1498->1467|1498->1467|1498->1467|1499->1468|1499->1468|1499->1468|1500->1469|1500->1469|1500->1469|1501->1470|1501->1470|1501->1470|1502->1471|1502->1471|1502->1471|1503->1472|1503->1472|1503->1472|1504->1473|1504->1473|1504->1473|1505->1474|1505->1474|1505->1474|1506->1475|1506->1475|1506->1475|1507->1476|1507->1476|1507->1476|1508->1477|1508->1477|1508->1477|1509->1478|1509->1478|1509->1478|1510->1479|1510->1479|1510->1479|1511->1480|1511->1480|1511->1480|1512->1481|1512->1481|1512->1481|1513->1482|1513->1482|1513->1482|1514->1483|1514->1483|1514->1483|1515->1484|1515->1484|1515->1484|1516->1485|1516->1485|1516->1485|1517->1486|1517->1486|1517->1486|1518->1487|1518->1487|1518->1487|1519->1488|1519->1488|1519->1488|1521->1490|1522->1491|1610->1579|1610->1579|1610->1579|1635->1604|1635->1604|1635->1604|1637->1606|1637->1606|1637->1606|1637->1606|1637->1606|1638->1607|1640->1609|1719->1688|1722->1691|1722->1691|1722->1691|1722->1691|1725->1694
                  -- GENERATED --
              */
          