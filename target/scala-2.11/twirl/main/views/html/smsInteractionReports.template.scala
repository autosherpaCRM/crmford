
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object smsInteractionReports_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class smsInteractionReports extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template6[String,String,String,String,List[String],List[String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(fromDateValue:String,toDateValue:String,user:String,dealerName:String,allUsers:List[String],locations:List[String]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.118*/("""

"""),_display_(/*4.2*/mainPageCREManger("AutoSherpaCRM",user,dealerName)/*4.52*/ {_display_(Seq[Any](format.raw/*4.54*/("""

"""),format.raw/*6.1*/("""<input type="hidden" name="fromDateToAss" value=""""),_display_(/*6.51*/fromDateValue),format.raw/*6.64*/("""">
<input type="hidden" name="toDateToAss" value=""""),_display_(/*7.49*/toDateValue),format.raw/*7.60*/("""">
<div class="panel panel-primary">
        <div class="panel-heading"><strong>Select Date Range To view SMS Interactions</strong></div>
        <div class="panel-body"> 
                     """),_display_(/*11.23*/helper/*11.29*/.form(action = routes.AllCallInteractionController.downloadSMSHistoryReport)/*11.105*/ {_display_(Seq[Any](format.raw/*11.107*/("""
        
  """),format.raw/*13.3*/("""<div class="row">
  <div class="col-md-2">   
      <div class="form-group">
        
<label>Call Date From :<span style="color:red">*</span></label>    
	   
		<input type="text" class="form-control datepickerFilter" placeholder="Enter Call Date From" id="frombilldaterange" name="frombilldaterange" readonly>
      </div>
      </div>
  
   <div class="col-md-2">   
      <div class="form-group">   
       <label>Call Date To :<span style="color:red">*</span></label>    
					<input type="text" class="form-control datepickerFilter" placeholder="Enter Call Date To"  id="tobilldaterange" name="tobilldaterange" readonly>
     	</div>
      </div>

     <div class="col-md-2">
   
                    <label>Select Location :</label>
                     <div class="form-group">
    	  	<select  multiple="multiple"  id="location"  name="locations[]" class="selectpicker  form-control" onchange="workshopsBasedOnLocations(this);" required> 
    	                       	     
                     	"""),_display_(/*36.24*/for(data3 <- locations) yield /*36.47*/{_display_(Seq[Any](format.raw/*36.48*/("""                       	                         
                    
                        """),format.raw/*38.25*/("""<option value=""""),_display_(/*38.41*/data3),format.raw/*38.46*/("""">"""),_display_(/*38.49*/data3),format.raw/*38.54*/("""</option>
                      	
                      	""")))}),format.raw/*40.25*/("""	  	
                    	"""),format.raw/*41.22*/("""</select>	
                    	<span type="label" id="locationERR" style="color:red"></span>	
           
      </div>
       </div>
       
       <div class="col-md-2">
   
                    <label>Select Workshops :</label>
                     <div class="form-group">
    	  	<select  multiple="multiple"  id="workshopname"  name="workshopname[]" class="form-control" onchange="cresBasedOnLocations(this);"  required> 
    	                       	     
                     	  	
                    	</select>
                    	
           
      </div>
       </div>
       
        <div class="col-md-2">
   
                    <label>Select CRE's :</label>
                     <div class="form-group">
    	  	<select  multiple="multiple"  id="crename"  name="crename[]" class="form-control"  required> 
    	                       	     
                     	  	
                    	</select>
                    	<span type="label" id="crenameERR" style="color:red"></span>	
           
      </div>
       </div>
       <div class="col-md-2">
                    <label>Select SMS Sent Status :</label>
                     <div class="form-group">
    			<select  multiple="multiple" id="all_dispositions" name="all_dispositions" class="selectpicker form-control" >
    					                       			    
						<option value="1">Success</option>
						<option value="0">Failure</option>
						 	
                      					
                	</select>
                				
            </div>
             </div>
       <!-- <div class="col-md-2">
   
                    <label>Select CRE's :</label>
                     <div class="form-group">
              <select class="selectpicker form-control"  id="crename" name="crename" multiple>
    					                      			    
                     				 	
                    	</select>
           
      </div>
       </div> --> 
</div>
<div class="col-md-12">
  <div class="form-group pull-right">
  		<button type="button" name="selectedFile" class="btn btn-sm btn-primary viewSMSReports" value="viewSMS"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;View SMS Interactions</button>		     	
  		 <button type="submit" name="downloadFile" class="btn btn-sm btn-warning"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;Download</button>
		</div>
</div> 
  </div>


""")))}),format.raw/*106.2*/("""

  """),format.raw/*108.3*/("""</div>      
<span type="label" id="viewSMSERR" style="color:red"></span>
<div class="row">
      <div class="col-lg-12">
        <div class="panel panel-primary">
          <div class="panel-heading"><b> List Of SMS Interactions</b>
          </div>
          <div class="panel-body" style="overflow:auto">
          <table class="table table-striped table-bordered table-hover"
						id="datatableSMSIntraction">
						<thead>
							<tr>

								<th>Interaction Date</th> 
                                    <th>Interaction Time</th> 
                                    <th>Message content</th>                                   
                                    <th>SMS Status</th>
                                    <th>Mobile Number</th>
                                    <th>Customer Name</th>
                                    <th>Vehicle Reg No</th>
                                    <th>User name</th>
                                    
                                  


							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
                     
          </div>
          <!-- /.panel-body --> 
        </div>
        <!-- /.panel --> 
      </div>
      <!-- /.col-lg-12 --> 
    </div>
""")))}),format.raw/*147.2*/("""
	
"""),format.raw/*149.1*/("""<script>
$(document).ready(function () """),format.raw/*150.31*/("""{"""),format.raw/*150.32*/("""
	"""),format.raw/*151.2*/("""$('.viewSMSReports').click(function()"""),format.raw/*151.39*/("""{"""),format.raw/*151.40*/("""
    	
        """),format.raw/*153.9*/("""var fromDate     = $("#frombilldaterange").val();
        var toDate       = $("#tobilldaterange").val();
       
              
        var UserIds="",j,k;
        var sentStatuses="";       
        
        myOption = document.getElementById('crename');
        console.log("here"+myOption);		        
    for (j=0;j<myOption.options.length;j++)"""),format.raw/*162.44*/("""{"""),format.raw/*162.45*/("""
        """),format.raw/*163.9*/("""if(myOption.options[j].selected)"""),format.raw/*163.41*/("""{"""),format.raw/*163.42*/("""
            """),format.raw/*164.13*/("""if(myOption.options[j].value != "Select")"""),format.raw/*164.54*/("""{"""),format.raw/*164.55*/("""
        		"""),format.raw/*165.11*/("""UserIds = UserIds + myOption.options[j].value + ",";
            """),format.raw/*166.13*/("""}"""),format.raw/*166.14*/("""else"""),format.raw/*166.18*/("""{"""),format.raw/*166.19*/("""
				"""),format.raw/*167.5*/("""UserIds ="";
            """),format.raw/*168.13*/("""}"""),format.raw/*168.14*/("""
        """),format.raw/*169.9*/("""}"""),format.raw/*169.10*/("""
    """),format.raw/*170.5*/("""}"""),format.raw/*170.6*/("""
    	"""),format.raw/*171.6*/("""if(UserIds.length > 0)"""),format.raw/*171.28*/("""{"""),format.raw/*171.29*/("""
        	"""),format.raw/*172.10*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
        	
    	"""),format.raw/*174.6*/("""}"""),format.raw/*174.7*/("""else"""),format.raw/*174.11*/("""{"""),format.raw/*174.12*/("""
    		"""),format.raw/*175.7*/("""UserIds="";
    	"""),format.raw/*176.6*/("""}"""),format.raw/*176.7*/("""

    	"""),format.raw/*178.6*/("""sentStatus = document.getElementById('all_dispositions');
    	 for (j=0;j<sentStatus.options.length;j++)"""),format.raw/*179.48*/("""{"""),format.raw/*179.49*/("""
    	        """),format.raw/*180.14*/("""if(sentStatus.options[j].selected)"""),format.raw/*180.48*/("""{"""),format.raw/*180.49*/("""
    	            """),format.raw/*181.18*/("""if(sentStatus.options[j].value != "Select")"""),format.raw/*181.61*/("""{"""),format.raw/*181.62*/("""
    	            	"""),format.raw/*182.19*/("""sentStatuses = sentStatuses + sentStatus.options[j].value + ",";
    	            """),format.raw/*183.18*/("""}"""),format.raw/*183.19*/("""else"""),format.raw/*183.23*/("""{"""),format.raw/*183.24*/("""
    	            	"""),format.raw/*184.19*/("""sentStatuses ="";
    	            """),format.raw/*185.18*/("""}"""),format.raw/*185.19*/("""
    	        """),format.raw/*186.14*/("""}"""),format.raw/*186.15*/("""
    	    """),format.raw/*187.10*/("""}"""),format.raw/*187.11*/("""	   	
    	 """),format.raw/*188.7*/("""if(sentStatuses.length > 0)"""),format.raw/*188.34*/("""{"""),format.raw/*188.35*/("""
    		 """),format.raw/*189.8*/("""sentStatuses = sentStatuses.substring(0, sentStatuses.length - 1);
         	
     	"""),format.raw/*191.7*/("""}"""),format.raw/*191.8*/("""else"""),format.raw/*191.12*/("""{"""),format.raw/*191.13*/("""
     		"""),format.raw/*192.8*/("""sentStatuses="";
     	"""),format.raw/*193.7*/("""}"""),format.raw/*193.8*/("""


        """),format.raw/*196.9*/("""console.log("sentStatuses::"+sentStatuses);
     	var urlLink = "/CREManager/getAllSMSHistoryData"
    	    
    	    $('#datatableSMSIntraction').dataTable( """),format.raw/*199.50*/("""{"""),format.raw/*199.51*/("""
    	     """),format.raw/*200.11*/(""""bDestroy": true,
    	     "processing": true,
    	     "serverSide": true,
    	     "scrollY": 400,
 	         "scrollX": true,
    	     "paging": true,
    	     "searching": false,
    	     "ordering":false,
    	     "ajax": """),format.raw/*208.19*/("""{"""),format.raw/*208.20*/("""
    		        """),format.raw/*209.15*/("""'type': 'POST',
    		        'url': urlLink,
    		        'data': """),format.raw/*211.23*/("""{"""),format.raw/*211.24*/("""    		        	
    		        	"""),format.raw/*212.16*/("""fromDate:''+fromDate,
    		        	toDate:''+toDate ,
    		        	UserIds:''+UserIds,
    		        	sentStatuses:''+sentStatuses, 		        	
    		        """),format.raw/*216.15*/("""}"""),format.raw/*216.16*/("""
        	        
    	     """),format.raw/*218.11*/("""}"""),format.raw/*218.12*/(""",
    	     "fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*219.67*/("""{"""),format.raw/*219.68*/("""
                 """),format.raw/*220.18*/("""$('td', nRow).attr('nowrap','nowrap');
                 return nRow;
                 """),format.raw/*222.18*/("""}"""),format.raw/*222.19*/("""
    	 """),format.raw/*223.7*/("""}"""),format.raw/*223.8*/(""" """),format.raw/*223.9*/(""");


    """),format.raw/*226.5*/("""}"""),format.raw/*226.6*/(""");
"""),format.raw/*227.1*/("""}"""),format.raw/*227.2*/(""");
</script>
"""))
      }
    }
  }

  def render(fromDateValue:String,toDateValue:String,user:String,dealerName:String,allUsers:List[String],locations:List[String]): play.twirl.api.HtmlFormat.Appendable = apply(fromDateValue,toDateValue,user,dealerName,allUsers,locations)

  def f:((String,String,String,String,List[String],List[String]) => play.twirl.api.HtmlFormat.Appendable) = (fromDateValue,toDateValue,user,dealerName,allUsers,locations) => apply(fromDateValue,toDateValue,user,dealerName,allUsers,locations)

  def ref: this.type = this

}


}

/**/
object smsInteractionReports extends smsInteractionReports_Scope0.smsInteractionReports
              /*
                  -- GENERATED --
                  DATE: Tue Mar 20 14:39:16 IST 2018
                  SOURCE: D:/CRMFORDAUTOSHERPA/crmford/app/views/smsInteractionReports.scala.html
                  HASH: bd674952b163d08ae56ade51313104904b238202
                  MATRIX: 824->3|1036->119|1066->124|1124->174|1163->176|1193->180|1269->230|1302->243|1380->295|1411->306|1636->504|1651->510|1737->586|1778->588|1819->602|2873->1629|2912->1652|2951->1653|3076->1750|3119->1766|3145->1771|3175->1774|3201->1779|3292->1839|3347->1866|5797->4285|5831->4291|7138->5567|7171->5572|7240->5612|7270->5613|7301->5616|7367->5653|7397->5654|7442->5671|7828->6028|7858->6029|7896->6039|7957->6071|7987->6072|8030->6086|8100->6127|8130->6128|8171->6140|8266->6206|8296->6207|8329->6211|8359->6212|8393->6218|8448->6244|8478->6245|8516->6255|8546->6256|8580->6262|8609->6263|8644->6270|8695->6292|8725->6293|8765->6304|8862->6373|8891->6374|8924->6378|8954->6379|8990->6387|9036->6405|9065->6406|9102->6415|9237->6521|9267->6522|9311->6537|9374->6571|9404->6572|9452->6591|9524->6634|9554->6635|9603->6655|9715->6738|9745->6739|9778->6743|9808->6744|9857->6764|9922->6800|9952->6801|9996->6816|10026->6817|10066->6828|10096->6829|10137->6842|10193->6869|10223->6870|10260->6879|10374->6965|10403->6966|10436->6970|10466->6971|10503->6980|10555->7004|10584->7005|10626->7019|10816->7180|10846->7181|10887->7193|11158->7435|11188->7436|11233->7452|11332->7522|11362->7523|11423->7555|11618->7721|11648->7722|11708->7753|11738->7754|11836->7823|11866->7824|11914->7843|12031->7931|12061->7932|12097->7940|12126->7941|12155->7942|12195->7954|12224->7955|12256->7959|12285->7960
                  LINES: 27->2|32->2|34->4|34->4|34->4|36->6|36->6|36->6|37->7|37->7|41->11|41->11|41->11|41->11|43->13|66->36|66->36|66->36|68->38|68->38|68->38|68->38|68->38|70->40|71->41|136->106|138->108|177->147|179->149|180->150|180->150|181->151|181->151|181->151|183->153|192->162|192->162|193->163|193->163|193->163|194->164|194->164|194->164|195->165|196->166|196->166|196->166|196->166|197->167|198->168|198->168|199->169|199->169|200->170|200->170|201->171|201->171|201->171|202->172|204->174|204->174|204->174|204->174|205->175|206->176|206->176|208->178|209->179|209->179|210->180|210->180|210->180|211->181|211->181|211->181|212->182|213->183|213->183|213->183|213->183|214->184|215->185|215->185|216->186|216->186|217->187|217->187|218->188|218->188|218->188|219->189|221->191|221->191|221->191|221->191|222->192|223->193|223->193|226->196|229->199|229->199|230->200|238->208|238->208|239->209|241->211|241->211|242->212|246->216|246->216|248->218|248->218|249->219|249->219|250->220|252->222|252->222|253->223|253->223|253->223|256->226|256->226|257->227|257->227
                  -- GENERATED --
              */
          