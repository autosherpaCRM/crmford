
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object commonDispositionPage_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class commonDispositionPage extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template20[String,List[String],Insurance,Long,String,String,String,Customer,Vehicle,List[Location],WyzUser,Service,List[SMSTemplate],List[String],List[ServiceTypes],CallInteraction,List[SpecialOfferMaster],String,Html,Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(oem:String,statesList:List[String],latestinsurance:Insurance,uniqueid:Long,dealercode:String,dealerName:String,userName:String,customerData:Customer,vehicleData:Vehicle,locationList:List[Location],userData:WyzUser,latestService:Service,smsTemplates:List[SMSTemplate],complaintOFCust:List[String],servicetypeList:List[ServiceTypes],interOfCall:CallInteraction,offersList:List[SpecialOfferMaster],typeDispo:String,dispoOut:Html,dispoInBound:Html):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.447*/("""

"""),_display_(/*3.2*/mainPageCRE("AutoSherpaCRM",userName,dealerName,dealercode,oem)/*3.65*/ {_display_(Seq[Any](format.raw/*3.67*/("""
"""),format.raw/*4.1*/("""<style>
	.ColorGreen"""),format.raw/*5.13*/("""{"""),format.raw/*5.14*/("""
		"""),format.raw/*6.3*/("""background-color:green;
	"""),format.raw/*7.2*/("""}"""),format.raw/*7.3*/("""
	
	"""),format.raw/*9.2*/(""".ColorRed"""),format.raw/*9.11*/("""{"""),format.raw/*9.12*/("""
		"""),format.raw/*10.3*/("""background-color:red;
	"""),format.raw/*11.2*/("""}"""),format.raw/*11.3*/("""
"""),format.raw/*12.1*/("""</style>

"""),_display_(/*14.2*/helper/*14.8*/.form(action=routes.InsuranceController.postCommonCallDispositionForm())/*14.80*/{_display_(Seq[Any](format.raw/*14.81*/("""
"""),format.raw/*15.1*/("""<link rel="stylesheet" href=""""),_display_(/*15.31*/routes/*15.37*/.Assets.at("css/cre.css")),format.raw/*15.62*/("""">
<input type="hidden" id="dndCust" value=""""),_display_(/*16.43*/{customerData.isDoNotDisturb()}),format.raw/*16.74*/("""">
<input type="hidden" id="typeOfDispoPageView" name="typeOfDispoPageView" value=""""),_display_(/*17.82*/typeDispo),format.raw/*17.91*/("""">
<input type="hidden" id="pFlag" name="pFlag" value=""""),_display_(/*18.54*/{if(customerData.getPreferredAdress()!=null){customerData.getPreferredAdress().getAddressType()}}),format.raw/*18.151*/("""">
<input type="hidden" name="customer_Id" id="customer_Id" value=""""),_display_(/*19.66*/{customerData.getId()}),format.raw/*19.88*/("""">
<input type="hidden" id="wyzUser_Id" name="wyzUser_Id" value=""""),_display_(/*20.64*/{userData.getId()}),format.raw/*20.82*/("""">
<input type="hidden" id="vehical_Id" name="vehicalId" value=""""),_display_(/*21.63*/{vehicleData.getVehicle_id()}),format.raw/*21.92*/("""">
<input type="hidden" name="uniqueidForCallSync" value=""""),_display_(/*22.57*/uniqueid),format.raw/*22.65*/("""">
<!-- <input type="hidden" id="workshopIdis" value=""""),_display_(/*23.53*/{if(latestService.getWorkshop()!=null){latestService.getWorkshop().getId()}}),format.raw/*23.129*/(""""> -->
<input type="hidden" id="workshopIdis" value="0">

<input type="hidden" id="location_Id" value=""""),_display_(/*26.47*/{userData.getLocation().getCityId()}),format.raw/*26.83*/("""">

<input type="hidden" name="callInteractionId" value=""""),_display_(/*28.55*/{interOfCall.getId()}),format.raw/*28.76*/("""">

<input type="hidden" id="srdispo_id" value=""""),_display_(/*30.46*/{if(interOfCall.getSrdisposition()!=null){interOfCall.getSrdisposition().getId()}}),format.raw/*30.128*/("""">
<input type="hidden" id="servicebook_id" name="serviceBookId" value=""""),_display_(/*31.71*/{if(interOfCall.getServiceBooked()!=null){interOfCall.getServiceBooked().getServiceBookedId()}}),format.raw/*31.166*/("""">
<input type="hidden" id="selectedFBComp" value=""""),_display_(/*32.50*/{if(interOfCall.getSrdisposition()!=null){interOfCall.getSrdisposition().getComplaintOrFB_TagTo()}}),format.raw/*32.149*/("""">			  
<input type="hidden" id="enteredRMFB" value=""""),_display_(/*33.47*/{if(interOfCall.getSrdisposition()!=null){interOfCall.getSrdisposition().getRemarksOfFB()}}),format.raw/*33.138*/("""">			  
<input type="hidden" id="depForFB" value=""""),_display_(/*34.44*/{if(interOfCall.getSrdisposition()!=null){interOfCall.getSrdisposition().getDepartmentForFB()}}),format.raw/*34.139*/("""">			  
<input type="hidden" id="selectedUpselOpp" value=""""),_display_(/*35.52*/{if(interOfCall.getSrdisposition()!=null){interOfCall.getSrdisposition().getUpsellCount()}}),format.raw/*35.143*/("""">
<input type="hidden" id="selectedModeOfCont" value=""""),_display_(/*36.54*/{if(interOfCall.getServiceBooked()!=null){interOfCall.getServiceBooked().getTypeOfPickup()}}),format.raw/*36.146*/("""">

	

			

<div class="row">
<!-- 4User div Starts -->
	<div class="col-md-4">
			  <div id="panelDemo10" class="panel panel-info custinfoData">
      <div class="panel-heading" style="text-align:center; padding: 5px;"> <b><i class="fa fa-user"></i>&nbsp;
        
        """),_display_(/*48.10*/{if(customerData.getCustomerName()!=null){
        
        customerData.getCustomerName();
        
        }else{
        
        customerData.getConcatinatedName();							
        
        }}),format.raw/*56.11*/(""" """),format.raw/*56.12*/("""</b><br />
        <b><i class="fa fa-user-circle">
		<input type="text" id="driverIdUpdate" style="border:none;background-color:#1797be;" value=""""),_display_(/*58.96*/{if(customerData.getUserDriver()!=null){
        
        customerData.getUserDriver();
        
        }}),format.raw/*62.11*/("""" readonly></i>
         </b></div>
      <div class="panel-body">
        <div class="row">
          <div class="col-sm-6 phoneselect">
            <div class="col-xs-3">
             <!--  <input type="button"  class="btn btn-primary" value=""""),_display_(/*68.74*/{customerData.getPreferredPhone().getPhoneNumber()}),format.raw/*68.125*/("""" id="ddl_phone_no" name="ddl_phone_no" style="cursor:default ;width: 150px;margin-left: -137px;font-size: medium;"> -->
			 <select class="btn btn-primary" id="ddl_phone_no" name="ddl_phone_no" style="cursor:default ;width: 150px;margin-left: -137px;font-size: medium;">
									"""),_display_(/*70.11*/for( phoneList <- customerData.getPhones()) yield /*70.54*/{_display_(Seq[Any](format.raw/*70.55*/("""
											
									"""),_display_(/*72.11*/{if(phoneList.isIsPreferredPhone ==true){
									
									if(phoneList.getPhoneNumber()!=null){
									<option value={phoneList.getPhoneNumber()} selected="selected">{phoneList.getPhoneNumber()}</option>
									}}else{ 
									
										if(phoneList.getPhoneNumber()!=null){
										<option value={phoneList.getPhoneNumber()} >{phoneList.getPhoneNumber()}</option>
										}}}),format.raw/*80.14*/("""
									""")))}),format.raw/*81.11*/("""						
								 """),format.raw/*82.10*/("""</select>
            </div>
            <button type="button" class="btn btn-success phoneinfobtn"  data-target="#addBtn" data-toggle="modal" data-backdrop="static" data-keyboard="false"><i class="fa fa-pencil-square-o fa-lg"></i></button>
            """),_display_(/*85.14*/if(typeDispo=="insurance")/*85.40*/{_display_(Seq[Any](format.raw/*85.41*/("""
          	"""),format.raw/*86.12*/("""<!--  <button type="button" class="btn btn-primary" id="" data-target="#InsuPremiumPopup" data-toggle="modal" data-backdrop="static" data-keyboard="false" onClick="ajaxODPercentage();"><i class="fa fa-calculator fa-lg"></i></button> -->
            """)))}),format.raw/*87.14*/("""
"""),format.raw/*88.1*/("""<button type="button" style="Display:none" id="DNDbtn" class="btn btn-danger">DND</button>			
            <!--<button type="button" class="btn btn-success test" id="btnDnd" data-target="#DNDConfirm" data-toggle="modal" data-backdrop="static" data-keyboard="false">DND</button>--> 
            
          </div>
          <div class="col-sm-12" style="margin-top: 10px;">
            <div class="col-xs-4">
              <input type="hidden" id="isCallinitaited" name="isCallinitaited" value="NotDailed">
              <input type="hidden" id="pref_number_callini" value=""""),_display_(/*95.69*/{customerData.getPreferredPhone().getPhone_Id()}),format.raw/*95.117*/("""">
              <button type="button" id="callIdBtn" class="btn btn-primary btn-lg"  onclick ="callfunctionFromPage('"""),_display_(/*96.117*/{customerData.getPreferredPhone().getPhone_Id()}),format.raw/*96.165*/("""','"""),_display_(/*96.169*/uniqueid),format.raw/*96.177*/("""','"""),_display_(/*96.181*/customerData/*96.193*/.getId()),format.raw/*96.201*/("""');"  style="border-radius: 29px;" ><i class="fa fa-phone-square fa-2x" aria-hidden="true" title="Make Call" ></i></button>
            </div>
            <div class="col-xs-4">
              <button type="button" class="btn btn-primary btn-lg " style="border-radius: 29px;"><i class="fa fa-envelope-square fa-2x" aria-hidden="true"></i></button>
            </div>
            <div class="col-xs-4">
              <button type="button" data-toggle="modal" data-target="#smsPopuId" data-backdrop="static" data-keyboard="false" class="btn btn-primary  btn-lg" style="border-radius: 29px;"><i class="fa fa-comment fa-2x" title="SMS" ></i></button>
            </div>
          </div>
          <div class="col-sm-12" style="margin-top: 6px;">
            <div class="row">
              <div class="col-xs-12" > <span><b><u>Email:</u>&nbsp; </b></span> <span style="font-size:15px" id="ddl_email"> """),_display_(/*107.127*/{if(customerData.getPreferredEmail()!=null){
                customerData.getPreferredEmail().getEmailAddress()
                }}),format.raw/*109.19*/(""" """),format.raw/*109.20*/("""</span> </div>
            </div>
            <div class="row">
              <div class="col-xs-6"> <span><b><u>DOA:</u>&nbsp; </b></span> <span>"""),_display_(/*112.84*/{customerData.getAnniversaryDateStr()}),format.raw/*112.122*/("""</span> </div>
              <div class="col-xs-6" > <span><b><u>DOB:</u>&nbsp; </b></span> <span>"""),_display_(/*113.85*/{customerData.getDobStr()}),format.raw/*113.111*/("""</span> </div>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="row">
              <div class="col-xs-12"> <span><b><u>Address:</u>&nbsp; </b></span> <span class="addressBlog" id="prefAdressUpdate"> """),_display_(/*118.132*/{if(customerData.getPreferredAdress()!=null){
                if(customerData.getPreferredAdress().getConcatenatedAdress()!=null){
                customerData.getPreferredAdress().getConcatenatedAdress();
                }else{
                if(customerData.getConcatinatedPrefAddree()!=null){
                customerData.getConcatinatedPrefAddree();
                }    
                }
				}}),format.raw/*126.7*/(""" """),format.raw/*126.8*/("""</span> </div>
            </div>
          </div>
          <div class="col-xs-12">
            <div class="row">
              <div class="col-xs-6"> <b><u>Pref Time:</u>&nbsp;</b><span>"""),_display_(/*131.75*/{customerData.getPreferred_time_start()}),format.raw/*131.115*/(""" """),format.raw/*131.116*/("""To """),_display_(/*131.120*/{customerData.getPreferred_time_end()}),format.raw/*131.158*/("""</span> </div>
              <div class="col-xs-6"> <b><u>Pref Contact:</u>&nbsp;</b> <span>"""),_display_(/*132.79*/{customerData.getMode_of_contact()}),format.raw/*132.114*/("""</span> </div>
            </div>
          </div>
		  <div class="col-xs-12">
            <div class="row">
              <div class="col-xs-12"> <b><u>Pref Day:</u>&nbsp;</b> <span>"""),_display_(/*137.76*/{customerData.getPreferred_day()}),format.raw/*137.109*/("""</span> </div>
              
            </div>
          </div>
          <!--button-->
          <div class="col-sm-12">
            <div class="row">
              <div class="col-sm-6"> """),_display_(/*144.39*/if(customerData.getSegment()!=null)/*144.74*/{_display_(Seq[Any](format.raw/*144.75*/("""
                
                """),_display_(/*146.18*/for(segmentIterate <- customerData.getSegment()) yield /*146.66*/{_display_(Seq[Any](format.raw/*146.67*/("""
                """),format.raw/*147.17*/("""<button type="button" class="btn btn-info btn-flat btn-block loya" style="cursor:default;"><b style="font-size:15px"> """),_display_(/*147.136*/if(segmentIterate.getName()!="" && segmentIterate.getName()!=null)/*147.202*/{_display_(Seq[Any](format.raw/*147.203*/("""
                """),_display_(/*148.18*/segmentIterate/*148.32*/.getName()),format.raw/*148.42*/("""
                """)))}/*149.18*/else/*149.22*/{_display_(Seq[Any](format.raw/*149.23*/("""
                """),format.raw/*150.17*/("""NA
                """)))}),format.raw/*151.18*/(""" """),format.raw/*151.19*/("""</b></button>
                """)))}),format.raw/*152.18*/("""
                """)))}),format.raw/*153.18*/("""
				"""),format.raw/*154.5*/("""</div>
            </div>
          </div>
        </div>
      </div>
    </div>
			   </div>
		  <div class="col-md-8" style="padding: 0px;">
							<div class="col-sm-4" style="padding: 0px;">
			<!-- START panel-->
					<div id="panelDemo10" class="panel panel-info">
					<div class="panel-heading"><b>VEHICLE&nbsp;INFO</b><img src=""""),_display_(/*165.68*/routes/*165.74*/.Assets.at("images/car.png")),format.raw/*165.102*/("""" class="pull-right" style="width:28px;"></img></div>
						<div class="panel-body" style="height: 110px;overflow: auto;padding: 5px;">
						"""),_display_(/*167.8*/if(vehicleData!=null)/*167.29*/{_display_(Seq[Any](format.raw/*167.30*/("""
		
						
						"""),format.raw/*170.7*/("""<span><b>"""),_display_(/*170.17*/{vehicleData.getChassisNo()}),format.raw/*170.45*/("""</b></span>&nbsp;|&nbsp;<span><b>"""),_display_(/*170.79*/{vehicleData.getVehicleRegNo()}),format.raw/*170.110*/("""</b></span>&nbsp;|&nbsp;<span><b>"""),_display_(/*170.144*/{vehicleData.getModel()}),format.raw/*170.168*/("""</b></span></br>
						<span><b>"""),_display_(/*171.17*/{vehicleData.getVariant()}),format.raw/*171.43*/("""</b>&nbsp;|&nbsp;<b>"""),_display_(/*171.64*/{vehicleData.getColor()}),format.raw/*171.88*/("""</b></span></br>
						<span>MILEAGE(Kms):&nbsp;<b>"""),_display_(/*172.36*/{latestService.getServiceOdometerReading()}),format.raw/*172.79*/("""</b></span></br>
						<span>SALE&nbsp;DATE:&nbsp;<b class="servi" style="background-color: #f05050; color: #fff;border-radius: 3px;   font-size: 13px;border-color: transparent;padding:2px">"""),_display_(/*173.175*/{vehicleData.getSaleDateStr()}),format.raw/*173.205*/("""</b></b></span>
										
						
				""")))}),format.raw/*176.6*/("""
						"""),format.raw/*177.7*/("""</div>
									</div>
									<!-- END panel-->
							</div>
<div class="col-sm-4" style="padding: 0px;">
<!-- START panel-->
<div id="panelDemo10" class="panel panel-info">
<div class="panel-heading specialoffer"><b>NEXT&nbsp;SERVICE&nbsp;DUE</b>
<!-- <div class="wrapper">
       <div class="ribbon-wrapper-green"><div class="ribbon-green"  data-toggle="modal" data-target="#offer" data-backdrop="static" data-keyboard="false">Special <br/> Offer</div></div>
</div> -->

<div class="pull-right">
<img src="/assets/images/specialoffer.png"  style="width: 80px; height: 80px; margin-top: -11px; margin-right: -17px;" data-toggle="modal" data-target="#offer" data-backdrop="static" data-keyboard="false">
</div>
</div>
<div class="panel-body" style="height: 110px;overflow-x:hidden;padding: 5px;">
	
		
						

<span><b class="servi" style="background-color: #f05050; color: #fff;border-radius: 3px;font-size: 13px;border-color: transparent;padding:2px">"""),_display_(/*198.145*/latestService/*198.158*/.getNextServicedateStr()),format.raw/*198.182*/(""" """),format.raw/*198.183*/("""</b>&nbsp;|&nbsp;<b>"""),_display_(/*198.204*/latestService/*198.217*/.getNextServiceType()),format.raw/*198.238*/("""</b></span><br>

<span>FORECAST&nbsp;LOGIC: &nbsp;<b>"""),_display_(/*200.38*/{vehicleData.getForecastLogic()}),format.raw/*200.70*/("""</b></span><br>
<span>AVG&nbsp;RUNNING/MONTH(Kms):&nbsp;<b>"""),_display_(/*201.45*/{vehicleData.getAverageRunning()}),format.raw/*201.78*/("""</b> </span></br>
<span>NO&nbsp;SHOW(Months):&nbsp;<b>"""),_display_(/*202.38*/{vehicleData.getRunningBetweenvisits()}),format.raw/*202.77*/("""</b> </span>



</div>
</div>

</div>
							
<div class="col-sm-4" style="padding: 0px;">
<!-- START panel-->
<div id="panelDemo10" class="panel panel-info">
<div class="panel-heading"><b>LAST&nbsp;SERVICE&nbsp;STATUS</b><img src=""""),_display_(/*214.75*/routes/*214.81*/.Assets.at("images/car-insurance.png")),format.raw/*214.119*/("""" class="pull-right" style="width:28px;"></img></div>
<div class="panel-body" style="height: 110px;overflow: auto;padding: 5px;">
"""),_display_(/*216.2*/if(vehicleData!=null)/*216.23*/{_display_(Seq[Any](format.raw/*216.24*/("""
"""),format.raw/*217.1*/("""<span>Service : <b style="background-color: #f05050; color: #fff;border-radius: 3px;   font-size: 13px;border-color: transparent;padding:2px">"""),_display_(/*217.144*/latestService/*217.157*/.getLastServiceDateStr()),format.raw/*217.181*/("""</b> &nbsp;|&nbsp;<b>"""),_display_(/*217.203*/latestService/*217.216*/.getJobCardNumber()),format.raw/*217.235*/("""</b>&nbsp;|&nbsp;<b>"""),_display_(/*217.256*/latestService/*217.269*/.getLastServiceType()),format.raw/*217.290*/("""</b></span><br>
<span>&nbsp;<b>"""),_display_(/*218.17*/latestService/*218.30*/.getSaName()),format.raw/*218.42*/("""</b>&nbsp;|&nbsp;<b>
"""),_display_(/*219.2*/{if(latestService.getWorkshop()!=null){
latestService.getWorkshop().getWorkshopName()
}
}),format.raw/*222.2*/("""
""")))}),format.raw/*223.2*/("""</b></span><br>
<span>Last PSF :&nbsp;<b>"""),_display_(/*224.27*/latestService/*224.40*/.getLastPSFStatus()),format.raw/*224.59*/("""</b></span>
</div>

</div>
<!-- END panel-->
</div>

<div class="col-sm-4" style="padding: 0px;">
<!-- START panel-->
<div id="panelDemo10" class="panel panel-info">
<div class="panel-heading"><b>INSURANCE</b><img src=""""),_display_(/*234.55*/routes/*234.61*/.Assets.at("images/truck.png")),format.raw/*234.91*/("""" class="pull-right" style="width:28px;"></img></div>
<div class="panel-body" style="height: 110px;overflow: auto;padding: 5px;">

		

<span>EXPIRES&nbsp;ON:&nbsp;<b class="servi" style="background-color: #f05050;color: #fff;border-radius: 3px;   font-size: 13px;border-color: transparent;">"""),_display_(/*239.158*/{latestinsurance.getPolicyDueDateStr()}),format.raw/*239.197*/("""</b></span><br>
<span style="font-size:13px">NEW&nbsp;INDIA&nbsp;ASSURANCE:&nbsp; <b>"""),_display_(/*240.71*/{latestinsurance.getInsuranceCompanyName()}),format.raw/*240.114*/("""</b></span><br>
<span style="font-size:13px">POLICY&nbsp;NO:&nbsp;<b>"""),_display_(/*241.55*/{latestinsurance.getPolicyNo()}),format.raw/*241.86*/("""</b></span><br>
<span style="font-size:13px">PREMIUM(Rs.):&nbsp;<b>"""),_display_(/*242.53*/{latestinsurance.getPremiumAmountAfterTax()}),format.raw/*242.97*/("""</b></span>


</div>

</div>
<!-- END panel-->
</div>
<div class="col-sm-4" style="padding: 0px;">
<!-- START panel-->
<div id="panelDemo10" class="panel panel-info">
<div class="panel-heading"><b>OTHER&nbsp;UPSELL</b><img src=""""),_display_(/*253.63*/routes/*253.69*/.Assets.at("images/warranty-certificate.png")),format.raw/*253.114*/("""" class="pull-right" style="width:28px;"></img></div>
<div class="panel-body" style="height: 110px;overflow: auto;padding: 5px;">
<span>Ford&nbsp;Protect&nbsp;Plan:&nbsp;<b class="servi" style="font-size: 13px;"></b></span><br>
<span>EW&nbsp;ExpiryDate:&nbsp;<b class="servi" style="background-color: #f05050;color: #fff;border-radius: 3px; font-size: 13px;
border-color: transparent;">"""),_display_(/*257.30*/{vehicleData.getExtendedWarentyDue()}),format.raw/*257.67*/("""</b></span><br>
<span>OEM&nbsp;Warranty&nbsp; Expiry:&nbsp;<b class="servi" style="font-size: 13px;"></b></span><br>
<span style="font-size: 13px;">RSA&nbsp;Expiry&nbsp;Date:&nbsp;<b>"""),_display_(/*259.68*/{vehicleData.getExchange()}),format.raw/*259.95*/("""</b></span></br>
<span>&nbsp;</span>


</div>

</div>
<!-- END panel-->
</div>
<div class="col-sm-4" style="padding: 0px;">
<!-- START panel-->
<div id="panelDemo10" class="panel panel-info">
<div class="panel-heading"><b>COMPLAINTS</b><img src=""""),_display_(/*271.56*/routes/*271.62*/.Assets.at("images/businessman.png")),format.raw/*271.98*/("""" class="pull-right" style="width:28px;"></img></div>
<div class="panel-body" style="height: 110px;overflow: auto;padding: 5px;">
<span>OPEN:<b class="servi" style="background-color: #f05050; margin-left: 15px; color: #fff;border-radius: 3px; font-size: 17px;    border-color: transparent;">"""),_display_(/*273.163*/complaintOFCust/*273.178*/.get(0)),format.raw/*273.185*/("""</b></span><br>
<span> CLOSED:<b class="servi" style="background-color: #27c24c; color: #fff;border-radius: 3px;     font-size: 13px;
border-color: transparent;">"""),_display_(/*275.30*/complaintOFCust/*275.45*/.get(1)),format.raw/*275.52*/("""</b></span><br>
<span style="font-size: 14PX;">LAST&nbsp;RAISED&nbsp;ON:&nbsp;<b>"""),_display_(/*276.67*/complaintOFCust/*276.82*/.get(2)),format.raw/*276.89*/("""</b></span><br>
<span style="font-size: 13px;">RESOLVED&nbsp;BY:&nbsp;<b>"""),_display_(/*277.59*/complaintOFCust/*277.74*/.get(3)),format.raw/*277.81*/("""</b>&nbsp;</span>

</div>

</div>
<!-- END panel-->
</div>
			</div>
   <div class="modal fade" id="addBtn" role="dialog">
								<div class="modal-dialog modal-lg">

									<!-- Modal content-->
									<div class="modal-content">
										<div class="modal-header" style="padding:1px;text-align:center;color:red;">
											<button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*291.81*/routes/*291.87*/.Assets.at("images/close1.png")),format.raw/*291.118*/(""""></button>
											<h5 class="modal-title">UPDATE CUSTOMER INFORMATION</h5>
										</div>
										
								 <div class="modal-body">
					 <div class="row">
						  <div class="col-sm-12" style="margin-top: 5px;">
								<div class="col-sm-3">
								  <label><b>Owner:</b></label>
								  <input type="text" class="form-control textOnlyAccepted" id="customerNameEdit" value=""""),_display_(/*300.98*/customerData/*300.110*/.getCustomerName()),format.raw/*300.128*/("""" name="" readonly>
								  <br/>
								</div>
								
								<div class="col-sm-3">
								  <label><b>User/Driver:</b></label>
								  <input type="text" class="form-control textOnlyAccepted" id="driverNameEdit" value=""""),_display_(/*306.96*/customerData/*306.108*/.getUserDriver()),format.raw/*306.124*/("""" name="">
								  <br/>
								</div>
								<div class="col-sm-3">
								  <label><b>DND</b></label><br />
								  
								<label class="radio-inline"><input type="radio" name="doNotDisturb" id="dndcheckbox1" value="true">YES</label>
								<label class="radio-inline"><input type="radio" name="doNotDisturb" id="dndcheckbox2" value="false" checked>NO</label>
								 
								</div>
								
								
							  </div>
							  </br>
							  <div class="col-sm-12">
								<div class="col-sm-3">
								  <label><b>Permanent Address:</b></label>
								  <textarea class="form-control permanentAddress" rows="2" id="permanentAddress" name="permanentAddress" readonly >"""),_display_(/*323.125*/{if(customerData.getPermanentAddressData()!=null){
	  
	  customerData.getPermanentAddressData().getAddressLine1();
	  
	  }}),format.raw/*327.6*/("""_
	  """),_display_(/*328.5*/{if(customerData.getPermanentAddressData()!=null){
	  
	  customerData.getPermanentAddressData().getAddressLine2();
	  
	  }}),format.raw/*332.6*/("""_
	  """),_display_(/*333.5*/{if(customerData.getPermanentAddressData()!=null){
	  
	  customerData.getPermanentAddressData().getAddressLine3();
	  
	  }}),format.raw/*337.6*/("""</textarea>
								
								</div>
								<div class="col-sm-1">
								  <br/>
								  <button type="button" class="btn btn-success peditAddressmodal" data-toggle="modal" data-target="#driver_modal1" style="margin-top: 5px;">Add</button>
								</div>
								<div class="col-md-3">
								  <label><b>Residence Address:</b></label>
								  <textarea class="form-control" rows="2" id="residenceAddress" name="residenceAddress" readonly>"""),_display_(/*346.107*/{if(customerData.getResidenceAddressData()!=null){
	  
	  customerData.getResidenceAddressData().getAddressLine1();
	  
	  }}),format.raw/*350.6*/("""_
	  """),_display_(/*351.5*/{if(customerData.getResidenceAddressData()!=null){
	  
	  customerData.getResidenceAddressData().getAddressLine2();
	  
	  }}),format.raw/*355.6*/("""_
	  """),_display_(/*356.5*/{if(customerData.getResidenceAddressData()!=null){
	  
	  customerData.getResidenceAddressData().getAddressLine3();
	  
	  }}),format.raw/*360.6*/("""</textarea>
								</div>
								<div class="col-md-1">
								  <br/>
								  <button type="button" class="btn btn-success reditAddressmodal"  data-toggle="modal" data-target="#driver_modal2">Add</button>
								</div>
								<div class="col-md-3">
								  <label><b>Office Address:</b></label>
								  <textarea class="form-control" rows="2" id="officeAddress" name="officeAddress" readonly >"""),_display_(/*368.102*/{if(customerData.getOfficeAddressData()!=null){
	  
	  customerData.getOfficeAddressData().getAddressLine1();
	  
	  }}),format.raw/*372.6*/("""_
	  """),_display_(/*373.5*/{if(customerData.getOfficeAddressData()!=null){
	  
	  customerData.getOfficeAddressData().getAddressLine2();
	  
	  }}),format.raw/*377.6*/("""_
	  """),_display_(/*378.5*/{if(customerData.getOfficeAddressData()!=null){
	  
	  customerData.getOfficeAddressData().getAddressLine3();
	  
	  }}),format.raw/*382.6*/("""</textarea>
								</div>
								<div class="col-md-1">
								  <br/>
								  <button type="button" class="btn btn-success oeditAddressmodal" data-toggle="modal" data-target="#driver_modal3" >Add</button>
								</div>
							  </div>
							  <hr>
							  <div class="col-sm-12">
								<h5 style="color:Red;"><b>PREFERRED COMMUNICATION ADDRESS</b></h5>
								<div class="col-md-4">
								  <input type="radio"  id="checkbox1" name="ADDRESS" value="0">
								  Permanent Address</div>
								<div class="col-md-4">
								  <input type="radio"  id="checkbox2" name="ADDRESS" value="2" >
								  Residence Address</div>
								<div class="col-md-4">
								  <input type="radio"  id="checkbox3" name="ADDRESS" value="1" >
								  Office Address</div>
							  </div>
							   <hr>
							  <div class="col-sm-12" style="margin-top: 5px;">
								<div class="col-sm-3">
								  <label><b>Preferred Email:</b></label>
								  <select class="form-control" id="email" name="email">
									"""),_display_(/*407.11*/for( emailList <- customerData.getEmails()) yield /*407.54*/{_display_(Seq[Any](format.raw/*407.55*/("""
											
									"""),_display_(/*409.11*/{if(emailList.isIsPreferredEmail() ==true)
									
									<option value={emailList.getEmailAddress()} selected="selected">{emailList.getEmailAddress()}</option>
									else 
										<option value={emailList.getEmailAddress()} >{emailList.getEmailAddress()}</option>
										}),format.raw/*414.12*/("""
									""")))}),format.raw/*415.11*/("""						
								 """),format.raw/*416.10*/("""</select>
								</div>
								<div class="col-xs-1">
								  <br/>
								  <button type="button" class="btn btn-success oeditEmailmodal" data-toggle="modal" data-target="#driver_modal4" style="margin-top: 5px;">Add</button>
								</div>
								<div class="col-sm-3">
								  <label><b>Date OF Birth:</b></label>
								  <input type="text" class="datepicMYDropDown form-control" id="dob" value=""""),_display_(/*424.86*/{customerData.getDob()}),format.raw/*424.109*/("""" name="dob" readonly>
								</div>
								<div class="col-md-3">
								  <label><b>Date of Anniversary</b></label>
								  <input type="text" class="datepicMYDropDown form-control" id="anniversary_date" value=""""),_display_(/*428.99*/{customerData.getAnniversary_date()}),format.raw/*428.135*/("""" name="anniversary_date" readonly>
								</div>
							  </div>
							   <hr>
							  <div class="col-sm-12" style="margin-top: 5px;">
								<h5 style="color:Red;"><b>MANAGE PREFERENCES</b></h5>
								<div class="col-sm-3">
								  <label><b>Preferred Contact #:</b></label>
								  <select class="form-control" id="preffered_contact_num" name="preffered_contact_num">
									"""),_display_(/*437.11*/for( phoneList <- customerData.getPhones()) yield /*437.54*/{_display_(Seq[Any](format.raw/*437.55*/("""
											
									"""),_display_(/*439.11*/{if(phoneList.isIsPreferredPhone ==true)
									
									<option value={phoneList.phone_Id.toString()} selected="selected">{phoneList.getPhoneNumber()}</option>
									else 
										<option value={phoneList.phone_Id.toString()} >{phoneList.getPhoneNumber()}</option>
										}),format.raw/*444.12*/("""
									""")))}),format.raw/*445.11*/("""						
								 """),format.raw/*446.10*/("""</select>
								  
								</div>
								<div class="col-sm-1">
								<br />
								 <input type="button"  class="btn btn-success"  id="tanniversary_edit" data-toggle="modal" data-target="#myModalAddPhone" value="Add">
										   </div>
								<div class="col-md-3">
								  <label><b>Preferred Mode of Contact:</b></label>
								  <div class="col-sm-12" id="modeOfCon"  style="display:none">
									<select class="form-control" id="preffered_mode_contact" name="preffered_mode_contact" multiple="multiple" >
									  <option value="Email">Email</option>
									  <option value="Phone">Phone</option>
									  <option value="SMS">SMS</option>
									</select>
								  </div>
								  <input type="text" class="form-control" id="tpreffered_mode_contact"  value=""""),_display_(/*462.89*/{customerData.getMode_of_contact()}),format.raw/*462.124*/("""" readonly>
								</div>
								<div class="col-md-1">
								<br />
								  <input type="button"  class="btn btn-success"  id="tpreffered_mode_contact_edit" value="Add">
								  
								</div>
								<div class="col-md-3">
								  <label><b>Preferred Day to Contact:</b></label>
								  <div class="col-sm-12" id="daysWeek" style="display:none">
									<select class="form-control" id="preffered_day_contact" name="preffered_day_contact" multiple="multiple">
									  <option value="Sunday">Sunday</option>
									  <option value="Monday">Monday</option>
									  <option value="Tuesday">Tuesday</option>
									  <option value="Wednesday">Wednesday</option>
									  <option value="Thursday">Thursday</option>
									  <option value="Friday">Friday</option>
									  <option value="Saturday">Saturday</option>
									</select>
								  </div>
								  <input type="text" class="form-control" id="tpreffered_day_contact"  value=""""),_display_(/*482.88*/{customerData.getPreferred_day()}),format.raw/*482.121*/("""" readonly>
								</div>
								<div class="col-md-1">
								<br />
								  <input type="button"  class="btn btn-success"  id="tpreffered_day_contact_edit" value="Add">
								</div>
							  </div>
							  <br>
							   <hr>
							  <div class="col-sm-12" style="margin-top: 5px;">
								<div class="col-sm-3">
								  <label><b>Preferred Time Of Contact(Start Time):</b></label>
								  <input type="text" class="form-control single-input" id="dateStarttime" name="dateStarttime" value=""""),_display_(/*494.112*/{customerData.getPreferred_time_start()}),format.raw/*494.152*/("""" readonly>
								</div>
								<div class="col-md-3">
								  <label><b>Preferred Time Of Contact(End Time):</b></label>
								  <input type="text" class="form-control single-input" id="dateEndtime" name="dateEndtime" value=""""),_display_(/*498.108*/{customerData.getPreferred_time_end()}),format.raw/*498.146*/("""" readonly>
								</div>
								<div class="col-sm-6">
								  <label><b>Comments:(250 Characters Only)</b></label>
								  <textarea type="text" class="form-control" id="custEditComment" rows="2" maxlength="250" name="custEditComment" value=""""),_display_(/*502.131*/{customerData.getComments()}),format.raw/*502.159*/("""">"""),_display_(/*502.162*/{customerData.getComments()}),format.raw/*502.190*/("""</textarea>
								</div>
							  </div>
							 
							</div>
				</div>

										<div class="modal-footer">
											<button type="button" id="pn_save" class="btn btn-success" onClick="ajaxCallForAddcustomerinfo();" data-dismiss="modal">Save</button>

										</div>
									</div>

								</div>
							</div>
		  
			  
		  <!-- 4User Div End -->
   


</div>

<!--Shasi dispositio-->
<div class="row">
<div class="col-lg-12 disposit">
<div class="panel panel-primary">
<div class="panel-heading " align="center"; style="background-color:#1797be;" ><img src=""""),_display_(/*529.91*/routes/*529.97*/.Assets.at("images/phone1.png")),format.raw/*529.128*/("""" style="width:17px"/>&nbsp;<b>DISPOSITION FORM</b> 
</div>
<div class="panel-body disposit">
<div class="row animated  bounceInRight">

<div class="col-sm-12" id="SMRInteractionFirst" >
		  <label for=""><h4>Select The Mode of Interaction :-&nbsp;&nbsp;</h4></label>
			 
			  <div class="radio-inline">
				<label>
				  <input type="radio" name="InOutCallName" value="OutCall" checked id="OutGoingID">
				  Outbound Call </label>
			  </div>
			  <div class="radio-inline">
				<label>
				  <input type="radio" name="InOutCallName" value="InCall" id="" >
				  Inbound Call</label>
			  </div>
		</div>
		<div>
		
		"""),_display_(/*550.4*/dispoInBound),format.raw/*550.16*/("""
		
		 """),format.raw/*552.4*/("""</div>
	  <div class="col-md-12 animated  bounceInRight" id="OutBoundDiv" style="display:none;">
		<div class="form-group">
		<div id="DidYouTalkDiv">
		  <label for=""><b>Did you talk to the customer ?</b></label>
		  <br>
		  <div class="radio-inline">
			<label>
			  <input type="radio" name="typeOfDisposition" value="Contact" id="SpeakYes">
			  Yes </label>
		  </div>
		  <div class="radio-inline">
			<label>
			  <input type="radio" name="typeOfDisposition" value="NonContact" id="SpeakNo" >
			  No</label>
		  </div>
		 </div>
		  <!--No-->
		  
		  <div style="display:none;" class="animated  bounceInRight" id="NotSpeachDiv"><br>
			<label for=""><b>Choose reason for not being able to talk to customer:</b></label>
			<br>
			<div class="radio">
			  <label>
				<input type="radio" name="disposition" value="Ringing No response" id="">
				Ringing No response </label>
			</div>
			<div class="radio">
			  <label>
				<input type="radio" name="disposition" value="Wrong Number" id="">
				Wrong Number</label>
			</div>
			<div class="radio">
			  <label>
				<input type="radio" name="disposition" value="Busy" id="" >
				Busy</label>
			</div>
			<div class="radio">
			  <label>
				<input type="radio" name="disposition" value="Switched Off / Unreachable" id="" >
				Switched Off / Unreachable</label>
			</div>
			<div class="radio">
			  <label>
				<input type="radio" name="disposition" value="Invalid Number" id="" >
				Invalid Number</label>
			</div>
			<div class="radio">
			  <label>
				<input type="radio" name="disposition" value="NoOther" id="NOOthersCheck" >
				Other</label>
			</div>
			
			
			<div class="row animated  bounceInRight" style="display:none;"  id="NoOthers">
			<div class="col-md-3">
			<label><b>Please Specify</b></label>
			<textarea type="text" class="form-control NoOthersText1" rows="1" name="other"></textarea>
			</div>
			</div>
			<div class="pull-right">
					<button type="button" class="btn btn-info" id="DidUTalkNO" style="background-color:#1797BE">Back</button>						
					<button type="submit" class="btn btn-info" name="typeOfsubmit" style="background-color:#1797BE" id="nonContactValidation" value="nonContact">Submit</button>
			</div>
		  </div>
		  <!--Book My Service-->
		  <div style="display:none;" class="animated  bounceInRight whatDidCustSayDiv" id="WhatdidtheCustomersayDIV" >
		  <div id="whatDidCustSayDiv">
			<label for=""><b>What did the customer say?</b></label>
			<br>
			
		"""),_display_(/*623.4*/if(typeDispo=="insurance")/*623.30*/{_display_(Seq[Any](format.raw/*623.31*/("""	
			
			"""),format.raw/*625.4*/("""<div class="radio-inline">
			  <label>
				<input type="radio" name="disposition" value="Book Appointment" id="BookMyAppointment" >
				Book Appointment </label>
			</div>
			<div class="radio-inline">
			  <label>
				<input type="radio" name="disposition" value="Renewal Not Required" id="RenewalNotRequired" >
				Renewal Not Required</label>
			</div>
			
		
			""")))}),format.raw/*637.5*/("""

			"""),_display_(/*639.5*/if(interOfCall.getServiceBooked()==null && typeDispo != "insurance")/*639.73*/{_display_(Seq[Any](format.raw/*639.74*/(""" 
						
			"""),format.raw/*641.4*/("""<div class="radio-inline">
			  <label>
				<input type="radio" name="disposition" value="Book My Service" id="BookMyService" >
				Book My Service </label>
			</div>
			
			<div class="radio-inline">
			  <label>
				<input type="radio" name="disposition" value="Service Not Required" id="ServiceNotRequired" >
				Service Not Required</label>
			</div>
				
		""")))}),format.raw/*653.4*/("""	"""),_display_(/*653.6*/if(interOfCall.getServiceBooked() !=null && typeDispo != "insurance")/*653.75*/{_display_(Seq[Any](format.raw/*653.76*/(""" 
		"""),format.raw/*654.3*/("""<div class="radio-inline" >
			  <label>
				<input type="radio" name="disposition" value="Confirmed" id="ConfirmedId" >
				Confirmed </label>
			</div>
			
		<div class="radio-inline">
			  <label>
				<input type="radio" name="disposition" value="Book My Service" id="BookMyService" >
				Reschedule </label>
			</div>
				
			
			<div class="radio-inline">
			  <label>
				<input type="radio" name="disposition" value="Cancelled" id="CancelId" >
				Cancel</label>
			</div>
		""")))}),format.raw/*672.4*/("""
		"""),format.raw/*673.3*/("""<div class="radio-inline">
			  <label>
				<input type="radio" name="disposition" value="Call Me Later" id="CallMeLatter" >
				Call Me Later</label>
			</div>
			
		
		

		
			</div>
		  </div>
		  <div>

		  
		   """),_display_(/*688.7*/dispoOut),format.raw/*688.15*/(""" 		  
		
		  """),format.raw/*690.5*/("""<!--call me latter-->
		 </div>
		  </div>
		
		  </div>
		 </div>
		 </div>
		 </div>
		 </div>
		 <!-----------INTERACTIONH HISTORY--------->
		 <div class="col-lg-12">
	<div class="panel panel-primary">
	<div class="panel-heading " align="center"; style="background-color:#1797be;" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne3"><img src=""""),_display_(/*702.167*/routes/*702.173*/.Assets.at("images/INTERACTION HISTORY.png")),format.raw/*702.217*/("""" style="width:17px"/>&nbsp;<b>INTERACTION HISTORY</b> 
</div>

  <div id="collapseOne3" class="panel-collapse collapse in">
		<div class="panel-body">
	  <div class="panel with-nav-tabs">
			<div class="panel-heading clearfix">
		  <div>
				<ul class="nav nav-tabs">
			  <!--  <li class="expand"><a href="#tab1primary" data-toggle="tab" onclick="callHistoryByVehicle();">PREVIOUS COMMENTS</a></li> -->
			  <li class="expand"><a href="#tab2primary" data-toggle="tab" onclick="callHistoryByVehicle();">DISPOSITIONS</a></li>
			  <li class="expand"><a href="#tab3primary" data-toggle="tab" onclick="smsHistoryOfUser();">SMS</a></li>
			  <li class="expand"><a href="#tab4primary" data-toggle="tab">E-Mail</a></li>
			  <li class="expand"><a href="#tab5primary" data-toggle="tab" onclick="complaintsOFVehicle();">COMPLAINTS</a></li>
			 """),_display_(/*716.6*/if(typeDispo=="insurance")/*716.32*/{_display_(Seq[Any](format.raw/*716.33*/(""" 
			  """),format.raw/*717.6*/("""<li class="expand"><a href="#tab6primary" data-toggle="tab" onclick="insuranceHistoryOfCustomer();">APPOINTMENT HISTORY</a></li>
			 """)))}),format.raw/*718.6*/(""" 
			"""),format.raw/*719.4*/("""</ul>
			  </div>
		</div>
			<div class="panel-body">
		  <div class="tab-content">
				<!-- <div class="tab-pane fade in active" id="tab1primary">
			  <div class="row">
					<div class="col-sm-12">
				  <div class="form-group">
						<table class="table table-bordered table-responsive table-user-information" id="previousCommentTable">
					  <thead>
							<tr>
						  <th></th>
						  <th></th>						  
						 
						</tr>
						  </thead>
					  <tbody>
						  
					 
						</tbody>
					  
					  
					</table>
					  </div>
				</div>
				  </div>
			</div> -->
				<div class="tab-pane fade" id="tab2primary">
			  <div class="row">
					<div class="col-sm-12">
				  <div class="form-group">
						<table class="table table-bordered table-responsive" id="dispositionHistory">
					  <thead>
							<tr>
						  <th style="text-align:center">Module</th>
						  <th style="text-align:center">Call Date</th>
						  <th style="text-align:center">Time</th>
						  <th style="text-align:center">CRE ID</th>
						  <th style="text-align:center">Call Type</th>
						  <th style="text-align:center">Primary Disposition</th>
						  <th style="text-align:center">Secondary Disposition</th>
						  <th style="text-align:center">Remarks</th>						 
						  <th style="text-align:center">Recording</th>						 
						</tr>
						  </thead>
					  <tbody>
						  
					 
						</tbody>
					  
					</table>
					  </div>
				</div>
				  </div>
			</div>
			<div class="tab-pane fade" id="tab3primary">
			  <div class="row">
				<div class="col-sm-12">
				
						<table class="table table-bordered table-responsive" id="sms_history">
						 <thead>
							<tr>
						  <th>DATE</th>
						  <th>TIME</th>
						  <th>SOURCE AUTO/CRE ID</th>
						  <th>SCENARIO</th>
						  <th>MESSAGE</th>
						  <th>STATUS</th>
						 
						</tr>
						  </thead>
					  <tbody>
							
						</tbody>
					  
					</table>
					
				</div>
				
				</div>
			</div>
			<div class="tab-pane fade" id="tab4primary">
			  <div class="row">
					<div class="col-sm-12">
				  <div class="form-group">
						<table class="table table-bordered table-responsive table-user-information">
					  <tbody>
							
						</tbody>
					  
					</table>
					  </div>
				</div>
				  </div>
			</div>
			<div class="tab-pane fade" id="tab5primary">
			  <div class="row">
					<div class="col-sm-12">
				  <div class="form-group">
						<table class="table table-bordered table-responsive table-user-information" id="complaint_history">
					  <thead>
							<tr>
						  <th>COMPLAINT No.</th>
						  <th>ISSUE DATE</th>
						  <th>SOURCE</th>
						  <th>FUNCTION</th>
						  <th>CATEGORY</th>
						  <th>STATUS</th>
						 
						</tr>
						  </thead>
					  <tbody>
							
						</tbody>
					  
					</table>
					  </div>
				</div>
				  </div>
			</div>
			
			<div class="tab-pane fade" id="tab6primary">
			  <div class="row">
					<div class="col-sm-12">
				  <div class="form-group">
						<table class="table table-bordered table-responsive table-user-information" id="insurance_history">
					  <thead>
							<tr>
						  <th>INSURANCE COMPANY</th>
						  <th>IDV</th>
						  <th>OD %</th>
						  <th>OD (Rs.)</th>
						  <th>NCB %</th>
						  <th>NCB (Rs.)</th>
						  <th>DISCOUNT%</th>
						  <th>OD PREMIUM</th>
						  <th>LIABILITY PREMIUM</th>
						  <th>ADD-ON PREMIUM</th>
						  <th>PREMIUM (Before Tax)</th>
						  <th>TAX %</th>
						  <th>PREMIUM (After Tax)</th>
						</tr>
						  </thead>
					  <tbody>
							
						</tbody>
					  
					</table>
					  </div>
				</div>
				  </div>
			</div>
			  </div>
		</div>
		  </div>
	</div>
	  </div>
</div>
  </div>
		 </div>
		






		
		




<!--sashi model-->
<div class="modal fade" id="driver_modal1" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*898.72*/routes/*898.78*/.Assets.at("images/close1.png")),format.raw/*898.109*/(""""></button>
		<h4 class="modal-title">Permanent Address</h4>
	  </div>
	  <div class="modal-body">
		<table>
		  <tr>
			<td>Address Line1:</td>
			<td><textarea class="form-control paddr_line1" rows="1"   placeholder="Please enter Company Name/Flat No & Building name." ></textarea>
			  <br/></td>
		 
			<td>Address Line2:</td>
			<td><textarea class="form-control paddr_line2" rows="1"   placeholder="Please enter Road number/ Road name."></textarea>
			  <br/></td>
			  <td>Address Line3:</td>
			<td><textarea class="form-control paddr_line3" rows="1"  placeholder="Please enter Road number/ Road name."></textarea>
			  <br/></td>
		  </tr>
		  <tr>
		  
		  	<td>State</td>
			<td><select  class="form-control paddr_line5" id="pstateId" onchange="getCityByStateSelection('pstateId','paddrrCity');">
			<option value="--SELECT--">--SELECT--</option>
			"""),_display_(/*920.5*/for(states <- statesList) yield /*920.30*/{_display_(Seq[Any](format.raw/*920.31*/("""
           
          """),format.raw/*922.11*/("""<option value=""""),_display_(/*922.27*/states),format.raw/*922.33*/("""">"""),_display_(/*922.36*/states),format.raw/*922.42*/("""</option>
                              
	  """)))}),format.raw/*924.5*/("""
			
			"""),format.raw/*926.4*/("""</select>
			<br/></td>
		  
			<td>City</td>			
		  <td><select  class="form-control paddr_line4" id="paddrrCity">
			                    
	  </select><br/></td>
			</tr>

		  <tr>
			<td>PinCode</td>			
			<td><input type="text"  class="form-control paddr_line6 numberOnly" id="ppincode" maxlength="6" rows="1" value=""""),_display_(/*937.117*/{if( customerData.getPermanentAddressData()!=null){ customerData.getPermanentAddressData().getPincode()} }),format.raw/*937.223*/("""" placeholder="Please enter PinCode"/><br/></td>
		  
			<td>Country</td>
			<td><input type="text"  class="form-control paddr_line7" rows="1" value="India"  placeholder="Please enter Country" readonly/><br/></td>
		  </tr>
		</table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary paddr_submit" data-dismiss="modal">Submit</button>
	  </div>
	</div>
  </div>
</div>
<div class="modal fade" id="driver_modal2" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*954.72*/routes/*954.78*/.Assets.at("images/close1.png")),format.raw/*954.109*/(""""></button>
		<h4 class="modal-title">Residence Address</h4>
	  </div>
	  <div class="modal-body">
		<table>
		  <tr>
			<td>Address Line1:</td>
			<td><textarea class="form-control raddr_line1" rows="1"  name="raddressline1" placeholder="Please enter Company Name/Flat No & Building name." ></textarea>
			  <br/></td>
		
			<td>Address Line2:</td>
			<td><textarea class="form-control raddr_line2" rows="1"  name="raddressline2" placeholder="Please enter Road number/ Road name."></textarea>
			  <br/></td>
			  
			 <td>Address Line3:</td>
			<td><textarea class="form-control raddr_line3" rows="1"  name="raddressline3" placeholder="Please enter Road number/ Road name."></textarea>
			  <br/></td> 
		  </tr>
		   <tr>
		   
		   <td>State</td>			
		 <td><select  class="form-control raddr_line5" id="rstateId" onchange="getCityByStateSelection('rstateId','raddrrCity');">
			<option value="--SELECT--">--SELECT--</option>
			"""),_display_(/*977.5*/for(states <- statesList) yield /*977.30*/{_display_(Seq[Any](format.raw/*977.31*/("""
           
          """),format.raw/*979.11*/("""<option value=""""),_display_(/*979.27*/states),format.raw/*979.33*/("""">"""),_display_(/*979.36*/states),format.raw/*979.42*/("""</option>
                              
	  """)))}),format.raw/*981.5*/("""
			
			"""),format.raw/*983.4*/("""</select>
			<br/></td>
			<td>City</td>
			
		    <td><select  class="form-control raddr_line4" id="raddrrCity">
			                    
	  </select><br/></td>
			 </tr>

		  <tr>
			<td>PinCode</td>
			<td><input type="text"  class="form-control raddr_line6 numberOnly" rows="1" maxlength="6" value=""""),_display_(/*994.103*/{if( customerData.getResidenceAddressData()!=null){ customerData.getResidenceAddressData().getPincode()} }),format.raw/*994.209*/(""""placeholder="Please enter PinCode"/><br/></td>
		  
			<td>Country</td>
			<td><input type="text"  class="form-control raddr_line7" rows="1" value="India" placeholder="Please enter Country" readonly/><br/></td>
		  </tr>
		
		</table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary raddr_submit" data-dismiss="modal">Submit</button>
	  </div>
	</div>
  </div>
</div>
<div class="modal fade" id="driver_modal3" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1012.72*/routes/*1012.78*/.Assets.at("images/close1.png")),format.raw/*1012.109*/(""""></button>
		<h4 class="modal-title">Office Address</h4>
	  </div>
	  <div class="modal-body">
		<table>
		  <tr>
			<td>Address Line1:</td>
			<td><textarea class="form-control oaddr_line1" rows="2"  placeholder="Please enter Company Name/Flat No & Building name." name="oaddressline1">"""),_display_(/*1019.148*/{if( customerData.getOfficeAddressData()!=null){ customerData.getOfficeAddressData().getAddressLine1()} }),format.raw/*1019.253*/("""</textarea>
			  <br/></td>
		  
			<td>Address Line2:</td>
			<td><textarea class="form-control oaddr_line2" rows="2" placeholder="Please enter Road number/ Road name." name="oaddressline2">"""),_display_(/*1023.133*/{if( customerData.getOfficeAddressData()!=null){ customerData.getOfficeAddressData().getAddressLine2()} }),format.raw/*1023.238*/("""</textarea>
			  <br/></td>
			<td>Address Line3:</td>
			<td><textarea class="form-control oaddr_line3" rows="2" placeholder="Please enter Road number/ Road name." name="oaddressline3">"""),_display_(/*1026.133*/{if( customerData.getOfficeAddressData()!=null){ customerData.getOfficeAddressData().getAddressLine3()} }),format.raw/*1026.238*/("""</textarea>
			  <br/></td>
		  </tr>
		  <tr>
		  
		  <td>State</td>
			
		 <td><select  class="form-control oaddr_line4" id="paddr_line4" name="paddressline4"  onchange="getCityByStateSelection('paddr_line4','paddr_line7');">
			<option value="--SELECT--">--SELECT--</option>
			"""),_display_(/*1035.5*/for(states <- statesList) yield /*1035.30*/{_display_(Seq[Any](format.raw/*1035.31*/("""
           
          """),format.raw/*1037.11*/("""<option value=""""),_display_(/*1037.27*/states),format.raw/*1037.33*/("""">"""),_display_(/*1037.36*/states),format.raw/*1037.42*/("""</option>
                              
	  """)))}),format.raw/*1039.5*/("""
			
			"""),format.raw/*1041.4*/("""</select>
			<br/></td>
			<td>City</td>			
		  <td><select  class="form-control oaddr_line7"  id="paddr_line7" name="paddressline3">			                    
	  </select><br/></td>
			 </tr>

		  <tr>
			<td>PinCode</td>
			<td><input type="text"  class="form-control oaddr_line5 numberOnly" maxlength="6" id="paddr_line5" rows="1" value=""""),_display_(/*1050.120*/{if( customerData.getOfficeAddressData()!=null){ customerData.getOfficeAddressData().getPincode()} }),format.raw/*1050.220*/("""" name="paddressline5" placeholder="Please enter PinCode"/><br/></td>
		  
			<td>Country</td>
			<td><input type="text"  class="form-control oaddr_line6" id="paddr_line6" rows="1" value="India" name="paddressline6" placeholder="Please enter Country" readonly/><br/></td>
		  </tr>
		</table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary oaddr_submit" data-dismiss="modal">Submit</button>
	  </div>
	</div>
  </div>
</div>

<div class="modal fade" id="driver_modal4" role="dialog">
    <div class="modal-dialog modal-xs">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1070.80*/routes/*1070.86*/.Assets.at("images/close1.png")),format.raw/*1070.117*/(""""></button>
          <h4 class="modal-title">ADD EMAIL</h4>
        </div>
		
		
        <div class="modal-body">
		<div class="row">
        <div class="col-xs-12">
        <div class="col-xs-4">
        <input type="text" name="myEmailNum" class="form-control email"  maxlength="30" min="5" max="30" id="myEmailNum" >
      </div>
        </div>
        </div>
		</div>
        <div class="modal-footer">
   <button type="button" class="btn btn-default" data-dismiss="modal" id="saveEmailCust">SAVE</button>
        </div>
      </div>
      
    </div>
  </div>		  
<div id="smallModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
		<h4 class="modal-title">Message</h4>
	  </div>
	  <div class="modal-body">
		<p>This feature is not enabled for """),_display_(/*1099.39*/userName),format.raw/*1099.47*/(""".<br>
		  Contact Administrator.</p>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
	  </div>
	</div>
  </div>
</div>
<div id="smallModal1" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
		<h4 class="modal-title">E-Mail</h4>
	  </div>
	  <div class="modal-body">
		<p>This feature is not enabled for """),_display_(/*1116.39*/userName),format.raw/*1116.47*/(""" """),format.raw/*1116.48*/(""".<br>
		  Contact Administrator.</p>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
	  </div>
	</div>
  </div>
</div>
<div class="modal fade" id="sa_modal" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1129.72*/routes/*1129.78*/.Assets.at("images/close1.png")),format.raw/*1129.109*/(""""></button>
		<h4 class="modal-title">CHECK SMR LOAD</h4>
		<br>
		<h5 class="modal-title">Auto Service</h5>
	  </div>
	  <div class="modal-body">
		<table border="1" class="table table-striped table-bordered table-hover" id="dataTables-example60">
		  <thead>
			<tr>
			  
			  <th>DATE</th>
			  <th>SMR BOOKED</th>
			  <th>CAPACITY</th>
			</tr>
		  </thead>
		  <tbody>    
			
			
			
		  </tbody>
		</table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>
<div class="modal fade" id="sa_modal1" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1161.72*/routes/*1161.78*/.Assets.at("images/close1.png")),format.raw/*1161.109*/(""""></button>
		<h4 class="modal-title">CHECK DRIVER'S SCHEDULE</h4>
		<br>
		<h5 class="modal-title">Auto Service</h5>
	  </div>
	  <div class="modal-body">
		<table border="1"  class="table table-striped table-bordered table-hover" id="">
		  <thead>
			<tr>
			  <th>PICK-UP-BY</th>
			  <th>FROM TIME</th>
			  <th>TO TIME</th>
			  
			</tr>
		  </thead>
		  <tbody>
			
			
		  </tbody>
		</table>
	  </div>
	  <div class="row">
		<div class="col-md-10 col-md-offset-1" style="background-color: #ddd;">
		 
		</div>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>
<div class="modal fade" id="workshop_modal" role="dialog">
  <div class="modal-dialog modal-md">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1197.72*/routes/*1197.78*/.Assets.at("images/close1.png")),format.raw/*1197.109*/(""""></button>
		<h4 class="modal-title">Workshop Details</h4>
	  </div>
	  <div class="modal-body">
		<table class="table table-striped table-bordered table-hover" id="">
		  <thead>
			<tr>
			  <th>Workshop Name</th>
			  <th>Service Booked Date</th>
			</tr>
		  </thead>
		  <tbody>
		  </tbody>
		</table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>


<!-- Address Popup Model -->
<div class="modal fade" id="AddNewAddressPopup" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1227.72*/routes/*1227.78*/.Assets.at("images/close1.png")),format.raw/*1227.109*/(""""></button>
  <h4 class="modal-title"><b style="text-align:center; color:Red;">Add New Address</b></h4>
</div>
<div class="modal-body">
 
  <div class="row" id="AddAddressDiv">
   <div class="col-md-12">
<div class="col-md-6">
  <div class="form-group">
<label for="comments"><b>Add Address1<b></label>
<textarea class="form-control" rows="1" id="AddAddress1All" name="address1New" ></textarea>
  </div>
</div>
<div class="col-md-6">
  <div class="form-group">
<label for="comments"><b>Add Address2<b></label>
<textarea class="form-control" rows="1" id="AddAddress2All" name="address2New" ></textarea>
  </div>
</div>

<div class="col-md-6">
  <div class="form-group">
<label for="comments"><b>State<b></label>
<select class="form-control" name="stateNew" id="AddAddrMMSState" onchange="getCityByStateSelection('AddAddrMMSState','cityInputPopup3');">
  <option value="0">--SELECT--</option>
"""),_display_(/*1252.2*/for(states <- statesList) yield /*1252.27*/{_display_(Seq[Any](format.raw/*1252.28*/("""
           
          """),format.raw/*1254.11*/("""<option value=""""),_display_(/*1254.27*/states),format.raw/*1254.33*/("""">"""),_display_(/*1254.36*/states),format.raw/*1254.42*/("""</option>
                              
  """)))}),format.raw/*1256.4*/("""
"""),format.raw/*1257.1*/("""</select>
  </div>
</div>

<div class="col-md-6">
  <div class="form-group">
<label for="comments"><b>City<b></label>
<select class="form-control" name="cityNew" placeholder="City" id="cityInputPopup3">	
                                        
                                    </select>
  </div>
</div>
<div class="col-md-6">
  <div class="form-group">
<label for="comments"><b>PinCode<b></label>
<input type="text" class="form-control numberOnly" name="pincodeNew" value="0" id="PinCode1MMS" maxlength="6" />
  </div>
</div>

</div>
   
  </div>
  

<div class="modal-footer">
  <button type="button" class="btn btn-success" id="AddAddrMMSSave" data-dismiss="modal">Save</button>
</div>
</div>

</div>
</div>
</div>  <!-- end popup -->  <!-- end popup -->


<!---bottom fixed popup----->
<div class="modal fade" id="bottonFixedId" role="dialog">
<div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
	<div class="modal-header">
	  <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1298.73*/routes/*1298.79*/.Assets.at("images/close1.png")),format.raw/*1298.110*/(""""></button>
	  <h4 class="modal-title">Call Script</h4>
	</div>
	<div class="modal-body">
	  <p>Hi Good Morning!. </p>
	</div>
	<div class="modal-footer">
	  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
  </div>
  
</div>
</div>



<div class="modal fade" id="offer" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="background-color:#1797be;color:#ffffff;text-align:center">
          <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1320.80*/routes/*1320.86*/.Assets.at("images/close1.png")),format.raw/*1320.117*/(""""></button>
          <h4 class="modal-title">Select Offer For """),_display_(/*1321.53*/if(vehicleData!=null)/*1321.74*/{_display_(Seq[Any](format.raw/*1321.75*/("""
		"""),format.raw/*1322.3*/("""<b>  """),_display_(/*1322.9*/{vehicleData.getVehicleRegNo()}),format.raw/*1322.40*/("""</b>
		  """)))}),format.raw/*1323.6*/(""" """),format.raw/*1323.7*/("""</h4>
        </div>
        <div class="modal-body ofeerData">
          <table class="table table-striped">
    <thead class="makefixed">
      <tr>
	  <th>#</th>
        <th>OFFER&nbsp;CODE</th>
        <th>OFFER&nbsp;DETAIL</th>
        <th>CONDITION</th>
		<th>BENEFIT</th>
        <th>VALIDITY</th>
      </tr>
    </thead>
   <tbody>
     """),_display_(/*1338.7*/for(offer_list <- offersList) yield /*1338.36*/{_display_(Seq[Any](format.raw/*1338.37*/("""
           """),format.raw/*1339.12*/("""<tr id="row12">
        <td><input type="radio" class="case" name="case[]" value=""""),_display_(/*1340.68*/offer_list/*1340.78*/.id),format.raw/*1340.81*/("""" ></td>
		 <td>"""),_display_(/*1341.9*/offer_list/*1341.19*/.offerCode),format.raw/*1341.29*/("""</td>
        <td>"""),_display_(/*1342.14*/offer_list/*1342.24*/.offerDetails),format.raw/*1342.37*/("""</td>
        <td>"""),_display_(/*1343.14*/offer_list/*1343.24*/.offerConditions),format.raw/*1343.40*/("""</td>
		  <td>"""),_display_(/*1344.10*/offer_list/*1344.20*/.offerBenefit),format.raw/*1344.33*/("""</td>
        <td>"""),_display_(/*1345.14*/offer_list/*1345.24*/.offerValidity),format.raw/*1345.38*/("""</td>
      </tr>
	  """)))}),format.raw/*1347.5*/("""
    """),format.raw/*1348.5*/("""</tbody>
  </table>
      </div>
        <div class="modal-footer" style="padding: 7px;">
          <button type="button" class="btn btn-success btn-xs" id="applyOffers">Apply&nbsp;Offer</button>
        </div>
		<div id="offerSelected">
		<b style="margin-left:6px;">OFFER SELECTED</b>
<div class="modal-footer" style="border: 2px solid #1797be;margin-left: 6px;margin-right: 5px;">
<div class="col-sm-10 pull-left">
<span id="selectChkVal" style="text-align:left"></span>
<input type="hidden" id="offerSelectedId" name="offerSelId" value=0>

</div>
<div class="col-sm-2 pull-right">
          <button type="button" class="btn btn-danger btn-xs removeOffer" >REMOVE OFFER</button>
		  
		  </div>
		  
		 
        </div>
		
		</br>
      </div>
	  <div style="text-align:center">
	   <button type="button" class="btn btn-success saveOffer" data-dismiss="modal" style="margin-top:-24px;">SAVE</button><br />
	   </div>
      </div>
    </div>
  </div>
  
  
  <div class="modal fade" id="DNDConfirm" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1386.80*/routes/*1386.86*/.Assets.at("images/close1.png")),format.raw/*1386.117*/(""""></button>
          <h4 class="modal-title" style="text-align:center;">Do Not Disturb(DND) Tagging</h4>
        </div>
        <div class="modal-body">
          <b>Do you want to tag the customer """),_display_(/*1390.47*/customerData/*1390.59*/.getCustomerName()),format.raw/*1390.77*/("""  """),format.raw/*1390.79*/("""as �Do Not Disturb /Call� ?<br> Click �YES� to confirm</b><br><br>
		  
  <div class="radio-inline">
      <label><input type="radio" name="RadioDND" id="RadioDNDYes" value="makeDND">YES</label>
    </div>
    <div class="radio-inline">
      <label><input type="radio" name="RadioDND" id="RadioDNDNo" value="removeDND">NO</label>
    </div>
        </div>
        <div class="modal-footer" style="text-align:center;" >
         
<button type="button" class="btn btn-success" id="submitDndBtn" data-dismiss="modal"
>submit</button>		  
        </div>
      </div>
      
    </div>
  </div>
  
     <div class="modal fade" id="smsPopuId" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="text-align:center;background-color: #106A86;color: #fff;">
          <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1415.80*/routes/*1415.86*/.Assets.at("images/close1.png")),format.raw/*1415.117*/(""""></button>
          <h4 class="modal-title" ><b>SMS OPTIONS<b></h4>
        </div>
    
    <div class="">
		<div class="col-md-12">
		<div class="col-md-6">
			<div class="form-group">
  <input type="hidden" id="smsrequestReference" value=""""),_display_(/*1423.57*/{vehicleData.getVehicle_id()}),format.raw/*1423.86*/("""" />
    <label for="comment">SMS Type:</label>
  
    <select class="form-control" id="smstype" onchange="getSMSbyLocAndSMSType();">
      <option value="0" >--Select--</option>
  
      """),_display_(/*1429.8*/for(smsTemplate<-smsTemplates) yield /*1429.38*/{_display_(Seq[Any](format.raw/*1429.39*/("""

   		"""),format.raw/*1431.6*/("""<option value=""""),_display_(/*1431.22*/smsTemplate/*1431.33*/.getSmsType()),format.raw/*1431.46*/("""">"""),_display_(/*1431.49*/smsTemplate/*1431.60*/.getSmsType()),format.raw/*1431.73*/("""</option>
   	""")))}),format.raw/*1432.6*/("""
   

  """),format.raw/*1435.3*/("""</select>
  
  
  
  
  
</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
  <label for="sel1">Location</label>
    <select class="form-control" id="locId" onchange="getSMSbyLocAndSMSType();">
        <option value="0" >--Select--</option>
  
  """),_display_(/*1449.4*/for(locList<-locationList) yield /*1449.30*/{_display_(Seq[Any](format.raw/*1449.31*/("""
   		"""),format.raw/*1450.6*/("""<option value=""""),_display_(/*1450.22*/locList/*1450.29*/.getCityId()),format.raw/*1450.41*/("""">"""),_display_(/*1450.44*/locList/*1450.51*/.getName()),format.raw/*1450.61*/("""</option>
   	""")))}),format.raw/*1451.6*/("""
   

  """),format.raw/*1454.3*/("""</select>
</div>
		</div>
		</div>
		<div class="col-md-12">
			<div class="col-md-12">
			<div class="form-group">
  <label for="comment">SMS Template:</label>
  <textarea class="form-control" rows="5" id="smstemplate"></textarea>
</div>
		</div>
		</div>
		
		
  <!--  <ul class="nav nav-pills nav-stacked col-md-3">
   """),_display_(/*1469.5*/for(smsTemplate<-smsTemplates) yield /*1469.35*/{_display_(Seq[Any](format.raw/*1469.36*/("""
   		"""),format.raw/*1470.6*/("""<li><a class="clssTemplType" id="typAnch_"""),_display_(/*1470.48*/{smsTemplate.getSmsId()}),format.raw/*1470.72*/("""" href="#tab_"""),_display_(/*1470.86*/{smsTemplate.getSmsId()}),format.raw/*1470.110*/("""" data-toggle="pill">"""),_display_(/*1470.132*/{smsTemplate.getSmsType()}),format.raw/*1470.158*/("""</a></li>
   	""")))}),format.raw/*1471.6*/("""
 
"""),format.raw/*1473.1*/("""</ul>
<div class="tab-content col-md-9 smstabclass">
  """),_display_(/*1475.4*/for(smsTemplate<-smsTemplates) yield /*1475.34*/{_display_(Seq[Any](format.raw/*1475.35*/("""
  	

        """),format.raw/*1478.9*/("""<div class="tab-pane """),_display_(/*1478.31*/if(smsTemplate.getSmsId()==1)/*1478.60*/{_display_(Seq[Any](format.raw/*1478.61*/("""active""")))}),format.raw/*1478.68*/("""" id="tab_"""),_display_(/*1478.79*/{smsTemplate.getSmsId()}),format.raw/*1478.103*/("""">
        <input type="hidden" id="typ_"""),_display_(/*1479.39*/{smsTemplate.getSmsId()}),format.raw/*1479.63*/(""""  />
             <h4><u>"""),_display_(/*1480.22*/{smsTemplate.getSmsType()}),format.raw/*1480.48*/("""</u></h4>
			<textarea name="text" rows="5" cols="45" id="txt_"""),_display_(/*1481.54*/{smsTemplate.getSmsId()}),format.raw/*1481.78*/("""" class="cl_"""),_display_(/*1481.91*/{smsTemplate.getSmsId()}),format.raw/*1481.115*/("""" disabled="disabled" style="border:none; background-color:#fff;">"""),_display_(/*1481.182*/{smsTemplate.getSmsTemplate()}),format.raw/*1481.212*/("""</textarea>
<br/>
		<input type="button" name="set_Value" id="ed_"""),_display_(/*1483.49*/{smsTemplate.getSmsId()}),format.raw/*1483.73*/("""" class="btn btn-info cmmnbtnclass"  value="Edit"  >
     

        </div>
 
 """)))}),format.raw/*1488.3*/("""
 """),format.raw/*1489.2*/("""</div>	 -->
<!-- tab content -->
        </div>
    
    
    
    
    
    
    
        <div class="modal-footer">
          <button type="button" id="smsSendbtn" class="btn btn-success" data-dismiss="modal">SEND SMS</button>
        </div>
      </div>
    </div>
  </div>
 

  <!-- Modal -->
  
  
  <div class="modal fade" id="myModalAddPhone" role="dialog">
    <div class="modal-dialog modal-xs">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1516.80*/routes/*1516.86*/.Assets.at("images/close1.png")),format.raw/*1516.117*/(""""></button>
          <h4 class="modal-title">ADD PHONE NUMBER</h4>
        </div>
		
		
        <div class="modal-body">
		<div class="row">
        <div class="col-xs-12">
        <div class="col-xs-4">
        <input type="text" name="myPhoneNum" class="form-control numberOnly" maxlength="10" min="10" max="10" id="myPhoneNum" >
      </div>
        </div>
        </div>
		</div>
        <div class="modal-footer">
   <button type="button" class="btn btn-default" data-dismiss="modal" id="savePhoneCust">SAVE</button>
        </div>
      </div>
      
    </div>
  </div>
   <!--------------------------------Side bar popup start------------------------------------------>
  <div id="fixedsocial">
    <div class="facebookflat" data-toggle="modal" data-target="#myModal1" >
	<!-- <img src='"""),_display_(/*1540.18*/routes/*1540.24*/.Assets.at("images/car.png")),format.raw/*1540.52*/("""' style="width: 50px;" /> -->
	<img src='"""),_display_(/*1541.13*/routes/*1541.19*/.Assets.at("images/sports-car.jpg")),format.raw/*1541.54*/("""' style="width: 50px; border-radius: 5px;" data-toggle="tooltip" data-placement="left" title="Vehicle" />
	</div>
    <div class="twitterflat" data-toggle="modal" data-target="#myModal2" >
	<img src='"""),_display_(/*1544.13*/routes/*1544.19*/.Assets.at("images/service.jpg")),format.raw/*1544.51*/("""' style="width: 50px;border-radius: 5px;" data-toggle="tooltip" data-placement="left" title="Service" onclick="ajaxCallServiceLoadOfCustomer();"/>
	<!-- <img src='"""),_display_(/*1545.18*/routes/*1545.24*/.Assets.at("images/car-insurance.png")),format.raw/*1545.62*/("""' style="width: 50px;" /> -->
	</div> 
     <div class="twitterflat1" data-toggle="modal" data-target="#myModal3" >
	 <img src='"""),_display_(/*1548.14*/routes/*1548.20*/.Assets.at("images/car-insu.jpg")),format.raw/*1548.53*/("""' style="width: 50px; border-radius: 5px;" data-toggle="tooltip" data-placement="left" title="Insurance"/> 
	 <!-- <img src='"""),_display_(/*1549.19*/routes/*1549.25*/.Assets.at("images/INSURANCE1.png")),format.raw/*1549.60*/("""' style="width: 50px;" /> --> 
	 </div> 
      
</div>
<div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding: 5px; text-align: center;">
          <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1559.80*/routes/*1559.86*/.Assets.at("images/close1.png")),format.raw/*1559.117*/(""""></button>
          <h4 class="modal-title">VEHICLE INFORMATION</h4>
        </div>
        <div class="modal-body"  style="padding: 5px;">
		<div class="table table-responsive">
          <table id="example700" class = "table table-bordered">
 
  <thead class="makefixed">
				   <tr>
				  <th >Model</th>
			   <th>Variant</th>
				<th >Color</th>
			   <th >Registration&nbsp;No</th>
				<th>Engine&nbsp;No</th>
				<th >Chassis&nbsp;No</th>
				  <th >Sale&nbsp;Date</th>
				<th >Fuel&nbsp;Type</th>
				</tr>
			  </thead>
			  <tbody>
			   
			"""),_display_(/*1580.5*/for(post <- customerData.getVehicles()) yield /*1580.44*/ {_display_(Seq[Any](format.raw/*1580.46*/("""
			"""),format.raw/*1581.4*/("""<tr>
			  <td><input type="text-primary" id="model" value=""""),_display_(/*1582.56*/post/*1582.60*/.getModel()),format.raw/*1582.71*/(""""  style="border:none;text-align:center" readonly></td>
			  <td><input type="text-primary" id="variant" value=""""),_display_(/*1583.58*/post/*1583.62*/.getVariant()),format.raw/*1583.75*/("""" style="border:none;text-align:center" readonly></td>
			  <td><input type="text-primary" id="color" value=""""),_display_(/*1584.56*/post/*1584.60*/.getColor()),format.raw/*1584.71*/("""" style="border:none;text-align:center" readonly></td>
			  <td>
			  <div class="col-sm-12">
			  <div class="col-sm-8">
			  <input type="text-primary" class="vehicalRegNo" id="vehicalRegNo" value=""""),_display_(/*1588.80*/post/*1588.84*/.getVehicleRegNo()),format.raw/*1588.102*/("""" style="border:none;margin-right: 15px;" readonly>
				</p>
				</div>
				<div class="col-sm-2">
				  <!-- <input type="button" value="+"  id="editRegistrationno" style="color: rgb(255, 255, 255); background-color: rgba(0, 166, 90, 0.85098);"> -->
				  
				  <input type="button" value="+"  id="" style="color: rgb(255, 255, 255); background-color: rgba(0, 166, 90, 0.85098);">
				 </div>
				 <div class="col-sm-2">
				  <!-- <input type="button" class="btn btn-success btn-xs" value="Add" id="updateRegistrationno" onmouseenter="ajaxAddRegistrationno();" style="display:none;"> -->
				  
				</div>
			  </div></td>
			  <td> 
			  <div class="col-sm-12">
			   <div class="col-sm-8">
			  <input type="text-primary" id="engineNo" value=""""),_display_(/*1604.55*/post/*1604.59*/.getEngineNo()),format.raw/*1604.73*/("""" style="border:none;margin-right: 15px;" readonly>
				</p>
				  </div>
			<!-- <div class="col-sm-2">
				  <input type="button" value="+" id="editEngineno" style="color: rgb(255, 255, 255); background-color: rgba(0, 166, 90, 0.85098);">
				</div>  <div class="col-sm-2">
				  <input type="button" class="btn btn-success btn-xs" value="Add" id="updateEngineno" onmouseenter="ajaxAddEngineno();" style="display:none;">
				</div>-->
				</div></td>
			  <td><div class="col-sm-12 ">
			   <div class="col-sm-8">
			  <input type="text-primary" id="chassisNo" value=""""),_display_(/*1615.56*/post/*1615.60*/.getChassisNo()),format.raw/*1615.75*/("""" style="border:none;margin-right: 15px;" readonly>
				 </div>
			<!--	<div class="col-sm-2">
				  <input type="button" value="+" id="editChassisno" style="color: rgb(255, 255, 255); background-color: rgba(0, 166, 90, 0.85098);">
				  </div>  
				  <div class="col-sm-2">  
				  <input type="button" class="btn btn-success btn-xs" value="Add" id="updateChassisno" onmouseenter="ajaxAddChassisno();" style="display:none;">
				</div>-->
				</div></td>
			    <td><input type="text-primary" id="saleDate" value=""""),_display_(/*1624.61*/post/*1624.65*/.getSaleDateStr()),format.raw/*1624.82*/("""" style="border:none;text-align:center" readonly></td>
			  <td><input type="text-primary" id="fuelType" value=""""),_display_(/*1625.59*/post/*1625.63*/.getFuelType()),format.raw/*1625.77*/("""" style="border:none;text-align:center" readonly></td>
			</tr>			""")))}),format.raw/*1626.13*/("""
				"""),format.raw/*1627.5*/("""</tbody>
</table>
</div>
        </div>
     
      </div>
      
    </div>
  </div>
  
  <!-- Modal -->
  <div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog modal-lg" style="width:1200px;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding: 5px; text-align: center;">
          <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1644.80*/routes/*1644.86*/.Assets.at("images/close1.png")),format.raw/*1644.117*/(""""></button>
          <h4 class="modal-title">SERVICE INFORMATION</h4>
        </div>
        <div class="modal-body"  style="padding: 5px;">
		<div class="table table-responsive">
          <table id="example800" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
 
   <thead class="makefixed">
                <tr>
						<th>Repair Order&nbsp;Date</th>
						<th>Repair Order&nbsp;No.</th>
						<th>Bill&nbsp;Date</th>
						<th>Kilometer</th>
						<th>Model</th>
						<th>Registration No.</th>
						<th>Service Type</th>
						<th>Menu Code Description</th>
						<th>Service Advisor Name</th>
						<th>Customer Name</th>
						<th>Group (Category)</th>
						<th>Part Amount</th>
						<th>Labour Amount</th>
						<th>Total Amount</th> 
					</tr>
                  </thead>
				   <tbody>
                                   
                    </tbody>
			  
</table>

</div>
        </div>
     
      </div>
      
    </div>
  </div>
  
  <!-- Modal -->
  <div class="modal fade" id="myModal3" role="dialog">
    <div class="modal-dialog modal-lg" style="width:1200px;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding: 5px; text-align: center;">
          <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1690.80*/routes/*1690.86*/.Assets.at("images/close1.png")),format.raw/*1690.117*/(""""></button>
          <h4 class="modal-title">INSURANCE INFORMATION</h4>
        </div>
        <div class="modal-body"  style="padding: 5px;">
		<div class="table table-responsive">
          <table id="" class = "table table-bordered">
	 
	   <thead class="makefixed">
					  <tr>
					<th>Policy Issue Date</th>
					<th>Policy Number</th>
					<th>Policy type</th>
					<th>Class</th>
					<th>Add-On</th>
					<th>Due Date</th>
					<th>Gross Premium</th>
					<th>IDV</th>
					<th>OD(%)</th>
					<th>Basic OD</th>
					<th>NCB(%)</th>
					<th>NCB Value</th>
					<th>Discount</th>
					<th>OD Premium</th>
					<th>TP Premium</th>
					<th>PA Premium</th>
					<th>Legal Liability</th>
					<th>Net Liability</th>
					<th>Add-On Premium</th>
					<th>Other Premium</th>
					<th>Net Premium</th>
					<th>Tax</th>
					<!--<th>Gross Premium</th>
					<th>Coverage&nbsp;Period</th>
					<th>Policy&nbsp;Due&nbsp;Date</th>
					<th>Insurance&nbsp;Company</th>
					<th>NCB(%age)</th>
					<th>NCB&nbsp;Amount(Rs.)</th>
					<th>PREMIUM</th>
					<th>Service&nbsp;Tax</th>
					<th>Total&nbsp;Premium</th>
					<th>Last&nbsp;Renewal&nbsp;By</th>-->
					
					</tr>
				  </thead>
				<tbody>
				"""),_display_(/*1735.6*/for(insuranceData <-customerData.getInsurances()) yield /*1735.55*/{_display_(Seq[Any](format.raw/*1735.56*/("""
				 
				  """),format.raw/*1737.7*/("""<tr>
				 <!--  <td><a data-toggle="modal" data-target="#otherdetails" ><i class="fa fa-info-circle" data-toggel="tooltip" title="Other Details" style="font-size:30px;color:red;"></i></a></td> -->
					<td>"""),_display_(/*1739.11*/insuranceData/*1739.24*/.getPolicyDueDateStr()),format.raw/*1739.46*/("""</td>
					<td>"""),_display_(/*1740.11*/insuranceData/*1740.24*/.getPolicyNo()),format.raw/*1740.38*/("""</td>
					<td>"""),_display_(/*1741.11*/insuranceData/*1741.24*/.getInsuranceCompanyName()),format.raw/*1741.50*/("""</td>
					<td>"""),_display_(/*1742.11*/insuranceData/*1742.24*/.getPolicyType()),format.raw/*1742.40*/("""</td>
					<td>"""),_display_(/*1743.11*/insuranceData/*1743.24*/.getClassType()),format.raw/*1743.39*/("""</td>
					<td>"""),_display_(/*1744.11*/insuranceData/*1744.24*/.getAddOn()),format.raw/*1744.35*/("""</td>
					<td>"""),_display_(/*1745.11*/insuranceData/*1745.24*/.getPolicyDueDate()),format.raw/*1745.43*/("""</td>
					<td>"""),_display_(/*1746.11*/insuranceData/*1746.24*/.getGrossPremium()),format.raw/*1746.42*/("""</td>
					<td>"""),_display_(/*1747.11*/insuranceData/*1747.24*/.getIdv()),format.raw/*1747.33*/("""</td>
					<td>"""),_display_(/*1748.11*/insuranceData/*1748.24*/.getOdPercentage()),format.raw/*1748.42*/("""</td>
					 <td>"""),_display_(/*1749.12*/insuranceData/*1749.25*/.getNcBPercentage()),format.raw/*1749.44*/("""</td>
					<td>"""),_display_(/*1750.11*/insuranceData/*1750.24*/.getNcBAmountStr()),format.raw/*1750.42*/("""</td>
					<td>"""),_display_(/*1751.11*/insuranceData/*1751.24*/.getDiscountPercentage()),format.raw/*1751.48*/("""</td>
					<td>"""),_display_(/*1752.11*/insuranceData/*1752.24*/.getODpremium()),format.raw/*1752.39*/("""</td>
					<td>"""),_display_(/*1753.11*/insuranceData/*1753.24*/.getaPPremium()),format.raw/*1753.39*/("""</td>
					<td>"""),_display_(/*1754.11*/insuranceData/*1754.24*/.getpAPremium()),format.raw/*1754.39*/("""</td>
					<td>"""),_display_(/*1755.11*/insuranceData/*1755.24*/.getLegalLiability()),format.raw/*1755.44*/("""</td>
					<td>"""),_display_(/*1756.11*/insuranceData/*1756.24*/.getNetLiability()),format.raw/*1756.42*/("""</td>
					<td>"""),_display_(/*1757.11*/insuranceData/*1757.24*/.getAdd_ON_Premium()),format.raw/*1757.44*/("""</td>
					<td>"""),_display_(/*1758.11*/insuranceData/*1758.24*/.getOtherPremium()),format.raw/*1758.42*/("""</td>
					<td>"""),_display_(/*1759.11*/insuranceData/*1759.24*/.getPremiumAmountStr()),format.raw/*1759.46*/("""</td>
					<td>"""),_display_(/*1760.11*/insuranceData/*1760.24*/.getServiceTax()),format.raw/*1760.40*/("""</td>
				  </tr>
				  """)))}),format.raw/*1762.8*/("""
					"""),format.raw/*1763.6*/("""</tbody> 
	</table>
        </div>
		</div>
       
      </div>
      
    </div>
  </div>
  <!------------Service Popup Model---------->
  <div class="modal fade" id="myModalForInfo" role="dialog">
    <div class="modal-dialog modal-lg" style="width:1200px;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="background-color:#1797BE;text-align:center;">
          <button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*1779.80*/routes/*1779.86*/.Assets.at("images/close1.png")),format.raw/*1779.117*/(""""></button>
          <h4 class="modal-title heading" style="color:#fff"><b>SERVICE HISTORY -<<VEH No.>> - <<SERVICE DATE>><b> </h4>
        </div>
        <div class="modal-body">
         

   <div class="row" style="border:2px solid #1797BE;margin-right: -8px;margin-left: -6px;">
                <div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                    <tr>
                      <td><strong>CUSTOMER ID</strong></td>
                      <td class="text-primary"> 1311935638 </td>
                    </tr>
					
					<tr>
                      <td><strong>S/ADVISOR</strong></td>
                      <td class="text-primary"> </td>
                    </tr>
					<tr>
                      <td><strong>MILEAGE (Kms)</strong></td>
                      <td class="text-primary">34233</td>
                    </tr>
					  
					 <tr>
                      <td><strong>PART DISC</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
                   <tr>
                      <td><strong>PARTS BILL</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
                    </tbody>
                    </table>
                </div>
                <div class="col-md-4">
                 <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                 
					<tr>
                      <td><strong> JOBCARD No.</strong></td>
                      <td class="text-primary">JC13010045</td>
                    </tr>
				
					<tr>
                      <td><strong>TECHNICIAN</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					<tr>
                      <td><strong>Bill No.</strong></td>
                      <td class="text-primary">BR13010474</td>
                    </tr>
					 
					
					<tr>
                      <td><strong>LAB DISC</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					 <tr>
                      <td><strong>LAB BILL</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
                   
                  </tbody>
                </table>
                </div>
                 <div class="col-md-4">
                 <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                
					<tr>
                      <td><strong>W/SHOP</strong></td>
                      <td class="text-primary">UNIV RD</td>
                    </tr>
					
					
					<tr>
                      <td><strong>S/TYPE</strong></td>
                      <td class="text-primary">FR3</td>
                    </tr>
					<tr>
                      <td><strong>Bill Date</strong></td>
                      <td class="text-primary">10/27/2015</td>
                    </tr>
					
					
					 <tr>
                      <td><strong>DISCOUNT</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
                  
					 <tr>
                      <td><strong>BILL AMOUNT</strong></td>
                      <td class="text-primary">656577</td>
                    </tr>
                  </tbody>
                </table>
                </div>
	</div>
			<div class="row" style="border:2px solid #1797BE;margin-top: 5px;margin-right: -8px;margin-left: -6px;">
	   
		
			<h4 style="text-align:center;"><b>REMARKS<b></h4>
    			<div class="col-lg-4" style="text-align: center;">
  			       <div class="form-group">
     			    <label for="">DEFECT DETAILS</label>
     			     <textarea type="" class="form-control" rows="3" id=""  readonly></textarea>
 			     </div>
		 </div> 
  		    <div class="col-lg-4" style="    text-align: center;">
  			<div class="form-group">
     			 <label for="">LABOUR DETAILS</label>
     			 <textarea type="" class="form-control" id=""  rows="3" readonly></textarea>
 			 </div>
		 </div>
       
  		<div class="col-lg-4" style="    text-align: center;">
  			<div class="form-group">
     			 <label for="">JC REMARKS</label>
     			  <textarea type="" class="form-control" id=""  rows="3" readonly></textarea>
 			 </div>
		 </div>
		 
 

</div>

<div class="row" style="border:2px solid #1797BE;    margin-right: -8px;
    margin-left: -6px;    margin-top: 5px;">
<div class="row">
                <div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                    <tr>
                      <td><strong>3rd DAY PSF</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4" >
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                  
					<tr>
                      <td><strong>DATE</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                   
					
					<tr>
                      <td><strong>DONE BY</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
                    </tbody>
                    </table>
                </div>
				</div>
				
				
<div class="row">
                <div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                    <tr>
                      <td><strong>6th DAY PSF</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                  
					<tr>
                      <td><strong>DATE</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                   
					
					<tr>
                      <td><strong>DONE BY</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
                    </tbody>
                    </table>
                </div>
				</div>
				
<div class="row">
                <div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                    <tr>
                      <td><strong>30th DAY PSF</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                  
					<tr>
                      <td><strong>DATE</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
					
                    </tbody>
                    </table>
                </div>
				<div class="col-md-4">
                  <table class="table table-condensed table-responsive table-user-information">
                  <tbody>
                   
					
					<tr>
                      <td><strong>DONE BY</strong></td>
                      <td class="text-primary">  </td>
                    </tr>
					
                    </tbody>
                    </table>
                </div>
				</div>
</div>
        </div>
      
      </div>
      
    </div>
  </div>
	 <!------------------Calculator modal Popup------------------------->
   <!------------------Calculator modal Popup------------------------->
  <div class="modal fade" id="InsuPremiumPopup" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding: 10px; color: red">
		<button type="button" class="close" data-dismiss="modal" ><img src=""""),_display_(/*2050.72*/routes/*2050.78*/.Assets.at("images/close1.png")),format.raw/*2050.109*/(""""></button>
         
          <p class="modal-title" style="text-align:center;"><b>INSURANCE PREMIUM CALCULATOR</b></p>
        </div>
        <div class="modal-body" style="padding-bottom: 0px;">
       
        
        <b style="color:red;"><u>VEHICLE DETAILS:</u></b>
         <div class="row">
      
      <div class="col-xs-2">
       <div class="form-group">
        <label>Cubic&nbsp;Capacity(CC)</label>
         <select class="form-control input-sm" name="cubicCapacity" id="cubicCapacityId" onchange="ajaxODPercentage();">
		 <option value="1001 -1500 CC">1001 -1500 CC</option>
          <option value="<=1000 CC"><=1000 CC</option>
          <option value=">=1501 CC"> >=1501 CC</option>
        </select>
      </div>
      </div>
       <div class="col-xs-2">
        <div class="form-group">
        <label>Vehicle&nbsp;Age</label>
         <select class="form-control input-sm" name="vehicleAge" id="vehicleAgeId" onchange="ajaxODPercentage();">
          <option value="<= 5 Years"><= 5 Years</option>
          <option value="5-10 Years">5-10 Years</option>
          <option value=">10 Years"> >10 Years </option>
        </select>
      </div>
      </div>
      <div class="col-xs-2">
       <div class="form-group">
        <label>City</label>
         <select class="form-control input-sm" name="vehicleCity">
          <option value="Rest of India">Rest of India</option>
          <option value="Ahmedabad">Ahmedabad</option>
          <option value="Bangalore">Bangalore</option>
          <option value="Chennai">Chennai</option>
          <option value="Hyderabad">Hyderabad</option>
          <option value="Kolkata">Kolkata</option>
          <option value="Mumbai">Mumbai</option>
          <option value="New Delhi">New Delhi</option>
          <option value="Pune">Pune</option>
             
        </select>
      </div>
      </div>
      <div class="col-xs-2">
       <div class="form-group">
        <label>Zone</label>
         <select class="form-control input-sm" name="vehicleZone" id="zoneId" onchange="ajaxODPercentage();">
		 <option value="Zone B">Zone B</option>
         <option value="Zone A">Zone A</option>
           </select>
         </div>
         </div>

      <div class="col-xs-2">
       <div class="form-group">
        <label>Ex&nbsp;Showroom&nbsp;Price</label>
         <input type="text" class="form-control input-sm numberOnly" value="0" name="exShowroomPrice">
         </div>
         </div>
      
        </div>
         <b style="color:red;"><u>OWN DAMAGE PREMIUM (I):</u></b>
<div class="row">
         <div class="col-xs-2">
      <div class="form-group">
        <label>IDV</label>
        <input class="form-control input-sm numberOnly" id="idvId" value=""""),_display_(/*2120.76*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getIdv()}else{0}}else{0}}),format.raw/*2120.241*/("""" name="idv" type="text">
     </div>
     </div>
       <div class="col-xs-2">
       <div class="form-group">
        <label>OD(%)</label>
         <input class="form-control input-sm" id="odId" name="odPercentage" value=""""),_display_(/*2126.85*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getOdPercentage()}else{0}}else{0}}),format.raw/*2126.259*/("""" type="text" readonly>
      </div>
      </div>
      <div class="col-xs-2">
      <div class="form-group">
        <label>Basic OD</label>
         <input class="form-control input-sm" id="basicODId" name="odAmount" value=""""),_display_(/*2132.86*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getOdAmount()}else{0}}else{0}}),format.raw/*2132.256*/("""" type="text" readonly>
      </div>
      </div>
      <div class="col-xs-2">
      <div class="form-group">
        <label>NCB(%)</label>
         <input class="form-control input-sm" id="ncbPercenId" name="ncBPercentage" value=""""),_display_(/*2138.93*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getNcBPercentage()}else{0}}else{0}}),format.raw/*2138.268*/("""" type="text">
      </div>
      </div>
      <div class="col-xs-2">
      <div class="form-group">
        <label>NCB Value</label>
         <input class="form-control input-sm" id="ncbValueId"  name="ncBAmount" value=""""),_display_(/*2144.89*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getNcBAmount()}else{0}}else{0}}),format.raw/*2144.260*/("""" type="text" readonly>
      </div>
      </div>
	  </div>
 <div class="row">
      <div class="col-xs-2">
      <div class="form-group">
        <label>OD Premium</label>
         <input class="form-control input-sm" id="odPremiumId" name="ODpremium" value=""""),_display_(/*2152.89*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getODpremium()}else{0}}else{0}}),format.raw/*2152.260*/("""" type="text" readonly>
      </div>
      </div>
      <div class="col-xs-2">
      <div class="form-group">
        <label>Commercial&nbsp;Disc(%)</label>
         <input class="form-control input-sm" id="commercialDiscId" value=""""),_display_(/*2158.77*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getDiscountPercentage()}else{0}}else{0}}),format.raw/*2158.257*/("""" name="discountPercentage" type="text">
      </div>
      </div>
      <div class="col-xs-2">
      <div class="form-group">
        <label>Disc Value </label>
         <input class="form-control input-sm" id="discValueId" name="discValue" value=""""),_display_(/*2164.89*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getDiscValue()}else{0}}else{0}}),format.raw/*2164.260*/("""" type="text" readonly>
      </div>
      </div>
      <div class="col-xs-2">
      <div class="form-group">
         <label>Total&nbsp;OD&nbsp;Premium</label>
         <input class="form-control input-sm" id="totalODPremiumId" name="totalODpremium" value=""""),_display_(/*2170.99*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getTotalODpremium()}else{0}}else{0}}),format.raw/*2170.275*/("""" type="text" readonly>
      </div>
      </div>
    
    
        </div>
        <b style="color:red;"><u>LIABILITY & BENEFIT (II):</u></b>
		<b style="color:red;margin-left: 272px;"><u>ADD ON COVERS (III):</u></b>
         <div class="row">
      
      <div class="col-xs-2">
       <div class="form-group">
        <label>3rd&nbsp;Party&nbsp;Premium</label>
         <input type="text" class="form-control input-sm" name="" id="thirdPartyPremId" readonly>
      </div>
      </div>
       <div class="col-xs-2">
        <div class="form-group">
        <label>PA&nbsp;Premium</label>
         <input type="text" class="form-control input-sm" name="" value="100" readonly>
      </div>
      </div>
      <div class="col-xs-2">
       <div class="form-group">
          <label>Total&nbsp;Premium</label>
         <input type="text" class="form-control input-sm" name="" id="totalPremiumID" readonly>
      </div>
      </div>
	  
	    <div class="col-xs-2">
       <div class="form-group">
        <label>Add&nbsp;On&nbsp;Cover(%)</label>
         <input type="text" class="form-control input-sm" value=""""),_display_(/*2202.67*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getAdd_ON_CoverPercentage()}else{0}}else{0}}),format.raw/*2202.251*/("""" name="add_ON_CoverPercentage" id="add_ON_CoverPercentageId" >
      </div>
      </div>
       <div class="col-xs-2">
        <div class="form-group">
        <label>Add&nbsp;On&nbsp;Premium </label>
         <input type="text" class="form-control input-sm" name="add_ON_Premium" value=""""),_display_(/*2208.89*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getAdd_ON_Premium()}else{0}}else{0}}),format.raw/*2208.265*/("""" id="addOnPremiumId">
      </div>
      </div>
      
      
        </div>
        
	    <b style="color:red;"><u>PACKAGE PREMIUM (I + II + III):</u></b>
         <div class="row">
     
      <div class="col-xs-2">
        <div class="form-group">
         <label>Total&nbsp;Pkg&nbsp;Premium</label>
         <input type="text" class="form-control input-sm" value=""""),_display_(/*2221.67*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getPremiumAmountBeforeTax()}else{0}}else{0}}),format.raw/*2221.251*/("""" name="premiumAmountBeforeTax" id="totalPackagePremiumId" readonly>
      </div>
      </div>
      <div class="col-xs-2">
        <div class="form-group">
        <label>Service&nbsp;Tax(%)</label>
         <input type="text" class="form-control input-sm" name="serviceTax" value="15" readonly>
      </div>
      </div>
      <div class="col-xs-2">
        <div class="form-group">
        <label>Final Premium</label>
         <input type="text" class="form-control input-sm" value=""""),_display_(/*2233.67*/{if(interOfCall.getVehicle()!=null){if(interOfCall.getVehicle().getCurrentInsurance()!=null){interOfCall.getVehicle().getCurrentInsurance().getPremiumAmountAfterTax()}else{0}}else{0}}),format.raw/*2233.250*/("""" name="premiumAmountAfterTax" id="finalPremiumId" readonly style="background-color: #8BC34A; color:#fff; font-size: 18px;">
      </div>
      </div>
       </div>
</div>
      
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal" id="saveFinalPremium">Save</button>
        </div>
      </div>
      </div>
      
    </div>
	
  <!--------------------------------Side bar popup end------------------------------------------>
   <!--Popup For Driver Allocation-->
<div class="modal fade" id="DriverAllcationPOPUp" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
	   <div class="modal-header" style="text-align:center">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Driver Allocation</h4>																													
        </div>
             <div class="modal-body" style="padding:4px;">
  <div class="panel panel-primary" >
	<div class="panel-heading" style="padding:0px;"> </div>
	<div class="panel-body" style="height: 500px; overflow: auto; ">
			<div class="col-md-12">
			
			<div class="form-inline">
  <div class="form-group">
<label for="comments"><b>CHANGE SERVICE BOOKING DATE :<b></label>
<input type="text" class="form-control datepickerchange" id="changeserviceBookingDate" onchange="ajaxAssignBtnBkreview('workshop','date12345');"   readonly>
  </div>
  </div>
<br />
</div>

              <div class="col-md-6">
                <table class="table table-responsive">
                  <tbody>
                    <tr>
                      <td><strong> New Booking Date: </strong></td>
                      <td class="text-primary" id="newbookingdate"></td>
                    </tr>
                     <tr>
                      <td><strong> New Driver Name:</strong></td>
                      <td class="text-primary" id="newDriver"></td>
                    </tr>
					<tr>
                      <td><strong>New Time Slot:</strong></td>
                      <td class="text-primary" id="newTimeSlot"></td>
                    </tr>
                  </tbody>
                </table>
                </div>
              
	 <div>
        <input type="hidden" id="tempValue" value="0">
        <input type="hidden" id="tempIncreValue" value="0">
        <input type="hidden" name="time_From" id="startValue" value="0">
        <input type="hidden" name="time_To" id="endValue" value="0">
        <input type="hidden" name="driverId" id="driverValue" value="0">
      </div>
  <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" id="tableID"  style="cursor: pointer;">
    <thead>  
        
    </thead>
    <tbody> 
           
    </tbody>
  </table>
  <button type="button" id="allcateSubmit" class="btn btn-primary pull-right" data-dismiss="modal">Submit</button>
</div>
</div>
 </div>
       </div>
      
    </div>
  </div>
  <!-- Hidden div with details for Auto selection of SA  -->
  <div style="display:none">
  	<input type="hidden" id="preSaDetails"  />
					<input type="hidden" id="newSaDetails"  />
					<select class="form-control" id="serviceAdvisorTemp"  name="serviceAdvisorTemp" style="display:none">
						<option value="0">--Select--</option>
					</select>
  </div>
  
  <div style="display:none">
  	<input type="hidden" id="preSaDetailsIns"  />
					<input type="hidden" id="newSaDetailsIns"  />
					<select class="form-control" id="serviceAdvisorTempIns"  name="serviceAdvisorTempIns" style="display:none">
						<option value="0">--Select--</option>
					</select>
  </div>

""")))}),format.raw/*2332.2*/("""
"""),format.raw/*2333.1*/("""<script src=""""),_display_(/*2333.15*/routes/*2333.21*/.Assets.at("javascripts/commondisposition.js")),format.raw/*2333.67*/("""" type="text/javascript"></script>
""")))}))
      }
    }
  }

  def render(oem:String,statesList:List[String],latestinsurance:Insurance,uniqueid:Long,dealercode:String,dealerName:String,userName:String,customerData:Customer,vehicleData:Vehicle,locationList:List[Location],userData:WyzUser,latestService:Service,smsTemplates:List[SMSTemplate],complaintOFCust:List[String],servicetypeList:List[ServiceTypes],interOfCall:CallInteraction,offersList:List[SpecialOfferMaster],typeDispo:String,dispoOut:Html,dispoInBound:Html): play.twirl.api.HtmlFormat.Appendable = apply(oem,statesList,latestinsurance,uniqueid,dealercode,dealerName,userName,customerData,vehicleData,locationList,userData,latestService,smsTemplates,complaintOFCust,servicetypeList,interOfCall,offersList,typeDispo,dispoOut,dispoInBound)

  def f:((String,List[String],Insurance,Long,String,String,String,Customer,Vehicle,List[Location],WyzUser,Service,List[SMSTemplate],List[String],List[ServiceTypes],CallInteraction,List[SpecialOfferMaster],String,Html,Html) => play.twirl.api.HtmlFormat.Appendable) = (oem,statesList,latestinsurance,uniqueid,dealercode,dealerName,userName,customerData,vehicleData,locationList,userData,latestService,smsTemplates,complaintOFCust,servicetypeList,interOfCall,offersList,typeDispo,dispoOut,dispoInBound) => apply(oem,statesList,latestinsurance,uniqueid,dealercode,dealerName,userName,customerData,vehicleData,locationList,userData,latestService,smsTemplates,complaintOFCust,servicetypeList,interOfCall,offersList,typeDispo,dispoOut,dispoInBound)

  def ref: this.type = this

}


}

/**/
object commonDispositionPage extends commonDispositionPage_Scope0.commonDispositionPage
              /*
                  -- GENERATED --
                  DATE: Fri Mar 16 17:47:58 IST 2018
                  SOURCE: D:/CRMFORDAUTOSHERPA/crmford/app/views/commonDispositionPage.scala.html
                  HASH: c94e96b289ec45694613e2c25c2c9e4275171dc8
                  MATRIX: 983->1|1524->446|1552->449|1623->512|1662->514|1689->515|1736->535|1764->536|1793->539|1844->564|1871->565|1901->569|1937->578|1965->579|1995->582|2045->605|2073->606|2101->607|2138->618|2152->624|2233->696|2272->697|2300->698|2357->728|2372->734|2418->759|2490->804|2542->835|2653->919|2683->928|2766->984|2885->1081|2980->1149|3023->1171|3116->1237|3155->1255|3247->1320|3297->1349|3383->1408|3412->1416|3494->1471|3592->1547|3723->1651|3780->1687|3865->1745|3907->1766|3983->1815|4087->1897|4187->1970|4304->2065|4383->2117|4504->2216|4585->2270|4698->2361|4776->2412|4893->2507|4979->2566|5092->2657|5175->2713|5289->2805|5591->3080|5807->3275|5836->3276|6010->3423|6138->3530|6411->3776|6484->3827|6793->4109|6852->4152|6891->4153|6941->4176|7352->4566|7394->4577|7438->4593|7719->4847|7754->4873|7793->4874|7833->4886|8114->5136|8142->5137|8741->5709|8811->5757|8958->5876|9028->5924|9060->5928|9090->5936|9122->5940|9144->5952|9174->5960|10100->6857|10252->6987|10282->6988|10457->7135|10518->7173|10645->7272|10694->7298|10969->7544|11391->7945|11420->7946|11637->8135|11700->8175|11731->8176|11764->8180|11825->8218|11946->8311|12004->8346|12216->8530|12272->8563|12492->8755|12537->8790|12577->8791|12640->8826|12705->8874|12745->8875|12791->8892|12939->9011|13016->9077|13057->9078|13103->9096|13127->9110|13159->9120|13197->9138|13211->9142|13251->9143|13297->9160|13349->9180|13379->9181|13442->9212|13492->9230|13525->9235|13892->9574|13908->9580|13959->9608|14129->9751|14160->9772|14200->9773|14245->9790|14283->9800|14333->9828|14395->9862|14449->9893|14512->9927|14559->9951|14620->9984|14668->10010|14717->10031|14763->10055|14843->10107|14908->10150|15128->10341|15181->10371|15251->10410|15286->10417|16272->11374|16296->11387|16343->11411|16374->11412|16424->11433|16448->11446|16492->11467|16574->11521|16628->11553|16716->11613|16771->11646|16854->11701|16915->11740|17176->11973|17192->11979|17253->12017|17411->12148|17442->12169|17482->12170|17511->12171|17683->12314|17707->12327|17754->12351|17805->12373|17829->12386|17871->12405|17921->12426|17945->12439|17989->12460|18049->12492|18072->12505|18106->12517|18155->12539|18265->12628|18298->12630|18368->12672|18391->12685|18432->12704|18680->12924|18696->12930|18748->12960|19069->13252|19131->13291|19245->13377|19311->13420|19409->13490|19462->13521|19558->13589|19624->13633|19881->13862|19897->13868|19965->13913|20380->14300|20439->14337|20651->14521|20700->14548|20975->14795|20991->14801|21049->14837|21370->15129|21396->15144|21426->15151|21617->15314|21642->15329|21671->15336|21781->15418|21806->15433|21835->15440|21937->15514|21962->15529|21991->15536|22421->15938|22437->15944|22491->15975|22909->16365|22932->16377|22973->16395|23232->16626|23255->16638|23294->16654|24008->17339|24154->17464|24187->17470|24333->17595|24366->17601|24512->17726|24988->18173|25134->18298|25167->18304|25313->18429|25346->18435|25492->18560|25927->18966|26067->19085|26100->19091|26240->19210|26273->19216|26413->19335|27458->20352|27518->20395|27558->20396|27609->20419|27916->20704|27959->20715|28004->20731|28441->21140|28487->21163|28734->21382|28793->21418|29213->21810|29273->21853|29313->21854|29364->21877|29671->22162|29714->22173|29759->22189|30575->22977|30633->23012|31626->23977|31682->24010|32218->24517|32281->24557|32544->24791|32605->24829|32885->25080|32936->25108|32968->25111|33019->25139|33622->25714|33638->25720|33692->25751|34341->26373|34375->26385|34410->26392|36908->28863|36944->28889|36984->28890|37021->28899|37420->29267|37453->29273|37531->29341|37571->29342|37611->29354|38005->29717|38034->29719|38113->29788|38153->29789|38185->29793|38699->30276|38730->30279|38975->30497|39005->30505|39046->30518|39447->30890|39464->30896|39531->30940|40396->31778|40432->31804|40472->31805|40507->31812|40672->31946|40705->31951|44710->35928|44726->35934|44780->35965|45669->36827|45711->36852|45751->36853|45803->36876|45847->36892|45875->36898|45906->36901|45934->36907|46010->36952|46046->36960|46396->37281|46525->37387|47186->38020|47202->38026|47256->38057|48216->38990|48258->39015|48298->39016|48350->39039|48394->39055|48422->39061|48453->39064|48481->39070|48557->39115|48593->39123|48925->39426|49054->39532|49717->40166|49734->40172|49789->40203|50108->40492|50237->40597|50459->40789|50588->40894|50805->41081|50934->41186|51245->41469|51288->41494|51329->41495|51382->41518|51427->41534|51456->41540|51488->41543|51517->41549|51594->41594|51631->41602|52000->41941|52124->42041|52898->42786|52915->42792|52970->42823|53939->43763|53970->43771|54561->44333|54592->44341|54623->44342|55065->44755|55082->44761|55137->44792|55961->45587|55978->45593|56033->45624|56966->46528|56983->46534|57038->46565|57804->47302|57821->47308|57876->47339|58796->48231|58839->48256|58880->48257|58933->48280|58978->48296|59007->48302|59039->48305|59068->48311|59144->48355|59174->48356|60236->49389|60253->49395|60308->49426|60941->50030|60958->50036|61013->50067|61106->50131|61138->50152|61179->50153|61211->50156|61245->50162|61299->50193|61341->50203|61371->50204|61746->50551|61793->50580|61834->50581|61876->50593|61988->50676|62009->50686|62035->50689|62080->50706|62101->50716|62134->50726|62182->50745|62203->50755|62239->50768|62287->50787|62308->50797|62347->50813|62391->50828|62412->50838|62448->50851|62496->50870|62517->50880|62554->50894|62608->50916|62642->50921|63898->52148|63915->52154|63970->52185|64199->52385|64222->52397|64263->52415|64295->52417|65254->53347|65271->53353|65326->53384|65599->53628|65651->53657|65868->53846|65916->53876|65957->53877|65993->53884|66038->53900|66060->53911|66096->53924|66128->53927|66150->53938|66186->53951|66233->53966|66270->53974|66563->54239|66607->54265|66648->54266|66683->54272|66728->54288|66746->54295|66781->54307|66813->54310|66831->54317|66864->54327|66911->54342|66948->54350|67299->54673|67347->54703|67388->54704|67423->54710|67494->54752|67541->54776|67584->54790|67632->54814|67684->54836|67734->54862|67781->54877|67813->54880|67897->54936|67945->54966|67986->54967|68029->54981|68080->55003|68120->55032|68161->55033|68201->55040|68241->55051|68289->55075|68359->55116|68406->55140|68462->55167|68511->55193|68603->55256|68650->55280|68692->55293|68740->55317|68837->55384|68891->55414|68986->55480|69033->55504|69144->55583|69175->55585|69790->56171|69807->56177|69862->56208|70688->57005|70705->57011|70756->57039|70827->57081|70844->57087|70902->57122|71132->57323|71149->57329|71204->57361|71397->57525|71414->57531|71475->57569|71633->57698|71650->57704|71706->57737|71861->57863|71878->57869|71936->57904|72336->58275|72353->58281|72408->58312|72992->58868|73049->58907|73091->58909|73124->58913|73213->58973|73228->58977|73262->58988|73404->59101|73419->59105|73455->59118|73594->59228|73609->59232|73643->59243|73873->59444|73888->59448|73930->59466|74708->60215|74723->60219|74760->60233|75360->60804|75375->60808|75413->60823|75959->61340|75974->61344|76014->61361|76156->61474|76171->61478|76208->61492|76308->61559|76342->61564|76817->62010|76834->62016|76889->62047|78270->63399|78287->63405|78342->63436|79575->64641|79642->64690|79683->64691|79725->64704|79961->64911|79985->64924|80030->64946|80075->64962|80099->64975|80136->64989|80181->65005|80205->65018|80254->65044|80299->65060|80323->65073|80362->65089|80407->65105|80431->65118|80469->65133|80514->65149|80538->65162|80572->65173|80617->65189|80641->65202|80683->65221|80728->65237|80752->65250|80793->65268|80838->65284|80862->65297|80894->65306|80939->65322|80963->65335|81004->65353|81050->65370|81074->65383|81116->65402|81161->65418|81185->65431|81226->65449|81271->65465|81295->65478|81342->65502|81387->65518|81411->65531|81449->65546|81494->65562|81518->65575|81556->65590|81601->65606|81625->65619|81663->65634|81708->65650|81732->65663|81775->65683|81820->65699|81844->65712|81885->65730|81930->65746|81954->65759|81997->65779|82042->65795|82066->65808|82107->65826|82152->65842|82176->65855|82221->65877|82266->65893|82290->65906|82329->65922|82386->65947|82421->65953|82945->66448|82962->66454|83017->66485|91905->75344|91922->75350|91977->75381|94741->78116|94930->78281|95184->78506|95382->78680|95638->78907|95832->79077|96093->79309|96292->79484|96543->79706|96738->79877|97028->80138|97223->80309|97485->80542|97689->80722|97968->80972|98163->81143|98451->81402|98651->81578|99789->82687|99997->82871|100316->83161|100516->83337|100915->83707|101123->83891|101640->84379|101847->84562|105564->88247|105594->88248|105637->88262|105654->88268|105723->88314
                  LINES: 27->1|32->1|34->3|34->3|34->3|35->4|36->5|36->5|37->6|38->7|38->7|40->9|40->9|40->9|41->10|42->11|42->11|43->12|45->14|45->14|45->14|45->14|46->15|46->15|46->15|46->15|47->16|47->16|48->17|48->17|49->18|49->18|50->19|50->19|51->20|51->20|52->21|52->21|53->22|53->22|54->23|54->23|57->26|57->26|59->28|59->28|61->30|61->30|62->31|62->31|63->32|63->32|64->33|64->33|65->34|65->34|66->35|66->35|67->36|67->36|79->48|87->56|87->56|89->58|93->62|99->68|99->68|101->70|101->70|101->70|103->72|111->80|112->81|113->82|116->85|116->85|116->85|117->86|118->87|119->88|126->95|126->95|127->96|127->96|127->96|127->96|127->96|127->96|127->96|138->107|140->109|140->109|143->112|143->112|144->113|144->113|149->118|157->126|157->126|162->131|162->131|162->131|162->131|162->131|163->132|163->132|168->137|168->137|175->144|175->144|175->144|177->146|177->146|177->146|178->147|178->147|178->147|178->147|179->148|179->148|179->148|180->149|180->149|180->149|181->150|182->151|182->151|183->152|184->153|185->154|196->165|196->165|196->165|198->167|198->167|198->167|201->170|201->170|201->170|201->170|201->170|201->170|201->170|202->171|202->171|202->171|202->171|203->172|203->172|204->173|204->173|207->176|208->177|229->198|229->198|229->198|229->198|229->198|229->198|229->198|231->200|231->200|232->201|232->201|233->202|233->202|245->214|245->214|245->214|247->216|247->216|247->216|248->217|248->217|248->217|248->217|248->217|248->217|248->217|248->217|248->217|248->217|249->218|249->218|249->218|250->219|253->222|254->223|255->224|255->224|255->224|265->234|265->234|265->234|270->239|270->239|271->240|271->240|272->241|272->241|273->242|273->242|284->253|284->253|284->253|288->257|288->257|290->259|290->259|302->271|302->271|302->271|304->273|304->273|304->273|306->275|306->275|306->275|307->276|307->276|307->276|308->277|308->277|308->277|322->291|322->291|322->291|331->300|331->300|331->300|337->306|337->306|337->306|354->323|358->327|359->328|363->332|364->333|368->337|377->346|381->350|382->351|386->355|387->356|391->360|399->368|403->372|404->373|408->377|409->378|413->382|438->407|438->407|438->407|440->409|445->414|446->415|447->416|455->424|455->424|459->428|459->428|468->437|468->437|468->437|470->439|475->444|476->445|477->446|493->462|493->462|513->482|513->482|525->494|525->494|529->498|529->498|533->502|533->502|533->502|533->502|560->529|560->529|560->529|581->550|581->550|583->552|654->623|654->623|654->623|656->625|668->637|670->639|670->639|670->639|672->641|684->653|684->653|684->653|684->653|685->654|703->672|704->673|719->688|719->688|721->690|733->702|733->702|733->702|747->716|747->716|747->716|748->717|749->718|750->719|929->898|929->898|929->898|951->920|951->920|951->920|953->922|953->922|953->922|953->922|953->922|955->924|957->926|968->937|968->937|985->954|985->954|985->954|1008->977|1008->977|1008->977|1010->979|1010->979|1010->979|1010->979|1010->979|1012->981|1014->983|1025->994|1025->994|1043->1012|1043->1012|1043->1012|1050->1019|1050->1019|1054->1023|1054->1023|1057->1026|1057->1026|1066->1035|1066->1035|1066->1035|1068->1037|1068->1037|1068->1037|1068->1037|1068->1037|1070->1039|1072->1041|1081->1050|1081->1050|1101->1070|1101->1070|1101->1070|1130->1099|1130->1099|1147->1116|1147->1116|1147->1116|1160->1129|1160->1129|1160->1129|1192->1161|1192->1161|1192->1161|1228->1197|1228->1197|1228->1197|1258->1227|1258->1227|1258->1227|1283->1252|1283->1252|1283->1252|1285->1254|1285->1254|1285->1254|1285->1254|1285->1254|1287->1256|1288->1257|1329->1298|1329->1298|1329->1298|1351->1320|1351->1320|1351->1320|1352->1321|1352->1321|1352->1321|1353->1322|1353->1322|1353->1322|1354->1323|1354->1323|1369->1338|1369->1338|1369->1338|1370->1339|1371->1340|1371->1340|1371->1340|1372->1341|1372->1341|1372->1341|1373->1342|1373->1342|1373->1342|1374->1343|1374->1343|1374->1343|1375->1344|1375->1344|1375->1344|1376->1345|1376->1345|1376->1345|1378->1347|1379->1348|1417->1386|1417->1386|1417->1386|1421->1390|1421->1390|1421->1390|1421->1390|1446->1415|1446->1415|1446->1415|1454->1423|1454->1423|1460->1429|1460->1429|1460->1429|1462->1431|1462->1431|1462->1431|1462->1431|1462->1431|1462->1431|1462->1431|1463->1432|1466->1435|1480->1449|1480->1449|1480->1449|1481->1450|1481->1450|1481->1450|1481->1450|1481->1450|1481->1450|1481->1450|1482->1451|1485->1454|1500->1469|1500->1469|1500->1469|1501->1470|1501->1470|1501->1470|1501->1470|1501->1470|1501->1470|1501->1470|1502->1471|1504->1473|1506->1475|1506->1475|1506->1475|1509->1478|1509->1478|1509->1478|1509->1478|1509->1478|1509->1478|1509->1478|1510->1479|1510->1479|1511->1480|1511->1480|1512->1481|1512->1481|1512->1481|1512->1481|1512->1481|1512->1481|1514->1483|1514->1483|1519->1488|1520->1489|1547->1516|1547->1516|1547->1516|1571->1540|1571->1540|1571->1540|1572->1541|1572->1541|1572->1541|1575->1544|1575->1544|1575->1544|1576->1545|1576->1545|1576->1545|1579->1548|1579->1548|1579->1548|1580->1549|1580->1549|1580->1549|1590->1559|1590->1559|1590->1559|1611->1580|1611->1580|1611->1580|1612->1581|1613->1582|1613->1582|1613->1582|1614->1583|1614->1583|1614->1583|1615->1584|1615->1584|1615->1584|1619->1588|1619->1588|1619->1588|1635->1604|1635->1604|1635->1604|1646->1615|1646->1615|1646->1615|1655->1624|1655->1624|1655->1624|1656->1625|1656->1625|1656->1625|1657->1626|1658->1627|1675->1644|1675->1644|1675->1644|1721->1690|1721->1690|1721->1690|1766->1735|1766->1735|1766->1735|1768->1737|1770->1739|1770->1739|1770->1739|1771->1740|1771->1740|1771->1740|1772->1741|1772->1741|1772->1741|1773->1742|1773->1742|1773->1742|1774->1743|1774->1743|1774->1743|1775->1744|1775->1744|1775->1744|1776->1745|1776->1745|1776->1745|1777->1746|1777->1746|1777->1746|1778->1747|1778->1747|1778->1747|1779->1748|1779->1748|1779->1748|1780->1749|1780->1749|1780->1749|1781->1750|1781->1750|1781->1750|1782->1751|1782->1751|1782->1751|1783->1752|1783->1752|1783->1752|1784->1753|1784->1753|1784->1753|1785->1754|1785->1754|1785->1754|1786->1755|1786->1755|1786->1755|1787->1756|1787->1756|1787->1756|1788->1757|1788->1757|1788->1757|1789->1758|1789->1758|1789->1758|1790->1759|1790->1759|1790->1759|1791->1760|1791->1760|1791->1760|1793->1762|1794->1763|1810->1779|1810->1779|1810->1779|2081->2050|2081->2050|2081->2050|2151->2120|2151->2120|2157->2126|2157->2126|2163->2132|2163->2132|2169->2138|2169->2138|2175->2144|2175->2144|2183->2152|2183->2152|2189->2158|2189->2158|2195->2164|2195->2164|2201->2170|2201->2170|2233->2202|2233->2202|2239->2208|2239->2208|2252->2221|2252->2221|2264->2233|2264->2233|2363->2332|2364->2333|2364->2333|2364->2333|2364->2333
                  -- GENERATED --
              */
          