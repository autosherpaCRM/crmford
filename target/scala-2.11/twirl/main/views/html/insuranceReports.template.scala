
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object insuranceReports_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class insuranceReports extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template7[String,String,String,String,List[String],List[String],List[Campaign],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(fromDateValue:String,toDateValue:String,user:String,dealerName:String,allUsers:List[String],locations:List[String],campaignListInsurance :List[Campaign]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.156*/("""

"""),_display_(/*4.2*/mainPageCREManger("AutoSherpaCRM",user,dealerName)/*4.52*/ {_display_(Seq[Any](format.raw/*4.54*/("""

"""),format.raw/*6.1*/("""<input type="hidden" name="fromDateToAss" value=""""),_display_(/*6.51*/fromDateValue),format.raw/*6.64*/("""">
<input type="hidden" name="toDateToAss" value=""""),_display_(/*7.49*/toDateValue),format.raw/*7.60*/("""">
<div class="panel panel-primary">
        <div class="panel-heading"><strong>Select Date Range</strong></div>
        <div class="panel-body"> 
              """),_display_(/*11.16*/helper/*11.22*/.form(action = routes.AllCallInteractionController.downloadInsuranceHistoryReport)/*11.104*/ {_display_(Seq[Any](format.raw/*11.106*/("""
        
  """),format.raw/*13.3*/("""<div class="row">
  <div class="col-md-2">   
      <div class="form-group">
        
<label>Call Date From :<span style="color:red">*</span></label>    
	   
		<input type="text" class="form-control datepickerFilter" placeholder="Enter Call Date From" id="frombilldaterange" name="frombilldaterange" readonly>
      </div>
      </div>
  
   <div class="col-md-2">   
      <div class="form-group">   
       <label>Call Date To :<span style="color:red">*</span></label>    
					<input type="text" class="form-control datepickerFilter" placeholder="Enter Call Date To"  id="tobilldaterange" name="tobilldaterange" readonly>
     	</div>
      </div>
 <div class="col-md-2">
   
                    <label>Select Location :</label>
                     <div class="form-group">
    	  	<select  multiple="multiple"  id="location"  name="locations[]" class="selectpicker  form-control" onchange="workshopsBasedOnLocations(this);" required> 
    	                       	     
                     	"""),_display_(/*35.24*/for(data3 <- locations) yield /*35.47*/{_display_(Seq[Any](format.raw/*35.48*/("""                       	                         
                    
                        """),format.raw/*37.25*/("""<option value=""""),_display_(/*37.41*/data3),format.raw/*37.46*/("""">"""),_display_(/*37.49*/data3),format.raw/*37.54*/("""</option>
                      	
                      	""")))}),format.raw/*39.25*/("""	  	
                    	"""),format.raw/*40.22*/("""</select>	
                    	<span type="label" id="locationERR" style="color:red"></span>	
           
      </div>
       </div>
       
       <div class="col-md-2">
   
                    <label>Select Workshops :</label>
                     <div class="form-group">
    	  	<select  multiple="multiple"  id="workshopname"  name="workshopname[]" class="form-control" onchange="cresBasedOnLocations(this);"  required> 
    	                       	     
                     	  	
                    	</select>
                    	
           
      </div>
       </div>
       
        <div class="col-md-2">
   
                    <label>Select CRE's :</label>
                     <div class="form-group">
    	  	<select  multiple="multiple"  id="crename"  name="crename[]" class="form-control"  required> 
    	                       	     
                     	  	
                    	</select>
                    	<span type="label" id="crenameERR" style="color:red"></span>	
           
      </div>
       </div>
</div>
<div class="col-md-12">
  <div class="form-group pull-right">
  		<button type="button" name="selectedFile" class="btn btn-sm btn-primary viewInsuranceReports" value="viewInsurance"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;View Insurance History</button>		     	
  		
  		 <button type="submit" name="downloadFile" class="btn btn-sm btn-warning"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;Download</button>
		</div>
</div> 
  </div>
""")))}),format.raw/*80.2*/("""

  """),format.raw/*82.3*/("""</div>      
<span type="label" id="viewSMSERR" style="color:red"></span>
<div class="row">
      <div class="col-lg-12">
        <div class="panel panel-primary">
          <div class="panel-heading"><b> List Of Insurance History</b>
          </div>
          <div class="panel-body" style="overflow:auto">
          <table class="table table-striped table-bordered table-hover"
						id="datatableInsuranceIntraction">
						<thead>
							<tr>

									<th>CRE Name</th> 
                                    <th>Call Date</th> 
                                    <th>Call Time</th>                                   
                                    <th>Is CallInitiated</th>
                                    <th>Call Type</th>
                                    <th>Call Duration</th>
                                    <th>Ring Time</th>
                                    <th>Campaign Type</th>
                                    <th>Chassis No</th>
                                    <th>Model</th>
                                    <th>Variant</th>
                                    <th>Sale Date</th>
                                    <th>Vehicle Reg No</th>
                                    <th>Customer Name</th>
                                    <th> Preffered Name</th>
                                    <th> Preffered Phone Number</th>
                                    <th>Primary Disposition</th>
                                    <th>Secondary Disposition</th>
                                    <th>Cover Note No</th>
                                    <th>Last Renewal Location</th>
                                    <th>Last Renewal Date</th>
                                    <th>Premium</th>
                                    <th>Renewal Done By</th>
                                    <th>Tertiary Disposition</th>
                                    <th>FollowUp Date</th>
                                    <th> FollowUp Time</th>
                                    <th>RemarksOfFB</th>
                                    <th>DepartmentForFB</th>
                                    <th>Address Of Visit</th>
                                    <th>Appointee Name </th>
                                    <th>Appointment Date</th>
                                    <th>Appointment From Time</th>
                                    <th>DSA</th>
                                    <th>Insurance Agent</th>
                                    <th>Insurance Company</th>
                                    <th>Nominee Name</th>
                                    <th>Nominee Relation With Owner</th> 
                                    <th>Premium With Tax</th>
                                    <th>Renewal Mode</th>
                                    <th>Renewal Type</th>
                                    <th>Type Of PickUp</th>
                                    <th>Premium Yes</th>
                                    <th>Comments</th>
                                    <th>CRE Manager</th>
                           </tr>
						</thead>
						<tbody>

						</tbody>
					</table>
                     
          </div>
          <!-- /.panel-body --> 
        </div>
        <!-- /.panel --> 
      </div>
      <!-- /.col-lg-12 --> 
    </div>
""")))}),format.raw/*153.2*/("""
	
"""),format.raw/*155.1*/("""<script>
$(document).ready(function () """),format.raw/*156.31*/("""{"""),format.raw/*156.32*/("""
	"""),format.raw/*157.2*/("""$('.viewInsuranceReports').click(function()"""),format.raw/*157.45*/("""{"""),format.raw/*157.46*/("""
    	
        """),format.raw/*159.9*/("""var fromDate     = $("#frombilldaterange").val();
        var toDate       = $("#tobilldaterange").val();
     	console.log("from date:"+fromDate);
     	console.log("to date:"+toDate);
        var UserIds="",j,k;
        var campaignName="";       
        
        myOption = document.getElementById('crename');
        console.log("here"+myOption);		        
		   for (j=0;j<myOption.options.length;j++)"""),format.raw/*168.45*/("""{"""),format.raw/*168.46*/("""
		       """),format.raw/*169.10*/("""if(myOption.options[j].selected)"""),format.raw/*169.42*/("""{"""),format.raw/*169.43*/("""
		           """),format.raw/*170.14*/("""if(myOption.options[j].value != "Select")"""),format.raw/*170.55*/("""{"""),format.raw/*170.56*/("""
		       		"""),format.raw/*171.12*/("""UserIds = UserIds + myOption.options[j].value + ",";
		           """),format.raw/*172.14*/("""}"""),format.raw/*172.15*/("""else"""),format.raw/*172.19*/("""{"""),format.raw/*172.20*/("""
					"""),format.raw/*173.6*/("""UserIds ="";
		           """),format.raw/*174.14*/("""}"""),format.raw/*174.15*/("""
		       """),format.raw/*175.10*/("""}"""),format.raw/*175.11*/("""
		   """),format.raw/*176.6*/("""}"""),format.raw/*176.7*/("""
    	"""),format.raw/*177.6*/("""if(UserIds.length > 0)"""),format.raw/*177.28*/("""{"""),format.raw/*177.29*/("""
        	"""),format.raw/*178.10*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
        	
    	"""),format.raw/*180.6*/("""}"""),format.raw/*180.7*/("""else"""),format.raw/*180.11*/("""{"""),format.raw/*180.12*/("""
    		"""),format.raw/*181.7*/("""UserIds="";
    	"""),format.raw/*182.6*/("""}"""),format.raw/*182.7*/("""

    	"""),format.raw/*184.6*/("""campname = document.getElementById('campaignName');
    	 for (j=0;j<campname.options.length;j++)"""),format.raw/*185.46*/("""{"""),format.raw/*185.47*/("""
    	        """),format.raw/*186.14*/("""if(campname.options[j].selected)"""),format.raw/*186.46*/("""{"""),format.raw/*186.47*/("""
    	            """),format.raw/*187.18*/("""if(campname.options[j].value != "Select")"""),format.raw/*187.59*/("""{"""),format.raw/*187.60*/("""
    	            	"""),format.raw/*188.19*/("""campaignName = campaignName + campname.options[j].value + ",";
    	            """),format.raw/*189.18*/("""}"""),format.raw/*189.19*/("""else"""),format.raw/*189.23*/("""{"""),format.raw/*189.24*/("""
    	            	"""),format.raw/*190.19*/("""campaignName ="";
    	            """),format.raw/*191.18*/("""}"""),format.raw/*191.19*/("""
    	        """),format.raw/*192.14*/("""}"""),format.raw/*192.15*/("""
    	    """),format.raw/*193.10*/("""}"""),format.raw/*193.11*/("""	   	
    	 """),format.raw/*194.7*/("""if(campaignName.length > 0)"""),format.raw/*194.34*/("""{"""),format.raw/*194.35*/("""
    		 """),format.raw/*195.8*/("""campaignName = campaignName.substring(0, campaignName.length - 1);
         	
     	"""),format.raw/*197.7*/("""}"""),format.raw/*197.8*/("""else"""),format.raw/*197.12*/("""{"""),format.raw/*197.13*/("""
     		"""),format.raw/*198.8*/("""campaignName="";
     	"""),format.raw/*199.7*/("""}"""),format.raw/*199.8*/("""



     	"""),format.raw/*203.7*/("""console.log("campaign name :"+campaignName);
     	var urlLink = "/CREManager/getAllInsuranceHistoryData"
    	    console.log("url link"+urlLink);
    	    $('#datatableInsuranceIntraction').dataTable( """),format.raw/*206.56*/("""{"""),format.raw/*206.57*/("""
    	     """),format.raw/*207.11*/(""""bDestroy": true,
    	     "processing": true,
    	     "serverSide": true,
    	     "scrollY": 400,
 	         "scrollX": true,
    	     "paging": true,
    	     "searching": false,
    	     "ordering":false,
    	     "ajax": """),format.raw/*215.19*/("""{"""),format.raw/*215.20*/("""
    		        """),format.raw/*216.15*/("""'type': 'POST',
    		        'url': urlLink,
    		        'data': """),format.raw/*218.23*/("""{"""),format.raw/*218.24*/("""    		        	
    		        	"""),format.raw/*219.16*/("""fromDate:''+fromDate,
    		        	toDate:''+toDate ,
    		        	UserIds:''+UserIds,
    		        	campaignName:''+campaignName, 		        	
    		        """),format.raw/*223.15*/("""}"""),format.raw/*223.16*/("""
        	        
    	     """),format.raw/*225.11*/("""}"""),format.raw/*225.12*/(""",
    	     "fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*226.67*/("""{"""),format.raw/*226.68*/("""
                 """),format.raw/*227.18*/("""$('td', nRow).attr('nowrap','nowrap');
                 return nRow;
                 """),format.raw/*229.18*/("""}"""),format.raw/*229.19*/("""
    	 """),format.raw/*230.7*/("""}"""),format.raw/*230.8*/(""" """),format.raw/*230.9*/(""");


    """),format.raw/*233.5*/("""}"""),format.raw/*233.6*/(""");
"""),format.raw/*234.1*/("""}"""),format.raw/*234.2*/(""");
</script>
"""))
      }
    }
  }

  def render(fromDateValue:String,toDateValue:String,user:String,dealerName:String,allUsers:List[String],locations:List[String],campaignListInsurance:List[Campaign]): play.twirl.api.HtmlFormat.Appendable = apply(fromDateValue,toDateValue,user,dealerName,allUsers,locations,campaignListInsurance)

  def f:((String,String,String,String,List[String],List[String],List[Campaign]) => play.twirl.api.HtmlFormat.Appendable) = (fromDateValue,toDateValue,user,dealerName,allUsers,locations,campaignListInsurance) => apply(fromDateValue,toDateValue,user,dealerName,allUsers,locations,campaignListInsurance)

  def ref: this.type = this

}


}

/**/
object insuranceReports extends insuranceReports_Scope0.insuranceReports
              /*
                  -- GENERATED --
                  DATE: Tue Mar 20 14:39:16 IST 2018
                  SOURCE: D:/CRMFORDAUTOSHERPA/crmford/app/views/insuranceReports.scala.html
                  HASH: 48598100db27b23a22e8f0fa0b5f66d19adeb540
                  MATRIX: 829->3|1079->157|1109->162|1167->212|1206->214|1236->218|1312->268|1345->281|1423->333|1454->344|1647->510|1662->516|1754->598|1795->600|1836->614|2884->1635|2923->1658|2962->1659|3087->1756|3130->1772|3156->1777|3186->1780|3212->1785|3303->1845|3358->1872|4926->3410|4959->3416|8397->6823|8430->6828|8499->6868|8529->6869|8560->6872|8632->6915|8662->6916|8707->6933|9151->7348|9181->7349|9221->7360|9282->7392|9312->7393|9356->7408|9426->7449|9456->7450|9498->7463|9594->7530|9624->7531|9657->7535|9687->7536|9722->7543|9778->7570|9808->7571|9848->7582|9878->7583|9913->7590|9942->7591|9977->7598|10028->7620|10058->7621|10098->7632|10195->7701|10224->7702|10257->7706|10287->7707|10323->7715|10369->7733|10398->7734|10435->7743|10562->7841|10592->7842|10636->7857|10697->7889|10727->7890|10775->7909|10845->7950|10875->7951|10924->7971|11034->8052|11064->8053|11097->8057|11127->8058|11176->8078|11241->8114|11271->8115|11315->8130|11345->8131|11385->8142|11415->8143|11456->8156|11512->8183|11542->8184|11579->8193|11693->8279|11722->8280|11755->8284|11785->8285|11822->8294|11874->8318|11903->8319|11945->8333|12180->8539|12210->8540|12251->8552|12522->8794|12552->8795|12597->8811|12696->8881|12726->8882|12787->8914|12982->9080|13012->9081|13072->9112|13102->9113|13200->9182|13230->9183|13278->9202|13395->9290|13425->9291|13461->9299|13490->9300|13519->9301|13559->9313|13588->9314|13620->9318|13649->9319
                  LINES: 27->2|32->2|34->4|34->4|34->4|36->6|36->6|36->6|37->7|37->7|41->11|41->11|41->11|41->11|43->13|65->35|65->35|65->35|67->37|67->37|67->37|67->37|67->37|69->39|70->40|110->80|112->82|183->153|185->155|186->156|186->156|187->157|187->157|187->157|189->159|198->168|198->168|199->169|199->169|199->169|200->170|200->170|200->170|201->171|202->172|202->172|202->172|202->172|203->173|204->174|204->174|205->175|205->175|206->176|206->176|207->177|207->177|207->177|208->178|210->180|210->180|210->180|210->180|211->181|212->182|212->182|214->184|215->185|215->185|216->186|216->186|216->186|217->187|217->187|217->187|218->188|219->189|219->189|219->189|219->189|220->190|221->191|221->191|222->192|222->192|223->193|223->193|224->194|224->194|224->194|225->195|227->197|227->197|227->197|227->197|228->198|229->199|229->199|233->203|236->206|236->206|237->207|245->215|245->215|246->216|248->218|248->218|249->219|253->223|253->223|255->225|255->225|256->226|256->226|257->227|259->229|259->229|260->230|260->230|260->230|263->233|263->233|264->234|264->234
                  -- GENERATED --
              */
          