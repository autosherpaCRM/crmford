
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object messageTemplateSuperAdmin_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class messageTemplateSuperAdmin extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template8[String,String,String,String,List[SMSTemplate],List[MessageParameters],List[SavedSearchResult],String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(dealercode:String,dealerName:String,userName:String,oem:String,smsTemplateList :List[SMSTemplate],msgParameters :List[MessageParameters], savedSearchList :List[SavedSearchResult],getsmsapi:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.199*/("""

"""),_display_(/*5.2*/mainPageCREManger("AutoSherpaCRM",userName,dealerName)/*5.56*/ {_display_(Seq[Any](format.raw/*5.58*/("""
"""),format.raw/*6.1*/("""<link rel="stylesheet" href=""""),_display_(/*6.31*/routes/*6.37*/.Assets.at("css/gsdk-base.css")),format.raw/*6.68*/("""">
<div class="container-fluid">
  <div class="panel panel-primary">
    <div class="panel-heading" style="text-align:center;"><strong>SEARCH CUSTOMER</strong> </div>
    <div class="panel-body">    
      <!---------------------Wizard1----------------------------------------->
      <div class="wizard-container">
        <div class="card wizard-card ct-wizard-green" id="wizard">        
          <!--You can switch "ct-wizard-azzure"  with one of the next bright colors: "ct-wizard-blue", "ct-wizard-green", "ct-wizard-orange", "ct-wizard-red"-->
 <ul>
            <li><a href="#step1" data-toggle="tab" class="tabRound">SEARCH</a></li>
            <li><a href="#step2" data-toggle="tab" class="tabRound">MESSAGE</a></li>
            <li><a href="#step14" data-toggle="tab" class="tabRound">LIST</a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane animated  bounceInRight" id="step1">       
              <!-------------------------------Step1------------------------------------->
              <div class="panel panel-primary" >
                <div class="panel-heading" style="text-align:center;">Search</div>
                <div class="panel-body" style="overflow:auto">
				<label class="radio-inline">
				<input type="hidden" id="hiddenSelectAllFlag" value="">
      <input type="radio" name="SearchCust" value="Search Customer">Search Customer
    </label>
    <label class="radio-inline">
     <input type="radio" name="SearchCust" value="Upload Customer">Upload Customer 
    </label> 
				<div style="display:none" id="SarchDivID" class="animated  bounceInRight">
				 <br />
                  <div class="row">
                    <div class="col-md-12">
			
	<a href="/searchByCustomerManager"><button class="btn btn-info" >Create New Search</button></a>
	
  </div>
 
                  </div>
                  </div>
				
<div style="display:none;" id="uploadDivID" class="animated  bounceInRight" >
	 <br />
                   <div class="row">
                    <div class="col-md-12">
				
	<a href="/fileuploadPage"><button class="btn btn-info" >Upload Customer</button></a>
	
	
  
  </div>
  
                  </div>
                  </div>
                  <br />
                  <div class="row" style="display:none" id="getCustomerId" >
                  <div class="col-md-3">
	<div class="form-group">
	<label>Select Saved Search</label>
	<select class="form-control" id="savedName" name="savedName">
		<option value="0" >--Select--</option>
                                """),_display_(/*65.34*/for(savedSearch_List<-savedSearchList) yield /*65.72*/{_display_(Seq[Any](format.raw/*65.73*/("""
                                """),format.raw/*66.33*/("""<option value=""""),_display_(/*66.49*/savedSearch_List/*66.65*/.getId()),format.raw/*66.73*/("""">"""),_display_(/*66.76*/savedSearch_List/*66.92*/.getSavedName()),format.raw/*66.107*/("""</option>
                             """)))}),format.raw/*67.31*/("""
	"""),format.raw/*68.2*/("""</select>
  </div>
  </div>
                  <div class="col-md-4">
				  <div class="form-group"><br />
				  	 <button type="button" id="SearchId" class="btn btn-primary" >Get Customer</button>
				  </div>
				  </div>
				  </div>
				  
				  
				
				    <div class="SearchCustId" style="display:none;" >
					 <div class="panel panel-primary" >
                <div class="panel-heading" style="text-align:center;">Search List </div>
                <div class="panel-body">
      <table class="table table-striped table-bordered table-hover" id="customerSearchTables">
                                    <thead>
                                 <tr>
                                    <th>Customer Name</th>
                                    <th>Vehicle RegNo.</th>
                                    <th>Category</th>                                   
                                    <th>Phone Number</th>                                    
                                    <th>Next Service Date</th>
                                    <th>Model</th>
                                    <th>Variant</th>
                                    <th>Last Disposition</th>
									<th>Select All this <input type="checkbox" id="selectAll"/></th>
                                 </tr>
                              </thead>
                                <tbody>
							
                   </tbody>
           </table>
        </div>
        </div>
        </div>
		<div class="UploadCustIdDiv" style="display:none;" >
		<div class="panel panel-primary" >
                <div class="panel-heading" style="text-align:center;">Upload List </div>
                <div class="panel-body">
      <table class="table table-striped table-bordered table-hover" id="customerUploadTable">
                                    <thead>
                                 <tr>                            
                                    <th>Customer Name</th>
                                    <th>Vehicle RegNo.</th>
                                    <th>Category</th>                                   
                                    <th>Phone Number</th>                                    
                                    <th>Next Service Date</th>
                                    <th>Model</th>
                                    <th>Variant</th>
                                    <th>Last Disposition</th>
									<th>Select All <input type="checkbox" id=""/></th>    
                                 </tr>
                              </thead>
                                <tbody>
								<tr>
									<td>Rohit</td>
									<td>KA-06 N-2201</td>
									<td></td>
									<td>9879908703</td>
									<td>30/06/2017</td>
									<td>i10 Honda</td>
									<td>Blue</td>
									<td></td>
									<td><input type="checkbox" name="uploadname"/></td>
								</tr>
								<tr>
									<td>Lohit</td>
									<td>KA-06 N-2202</td>
									<td></td>
									<td>9879908703</td>
									<td>30/06/2017</td>
									<td>i11 Honda</td>
									<td>Red</td>
									<td></td>
									<td><input type="checkbox" name="uploadname"/></td>
								</tr>
                   </tbody>
           </table>
        </div>
        </div>
        </div>
                  <!-- <div class="row">
			<div class="col-md-12">
			<div class="pull-right">
					<button type="button" class="btn btn-success"><i class="fa fa-check-square" aria-hidden="true"></i> &nbsp;Submit</button>
			</div>
			</div>
		</div> --> 
                </div>
              </div>
            </div>
            <div class="tab-pane animated  bounceInRight" id="step2" > 
              <!----------------------------------Step2----------------------------->
              <div class="panel panel-primary">
                <div class="panel-heading" style="text-align:center;">MESSAGE</div>
                <div class="panel-body">
               <div>
	<div class="row">
    <div class="col-sm-4">
     <div class="form-group">
  <label for="sel1">Message Template</label>
 <select id="msgTypeID" name="smsTemplate" class="form-control">
                               <option value="0" >--Select--</option>
                               """),_display_(/*173.33*/for(smsTemplate_List<-smsTemplateList) yield /*173.71*/{_display_(Seq[Any](format.raw/*173.72*/("""
                               """),format.raw/*174.32*/("""<option value=""""),_display_(/*174.48*/smsTemplate_List/*174.64*/.getSmsId()),format.raw/*174.75*/("""">"""),_display_(/*174.78*/smsTemplate_List/*174.94*/.getSmsTemplate()),format.raw/*174.111*/("""</option>
                            """)))}),format.raw/*175.30*/("""
  """),format.raw/*176.3*/("""</select>
</div>
    </div>
    <div class="col-sm-4">
		<div class="form-group">
  <label for="sel1">Message Parameters</label>
  <select class="form-control" id="ParameteID" >	
     <option value="0" >--Select--</option>
                                """),_display_(/*184.34*/for(msgParameters_list<-msgParameters) yield /*184.72*/{_display_(Seq[Any](format.raw/*184.73*/("""
                                """),format.raw/*185.33*/("""<option value=""""),_display_(/*185.49*/msgParameters_list/*185.67*/.getMessageParameter()),format.raw/*185.89*/("""">"""),_display_(/*185.92*/msgParameters_list/*185.110*/.getMessageParameter()),format.raw/*185.132*/("""</option>
                             """)))}),format.raw/*186.31*/("""
  """),format.raw/*187.3*/("""</select>
</div>
    </div>
     <div class="col-sm-3">
		<div class="form-group">
  <label for="sel1">SMS API</label>
  <input type="text" class="form-control" name="smsAPI" id="smsAPI" value=""""),_display_(/*193.77*/getsmsapi),format.raw/*193.86*/("""">
</div>
    </div>
	<div class="col-sm-3"><br />
	<button id="AddId" class="btn btn-info">Add</button>
	<br />
	</div>
  </div>
  <div class="row">
	<div class="col-sm-4">
    <div class="form-group">
  <label for="comment"><label>
  <textarea class="form-control addedTxt" rows="4" cols="90" id="txtAreaId" name="txtAreaId">
  </textarea>
</div>
    </div>
  </div>
  <button class="btn btn-success" id="SaveTemp" onclick="saveNewSmsTemplate();">Save Template</button> 
  </div>
                </div>
              </div>
            </div>  
    
            
            <!------------------------------------step14--------------------------------------------->
            <div class="tab-pane animated  bounceInRight" id="step14">
              <div class="panel panel-primary">
                <div class="panel-heading" style="text-align:center;">List</div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover" id="selectedCustomers">
                                    <thead>
	                                 <tr>
	                                    <th>Customer Name</th>
	                                    <th>Vehicle RegNo.</th>
	                                    <th>Category</th>                                   
	                                    <th>Phone Number</th>                                    
	                                    <th>Next Service Date</th>
	                                    <th>Model</th>
	                                    <th>Variant</th>
	                                    <th>Last Disposition</th>                                    
	                                 </tr>
                              </thead>
                                <tbody>		
                   </tbody>
           </table>
                </div>
                </div>
            </div>
          </div>
          <div class="wizard-footer">
            <div class="pull-right">
              <button step2='button' class='btn btn-next btn-fill btn-warning btn-wd btn-sm' id=""  name='next' value='Next' />
              Next
              </button>
              <button step2='button' class='btn btn-finish btn-fill btn-warning btn-wd btn-sm' name='finish' value='Finish' onclick="sendBulkSMS();"/>
              <i class="fa fa-check-square" aria-hidden="true" ></i>&nbsp;Send Bulk SMS
              </button>
            </div>
            <div class="pull-left">
              <button step2='button' class='btn btn-previous btn-fill btn-default btn-wd btn-sm' name='previous' value='Previous' />
              Previous
              </button>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
      <!-- wizard container --> 
      <!----------------------------Wizard End-----------------------------------> 
      <!------------------------------------Last--------------------------------------------->
      <div class="panel panel-primary" style="display:none;">
        <div class="panel-heading" style="text-align:center;">INTERACTION HISTORY</div>
        <div class="panel-body" style="overflow:auto"> </div>
      </div>
      <!---------------------------------------- Last End---------------------------------------------------->       
    </div>
  </div>
</div>
""")))}),format.raw/*271.2*/("""
"""),format.raw/*272.1*/("""<script>
var selectedUsers = [];
var dataInfo ="""),format.raw/*274.15*/("""{"""),format.raw/*274.16*/("""}"""),format.raw/*274.17*/(""";
var setFlag = false;
$("#selectAll").click(function()"""),format.raw/*276.33*/("""{"""),format.raw/*276.34*/("""
	"""),format.raw/*277.2*/("""$("input[name=searchname]").prop('checked', $(this).prop("checked"));
	setFlag = true;   
		$("#hiddenSelectAllFlag").val("selectAll");
		var savedsearchname = $("#savedName option:selected").val();
		console.log("savedname in click function::"+savedsearchname);	
		
		getData(selectedUsers,setFlag,savedsearchname);
"""),format.raw/*284.1*/("""}"""),format.raw/*284.2*/(""");
function getSelectedUser()"""),format.raw/*285.27*/("""{"""),format.raw/*285.28*/("""
	"""),format.raw/*286.2*/("""$("input[name='searchname']:checked").each(function(i) """),format.raw/*286.57*/("""{"""),format.raw/*286.58*/("""
		"""),format.raw/*287.3*/("""selectedUsers.indexOf($(this).val()) === -1 ? selectedUsers.push($(this).val()) : console.log("This item already exists");	
		 """),format.raw/*288.4*/("""}"""),format.raw/*288.5*/("""); 
		var savedsearchname = $("#savedName option:selected").val(); 
		console.log(" getSelectedUsersavedname in click function::"+savedsearchname);	
		
		if($("#hiddenSelectAllFlag").val() == "selectAll")"""),format.raw/*292.53*/("""{"""),format.raw/*292.54*/("""
			"""),format.raw/*293.4*/("""setFlag = true;
		"""),format.raw/*294.3*/("""}"""),format.raw/*294.4*/("""else"""),format.raw/*294.8*/("""{"""),format.raw/*294.9*/("""
			"""),format.raw/*295.4*/("""setFlag = false;
		"""),format.raw/*296.3*/("""}"""),format.raw/*296.4*/("""
	 	"""),format.raw/*297.4*/("""getData(selectedUsers,setFlag,savedsearchname);
"""),format.raw/*298.1*/("""}"""),format.raw/*298.2*/("""


"""),format.raw/*301.1*/("""function getData(selectedUsers,setFlag,savedsearchname)"""),format.raw/*301.56*/("""{"""),format.raw/*301.57*/("""
	"""),format.raw/*302.2*/("""if(setFlag == true)"""),format.raw/*302.21*/("""{"""),format.raw/*302.22*/("""
		"""),format.raw/*303.3*/("""selectedUsers = "all";
	"""),format.raw/*304.2*/("""}"""),format.raw/*304.3*/("""
	"""),format.raw/*305.2*/("""console.log("savedname in getData::"+savedsearchname);	
	console.log("selectedUsers in getData::"+selectedUsers);	
	var urlLink="/getSelectedUserListforDatatable/"+ selectedUsers + "/"+setFlag+"/"+savedsearchname+""; 
	console.log(urlLink);
	 $('#selectedCustomers').dataTable("""),format.raw/*309.37*/("""{"""),format.raw/*309.38*/("""
	     """),format.raw/*310.7*/(""""bDestroy"  : true,
	     "processing": true,
	     "serverSide": true,
	     "scrollY"   : 300,
	     "paging"    : true,
	     "searching" : false,
	     "ordering"  : false,
	     "ajax"      : urlLink
	     """),format.raw/*318.7*/("""}"""),format.raw/*318.8*/(""");
"""),format.raw/*319.1*/("""}"""),format.raw/*319.2*/("""
"""),format.raw/*320.1*/("""function saveNewSmsTemplate()"""),format.raw/*320.30*/("""{"""),format.raw/*320.31*/("""
	 """),format.raw/*321.3*/("""var messageTemplate = $("#txtAreaId").val(); 
	 var msgAPI = $("#smsAPI").val();
	dataInfo = """),format.raw/*323.13*/("""{"""),format.raw/*323.14*/("""
			"""),format.raw/*324.4*/("""messageTemplate: messageTemplate,
			msgAPI: msgAPI,
	       """),format.raw/*326.9*/("""}"""),format.raw/*326.10*/(""";

		 $.ajax("""),format.raw/*328.11*/("""{"""),format.raw/*328.12*/("""
			   """),format.raw/*329.7*/("""type       : "POST",
	           url        : "/postSMSTemplate",
	           dataType   : "json",
	           data       : JSON.stringify(dataInfo),
	           contentType: "application/json",
	     
	   """),format.raw/*335.5*/("""}"""),format.raw/*335.6*/(""").done(function (getLatestSMS) """),format.raw/*335.37*/("""{"""),format.raw/*335.38*/("""
		   """),format.raw/*336.6*/("""if (getLatestSMS != null) """),format.raw/*336.32*/("""{"""),format.raw/*336.33*/("""
			"""),format.raw/*337.4*/("""var dropdown = document.getElementById("msgTypeID"); 
		       dropdown[0]= new Option('Select','0');

		     console.log("inside forloop:"+getLatestSMS.smsTemplate);
	         dropdown[dropdown.length] = new Option(getLatestSMS.smsTemplate,getLatestSMS.smsId);
		"""),format.raw/*342.3*/("""}"""),format.raw/*342.4*/("""
	   """),format.raw/*343.5*/("""}"""),format.raw/*343.6*/(""");
"""),format.raw/*344.1*/("""}"""),format.raw/*344.2*/("""

"""),format.raw/*346.1*/("""function sendBulkSMS()"""),format.raw/*346.23*/("""{"""),format.raw/*346.24*/("""
	"""),format.raw/*347.2*/("""//var searchResultIds = $("#hiddenSelectAllFlag").val();
		var savedName 		=  $("#savedName option:selected").val(); 
		var smstemplateId   =  $('#msgTypeID option:selected').val();
		//var smstemplateId 		= $('.addedTxt').val();
		console.log("after sendBulkSms"+selectedUsers);
		var searchResultIds = selectedUsers;
		dataInfo = """),format.raw/*353.14*/("""{"""),format.raw/*353.15*/("""
				"""),format.raw/*354.5*/("""savedName: savedName,
				smstemplateId: smstemplateId,
				searchResultIds: searchResultIds,
				setFlag: setFlag
		       """),format.raw/*358.10*/("""}"""),format.raw/*358.11*/(""";
		console.log("this is inside sendbulksms"+JSON.stringify(dataInfo));
		//var urlLink = "/postSMSBulk"
		 $.ajax("""),format.raw/*361.11*/("""{"""),format.raw/*361.12*/("""
			   """),format.raw/*362.7*/("""type       : "POST",
	           url        : "/postSMSBulk",
	           dataType   : "json",
	           data       : JSON.stringify(dataInfo),
	           contentType: "application/json",
	           success    : function () """),format.raw/*367.38*/("""{"""),format.raw/*367.39*/("""
	             """),format.raw/*368.15*/("""alert("data sent to controller");
	           """),format.raw/*369.13*/("""}"""),format.raw/*369.14*/(""",
	       """),format.raw/*370.9*/("""}"""),format.raw/*370.10*/(""");
"""),format.raw/*371.1*/("""}"""),format.raw/*371.2*/("""


"""),format.raw/*374.1*/("""$(document).ready(function() """),format.raw/*374.30*/("""{"""),format.raw/*374.31*/("""
"""),format.raw/*375.1*/("""$('#SearchId').on('click',function()"""),format.raw/*375.37*/("""{"""),format.raw/*375.38*/("""
	 """),format.raw/*376.3*/("""var savedsearchname = $("#savedName option:selected").val(); 
	 var urlLink         = "/getCustomersListBySavedName/"+savedsearchname+""; 
	 console.log(savedsearchname);
	 $('#customerSearchTables').dataTable("""),format.raw/*379.40*/("""{"""),format.raw/*379.41*/("""
	     """),format.raw/*380.7*/(""""bDestroy"  : true,
	     "processing": true,
	     "serverSide": true,
	     "scrollY"   : 300,
	     "paging"    : true,
	     "searching" : false,
	     "ordering"  : false,
	     "ajax"      : urlLink
	     """),format.raw/*388.7*/("""}"""),format.raw/*388.8*/(""");
	 """),format.raw/*389.3*/("""}"""),format.raw/*389.4*/("""); 
$('#AddId').click(function()"""),format.raw/*390.29*/("""{"""),format.raw/*390.30*/(""" 
    """),format.raw/*391.5*/("""var vpara       = $('#ParameteID :selected').val();
	var xpara       = "("+vpara+")";
	var enteredText = $('#txtAreaId').val();
	var res         = enteredText.concat(xpara);
	$('.addedTxt').val(xpara);	
	$('#txtAreaId').val(res);
"""),format.raw/*397.1*/("""}"""),format.raw/*397.2*/(""");


/*var mydropdown = document.getElementById('msgTypeID');
var mytextbox = document.getElementById('txtAreaId');
mydropdown.onchange = function()"""),format.raw/*402.33*/("""{"""),format.raw/*402.34*/("""
	 """),format.raw/*403.3*/("""mytextbox.focus();
     mytextbox.value = mytextbox.value  + this.value;
"""),format.raw/*405.1*/("""}"""),format.raw/*405.2*/("""*/


$('#msgTypeID').on('click',function()"""),format.raw/*408.38*/("""{"""),format.raw/*408.39*/("""
"""),format.raw/*409.1*/("""var	mydropdown = $('#msgTypeID option:selected').text();
$('#txtAreaId').val(mydropdown);
"""),format.raw/*411.1*/("""}"""),format.raw/*411.2*/(""");
$("#UploadID").click(function()"""),format.raw/*412.32*/("""{"""),format.raw/*412.33*/("""
	"""),format.raw/*413.2*/("""$(".dataTable_wrapper").show();
"""),format.raw/*414.1*/("""}"""),format.raw/*414.2*/(""");
$("#checkAll").change(function () """),format.raw/*415.35*/("""{"""),format.raw/*415.36*/("""
    """),format.raw/*416.5*/("""$("input[name=Searchname]").prop('checked', $(this).prop("checked"));
"""),format.raw/*417.1*/("""}"""),format.raw/*417.2*/(""");
//$(this).prop("checked"));
/*$(function()"""),format.raw/*419.15*/("""{"""),format.raw/*419.16*/("""
	"""),format.raw/*420.2*/("""$(".selectAll").change(function () """),format.raw/*420.37*/("""{"""),format.raw/*420.38*/("""
	    """),format.raw/*421.6*/("""var sel = $('input[name=searchname]').map(function(_, el) """),format.raw/*421.64*/("""{"""),format.raw/*421.65*/("""
	        """),format.raw/*422.10*/("""return $(el).val();
	    """),format.raw/*423.6*/("""}"""),format.raw/*423.7*/(""").get();
	    alert(sel);
    """),format.raw/*425.5*/("""}"""),format.raw/*425.6*/(""");
  """),format.raw/*426.3*/("""}"""),format.raw/*426.4*/(""");*/
  
$("input[name='SearchCust']").click(function()"""),format.raw/*428.47*/("""{"""),format.raw/*428.48*/("""
	"""),format.raw/*429.2*/("""var vsaerch = $(this).val();
	if(vsaerch=='Search Customer')"""),format.raw/*430.32*/("""{"""),format.raw/*430.33*/("""
		"""),format.raw/*431.3*/("""$("#SarchDivID").show();
		$("#getCustomerId").show();
		
		$("#uploadDivID").hide();
		$(".UploadCustIdDiv").hide();
	"""),format.raw/*436.2*/("""}"""),format.raw/*436.3*/("""
	"""),format.raw/*437.2*/("""else if(vsaerch=='Upload Customer')"""),format.raw/*437.37*/("""{"""),format.raw/*437.38*/("""
		"""),format.raw/*438.3*/("""$("#uploadDivID").show();
		$("#SarchDivID").hide();
		$(".SearchCustId").hide();
		$("#getCustomerId").show();
	"""),format.raw/*442.2*/("""}"""),format.raw/*442.3*/("""
	"""),format.raw/*443.2*/("""else"""),format.raw/*443.6*/("""{"""),format.raw/*443.7*/("""
    """),format.raw/*444.5*/("""}"""),format.raw/*444.6*/("""
"""),format.raw/*445.1*/("""}"""),format.raw/*445.2*/(""");

$("#SearchId").click(function()"""),format.raw/*447.32*/("""{"""),format.raw/*447.33*/("""
"""),format.raw/*448.1*/("""$(".SearchCustId").show();
"""),format.raw/*449.1*/("""}"""),format.raw/*449.2*/(""");

$("#uploadcustId").click(function()"""),format.raw/*451.36*/("""{"""),format.raw/*451.37*/("""
"""),format.raw/*452.1*/("""$(".UploadCustIdDiv").show();
"""),format.raw/*453.1*/("""}"""),format.raw/*453.2*/(""");

/*$(document).ready(function() """),format.raw/*455.32*/("""{"""),format.raw/*455.33*/("""
  """),format.raw/*456.3*/("""//  $('#customerSearchTable').DataTable();
"""),format.raw/*457.1*/("""}"""),format.raw/*457.2*/(""");

$(document).ready(function() """),format.raw/*459.30*/("""{"""),format.raw/*459.31*/("""
  """),format.raw/*460.3*/("""//  $('#customerUploadTable').DataTable();
"""),format.raw/*461.1*/("""}"""),format.raw/*461.2*/(""");*/
"""),format.raw/*462.1*/("""}"""),format.raw/*462.2*/(""");
</script>"""))
      }
    }
  }

  def render(dealercode:String,dealerName:String,userName:String,oem:String,smsTemplateList:List[SMSTemplate],msgParameters:List[MessageParameters],savedSearchList:List[SavedSearchResult],getsmsapi:String): play.twirl.api.HtmlFormat.Appendable = apply(dealercode,dealerName,userName,oem,smsTemplateList,msgParameters,savedSearchList,getsmsapi)

  def f:((String,String,String,String,List[SMSTemplate],List[MessageParameters],List[SavedSearchResult],String) => play.twirl.api.HtmlFormat.Appendable) = (dealercode,dealerName,userName,oem,smsTemplateList,msgParameters,savedSearchList,getsmsapi) => apply(dealercode,dealerName,userName,oem,smsTemplateList,msgParameters,savedSearchList,getsmsapi)

  def ref: this.type = this

}


}

/**/
object messageTemplateSuperAdmin extends messageTemplateSuperAdmin_Scope0.messageTemplateSuperAdmin
              /*
                  -- GENERATED --
                  DATE: Fri Mar 16 17:48:03 IST 2018
                  SOURCE: D:/CRMFORDAUTOSHERPA/crmford/app/views/messageTemplateSuperAdmin.scala.html
                  HASH: c0832ebaaa109609993d9bcb347eed0eec79b408
                  MATRIX: 879->5|1172->202|1202->207|1264->261|1303->263|1331->265|1387->295|1401->301|1452->332|4094->2947|4148->2985|4187->2986|4249->3020|4292->3036|4317->3052|4346->3060|4376->3063|4401->3079|4438->3094|4510->3135|4540->3138|8929->7499|8984->7537|9024->7538|9086->7571|9130->7587|9156->7603|9189->7614|9220->7617|9246->7633|9286->7650|9358->7690|9390->7694|9682->7958|9737->7996|9777->7997|9840->8031|9884->8047|9912->8065|9956->8087|9987->8090|10016->8108|10061->8130|10134->8171|10166->8175|10395->8376|10426->8385|13891->11819|13921->11821|13999->11870|14029->11871|14059->11872|14145->11929|14175->11930|14206->11933|14558->12257|14587->12258|14646->12288|14676->12289|14707->12292|14791->12347|14821->12348|14853->12352|15009->12480|15038->12481|15275->12689|15305->12690|15338->12695|15385->12714|15414->12715|15446->12719|15475->12720|15508->12725|15556->12745|15585->12746|15618->12751|15695->12800|15724->12801|15758->12807|15842->12862|15872->12863|15903->12866|15951->12885|15981->12886|16013->12890|16066->12915|16095->12916|16126->12919|16436->13200|16466->13201|16502->13209|16749->13428|16778->13429|16810->13433|16839->13434|16869->13436|16927->13465|16957->13466|16989->13470|17113->13565|17143->13566|17176->13571|17267->13634|17297->13635|17341->13650|17371->13651|17407->13659|17647->13871|17676->13872|17736->13903|17766->13904|17801->13911|17856->13937|17886->13938|17919->13943|18216->14212|18245->14213|18279->14219|18308->14220|18340->14224|18369->14225|18401->14229|18452->14251|18482->14252|18513->14255|18880->14593|18910->14594|18944->14600|19101->14728|19131->14729|19278->14847|19308->14848|19344->14856|19606->15089|19636->15090|19681->15106|19757->15153|19787->15154|19826->15165|19856->15166|19888->15170|19917->15171|19951->15177|20009->15206|20039->15207|20069->15209|20134->15245|20164->15246|20196->15250|20438->15463|20468->15464|20504->15472|20751->15691|20780->15692|20814->15698|20843->15699|20905->15732|20935->15733|20970->15740|21234->15976|21263->15977|21445->16130|21475->16131|21507->16135|21610->16210|21639->16211|21713->16256|21743->16257|21773->16259|21893->16351|21922->16352|21986->16387|22016->16388|22047->16391|22108->16424|22137->16425|22204->16463|22234->16464|22268->16470|22367->16541|22396->16542|22472->16589|22502->16590|22533->16593|22597->16628|22627->16629|22662->16636|22749->16694|22779->16695|22819->16706|22873->16732|22902->16733|22962->16765|22991->16766|23025->16772|23054->16773|23139->16829|23169->16830|23200->16833|23290->16894|23320->16895|23352->16899|23504->17023|23533->17024|23564->17027|23628->17062|23658->17063|23690->17067|23835->17184|23864->17185|23895->17188|23927->17192|23956->17193|23990->17199|24019->17200|24049->17202|24078->17203|24144->17240|24174->17241|24204->17243|24260->17271|24289->17272|24359->17313|24389->17314|24419->17316|24478->17347|24507->17348|24573->17385|24603->17386|24635->17390|24707->17434|24736->17435|24800->17470|24830->17471|24862->17475|24934->17519|24963->17520|24997->17526|25026->17527
                  LINES: 27->3|32->3|34->5|34->5|34->5|35->6|35->6|35->6|35->6|94->65|94->65|94->65|95->66|95->66|95->66|95->66|95->66|95->66|95->66|96->67|97->68|202->173|202->173|202->173|203->174|203->174|203->174|203->174|203->174|203->174|203->174|204->175|205->176|213->184|213->184|213->184|214->185|214->185|214->185|214->185|214->185|214->185|214->185|215->186|216->187|222->193|222->193|300->271|301->272|303->274|303->274|303->274|305->276|305->276|306->277|313->284|313->284|314->285|314->285|315->286|315->286|315->286|316->287|317->288|317->288|321->292|321->292|322->293|323->294|323->294|323->294|323->294|324->295|325->296|325->296|326->297|327->298|327->298|330->301|330->301|330->301|331->302|331->302|331->302|332->303|333->304|333->304|334->305|338->309|338->309|339->310|347->318|347->318|348->319|348->319|349->320|349->320|349->320|350->321|352->323|352->323|353->324|355->326|355->326|357->328|357->328|358->329|364->335|364->335|364->335|364->335|365->336|365->336|365->336|366->337|371->342|371->342|372->343|372->343|373->344|373->344|375->346|375->346|375->346|376->347|382->353|382->353|383->354|387->358|387->358|390->361|390->361|391->362|396->367|396->367|397->368|398->369|398->369|399->370|399->370|400->371|400->371|403->374|403->374|403->374|404->375|404->375|404->375|405->376|408->379|408->379|409->380|417->388|417->388|418->389|418->389|419->390|419->390|420->391|426->397|426->397|431->402|431->402|432->403|434->405|434->405|437->408|437->408|438->409|440->411|440->411|441->412|441->412|442->413|443->414|443->414|444->415|444->415|445->416|446->417|446->417|448->419|448->419|449->420|449->420|449->420|450->421|450->421|450->421|451->422|452->423|452->423|454->425|454->425|455->426|455->426|457->428|457->428|458->429|459->430|459->430|460->431|465->436|465->436|466->437|466->437|466->437|467->438|471->442|471->442|472->443|472->443|472->443|473->444|473->444|474->445|474->445|476->447|476->447|477->448|478->449|478->449|480->451|480->451|481->452|482->453|482->453|484->455|484->455|485->456|486->457|486->457|488->459|488->459|489->460|490->461|490->461|491->462|491->462
                  -- GENERATED --
              */
          