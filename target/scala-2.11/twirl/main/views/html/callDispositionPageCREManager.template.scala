
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object callDispositionPageCREManager_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class callDispositionPageCREManager extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template8[List[Location],String,String,Map[Long, String],List[Campaign],List[ServiceTypes],List[String],List[CallDispositionData],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(listCity:List[Location],dealerName:String,user:String,allCREHash:Map[Long,String],campaignList :List[Campaign],serviceTypeList :List[ServiceTypes],bookedServiceTypeList:List[String],dispoList:List[CallDispositionData]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.221*/("""
"""),_display_(/*2.2*/mainPageCREManger("AutoSherpaCRM",user,dealerName)/*2.52*/ {_display_(Seq[Any](format.raw/*2.54*/("""	



"""),format.raw/*6.1*/("""<style>
    td, th """),format.raw/*7.12*/("""{"""),format.raw/*7.13*/("""
                
        """),format.raw/*9.9*/("""border: 1px solid black;
        overflow: hidden; 
        text-align:center!important;
    """),format.raw/*12.5*/("""}"""),format.raw/*12.6*/("""

"""),format.raw/*14.1*/("""</style>
      <div class="row">
        <div class="col-sm-2"> 
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><div id="creMFreshCallsCountCRE">..</div></h3>
              <p>Fresh Calls</p>
            </div>
            <div class="icon"> <i class="ion-android-call"></i> </div>
             
            <!--<span class="pull-left" data-toggle="modal" data-target=".myModal1">View Details</span>--> 
          </div>
        </div>      <!-- ./col -->
        <div class="col-sm-2 col-half-offset"> 
          <!-- small box -->
          <div class="small-box bg-orange">
            <div class="inner">
              <h3><div id="creMfollowupcountCRE">..</div></h3>
              <p>Pending Follow Ups</p>
            </div>
            <div class="icon"> <i class="ion-android-checkbox-outline"></i> </div>
            </div>
        </div>        <!-- ./col -->
        <div class="col-sm-2 col-half-offset"> 
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><div id="creMnonContactsCRE">..</div></h3>
              <p> Non Contacts </p>
            </div>
            <div class="icon"> <i class="ion-person-add"></i> </div>
             </div>
        </div>        <!-- ./col -->
        <div class="col-sm-2 col-half-offset"> 
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3><div id="creMoverDueCountCRE">..</div></h3>
              <p>Over Due Bookings</p>
            </div>
            <div class="icon"> <i class="ion-android-download"></i> </div>
             </div>
        </div> 
       <!-- <div class="col-sm-2 col-half-offset"> 
          
          <div class="small-box bg-green">
            <div class="inner">
              <h3><div id="creMDailyPerCountCRE">../..</div></h3>
              <p>Bookings/Total Calls/Day</p>
            </div>
            <div class="icon"> <i class="ion-android-download"></i> </div>
             </div>
        </div> -->
        
        <div class="col-sm-2 col-half-offset"> 
          <!-- small box -->
          <div class="small-box bg-yellow" style="height: 102px;">
             <div class="row"  style="margin-left:4px">
   <div class=" col-sm-12 col-xs-12">
                 
                 <h4><span id="creMTotalBookingsperDay" style="font-size: 22px; font-weight: bold;"></span>&nbsp;&nbsp;&nbsp;&nbsp;Booking</h4>
  </div>
  </div>
  <p style="border-bottom:4px solid white;width:55px; margin-left:4px"></p>
 <div class="row" style="position:absolute;margin-top:-12px; margin-left:4px">
   <div class="col-sm-12 col-xs-12">
                 
                  <h4><span id="creMTotalCallsperDay" style="font-size: 22px; font-weight: bold;"></span> &nbsp;&nbsp;&nbsp;&nbsp;Total Calls</h4>
</div></div>
 <div class="pull-left" style="margin-left: 100px; position: absolute; margin-top: 20px;" id="creMCurrentdate">...</div>
 
           
          </div>
        </div>
        
        
        
               <!-- ./col --> 
      </div> 
 
  <div>

        <div class="panel panel-primary">
            <div class="panel-heading">   
                Scheduled Call Log Information </div>
            <div class="panel-body">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#home" data-toggle="tab" id="Tab1" onclick="assignedInteractionDataMR();">Scheduled</a> </li>
                    <li><a href="#profile" data-toggle="tab" id="Tab2" onclick="ajaxCallForFollowUpRequiredServerMR();">Follow Up Required</a> </li>
                    <li><a href="#messages" data-toggle="tab" id="Tab3" onclick="ajaxCallForserviceBookedServerDataMR();">Service Booked</a> </li>
                    <li><a href="#settings" data-toggle="tab" id="Tab4" onclick="ajaxCallForServiceNotRequiredServerMR();" >Service Not Required</a> </li>
                    <li><a href="#nonContacts" data-toggle="tab" id="Tab5" onclick="ajaxCallForNonContactsServerMR();">Non Contacts</a> </li>
                    <li><a href="#droppedBuc" data-toggle="tab" id="Tab6" onclick="ajaxCallFordroppedCallsServerDataMR();">Dropped</a> </li>
					<li><a href="#missedCallBuc" data-toggle="tab" id="Tab7" onclick="ajaxCallMissedCallsServerDataMR();">Missed Calls</a> </li>
                    <li><a href="#incomingCallBuc" data-toggle="tab" id="Tab8" onclick="ajaxCallForincomingCallsServerDataMR();">Incoming Calls</a> </li>  
                    <li><a href="#outgoingCallBuc" data-toggle="tab" id="Tab9" onclick="ajaxCallForOutgoingCallBucCallsServerDataMR();">Outgoing Calls</a> </li>
                    
                </ul>
                <div class="tab-content">
                <div class="panel panel-default" style="border-top: #fff;">
				<div class="panel-body">
                <div class="row">
		<div class="col-md-2" id="cityDiv">
		<div class="form-group">
                <label>City </label>
               
               <select class="form-control" id="city" name="cityName" onchange="ajaxCallToLoadWorkShopByCity();"> 
                <option value='--Select--'>--Select--</option>
                
				"""),_display_(/*124.6*/for(city <- listCity) yield /*124.27*/{_display_(Seq[Any](format.raw/*124.28*/("""                  	                         
                    """),format.raw/*125.21*/("""<option value=""""),_display_(/*125.37*/city/*125.41*/.getName()),format.raw/*125.51*/("""">"""),_display_(/*125.54*/city/*125.58*/.getName()),format.raw/*125.68*/("""</option>
                    """)))}),format.raw/*126.22*/("""
                """),format.raw/*127.17*/("""</select>
</div>
            </div> 
			<div class="col-md-2" id="workshopDiv">
			<div class="form-group">
                <label>WorkShop Location </label>               
                <select class="form-control" id="workshop" name="workshopId" onchange="ajaxCallToLoadCRESByWorkshop();">                 			    
                

                </select>
			</div>
            </div> 
		
            <div class="col-md-2" id="cresDiv">
			 <label>Select CRE's </label>
			<div class="form-group">
              <select class="selectpicker filter form-control" data-column-index="7" id="ddlCreIds" name="ddlCreIds" multiple>
                
                    
                </select> 
</div>


            </div>
            
                <div class="col-md-2 tab-pane" id="campaignDiv">
                                
                                <label>Select Campaign</label>
						<select class="filter form-control" id="campaignName" data-column-index="0" name="campaignName">
							<option value="0" >--Select--</option>
							"""),_display_(/*157.9*/for(campaign_List<-campaignList) yield /*157.41*/{_display_(Seq[Any](format.raw/*157.42*/("""
		                                """),format.raw/*158.35*/("""<option value=""""),_display_(/*158.51*/campaign_List/*158.64*/.getCampaignName()),format.raw/*158.82*/("""">"""),_display_(/*158.85*/campaign_List/*158.98*/.getCampaignName()),format.raw/*158.116*/("""</option>
		                             """)))}),format.raw/*159.33*/("""
		                                
						"""),format.raw/*161.7*/("""</select>
					</div>
					
					<div class="col-md-2" id="fromDateDiv">
					<label>From Date : </label>
					<input type="text" class="filter form-control datepickerFilter" data-column-index="1" id="fromduedaterange" name="fromduedaterange" readonly>
				  </div>
				  <div class="col-md-2" id="toDateDiv">
					<label>To Date : </label>
					<input type="text" class="filter form-control datepickerFilter" data-column-index="2" id="toduedaterange" name="toduedaterange" readonly>
				  </div>
				  </div>
				  <div class="row">
				   
						<div class="col-md-2" id="serviceTypeDiv">
					
			
						<label>Select service type</label>
						<select class="filter form-control" id="ServiceTypeName" data-column-index="3" name="ServiceTypeName">
							<option value="0" >--Select--</option>
							"""),_display_(/*181.9*/for(serviceType_List<-serviceTypeList) yield /*181.47*/{_display_(Seq[Any](format.raw/*181.48*/("""
			                                """),format.raw/*182.36*/("""<option value=""""),_display_(/*182.52*/serviceType_List/*182.68*/.getServiceTypeName()),format.raw/*182.89*/("""">"""),_display_(/*182.92*/serviceType_List/*182.108*/.getServiceTypeName()),format.raw/*182.129*/("""</option>
			                             """)))}),format.raw/*183.34*/("""
			                                
						"""),format.raw/*185.7*/("""</select>
					</div>
					 
					<div class="col-md-2" id="serviceBookTypeDiv" style="display:none">						
					<label>Select Booked service type</label>
						<select class="filter form-control" id="serviceBookedType" data-column-index="4" name="serviceBookedType">
							<option value="0" >--Select--</option>
							"""),_display_(/*192.9*/for(bookedServiceType_List<-bookedServiceTypeList) yield /*192.59*/{_display_(Seq[Any](format.raw/*192.60*/("""
 """),format.raw/*193.2*/("""<option value=""""),_display_(/*193.18*/bookedServiceType_List),format.raw/*193.40*/("""">"""),_display_(/*193.43*/bookedServiceType_List),format.raw/*193.65*/("""</option>					                   
           """)))}),format.raw/*194.13*/("""
					                                
						"""),format.raw/*196.7*/("""</select>
					</div>
					<div class="col-md-2" id="lastDispoTypeDiv" style="display:none">						
					<label>Last Disposition</label>
						<select class="filter form-control" id="lastDispo" data-column-index="5" >
							<option value="0" >--Select--</option>
								"""),_display_(/*202.10*/for(dispo <- dispoList) yield /*202.33*/{_display_(Seq[Any](format.raw/*202.34*/("""
			"""),format.raw/*203.4*/("""<option value=""""),_display_(/*203.20*/dispo/*203.25*/.getDisposition()),format.raw/*203.42*/("""">"""),_display_(/*203.45*/dispo/*203.50*/.getDisposition()),format.raw/*203.67*/("""</option>
			
		""")))}),format.raw/*205.4*/("""
							
					                                
						"""),format.raw/*208.7*/("""</select>
					</div>
					<div class="col-md-2" id="droppedcountDiv" style="display:none">						
					<label>Dropped Count</label>
						<select class="filter form-control" id="droppedCount" data-column-index="6">
							<option value="0" >--Select--</option>
							<option value="1" >1</option>
							<option value="2" >2</option>
							<option value="3" >3</option>
							<option value="4" >4</option>
							
					                                
						</select>
					</div>
				</div>
				</div>
				</div>
                
                    <div class="panel panel-default tab-pane fade in active" id="home">
                        <div class="panel-body inf-content">              

                            <div class="dataTable_wrapper">                            
                                <div >
                                    <table class="table table-striped table-bordered table-hover" id="assignedInteractionTableMR" width="100%">
                                        <thead>
                                            <tr> 

                                             <th>Customer Name</th>
                                                <th>RegNo.</th>
                                                <th>Model</th> 
                                                <th>Category</th>
                                                <th>Loyalty</th>
                                                <th>DueDate</th>
                                                <th>Type</th>
                                                <th>Forecast </th>
                                                <th>PSFStatus</th>
                                                <th>DND</th>
                                                <th>Complaint</th>
                                                <th>CRE Name</th>
                                            </tr>
                                        </thead>
                                        <tbody>								
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default tab-pane fade" id="profile">
                        <div class="panel-body inf-content">
                            <div class="dataTable_wrapper">                            
                                <div >
                                    <table class="table table-striped table-bordered table-hover" id="followUpRequiredServerDataMR" width="100%">
                                        <thead>
                                         <tr> 
                                          		<th>Campaign Name</th>
                                          		<th>Call Date</th>
                                                <th>Customer Name</th>
                                                <th>Mobile Number</th>                                                                   
                                                <th>Vehicle RegNo.</th>
                                                <th>Service Due</th>
                                                <th>CRE Name</th>
                                                <th>FollowUp Date</th>                                    
                                                <th>FollowUp Time</th>
                                                <th>Last Disposition</th>
												<th>Is Call Initiated</th>         
                                               <!--  <th>Download Media</th> -->
                                            </tr>   
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default tab-pane fade" id="messages">
                        <div class="panel-body inf-content">
                            <div class="dataTable_wrapper">                            
                                <div >
                                    <table class="table table-striped table-bordered table-hover" id="serviceBookedServerDataMR" width="100%">
                                        <thead>
                                            <tr> 
                                            	<th>Campaign Name</th>
                                            	<th>Call Date</th>
                                                <th>Customer Name</th>
                                                <th>Mobile Number</th>         
                                                <th>Vehicle RegNo.</th>
                                                <th>Service Due</th>
                                                <th>CRE Name</th>
                                                <th>Service ScheduledDate</th>  
                                                <th>Service ScheduledTime</th>  
                                                <th>Booking Status</th>
												<th>Is Call Initiated</th>
                                                <!--  <th>Download Media</th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default tab-pane fade" id="settings">
                         <div class="panel-body inf-content">
                        <div class="dataTable_wrapper">                            
                            <div >
                                <table class="table table-striped table-bordered table-hover" id="serviceNotRequiredServerDataMR" width="100%">
                                    <thead>
                                        <tr> 
                                        	<th>Campaign Name</th>
                                        	<th>Call Date</th>             	
                                            <th>Customer Name</th>
                                            <th>Mobile Number</th>         
                                            <th>Vehicle RegNo.</th>
                                            <th>Service Due</th>
                                            <th>CRE Name</th>
                                            <th>Last Disposition</th>
                                            <th>Reason</th>
                                           <!--  <th>Download Media</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    </div><!--End tab panel-->
                    <div class="panel panel-default tab-pane fade " id="nonContacts">
                        <div class="panel-body inf-content"> 
                            <div >
                                <table class="table table-striped table-bordered table-hover" id="nonContactsServerDataMR" width="100%">
                                    <thead>
                                        <tr> 
                                        <th>Campaign Name</th>
                                        <th>Call Date</th>                         
                                            <th>Customer Name</th>
                                            <th>Mobile Number</th>         
                                            <th>Vehicle RegNo.</th>
                                            <th>Service Due</th>
                                            <th>CRE Name</th>
                                            <th>Last Disposition</th>
                                            <th>Is Call Initiated</th>         											
                                         <!--  <th>Download Media</th> -->   
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default tab-pane fade" id="droppedBuc">
                        <div class="panel-body inf-content">
                            <div class="dataTable_wrapper">                            
                                <div style="overflow-x: auto">
                                    <table class="table table-striped table-bordered table-hover" id="droppedCallsServerDataMR">
                                        <thead>
                                            <tr>
                                            	<th>Campaign Name</th> 
                                            	<th>Call Date</th>
                                                <th>Customer Name</th>
                                                <th>Mobile Number</th>         
                                                <th>Vehicle RegNo.</th>
                                                <th>Service Due</th>
                                                <th>CRE Name</th>
												<th>Is Call Initiated</th>         
                                                <th>Last Disposition</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
					 <div class="panel panel-default tab-pane fade" id="missedCallBuc">
                        <div class="panel-body inf-content">
                            <div class="dataTable_wrapper">                            
                                <div style="overflow-x: auto">
                                    <table class="table table-striped table-bordered table-hover" id="missedCallsServerDataMR" width="100%">
                                        <thead>
                                            <tr>
       											<th>Call Date</th>
                                                <th>Customer Name</th>
                                                <th>Vehicle RegNo.</th>                                               
                                                <th>Mobile Number</th>
                                                <th>CRE Name</th>
                                                <!--  <th>Download Media</th> -->
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="panel panel-default tab-pane fade" id="incomingCallBuc">
                        <div class="panel-body inf-content">
                            <div class="dataTable_wrapper">                            
                                <div style="overflow-x: auto">
                                    <table class="table table-striped table-bordered table-hover" id="incomingCallsServerDataMR" width="100%">
                                        <thead>
                                            <tr>
                                            	<th>Call Date</th>
                                                <th>Customer Name</th>
                                                <th>Vehicle RegNo.</th>                                                
                                                <th>Mobile Number</th>
                                                <th>CRE Name</th>
                                               <!--  <th>Download Media</th> -->
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="panel panel-default tab-pane fade" id="outgoingCallBuc">
                        <div class="panel-body inf-content">
                            <div class="dataTable_wrapper">                            
                                <div style="overflow-x: auto">
                                    <table class="table table-striped table-bordered table-hover" id="outgoingCallsServerDataMR" width="100%">
                                        <thead>
                                            <tr>
                                            	<th>Call Date</th>
                                                <th>Customer Name</th>
                                                <th>Vehicle RegNo.</th>                                                
                                                <th>Mobile Number</th>
                                                <th>CRE Name</th>
                                               <!--  <th>Download Media</th> -->
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div><!--End tab content-->
            </div><!--End panel body-->
        </div>
   
</div>
""")))}),format.raw/*465.2*/(""" 
"""),format.raw/*466.1*/("""<script>
	 
	var urlDisposition = "/CREManager/dashboardCounts";

	
		
    $.ajax("""),format.raw/*472.12*/("""{"""),format.raw/*472.13*/("""
        """),format.raw/*473.9*/("""url: urlDisposition

    """),format.raw/*475.5*/("""}"""),format.raw/*475.6*/(""").done(function (insdashboardCount) """),format.raw/*475.42*/("""{"""),format.raw/*475.43*/("""
    	       
     """),format.raw/*477.6*/("""var dateis = GetFormattedDate();
	
	 console.log("dateis : "+dateis);
	 	
       $("#creMCurrentdate").html(dateis);  
    	
  	  var dashCount = insdashboardCount;
  		 $("#creMFreshCallsCountCRE").html(dashCount[0]);
  		 $("#creMfollowupcountCRE").html(dashCount[1]);
  	     $("#creMnonContactsCRE").html(dashCount[2]);
  	     $("#creMoverDueCountCRE").html(dashCount[3])
  	     $("#creMTotalBookingsperDay").html(dashCount[4]);
  	   	 $("#creMTotalCallsperDay").html(dashCount[5]);
    """),format.raw/*490.5*/("""}"""),format.raw/*490.6*/(""");



</script>
<script type="text/javascript"> 

function assignedInteractionDataMR()"""),format.raw/*497.37*/("""{"""),format.raw/*497.38*/("""
    """),format.raw/*498.5*/("""// alert("server side data load");
    document.getElementById("cresDiv").style.display = "block";
	document.getElementById("workshopDiv").style.display = "block";
	document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("campaignDiv").style.display = "block";
    document.getElementById("serviceBookTypeDiv").style.display = "none";
    document.getElementById("serviceTypeDiv").style.display = "block";
    document.getElementById("fromDateDiv").style.display = "block";
	document.getElementById("toDateDiv").style.display = "block";
	document.getElementById("lastDispoTypeDiv").style.display = "none";
	document.getElementById("droppedcountDiv").style.display = "none";

    var UserIds="",i;
    myOption = document.getElementById('ddlCreIds');
    
for (i=0;i<myOption.options.length;i++)"""),format.raw/*513.40*/("""{"""),format.raw/*513.41*/("""
    """),format.raw/*514.5*/("""if(myOption.options[i].selected)"""),format.raw/*514.37*/("""{"""),format.raw/*514.38*/("""
        """),format.raw/*515.9*/("""if(myOption.options[i].value != "--Select--")"""),format.raw/*515.54*/("""{"""),format.raw/*515.55*/("""
    	 """),format.raw/*516.7*/("""UserIds = UserIds + myOption.options[i].value + ",";
        """),format.raw/*517.9*/("""}"""),format.raw/*517.10*/("""
    """),format.raw/*518.5*/("""}"""),format.raw/*518.6*/("""
"""),format.raw/*519.1*/("""}"""),format.raw/*519.2*/("""
"""),format.raw/*520.1*/("""if(UserIds.length > 0)"""),format.raw/*520.23*/("""{"""),format.raw/*520.24*/("""
    	"""),format.raw/*521.6*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
"""),format.raw/*522.1*/("""}"""),format.raw/*522.2*/("""
"""),format.raw/*523.1*/("""else"""),format.raw/*523.5*/("""{"""),format.raw/*523.6*/("""
"""),format.raw/*524.1*/("""UserIds="Select";
"""),format.raw/*525.1*/("""}"""),format.raw/*525.2*/("""
    """),format.raw/*526.5*/("""var ajaxUrl = "/assignedInteractionTableDataMR/"+UserIds+"";
    
    
    var table= $('#assignedInteractionTableMR').dataTable( """),format.raw/*529.60*/("""{"""),format.raw/*529.61*/("""
        """),format.raw/*530.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
		 "scrollX": true,
        "paging": true,
        "searching":true,
        "ordering":false,	
        "ajax": ajaxUrl,
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*539.59*/("""{"""),format.raw/*539.60*/("""
              """),format.raw/*540.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*542.15*/("""}"""),format.raw/*542.16*/("""
    """),format.raw/*543.5*/("""}"""),format.raw/*543.6*/(""" """),format.raw/*543.7*/(""");

    FilterOptionDataMR(table);
    
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e)"""),format.raw/*547.61*/("""{"""),format.raw/*547.62*/("""
      """),format.raw/*548.7*/("""$($.fn.dataTable.tables(true)).DataTable()
         .columns.adjust();
   """),format.raw/*550.4*/("""}"""),format.raw/*550.5*/(""");
    
    """),format.raw/*552.5*/("""}"""),format.raw/*552.6*/("""




"""),format.raw/*557.1*/("""function ajaxCallForFollowUpRequiredServerMR()"""),format.raw/*557.47*/("""{"""),format.raw/*557.48*/("""
	"""),format.raw/*558.2*/("""document.getElementById("cresDiv").style.display = "block";
	document.getElementById("workshopDiv").style.display = "block";
	document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("campaignDiv").style.display = "block";
	document.getElementById("serviceBookTypeDiv").style.display = "none";
	document.getElementById("serviceTypeDiv").style.display = "block";
	document.getElementById("fromDateDiv").style.display = "block";
	document.getElementById("toDateDiv").style.display = "block";
	document.getElementById("lastDispoTypeDiv").style.display = "none";
	document.getElementById("droppedcountDiv").style.display = "none";
	          

	    var UserIds="",i;
	    myOption = document.getElementById('ddlCreIds');
	    
	for (i=0;i<myOption.options.length;i++)"""),format.raw/*573.41*/("""{"""),format.raw/*573.42*/("""
	    """),format.raw/*574.6*/("""if(myOption.options[i].selected)"""),format.raw/*574.38*/("""{"""),format.raw/*574.39*/("""
	        """),format.raw/*575.10*/("""if(myOption.options[i].value != "--Select--")"""),format.raw/*575.55*/("""{"""),format.raw/*575.56*/("""
	    	 """),format.raw/*576.8*/("""UserIds = UserIds + myOption.options[i].value + ",";
	        """),format.raw/*577.10*/("""}"""),format.raw/*577.11*/("""
	    """),format.raw/*578.6*/("""}"""),format.raw/*578.7*/("""
	"""),format.raw/*579.2*/("""}"""),format.raw/*579.3*/("""
	"""),format.raw/*580.2*/("""if(UserIds.length > 0)"""),format.raw/*580.24*/("""{"""),format.raw/*580.25*/("""
	    	"""),format.raw/*581.7*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*582.2*/("""}"""),format.raw/*582.3*/("""
	"""),format.raw/*583.2*/("""else"""),format.raw/*583.6*/("""{"""),format.raw/*583.7*/("""
	"""),format.raw/*584.2*/("""UserIds="Select";
	"""),format.raw/*585.2*/("""}"""),format.raw/*585.3*/("""
	    """),format.raw/*586.6*/("""var ajaxUrl = "/followUpCallLogTableDataMR/"+UserIds+"";
	    
	    
	    var table= $('#followUpRequiredServerDataMR').dataTable( """),format.raw/*589.63*/("""{"""),format.raw/*589.64*/("""
	        """),format.raw/*590.10*/(""""bDestroy": true,
	        "processing": true,
	        "serverSide": true,
	        "scrollY": 300,
			 "scrollX": true,
	        "paging": true,
	        "searching":true,
	        "ordering":false,	
	        "ajax": ajaxUrl,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*599.60*/("""{"""),format.raw/*599.61*/("""
              """),format.raw/*600.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*602.15*/("""}"""),format.raw/*602.16*/("""
	    """),format.raw/*603.6*/("""}"""),format.raw/*603.7*/(""" """),format.raw/*603.8*/(""");

	    FilterOptionDataMR(table);
	    
	"""),format.raw/*607.2*/("""}"""),format.raw/*607.3*/("""


"""),format.raw/*610.1*/("""function ajaxCallForserviceBookedServerDataMR()"""),format.raw/*610.48*/("""{"""),format.raw/*610.49*/("""
	"""),format.raw/*611.2*/("""document.getElementById("cresDiv").style.display = "block";
	document.getElementById("workshopDiv").style.display = "block";
	document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("campaignDiv").style.display = "block";
	document.getElementById("serviceBookTypeDiv").style.display = "block";
	document.getElementById("serviceTypeDiv").style.display = "none";
	document.getElementById("fromDateDiv").style.display = "block";
	document.getElementById("toDateDiv").style.display = "block";
	document.getElementById("lastDispoTypeDiv").style.display = "none";
	document.getElementById("droppedcountDiv").style.display = "none";

	          

	    var UserIds="",i;
	    myOption = document.getElementById('ddlCreIds');
	    
	for (i=0;i<myOption.options.length;i++)"""),format.raw/*627.41*/("""{"""),format.raw/*627.42*/("""
	    """),format.raw/*628.6*/("""if(myOption.options[i].selected)"""),format.raw/*628.38*/("""{"""),format.raw/*628.39*/("""
	        """),format.raw/*629.10*/("""if(myOption.options[i].value != "--Select--")"""),format.raw/*629.55*/("""{"""),format.raw/*629.56*/("""
	    	 """),format.raw/*630.8*/("""UserIds = UserIds + myOption.options[i].value + ",";
	        """),format.raw/*631.10*/("""}"""),format.raw/*631.11*/("""
	    """),format.raw/*632.6*/("""}"""),format.raw/*632.7*/("""
	"""),format.raw/*633.2*/("""}"""),format.raw/*633.3*/("""
	"""),format.raw/*634.2*/("""if(UserIds.length > 0)"""),format.raw/*634.24*/("""{"""),format.raw/*634.25*/("""
	    	"""),format.raw/*635.7*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*636.2*/("""}"""),format.raw/*636.3*/("""
	"""),format.raw/*637.2*/("""else"""),format.raw/*637.6*/("""{"""),format.raw/*637.7*/("""
	"""),format.raw/*638.2*/("""UserIds="Select";
	"""),format.raw/*639.2*/("""}"""),format.raw/*639.3*/("""
	    """),format.raw/*640.6*/("""var ajaxUrl = "/serviceBookedServerDataTableMR/"+UserIds+"";
	    
	    
	    var table= $('#serviceBookedServerDataMR').dataTable( """),format.raw/*643.60*/("""{"""),format.raw/*643.61*/("""
	        """),format.raw/*644.10*/(""""bDestroy": true,
	        "processing": true,
	        "serverSide": true,
	        "scrollY": 300,
			 "scrollX": true,
	        "paging": true,
	        "searching":true,
	        "ordering":false,	
	        "ajax": ajaxUrl,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*653.60*/("""{"""),format.raw/*653.61*/("""
              """),format.raw/*654.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*656.15*/("""}"""),format.raw/*656.16*/("""
	    """),format.raw/*657.6*/("""}"""),format.raw/*657.7*/(""" """),format.raw/*657.8*/(""");

	    FilterOptionDataMR(table);
	    
	   	    
	    """),format.raw/*662.6*/("""}"""),format.raw/*662.7*/("""

"""),format.raw/*664.1*/("""function ajaxCallForNonContactsServerMR()"""),format.raw/*664.42*/("""{"""),format.raw/*664.43*/("""
	"""),format.raw/*665.2*/("""//alert("noncontact");
	
		document.getElementById("cresDiv").style.display = "block";
		document.getElementById("workshopDiv").style.display = "block";
		document.getElementById("cityDiv").style.display = "block";	
		document.getElementById("campaignDiv").style.display = "block";
		document.getElementById("serviceBookTypeDiv").style.display = "none";
		document.getElementById("serviceTypeDiv").style.display = "none";
		document.getElementById("fromDateDiv").style.display = "block";
		document.getElementById("toDateDiv").style.display = "block";
		document.getElementById("lastDispoTypeDiv").style.display = "block";
		document.getElementById("droppedcountDiv").style.display = "block";

	var UserIds="",i;
    myOption = document.getElementById('ddlCreIds');
    
for (i=0;i<myOption.options.length;i++)"""),format.raw/*681.40*/("""{"""),format.raw/*681.41*/("""
    """),format.raw/*682.5*/("""if(myOption.options[i].selected)"""),format.raw/*682.37*/("""{"""),format.raw/*682.38*/("""
        """),format.raw/*683.9*/("""if(myOption.options[i].value != "Select")"""),format.raw/*683.50*/("""{"""),format.raw/*683.51*/("""
    		"""),format.raw/*684.7*/("""UserIds = UserIds + myOption.options[i].value + ",";
        """),format.raw/*685.9*/("""}"""),format.raw/*685.10*/("""
    """),format.raw/*686.5*/("""}"""),format.raw/*686.6*/("""
"""),format.raw/*687.1*/("""}"""),format.raw/*687.2*/("""
	"""),format.raw/*688.2*/("""if(UserIds.length > 0)"""),format.raw/*688.24*/("""{"""),format.raw/*688.25*/("""
    	"""),format.raw/*689.6*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*690.2*/("""}"""),format.raw/*690.3*/("""
	"""),format.raw/*691.2*/("""else"""),format.raw/*691.6*/("""{"""),format.raw/*691.7*/("""
		"""),format.raw/*692.3*/("""UserIds="Select";
	"""),format.raw/*693.2*/("""}"""),format.raw/*693.3*/("""
    """),format.raw/*694.5*/("""var ajaxUrl = "/nonContactsServerDataTableMR/"+UserIds
     
    var table = $('#nonContactsServerDataMR').dataTable( """),format.raw/*696.58*/("""{"""),format.raw/*696.59*/("""
        """),format.raw/*697.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
		 "scrollX": true,
        "paging": true,
        "searching":true,
        "ordering":false,			
        "ajax": ajaxUrl,
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*706.59*/("""{"""),format.raw/*706.60*/("""
              """),format.raw/*707.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*709.15*/("""}"""),format.raw/*709.16*/("""
    """),format.raw/*710.5*/("""}"""),format.raw/*710.6*/(""" """),format.raw/*710.7*/(""");

    FilterOptionDataMR(table);
    
    """),format.raw/*714.5*/("""}"""),format.raw/*714.6*/("""





"""),format.raw/*720.1*/("""function ajaxCallForServiceNotRequiredServerMR()"""),format.raw/*720.49*/("""{"""),format.raw/*720.50*/("""
	"""),format.raw/*721.2*/("""//alert("servicenot required");
	
	document.getElementById("cresDiv").style.display = "block";
	document.getElementById("workshopDiv").style.display = "block";
	document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("campaignDiv").style.display = "block";
	document.getElementById("serviceBookTypeDiv").style.display = "none";
	document.getElementById("serviceTypeDiv").style.display = "none";
	document.getElementById("fromDateDiv").style.display = "none";
	document.getElementById("toDateDiv").style.display = "none";
	document.getElementById("lastDispoTypeDiv").style.display = "none";
	document.getElementById("droppedcountDiv").style.display = "none";

	var UserIds="",i;
    myOption = document.getElementById('ddlCreIds');
    
for (i=0;i<myOption.options.length;i++)"""),format.raw/*737.40*/("""{"""),format.raw/*737.41*/("""
    """),format.raw/*738.5*/("""if(myOption.options[i].selected)"""),format.raw/*738.37*/("""{"""),format.raw/*738.38*/("""
        """),format.raw/*739.9*/("""if(myOption.options[i].value != "Select")"""),format.raw/*739.50*/("""{"""),format.raw/*739.51*/("""
    		"""),format.raw/*740.7*/("""UserIds = UserIds + myOption.options[i].value + ",";
        """),format.raw/*741.9*/("""}"""),format.raw/*741.10*/("""
    """),format.raw/*742.5*/("""}"""),format.raw/*742.6*/("""
"""),format.raw/*743.1*/("""}"""),format.raw/*743.2*/("""
	"""),format.raw/*744.2*/("""if(UserIds.length > 0)"""),format.raw/*744.24*/("""{"""),format.raw/*744.25*/("""
    	"""),format.raw/*745.6*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*746.2*/("""}"""),format.raw/*746.3*/("""
	"""),format.raw/*747.2*/("""else"""),format.raw/*747.6*/("""{"""),format.raw/*747.7*/("""
		"""),format.raw/*748.3*/("""UserIds="Select";
	"""),format.raw/*749.2*/("""}"""),format.raw/*749.3*/("""
    """),format.raw/*750.5*/("""var ajaxUrl = "/serviceNotRequiredServerDataTableMR/"+UserIds
     
    var table= $('#serviceNotRequiredServerDataMR').dataTable( """),format.raw/*752.64*/("""{"""),format.raw/*752.65*/("""
        """),format.raw/*753.9*/(""""bDestroy": true,
        "processing": true,
        "serverSide": true,
        "scrollY": 300,
        "paging": true,
        "searching":true,
        "ordering":false,			
        "ajax": ajaxUrl,
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*761.59*/("""{"""),format.raw/*761.60*/("""
              """),format.raw/*762.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*764.15*/("""}"""),format.raw/*764.16*/("""
    """),format.raw/*765.5*/("""}"""),format.raw/*765.6*/(""" """),format.raw/*765.7*/(""");

    FilterOptionDataMR(table);
    
    """),format.raw/*769.5*/("""}"""),format.raw/*769.6*/("""

"""),format.raw/*771.1*/("""function ajaxCallFordroppedCallsServerDataMR()"""),format.raw/*771.47*/("""{"""),format.raw/*771.48*/("""
	"""),format.raw/*772.2*/("""document.getElementById("cresDiv").style.display = "block";
	document.getElementById("workshopDiv").style.display = "block";
	document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("campaignDiv").style.display = "none";
	document.getElementById("serviceBookTypeDiv").style.display = "none";
	document.getElementById("serviceTypeDiv").style.display = "none";
	document.getElementById("fromDateDiv").style.display = "none";
	document.getElementById("toDateDiv").style.display = "none";
	document.getElementById("lastDispoTypeDiv").style.display = "none";
	document.getElementById("droppedcountDiv").style.display = "none";


	    var UserIds="",i;
	    myOption = document.getElementById('ddlCreIds');
	    
	for (i=0;i<myOption.options.length;i++)"""),format.raw/*787.41*/("""{"""),format.raw/*787.42*/("""
	    """),format.raw/*788.6*/("""if(myOption.options[i].selected)"""),format.raw/*788.38*/("""{"""),format.raw/*788.39*/("""
	        """),format.raw/*789.10*/("""if(myOption.options[i].value != "--Select--")"""),format.raw/*789.55*/("""{"""),format.raw/*789.56*/("""
	    	 """),format.raw/*790.8*/("""UserIds = UserIds + myOption.options[i].value + ",";
	        """),format.raw/*791.10*/("""}"""),format.raw/*791.11*/("""
	    """),format.raw/*792.6*/("""}"""),format.raw/*792.7*/("""
	"""),format.raw/*793.2*/("""}"""),format.raw/*793.3*/("""
	"""),format.raw/*794.2*/("""if(UserIds.length > 0)"""),format.raw/*794.24*/("""{"""),format.raw/*794.25*/("""
	    	"""),format.raw/*795.7*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*796.2*/("""}"""),format.raw/*796.3*/("""
	"""),format.raw/*797.2*/("""else"""),format.raw/*797.6*/("""{"""),format.raw/*797.7*/("""
	"""),format.raw/*798.2*/("""UserIds="Select";
	"""),format.raw/*799.2*/("""}"""),format.raw/*799.3*/("""
	    """),format.raw/*800.6*/("""var ajaxUrl = "/droppedCallsServerDataTableMR/"+UserIds+"";
	    
	    
	    var table= $('#droppedCallsServerDataMR').dataTable( """),format.raw/*803.59*/("""{"""),format.raw/*803.60*/("""
	        """),format.raw/*804.10*/(""""bDestroy": true,
	        "processing": true,
	        "serverSide": true,
	        "scrollY": 300,
			 "scrollX": true,
	        "paging": true,
	        "searching":true,
	        "ordering":false,	
	        "ajax": ajaxUrl,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*813.60*/("""{"""),format.raw/*813.61*/("""
              """),format.raw/*814.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*816.15*/("""}"""),format.raw/*816.16*/("""
	    """),format.raw/*817.6*/("""}"""),format.raw/*817.7*/(""" """),format.raw/*817.8*/(""");

	    FilterOptionDataMR(table);
	   
	    
	"""),format.raw/*822.2*/("""}"""),format.raw/*822.3*/("""
"""),format.raw/*823.1*/("""function ajaxCallMissedCallsServerDataMR()"""),format.raw/*823.43*/("""{"""),format.raw/*823.44*/("""
	"""),format.raw/*824.2*/("""document.getElementById("cresDiv").style.display = "block";
	document.getElementById("workshopDiv").style.display = "block";
	document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("campaignDiv").style.display = "none";
	document.getElementById("serviceBookTypeDiv").style.display = "none";
	document.getElementById("serviceTypeDiv").style.display = "none";
	document.getElementById("fromDateDiv").style.display = "block";
	document.getElementById("toDateDiv").style.display = "block";
	document.getElementById("lastDispoTypeDiv").style.display = "none";
	document.getElementById("droppedcountDiv").style.display = "none";


	    var UserIds="",i;
	    myOption = document.getElementById('ddlCreIds');
	    
	for (i=0;i<myOption.options.length;i++)"""),format.raw/*839.41*/("""{"""),format.raw/*839.42*/("""
	    """),format.raw/*840.6*/("""if(myOption.options[i].selected)"""),format.raw/*840.38*/("""{"""),format.raw/*840.39*/("""
	        """),format.raw/*841.10*/("""if(myOption.options[i].value != "--Select--")"""),format.raw/*841.55*/("""{"""),format.raw/*841.56*/("""
	    	 """),format.raw/*842.8*/("""UserIds = UserIds + myOption.options[i].value + ",";
	        """),format.raw/*843.10*/("""}"""),format.raw/*843.11*/("""
	    """),format.raw/*844.6*/("""}"""),format.raw/*844.7*/("""
	"""),format.raw/*845.2*/("""}"""),format.raw/*845.3*/("""
	"""),format.raw/*846.2*/("""if(UserIds.length > 0)"""),format.raw/*846.24*/("""{"""),format.raw/*846.25*/("""
	    	"""),format.raw/*847.7*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*848.2*/("""}"""),format.raw/*848.3*/("""
	"""),format.raw/*849.2*/("""else"""),format.raw/*849.6*/("""{"""),format.raw/*849.7*/("""
	"""),format.raw/*850.2*/("""UserIds="Select";
	"""),format.raw/*851.2*/("""}"""),format.raw/*851.3*/("""
	    """),format.raw/*852.6*/("""var ajaxUrl = "/missedCallsServerDataTableMR/"+UserIds+"";
	    
	    
	    var table= $('#missedCallsServerDataMR').dataTable( """),format.raw/*855.58*/("""{"""),format.raw/*855.59*/("""
	        """),format.raw/*856.10*/(""""bDestroy": true,
	        "processing": true,
	        "serverSide": true,
	        "scrollY": 300,
			 "scrollX": true,
	        "paging": true,
	        "searching":true,
	        "ordering":false,	
	        "ajax": ajaxUrl,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*865.60*/("""{"""),format.raw/*865.61*/("""
              """),format.raw/*866.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*868.15*/("""}"""),format.raw/*868.16*/("""
	    """),format.raw/*869.6*/("""}"""),format.raw/*869.7*/(""" """),format.raw/*869.8*/(""");

	    FilterOptionDataMR(table);
	   
	    
	"""),format.raw/*874.2*/("""}"""),format.raw/*874.3*/("""

"""),format.raw/*876.1*/("""function ajaxCallForincomingCallsServerDataMR()"""),format.raw/*876.48*/("""{"""),format.raw/*876.49*/("""
	"""),format.raw/*877.2*/("""document.getElementById("cresDiv").style.display = "block";
	document.getElementById("workshopDiv").style.display = "block";
	document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("campaignDiv").style.display = "none";
	document.getElementById("serviceBookTypeDiv").style.display = "none";
	document.getElementById("serviceTypeDiv").style.display = "none";
	document.getElementById("fromDateDiv").style.display = "block";
	document.getElementById("toDateDiv").style.display = "block";
	document.getElementById("lastDispoTypeDiv").style.display = "none";
	document.getElementById("droppedcountDiv").style.display = "none";


	    var UserIds="",i;
	    myOption = document.getElementById('ddlCreIds');
	    
	for (i=0;i<myOption.options.length;i++)"""),format.raw/*892.41*/("""{"""),format.raw/*892.42*/("""
	    """),format.raw/*893.6*/("""if(myOption.options[i].selected)"""),format.raw/*893.38*/("""{"""),format.raw/*893.39*/("""
	        """),format.raw/*894.10*/("""if(myOption.options[i].value != "--Select--")"""),format.raw/*894.55*/("""{"""),format.raw/*894.56*/("""
	    	 """),format.raw/*895.8*/("""UserIds = UserIds + myOption.options[i].value + ",";
	        """),format.raw/*896.10*/("""}"""),format.raw/*896.11*/("""
	    """),format.raw/*897.6*/("""}"""),format.raw/*897.7*/("""
	"""),format.raw/*898.2*/("""}"""),format.raw/*898.3*/("""
	"""),format.raw/*899.2*/("""if(UserIds.length > 0)"""),format.raw/*899.24*/("""{"""),format.raw/*899.25*/("""
	    	"""),format.raw/*900.7*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*901.2*/("""}"""),format.raw/*901.3*/("""
	"""),format.raw/*902.2*/("""else"""),format.raw/*902.6*/("""{"""),format.raw/*902.7*/("""
	"""),format.raw/*903.2*/("""UserIds="Select";
	"""),format.raw/*904.2*/("""}"""),format.raw/*904.3*/("""
	    """),format.raw/*905.6*/("""var ajaxUrl = "/incomingCallsServerDataTableMR/"+UserIds+"";
	    
	    
	    var table= $('#incomingCallsServerDataMR').dataTable( """),format.raw/*908.60*/("""{"""),format.raw/*908.61*/("""
	        """),format.raw/*909.10*/(""""bDestroy": true,
	        "processing": true,
	        "serverSide": true,
	        "scrollY": 300,
			 "scrollX": true,
	        "paging": true,
	        "searching":true,
	        "ordering":false,	
	        "ajax": ajaxUrl,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*918.60*/("""{"""),format.raw/*918.61*/("""
              """),format.raw/*919.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*921.15*/("""}"""),format.raw/*921.16*/("""
	    """),format.raw/*922.6*/("""}"""),format.raw/*922.7*/(""" """),format.raw/*922.8*/(""");

	    FilterOptionDataMR(table);
	   
	    
	"""),format.raw/*927.2*/("""}"""),format.raw/*927.3*/("""

"""),format.raw/*929.1*/("""function ajaxCallForOutgoingCallBucCallsServerDataMR()"""),format.raw/*929.55*/("""{"""),format.raw/*929.56*/("""
	"""),format.raw/*930.2*/("""document.getElementById("cresDiv").style.display = "block";
	document.getElementById("workshopDiv").style.display = "block";
	document.getElementById("cityDiv").style.display = "block";	
	document.getElementById("campaignDiv").style.display = "none";
	document.getElementById("serviceBookTypeDiv").style.display = "none";
	document.getElementById("serviceTypeDiv").style.display = "none";
	document.getElementById("fromDateDiv").style.display = "block";
	document.getElementById("toDateDiv").style.display = "block";
	document.getElementById("lastDispoTypeDiv").style.display = "none";
	document.getElementById("droppedcountDiv").style.display = "none";


	    var UserIds="",i;
	    myOption = document.getElementById('ddlCreIds');
	    
	for (i=0;i<myOption.options.length;i++)"""),format.raw/*945.41*/("""{"""),format.raw/*945.42*/("""
	    """),format.raw/*946.6*/("""if(myOption.options[i].selected)"""),format.raw/*946.38*/("""{"""),format.raw/*946.39*/("""
	        """),format.raw/*947.10*/("""if(myOption.options[i].value != "--Select--")"""),format.raw/*947.55*/("""{"""),format.raw/*947.56*/("""
	    	 """),format.raw/*948.8*/("""UserIds = UserIds + myOption.options[i].value + ",";
	        """),format.raw/*949.10*/("""}"""),format.raw/*949.11*/("""
	    """),format.raw/*950.6*/("""}"""),format.raw/*950.7*/("""
	"""),format.raw/*951.2*/("""}"""),format.raw/*951.3*/("""
	"""),format.raw/*952.2*/("""if(UserIds.length > 0)"""),format.raw/*952.24*/("""{"""),format.raw/*952.25*/("""
	    	"""),format.raw/*953.7*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
	"""),format.raw/*954.2*/("""}"""),format.raw/*954.3*/("""
	"""),format.raw/*955.2*/("""else"""),format.raw/*955.6*/("""{"""),format.raw/*955.7*/("""
	"""),format.raw/*956.2*/("""UserIds="Select";
	"""),format.raw/*957.2*/("""}"""),format.raw/*957.3*/("""
	    """),format.raw/*958.6*/("""var ajaxUrl = "/outgoingCallsServerDataTableMR/"+UserIds+"";
	    
	    
	    var table= $('#outgoingCallsServerDataMR').dataTable( """),format.raw/*961.60*/("""{"""),format.raw/*961.61*/("""
	        """),format.raw/*962.10*/(""""bDestroy": true,
	        "processing": true,
	        "serverSide": true,
	        "scrollY": 300,
			 "scrollX": true,
	        "paging": true,
	        "searching":true,
	        "ordering":false,	
	        "ajax": ajaxUrl,
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) """),format.raw/*971.60*/("""{"""),format.raw/*971.61*/("""
              """),format.raw/*972.15*/("""$('td', nRow).attr('wrap','wrap');
              return nRow;
              """),format.raw/*974.15*/("""}"""),format.raw/*974.16*/("""
	    """),format.raw/*975.6*/("""}"""),format.raw/*975.7*/(""" """),format.raw/*975.8*/(""");

	    FilterOptionDataMR(table);
	   
"""),format.raw/*979.1*/("""}"""),format.raw/*979.2*/("""
"""),format.raw/*980.1*/("""</script>
<script type="text/javascript">  
function FilterOptionDataMR(table)"""),format.raw/*982.35*/("""{"""),format.raw/*982.36*/("""
	"""),format.raw/*983.2*/("""var values = [];
	$('.filter').on('change', function() """),format.raw/*984.39*/("""{"""),format.raw/*984.40*/("""
    	"""),format.raw/*985.6*/("""console.log("inside filteroptionsdata data");
	    var i= $(this).data('columnIndex');
	    var v = $(this).val();
	    console.log("i : "+i);
		console.log("v length "+v.length);
	    
	    if(v.length>0)"""),format.raw/*991.20*/("""{"""),format.raw/*991.21*/("""
		    
		    """),format.raw/*993.7*/("""if(i == 7)"""),format.raw/*993.17*/("""{"""),format.raw/*993.18*/("""	

		    	"""),format.raw/*995.8*/("""var UserIds="",j;
		        myOption = document.getElementById('ddlCreIds');
		        console.log("here"+myOption);		        
		    for (j=0;j<myOption.options.length;j++)"""),format.raw/*998.46*/("""{"""),format.raw/*998.47*/("""
		        """),format.raw/*999.11*/("""if(myOption.options[j].selected)"""),format.raw/*999.43*/("""{"""),format.raw/*999.44*/("""
		            """),format.raw/*1000.15*/("""if(myOption.options[j].value != "Select")"""),format.raw/*1000.56*/("""{"""),format.raw/*1000.57*/("""
		        		"""),format.raw/*1001.13*/("""UserIds = UserIds + myOption.options[j].value + ",";
		            """),format.raw/*1002.15*/("""}"""),format.raw/*1002.16*/("""
		        """),format.raw/*1003.11*/("""}"""),format.raw/*1003.12*/("""
		    """),format.raw/*1004.7*/("""}"""),format.raw/*1004.8*/("""
		    	"""),format.raw/*1005.8*/("""if(UserIds.length > 0)"""),format.raw/*1005.30*/("""{"""),format.raw/*1005.31*/("""
		        	"""),format.raw/*1006.12*/("""UserIds = UserIds.substring(0, UserIds.length - 1);
		        	values[7]=UserIds;
		    	"""),format.raw/*1008.8*/("""}"""),format.raw/*1008.9*/("""else"""),format.raw/*1008.13*/("""{"""),format.raw/*1008.14*/("""
		    		"""),format.raw/*1009.9*/("""UserIds="Select";
		    		values[7]=UserIds;
		    	"""),format.raw/*1011.8*/("""}"""),format.raw/*1011.9*/("""
		    
		    """),format.raw/*1013.7*/("""}"""),format.raw/*1013.8*/("""else"""),format.raw/*1013.12*/("""{"""),format.raw/*1013.13*/("""
		    	"""),format.raw/*1014.8*/("""values[i] = v;
			    """),format.raw/*1015.8*/("""}"""),format.raw/*1015.9*/("""
	
			       
		  
		    """),format.raw/*1019.7*/("""}"""),format.raw/*1019.8*/("""		    
	    
	    	"""),format.raw/*1021.7*/("""console.log(JSON.stringify(values));	   	
	  	  //clear global search values
	 	  //table.api().search('');
	 	  $(this).data('columnIndex');
	 	 //var table = $('#nonContactsServerDataMR').dataTable();
	 	 if(values.length>0) """),format.raw/*1026.25*/("""{"""),format.raw/*1026.26*/("""
			 """),format.raw/*1027.5*/("""for(var i=0;i<values.length;i++)"""),format.raw/*1027.37*/("""{"""),format.raw/*1027.38*/("""
				 """),format.raw/*1028.6*/("""console.log("values[i] : "+values[i]);	
			    table.api().columns(i).search(values[i]);
			 """),format.raw/*1030.5*/("""}"""),format.raw/*1030.6*/("""
			 """),format.raw/*1031.5*/("""table.api().draw(); 
		"""),format.raw/*1032.3*/("""}"""),format.raw/*1032.4*/("""
	 	
		 	  
	"""),format.raw/*1035.2*/("""}"""),format.raw/*1035.3*/(""");
	
	
"""),format.raw/*1038.1*/("""}"""),format.raw/*1038.2*/("""
"""),format.raw/*1039.1*/("""</script>

 <script type="text/javascript">
	window.onload = assignedInteractionDataMR();
</script>
    """))
      }
    }
  }

  def render(listCity:List[Location],dealerName:String,user:String,allCREHash:Map[Long, String],campaignList:List[Campaign],serviceTypeList:List[ServiceTypes],bookedServiceTypeList:List[String],dispoList:List[CallDispositionData]): play.twirl.api.HtmlFormat.Appendable = apply(listCity,dealerName,user,allCREHash,campaignList,serviceTypeList,bookedServiceTypeList,dispoList)

  def f:((List[Location],String,String,Map[Long, String],List[Campaign],List[ServiceTypes],List[String],List[CallDispositionData]) => play.twirl.api.HtmlFormat.Appendable) = (listCity,dealerName,user,allCREHash,campaignList,serviceTypeList,bookedServiceTypeList,dispoList) => apply(listCity,dealerName,user,allCREHash,campaignList,serviceTypeList,bookedServiceTypeList,dispoList)

  def ref: this.type = this

}


}

/**/
object callDispositionPageCREManager extends callDispositionPageCREManager_Scope0.callDispositionPageCREManager
              /*
                  -- GENERATED --
                  DATE: Fri Mar 16 17:47:55 IST 2018
                  SOURCE: D:/CRMFORDAUTOSHERPA/crmford/app/views/callDispositionPageCREManager.scala.html
                  HASH: 935622c1abdb0292df2dc303300cf3c4581e2ca6
                  MATRIX: 906->1|1221->220|1249->223|1307->273|1346->275|1381->284|1428->304|1456->305|1510->333|1633->429|1661->430|1692->434|7060->5775|7098->5796|7138->5797|7233->5863|7277->5879|7291->5883|7323->5893|7354->5896|7368->5900|7400->5910|7464->5942|7511->5960|8623->7045|8672->7077|8712->7078|8777->7114|8821->7130|8844->7143|8884->7161|8915->7164|8938->7177|8979->7195|9054->7238|9126->7282|9978->8107|10033->8145|10073->8146|10139->8183|10183->8199|10209->8215|10252->8236|10283->8239|10310->8255|10354->8276|10430->8320|10503->8365|10859->8694|10926->8744|10966->8745|10997->8748|11041->8764|11085->8786|11116->8789|11160->8811|11239->8858|11314->8905|11620->9183|11660->9206|11700->9207|11733->9212|11777->9228|11792->9233|11831->9250|11862->9253|11877->9258|11916->9275|11966->9294|12050->9350|27093->24362|27124->24365|27241->24453|27271->24454|27309->24464|27364->24491|27393->24492|27458->24528|27488->24529|27537->24550|28072->25057|28101->25058|28223->25151|28253->25152|28287->25158|29155->25997|29185->25998|29219->26004|29280->26036|29310->26037|29348->26047|29422->26092|29452->26093|29488->26101|29578->26163|29608->26164|29642->26170|29671->26171|29701->26173|29730->26174|29760->26176|29811->26198|29841->26199|29876->26206|29957->26259|29986->26260|30016->26262|30048->26266|30077->26267|30107->26269|30154->26288|30183->26289|30217->26295|30379->26428|30409->26429|30447->26439|30763->26726|30793->26727|30838->26743|30945->26821|30975->26822|31009->26828|31038->26829|31067->26830|31200->26934|31230->26935|31266->26943|31370->27019|31399->27020|31441->27034|31470->27035|31508->27045|31583->27091|31613->27092|31644->27095|32480->27902|32510->27903|32545->27910|32606->27942|32636->27943|32676->27954|32750->27999|32780->28000|32817->28009|32909->28072|32939->28073|32974->28080|33003->28081|33034->28084|33063->28085|33094->28088|33145->28110|33175->28111|33211->28119|33293->28173|33322->28174|33353->28177|33385->28181|33414->28182|33445->28185|33493->28205|33522->28206|33557->28213|33720->28347|33750->28348|33790->28359|34115->28655|34145->28656|34190->28672|34297->28750|34327->28751|34362->28758|34391->28759|34420->28760|34495->28807|34524->28808|34558->28814|34634->28861|34664->28862|34695->28865|35533->29674|35563->29675|35598->29682|35659->29714|35689->29715|35729->29726|35803->29771|35833->29772|35870->29781|35962->29844|35992->29845|36027->29852|36056->29853|36087->29856|36116->29857|36147->29860|36198->29882|36228->29883|36264->29891|36346->29945|36375->29946|36406->29949|36438->29953|36467->29954|36498->29957|36546->29977|36575->29978|36610->29985|36774->30120|36804->30121|36844->30132|37169->30428|37199->30429|37244->30445|37351->30523|37381->30524|37416->30531|37445->30532|37474->30533|37564->30595|37593->30596|37625->30600|37695->30641|37725->30642|37756->30645|38611->31471|38641->31472|38675->31478|38736->31510|38766->31511|38804->31521|38874->31562|38904->31563|38940->31571|39030->31633|39060->31634|39094->31640|39123->31641|39153->31643|39182->31644|39213->31647|39264->31669|39294->31670|39329->31677|39411->31731|39440->31732|39471->31735|39503->31739|39532->31740|39564->31744|39612->31764|39641->31765|39675->31771|39824->31891|39854->31892|39892->31902|40210->32191|40240->32192|40285->32208|40392->32286|40422->32287|40456->32293|40485->32294|40514->32295|40590->32343|40619->32344|40659->32356|40736->32404|40766->32405|40797->32408|41647->33229|41677->33230|41711->33236|41772->33268|41802->33269|41840->33279|41910->33320|41940->33321|41976->33329|42066->33391|42096->33392|42130->33398|42159->33399|42189->33401|42218->33402|42249->33405|42300->33427|42330->33428|42365->33435|42447->33489|42476->33490|42507->33493|42539->33497|42568->33498|42600->33502|42648->33522|42677->33523|42711->33529|42873->33662|42903->33663|42941->33673|43238->33941|43268->33942|43313->33958|43420->34036|43450->34037|43484->34043|43513->34044|43542->34045|43618->34093|43647->34094|43679->34098|43754->34144|43784->34145|43815->34148|44636->34940|44666->34941|44701->34948|44762->34980|44792->34981|44832->34992|44906->35037|44936->35038|44973->35047|45065->35110|45095->35111|45130->35118|45159->35119|45190->35122|45219->35123|45250->35126|45301->35148|45331->35149|45367->35157|45449->35211|45478->35212|45509->35215|45541->35219|45570->35220|45601->35223|45649->35243|45678->35244|45713->35251|45875->35384|45905->35385|45945->35396|46270->35692|46300->35693|46345->35709|46452->35787|46482->35788|46517->35795|46546->35796|46575->35797|46656->35850|46685->35851|46715->35853|46786->35895|46816->35896|46847->35899|47670->36693|47700->36694|47735->36701|47796->36733|47826->36734|47866->36745|47940->36790|47970->36791|48007->36800|48099->36863|48129->36864|48164->36871|48193->36872|48224->36875|48253->36876|48284->36879|48335->36901|48365->36902|48401->36910|48483->36964|48512->36965|48543->36968|48575->36972|48604->36973|48635->36976|48683->36996|48712->36997|48747->37004|48907->37135|48937->37136|48977->37147|49302->37443|49332->37444|49377->37460|49484->37538|49514->37539|49549->37546|49578->37547|49607->37548|49688->37601|49717->37602|49749->37606|49825->37653|49855->37654|49886->37657|50709->38451|50739->38452|50774->38459|50835->38491|50865->38492|50905->38503|50979->38548|51009->38549|51046->38558|51138->38621|51168->38622|51203->38629|51232->38630|51263->38633|51292->38634|51323->38637|51374->38659|51404->38660|51440->38668|51522->38722|51551->38723|51582->38726|51614->38730|51643->38731|51674->38734|51722->38754|51751->38755|51786->38762|51950->38897|51980->38898|52020->38909|52345->39205|52375->39206|52420->39222|52527->39300|52557->39301|52592->39308|52621->39309|52650->39310|52731->39363|52760->39364|52792->39368|52875->39422|52905->39423|52936->39426|53759->40220|53789->40221|53824->40228|53885->40260|53915->40261|53955->40272|54029->40317|54059->40318|54096->40327|54188->40390|54218->40391|54253->40398|54282->40399|54313->40402|54342->40403|54373->40406|54424->40428|54454->40429|54490->40437|54572->40491|54601->40492|54632->40495|54664->40499|54693->40500|54724->40503|54772->40523|54801->40524|54836->40531|55000->40666|55030->40667|55070->40678|55395->40974|55425->40975|55470->40991|55577->41069|55607->41070|55642->41077|55671->41078|55700->41079|55773->41124|55802->41125|55832->41127|55941->41207|55971->41208|56002->41211|56087->41267|56117->41268|56152->41275|56392->41486|56422->41487|56466->41503|56505->41513|56535->41514|56575->41526|56779->41701|56809->41702|56850->41714|56911->41746|56941->41747|56987->41763|57058->41804|57089->41805|57133->41819|57231->41887|57262->41888|57304->41900|57335->41901|57372->41909|57402->41910|57440->41919|57492->41941|57523->41942|57566->41955|57686->42046|57716->42047|57750->42051|57781->42052|57820->42062|57903->42116|57933->42117|57978->42133|58008->42134|58042->42138|58073->42139|58111->42148|58163->42171|58193->42172|58251->42201|58281->42202|58331->42223|58593->42455|58624->42456|58659->42462|58721->42494|58752->42495|58788->42502|58912->42597|58942->42598|58977->42604|59030->42628|59060->42629|59105->42645|59135->42646|59174->42656|59204->42657|59235->42659
                  LINES: 27->1|32->1|33->2|33->2|33->2|37->6|38->7|38->7|40->9|43->12|43->12|45->14|155->124|155->124|155->124|156->125|156->125|156->125|156->125|156->125|156->125|156->125|157->126|158->127|188->157|188->157|188->157|189->158|189->158|189->158|189->158|189->158|189->158|189->158|190->159|192->161|212->181|212->181|212->181|213->182|213->182|213->182|213->182|213->182|213->182|213->182|214->183|216->185|223->192|223->192|223->192|224->193|224->193|224->193|224->193|224->193|225->194|227->196|233->202|233->202|233->202|234->203|234->203|234->203|234->203|234->203|234->203|234->203|236->205|239->208|496->465|497->466|503->472|503->472|504->473|506->475|506->475|506->475|506->475|508->477|521->490|521->490|528->497|528->497|529->498|544->513|544->513|545->514|545->514|545->514|546->515|546->515|546->515|547->516|548->517|548->517|549->518|549->518|550->519|550->519|551->520|551->520|551->520|552->521|553->522|553->522|554->523|554->523|554->523|555->524|556->525|556->525|557->526|560->529|560->529|561->530|570->539|570->539|571->540|573->542|573->542|574->543|574->543|574->543|578->547|578->547|579->548|581->550|581->550|583->552|583->552|588->557|588->557|588->557|589->558|604->573|604->573|605->574|605->574|605->574|606->575|606->575|606->575|607->576|608->577|608->577|609->578|609->578|610->579|610->579|611->580|611->580|611->580|612->581|613->582|613->582|614->583|614->583|614->583|615->584|616->585|616->585|617->586|620->589|620->589|621->590|630->599|630->599|631->600|633->602|633->602|634->603|634->603|634->603|638->607|638->607|641->610|641->610|641->610|642->611|658->627|658->627|659->628|659->628|659->628|660->629|660->629|660->629|661->630|662->631|662->631|663->632|663->632|664->633|664->633|665->634|665->634|665->634|666->635|667->636|667->636|668->637|668->637|668->637|669->638|670->639|670->639|671->640|674->643|674->643|675->644|684->653|684->653|685->654|687->656|687->656|688->657|688->657|688->657|693->662|693->662|695->664|695->664|695->664|696->665|712->681|712->681|713->682|713->682|713->682|714->683|714->683|714->683|715->684|716->685|716->685|717->686|717->686|718->687|718->687|719->688|719->688|719->688|720->689|721->690|721->690|722->691|722->691|722->691|723->692|724->693|724->693|725->694|727->696|727->696|728->697|737->706|737->706|738->707|740->709|740->709|741->710|741->710|741->710|745->714|745->714|751->720|751->720|751->720|752->721|768->737|768->737|769->738|769->738|769->738|770->739|770->739|770->739|771->740|772->741|772->741|773->742|773->742|774->743|774->743|775->744|775->744|775->744|776->745|777->746|777->746|778->747|778->747|778->747|779->748|780->749|780->749|781->750|783->752|783->752|784->753|792->761|792->761|793->762|795->764|795->764|796->765|796->765|796->765|800->769|800->769|802->771|802->771|802->771|803->772|818->787|818->787|819->788|819->788|819->788|820->789|820->789|820->789|821->790|822->791|822->791|823->792|823->792|824->793|824->793|825->794|825->794|825->794|826->795|827->796|827->796|828->797|828->797|828->797|829->798|830->799|830->799|831->800|834->803|834->803|835->804|844->813|844->813|845->814|847->816|847->816|848->817|848->817|848->817|853->822|853->822|854->823|854->823|854->823|855->824|870->839|870->839|871->840|871->840|871->840|872->841|872->841|872->841|873->842|874->843|874->843|875->844|875->844|876->845|876->845|877->846|877->846|877->846|878->847|879->848|879->848|880->849|880->849|880->849|881->850|882->851|882->851|883->852|886->855|886->855|887->856|896->865|896->865|897->866|899->868|899->868|900->869|900->869|900->869|905->874|905->874|907->876|907->876|907->876|908->877|923->892|923->892|924->893|924->893|924->893|925->894|925->894|925->894|926->895|927->896|927->896|928->897|928->897|929->898|929->898|930->899|930->899|930->899|931->900|932->901|932->901|933->902|933->902|933->902|934->903|935->904|935->904|936->905|939->908|939->908|940->909|949->918|949->918|950->919|952->921|952->921|953->922|953->922|953->922|958->927|958->927|960->929|960->929|960->929|961->930|976->945|976->945|977->946|977->946|977->946|978->947|978->947|978->947|979->948|980->949|980->949|981->950|981->950|982->951|982->951|983->952|983->952|983->952|984->953|985->954|985->954|986->955|986->955|986->955|987->956|988->957|988->957|989->958|992->961|992->961|993->962|1002->971|1002->971|1003->972|1005->974|1005->974|1006->975|1006->975|1006->975|1010->979|1010->979|1011->980|1013->982|1013->982|1014->983|1015->984|1015->984|1016->985|1022->991|1022->991|1024->993|1024->993|1024->993|1026->995|1029->998|1029->998|1030->999|1030->999|1030->999|1031->1000|1031->1000|1031->1000|1032->1001|1033->1002|1033->1002|1034->1003|1034->1003|1035->1004|1035->1004|1036->1005|1036->1005|1036->1005|1037->1006|1039->1008|1039->1008|1039->1008|1039->1008|1040->1009|1042->1011|1042->1011|1044->1013|1044->1013|1044->1013|1044->1013|1045->1014|1046->1015|1046->1015|1050->1019|1050->1019|1052->1021|1057->1026|1057->1026|1058->1027|1058->1027|1058->1027|1059->1028|1061->1030|1061->1030|1062->1031|1063->1032|1063->1032|1066->1035|1066->1035|1069->1038|1069->1038|1070->1039
                  -- GENERATED --
              */
          