
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object addCustomer_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class addCustomer extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[String,String,String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(dealercode:String,dealerName:String,userName:String,oem:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.66*/("""

"""),_display_(/*4.2*/mainPageCRE("AutoSherpaCRM",userName,dealerName,dealercode,oem)/*4.65*/ {_display_(Seq[Any](format.raw/*4.67*/("""
"""),_display_(/*5.2*/helper/*5.8*/.form(routes.CallInteractionController.postAddCustomer() ,'id -> "form1")/*5.81*/  {_display_(Seq[Any](format.raw/*5.84*/("""

"""),format.raw/*7.1*/("""<div class="row" >
  <div class="col-lg-12">
    <div class="panel panel-primary">
      <div class="panel-heading">   
        Add Customer </div>
		<div class="panel-body">
            <div class="col-lg-3">
              <div class="form-group">
                <label for="customerName">Customer Name<i style="color:red">*</i>:</label>
                <input type="text" class="form-control textOnlyAccepted" name="customerName"  id="customerName" autocomplete="off"  required/>
              </div>
            </div>
             <div class="col-lg-3">             
              <div class="form-group">
                <label for="customerPhone">Customer Phone:<i style="color:red">*</i></label>
                <input type="text" class="form-control numberOnly" maxlength="10" name="phoneNumber" id="customerPhone" autocomplete="off" required/>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label for="customerEmail">Customer Email:</label>
                <input type="text" class="form-control" name="emailAddress" id="customerEmail" autocomplete="off"  required/>
              </div>
            </div>
           <div class="col-lg-3">
              <div class="form-group">
                <label for="customerAddress">Customer Address :</label>
                <input type="text" class="form-control" name="addressLine1" id="customerAddress" autocomplete="off"  required/>
              </div>
            </div>
           <!--   <div class="col-lg-3">
              <div class="form-group">
                <label for="vehicleNumber">Vehicle Number :</label>
                <input type="text" class="form-control" name="vehicleNumber" id="vehicleNumber" autocomplete="off"  required/>
              </div>
            </div> -->
          <div class="col-lg-3">
              <div class="form-group">
                <label for="vehicleRegNo">Vehicle Reg No :<i style="color:red">*</i></label>
                <input type="text" class="form-control" name="vehicleRegNo" id="vehicleRegNo" autocomplete="off"  required/>
              </div>
            </div>
            
             <div class="col-lg-3">
              <div class="form-group">
                <label for="chassisNo">Chassis No :<i style="color:red">*</i></label>
                <input type="text" class="form-control" name="chassisNo"  id="chassisNo" autocomplete="off"  required/>
              </div>
            </div>
            
            <div class="col-lg-3">
              <div class="form-group">
                <label for="model">Model :<i style="color:red">*</i></label>
                <input type="text" class="form-control" name="model"  id="model" autocomplete="off"  required/>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label for="milege">Mileage :<i style="color:red">*</i></label>
                <input type="text" class="form-control numberOnly" name="milege" maxlength="6" id="milege" autocomplete="off"  required/>
              </div>
            </div> 
            <div class="col-lg-3">                  
             <div class="form-group">
                <label for="">&nbsp;</label>
             <input type="button" class="btn btn-primary btn-block" onclick="return validatingCustomerForm()" value="Submit" />
       
      </div>
      
        </div>
        
        </div>
     </div>
     </div>
     
     </div>
     
    
     """)))}),format.raw/*85.7*/("""
     """)))}),format.raw/*86.7*/("""
     
        """),format.raw/*88.9*/("""<script>
            
            function validatingCustomerForm()"""),format.raw/*90.46*/("""{"""),format.raw/*90.47*/("""
			
			"""),format.raw/*92.4*/("""var custName=$("#customerName").val();
			var custPhone=$("#customerPhone").val();
			var custRegn=$("#vehicleRegNo").val();
			var custModel=$("#model").val();
			var custMilege=$("#milege").val();
				if(custName=='' && custPhone=='' && custRegn=='' && custModel=='' && custMilege=='')"""),format.raw/*97.89*/("""{"""),format.raw/*97.90*/("""
			"""),format.raw/*98.4*/("""Lobibox.notify('warning', """),format.raw/*98.30*/("""{"""),format.raw/*98.31*/("""
							"""),format.raw/*99.8*/("""continueDelayOnInactiveTab: true,
							msg: 'Please Enter The Mandatory Fields .'
						"""),format.raw/*101.7*/("""}"""),format.raw/*101.8*/(""");
						return false;
			
		"""),format.raw/*104.3*/("""}"""),format.raw/*104.4*/("""
		
		"""),format.raw/*106.3*/("""else if(custName=='')"""),format.raw/*106.24*/("""{"""),format.raw/*106.25*/("""
			"""),format.raw/*107.4*/("""Lobibox.notify('warning', """),format.raw/*107.30*/("""{"""),format.raw/*107.31*/("""
							"""),format.raw/*108.8*/("""continueDelayOnInactiveTab: true,
							msg: 'Please Enter Name'
						"""),format.raw/*110.7*/("""}"""),format.raw/*110.8*/(""");
						return false;
			
		"""),format.raw/*113.3*/("""}"""),format.raw/*113.4*/("""
		"""),format.raw/*114.3*/("""else if(custPhone=='')"""),format.raw/*114.25*/("""{"""),format.raw/*114.26*/("""
			"""),format.raw/*115.4*/("""Lobibox.notify('warning', """),format.raw/*115.30*/("""{"""),format.raw/*115.31*/("""
							"""),format.raw/*116.8*/("""continueDelayOnInactiveTab: true,
							msg: 'Please Enter Phone Number'
						"""),format.raw/*118.7*/("""}"""),format.raw/*118.8*/(""");
						return false;
			
		"""),format.raw/*121.3*/("""}"""),format.raw/*121.4*/("""
		"""),format.raw/*122.3*/("""else if(custPhone.length < 10 )"""),format.raw/*122.34*/("""{"""),format.raw/*122.35*/("""
			
		
			"""),format.raw/*125.4*/("""Lobibox.notify('warning', """),format.raw/*125.30*/("""{"""),format.raw/*125.31*/("""
							"""),format.raw/*126.8*/("""continueDelayOnInactiveTab: true,
							msg: 'Invalid Phone No.'
						"""),format.raw/*128.7*/("""}"""),format.raw/*128.8*/(""");
						
						return false;
						
						
			
		"""),format.raw/*134.3*/("""}"""),format.raw/*134.4*/("""
		"""),format.raw/*135.3*/("""else if(custRegn=='')"""),format.raw/*135.24*/("""{"""),format.raw/*135.25*/("""
			"""),format.raw/*136.4*/("""Lobibox.notify('warning', """),format.raw/*136.30*/("""{"""),format.raw/*136.31*/("""
							"""),format.raw/*137.8*/("""continueDelayOnInactiveTab: true,
							msg: 'Please Enter Registration No.'
						"""),format.raw/*139.7*/("""}"""),format.raw/*139.8*/(""");
						return false;
			
		"""),format.raw/*142.3*/("""}"""),format.raw/*142.4*/("""
			"""),format.raw/*143.4*/("""else if(custModel=='')"""),format.raw/*143.26*/("""{"""),format.raw/*143.27*/("""
			"""),format.raw/*144.4*/("""Lobibox.notify('warning', """),format.raw/*144.30*/("""{"""),format.raw/*144.31*/("""
							"""),format.raw/*145.8*/("""continueDelayOnInactiveTab: true,
							msg: 'Please Enter Model No.'
						"""),format.raw/*147.7*/("""}"""),format.raw/*147.8*/(""");
						return false;
			
		"""),format.raw/*150.3*/("""}"""),format.raw/*150.4*/("""
			"""),format.raw/*151.4*/("""else if(custMilege=='')"""),format.raw/*151.27*/("""{"""),format.raw/*151.28*/("""
			"""),format.raw/*152.4*/("""Lobibox.notify('warning', """),format.raw/*152.30*/("""{"""),format.raw/*152.31*/("""
							"""),format.raw/*153.8*/("""continueDelayOnInactiveTab: true,
							msg: 'Please Enter Milege'
						"""),format.raw/*155.7*/("""}"""),format.raw/*155.8*/(""");
						return false;
			
		"""),format.raw/*158.3*/("""}"""),format.raw/*158.4*/("""
			
			
			"""),format.raw/*161.4*/("""else"""),format.raw/*161.8*/("""{"""),format.raw/*161.9*/("""
			
			"""),format.raw/*163.4*/("""}"""),format.raw/*163.5*/("""
			 
               """),format.raw/*165.16*/("""var vehicleReg= document.getElementById("vehicleRegNo").value;
               

              // alert(vehicleReg);
               var urlDisposition = "/CRE/vehicleRegNoExist/" + vehicleReg + "";
               $.ajax("""),format.raw/*170.23*/("""{"""),format.raw/*170.24*/("""
                   """),format.raw/*171.20*/("""url: urlDisposition

               """),format.raw/*173.16*/("""}"""),format.raw/*173.17*/(""").done(function (data) """),format.raw/*173.40*/("""{"""),format.raw/*173.41*/("""

                   """),format.raw/*175.20*/("""if (data > 0) """),format.raw/*175.34*/("""{"""),format.raw/*175.35*/("""

                	   """),format.raw/*177.21*/("""Lobibox.notify('error', """),format.raw/*177.45*/("""{"""),format.raw/*177.46*/("""
						"""),format.raw/*178.7*/("""msg: 'This Vehicle Reg no already exist cannot add to database'
						"""),format.raw/*179.7*/("""}"""),format.raw/*179.8*/(""");
						return false;
           		    		        

                   """),format.raw/*183.20*/("""}"""),format.raw/*183.21*/("""else"""),format.raw/*183.25*/("""{"""),format.raw/*183.26*/("""
                	   """),format.raw/*184.21*/("""document.getElementById('form1').submit();
                	   Lobibox.notify('info', """),format.raw/*185.44*/("""{"""),format.raw/*185.45*/("""
   						"""),format.raw/*186.10*/("""msg: 'Submitted successfully!'
   						"""),format.raw/*187.10*/("""}"""),format.raw/*187.11*/(""");

                	   
   						return true;

                       """),format.raw/*192.24*/("""}"""),format.raw/*192.25*/("""
               """),format.raw/*193.16*/("""}"""),format.raw/*193.17*/(""");
               
               """),format.raw/*195.16*/("""}"""),format.raw/*195.17*/("""
    
  
  """),format.raw/*198.3*/("""$('.numberOnly').keypress(function (e) """),format.raw/*198.42*/("""{"""),format.raw/*198.43*/("""
		
        """),format.raw/*200.9*/("""if (isNaN(this.value + "" + "." + String.fromCharCode(e.charCode))) """),format.raw/*200.77*/("""{"""),format.raw/*200.78*/("""
					"""),format.raw/*201.6*/("""return false;
        """),format.raw/*202.9*/("""}"""),format.raw/*202.10*/("""

    """),format.raw/*204.5*/("""}"""),format.raw/*204.6*/(""");
	
	$(".textOnlyAccepted").keypress(function (e) """),format.raw/*206.47*/("""{"""),format.raw/*206.48*/("""
            """),format.raw/*207.13*/("""var code = e.keyCode || e.which;
            if ((code < 65 || code > 90) && (code < 97 || code > 122) && code != 32 && code != 46)
            """),format.raw/*209.13*/("""{"""),format.raw/*209.14*/("""
                 """),format.raw/*210.18*/("""return false;
            """),format.raw/*211.13*/("""}"""),format.raw/*211.14*/("""
        """),format.raw/*212.9*/("""}"""),format.raw/*212.10*/(""");
    

   
    
</script>"""))
      }
    }
  }

  def render(dealercode:String,dealerName:String,userName:String,oem:String): play.twirl.api.HtmlFormat.Appendable = apply(dealercode,dealerName,userName,oem)

  def f:((String,String,String,String) => play.twirl.api.HtmlFormat.Appendable) = (dealercode,dealerName,userName,oem) => apply(dealercode,dealerName,userName,oem)

  def ref: this.type = this

}


}

/**/
object addCustomer extends addCustomer_Scope0.addCustomer
              /*
                  -- GENERATED --
                  DATE: Fri Mar 16 17:47:51 IST 2018
                  SOURCE: D:/CRMFORDAUTOSHERPA/crmford/app/views/addCustomer.scala.html
                  HASH: e7d4a345420be85390b8bb4f11b187ec05550444
                  MATRIX: 778->3|937->67|967->72|1038->135|1077->137|1105->140|1118->146|1199->219|1239->222|1269->226|4913->3840|4951->3848|4995->3865|5092->3934|5121->3935|5158->3945|5478->4237|5507->4238|5539->4243|5593->4269|5622->4270|5658->4279|5778->4371|5807->4372|5867->4404|5896->4405|5932->4413|5982->4434|6012->4435|6045->4440|6100->4466|6130->4467|6167->4476|6269->4550|6298->4551|6358->4583|6387->4584|6419->4588|6470->4610|6500->4611|6533->4616|6588->4642|6618->4643|6655->4652|6765->4734|6794->4735|6854->4767|6883->4768|6915->4772|6975->4803|7005->4804|7047->4818|7102->4844|7132->4845|7169->4854|7271->4928|7300->4929|7384->4985|7413->4986|7445->4990|7495->5011|7525->5012|7558->5017|7613->5043|7643->5044|7680->5053|7794->5139|7823->5140|7883->5172|7912->5173|7945->5178|7996->5200|8026->5201|8059->5206|8114->5232|8144->5233|8181->5242|8288->5321|8317->5322|8377->5354|8406->5355|8439->5360|8491->5383|8521->5384|8554->5389|8609->5415|8639->5416|8676->5425|8780->5501|8809->5502|8869->5534|8898->5535|8941->5550|8973->5554|9002->5555|9040->5565|9069->5566|9121->5589|9374->5813|9404->5814|9454->5835|9521->5873|9551->5874|9603->5897|9633->5898|9685->5921|9728->5935|9758->5936|9811->5960|9864->5984|9894->5985|9930->5993|10029->6064|10058->6065|10162->6140|10192->6141|10225->6145|10255->6146|10306->6168|10422->6255|10452->6256|10492->6267|10562->6308|10592->6309|10697->6385|10727->6386|10773->6403|10803->6404|10868->6440|10898->6441|10940->6455|11008->6494|11038->6495|11080->6509|11177->6577|11207->6578|11242->6585|11293->6608|11323->6609|11359->6617|11388->6618|11470->6671|11500->6672|11543->6686|11718->6832|11748->6833|11796->6852|11852->6879|11882->6880|11920->6890|11950->6891
                  LINES: 27->2|32->2|34->4|34->4|34->4|35->5|35->5|35->5|35->5|37->7|115->85|116->86|118->88|120->90|120->90|122->92|127->97|127->97|128->98|128->98|128->98|129->99|131->101|131->101|134->104|134->104|136->106|136->106|136->106|137->107|137->107|137->107|138->108|140->110|140->110|143->113|143->113|144->114|144->114|144->114|145->115|145->115|145->115|146->116|148->118|148->118|151->121|151->121|152->122|152->122|152->122|155->125|155->125|155->125|156->126|158->128|158->128|164->134|164->134|165->135|165->135|165->135|166->136|166->136|166->136|167->137|169->139|169->139|172->142|172->142|173->143|173->143|173->143|174->144|174->144|174->144|175->145|177->147|177->147|180->150|180->150|181->151|181->151|181->151|182->152|182->152|182->152|183->153|185->155|185->155|188->158|188->158|191->161|191->161|191->161|193->163|193->163|195->165|200->170|200->170|201->171|203->173|203->173|203->173|203->173|205->175|205->175|205->175|207->177|207->177|207->177|208->178|209->179|209->179|213->183|213->183|213->183|213->183|214->184|215->185|215->185|216->186|217->187|217->187|222->192|222->192|223->193|223->193|225->195|225->195|228->198|228->198|228->198|230->200|230->200|230->200|231->201|232->202|232->202|234->204|234->204|236->206|236->206|237->207|239->209|239->209|240->210|241->211|241->211|242->212|242->212
                  -- GENERATED --
              */
          